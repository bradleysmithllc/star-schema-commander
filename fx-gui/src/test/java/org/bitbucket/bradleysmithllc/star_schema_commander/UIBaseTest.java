package org.bitbucket.bradleysmithllc.star_schema_commander;

/*
 * #%L
 * fx-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.scene.Parent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.UIApplicationState;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.UIView;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.loadui.testfx.GuiTest;

import java.io.File;
import java.io.IOException;

/**
 * Created by bsmith on 8/4/15.
 */
public abstract class UIBaseTest<T extends UIView> extends GuiTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	protected T view;
	protected Profile profile;

	@Override
	protected Parent getRootNode()
	{
		try
		{
			File path = temporaryFolder.newFolder();
			path.delete();

			profile = Profile.create(path);

			prepareProfile();

			view = newInstance();

			new UIContainerImpl(new UIApplicationState(profile)).addView(view);

			return (Parent) view.view();
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	protected void prepareProfile()
	{
	}

	protected abstract T newInstance();
}
