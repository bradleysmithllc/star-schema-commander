package org.bitbucket.bradleysmithllc.star_schema_commander;

/*
 * #%L
 * fx-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.StarCommanderUIView;
import org.junit.Assert;
import org.junit.Test;

public class StarCommanderConnectionsTest extends UIBaseTest<StarCommanderUIView>
{
	@Test
	public void connectionsInOrder()
	{
		ListView lv = find("#connectionList");

		Assert.assertNotNull(lv);

		ObservableList oi = lv.getItems();

		Assert.assertEquals(6, oi.size());

		Assert.assertSame(profile.connections().get("0"), oi.get(0));
		Assert.assertSame(profile.connections().get("1"), oi.get(1));
		Assert.assertSame(profile.connections().get("A1"), oi.get(2));
		Assert.assertSame(profile.connections().get("H"), oi.get(3));
		Assert.assertSame(profile.connections().get("Z"), oi.get(4));
		Assert.assertSame(profile.connections().get("ZA1"), oi.get(5));
	}

	@Override
	protected void prepareProfile()
	{
		// create at least one reporting area
		profile.newH2Connection().name("A1").create();
		profile.newH2Connection().name("1").create();
		profile.newH2Connection().name("0").create();
		profile.newH2Connection().name("Z").create();
		profile.newH2Connection().name("ZA1").create();
		profile.newH2Connection().name("H").create();
	}

	@Override
	protected StarCommanderUIView newInstance()
	{
		return new StarCommanderUIView();
	}
}
