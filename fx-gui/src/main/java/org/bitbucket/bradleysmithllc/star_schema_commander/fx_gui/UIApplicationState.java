package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui;

/*
 * #%L
 * fx-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

public class UIApplicationState
{
	private final Profile profile;
	private ReportingArea currentReportingArea;
	private Fact currentFact;
	private Dimension currentDimension;

	public UIApplicationState(Profile profile)
	{
		this.profile = profile;

		currentReportingArea = profile.defaultReportingArea();
	}

	public Profile profile()
	{
		return profile;
	}

	public ReportingArea reportingArea()
	{
		return currentReportingArea;
	}

	public Fact fact()
	{
		return currentFact;
	}

	public Dimension dimension()
	{
		return currentDimension;
	}
}