package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.util.ViewUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.UIContainerImpl;

import java.io.File;

public class StarCommanderJFXUI extends Application
{
	private StarCommanderUIView starCommanderUIView;
	private UIApplicationState uiApplicationState = null;

	/**
	 * Create the application.
	 */
	public StarCommanderJFXUI() throws Exception
	{
	}

	@Override
	public void init() throws Exception
	{
		final Profile profile;

		File pd = new File(getParameters().getNamed().get("profile"));

		if (pd.exists())
		{
			profile = new Profile(pd);
		}
		else
		{
			profile = Profile.create(pd);
		}

		uiApplicationState = new UIApplicationState(profile);
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		try
		{
			starCommanderUIView = new StarCommanderUIView();

			// create the container and start the lifecycle
			UIContainerImpl container = new UIContainerImpl(uiApplicationState);

			container.addView(starCommanderUIView);

			starCommanderUIView.uiController().build();

			Scene sc = new Scene((Parent) starCommanderUIView.view());
			primaryStage.setScene(sc);

			// aggregate all stylesheets, from the bottom up, into this scene
			sc.getStylesheets().addAll(ViewUtils.aggregateStyleSheets(starCommanderUIView));

			primaryStage.setTitle("Star(schema) Commander");
			primaryStage.sizeToScene();
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
			{
				@Override
				public void handle(WindowEvent windowEvent)
				{
					Platform.exit();
				}
			});
			primaryStage.centerOnScreen();

			primaryStage.show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void launchExt(String absolutePath)
	{
		launch("--profile=" + absolutePath);
	}
}
