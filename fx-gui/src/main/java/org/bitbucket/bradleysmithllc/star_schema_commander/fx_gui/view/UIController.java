package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.UIApplicationState;

import java.util.concurrent.CountDownLatch;

public interface UIController
{
	/**
	 * Lifecycle methods.  Initialize this controller - only ever called once.
	 * Life(0).
	 */
	void initialize() throws Exception;

	/**
	 * Convenience method to ensure initialization does not happen twice.
	 *
	 * @return
	 */
	boolean initialized();

	/**
	 * The application state object and coordinator
	 *
	 * @param state
	 */
	void applicationState(UIApplicationState state);

	/*
		 * Methods that act only on this UI instance (no children are polled).  To
		 * query a child component, locate the component by id first.
		 */
	boolean checkState(int value);

	<T> T getModel(int id);

	/**
	 * Dispatch the given message with an optional value object and response.
	 * The response is intended to let the caller know when the UI has adjusted
	 * to the response - not that the action is complete.  E.G., a message to start
	 * a wizard will be acknowledged when the wizard UI is visible, not when the wizard
	 * is completed.
	 *
	 * @param message
	 * @param value
	 * @return
	 */
	CountDownLatch dispatchAction(String message, Object value);

	CountDownLatch dispatchAction(String message);

	void dispatchActionAndWait(String message, Object value);

	void dispatchActionAndWait(String message);

	void stateChanged(String key, Object value);

	String controllerId();
}
