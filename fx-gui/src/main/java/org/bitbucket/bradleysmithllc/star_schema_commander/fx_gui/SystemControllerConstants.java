package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface SystemControllerConstants
{
	/**
	 * Requires a message object that is a Pair<ReportingArea, String>, where the string
	 * is the optional name of the new reporting area.  If the name is not supplied, the user will be prompted to supply one.
	 */
	String MSG_NEW_REPORTING_AREA = "MSG_NEW_REPORTING_AREA";

	/**
	 * Requires a message object that is a Pair<Fact, DatabaseConnection>
	 */
	String MSG_EXPLORE_FACT = "MSG_EXPLORE_FACT";

	/**
	 * No value object.  Starts the new connection wizard.
	 */
	String MSG_NEW_CONNECTION = "MSG_NEW_CONNECTION";
}
