package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.component;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.controller.ActionController;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.FXMLUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.UINodeReference;

import java.util.concurrent.CountDownLatch;

public class ActionView extends FXMLUIView<ActionController>
{
	public static final String MSG_NEW_REPORTING_AREA = "MSG_NEW_REPORTING_AREA";
	public static final String MSG_IMPORT_DIMENSIONS = "MSG_IMPORT_DIMENSIONS";
	public static final String MSG_IMPORT_FACTS = "MSG_IMPORT_FACTS";
	public static final String MSG_FACT_MEASURE = "MSG_FACT_MEASURE";
	public static final String MSG_NEW_CONNECTION = "MSG_NEW_CONNECTION";
	public static final String MSG_EXPLORE_FACT = "MSG_EXPLORE_FACT";

	public static final int STATE_EXPLORE_VISIBLE = 5000;

	private String newReportingAreaName;

	@UINodeReference(id = "ActionView_NewConnectionMenuItem")
	protected MenuItem newConnectionNode;

	@UINodeReference(id = "ActionView_NewReportingAreaMenuItem")
	protected MenuItem newReportingAreaMenuItemNode;

	@UINodeReference(id = "ActionView_ExploreMenuItem")
	protected MenuItem explorerMenuItemNode;

	@Override
	public void subSubInitialize()
	{
		newReportingAreaMenuItemNode.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
			}
		});

		explorerMenuItemNode.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
			}
		});

		newConnectionNode.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
			}
		});
	}

	@Override
	public void stateChanged(final stateKey key, Object value)
	{
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
	}

	/*
	private class MySwingWorker extends SwingWorker
	{
		private final String raName;

		private final CountDownLatch latch = new CountDownLatch(1);
		private ReportingArea nra;

		public MySwingWorker(String raName)
		{
			this.raName = raName;
		}

		@Override
		protected Object doInBackground() throws Exception
		{
			try
			{
				nra = UIHub.selectedReportingArea().newReportingArea().name(raName).create();
				UIHub.get().persistProfile();
			}
			catch (Throwable thr)
			{
				thr.printStackTrace();
				throw new RuntimeException(thr);
			}

			return null;
		}

		@Override
		protected void done()
		{
			try
			{
				if (nra == null)
				{
					throw new RuntimeException("Null Reporting Area");
				}

				UIHub.selectReportingArea(nra);
			}
			finally
			{
				latch.countDown();
			}
		}

		public void waitFor()
		{
			try
			{
				latch.await();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	 */

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_EXPLORE_VISIBLE:
				return explorerMenuItemNode.isVisible();
			default:
				throw new IllegalArgumentException();
		}
	}
}
