package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.component;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.controller.FXMLController;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.FXMLUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.UINodeReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class DataGridHistoryView extends FXMLUIView<FXMLController>
{
	public static final int STATE_NEXT_ENABLED = 0;
	public static final int STATE_PREVIOUS_ENABLED = 1;
	public static final String MSG_NEXT = "MSG_NEXT";
	public static final String MSG_PREVIOUS = "MSG_PREVIOUS";

	@UINodeReference(id = "DataGridHistoryView_Previous")
	private Button previousButton;

	@UINodeReference(id = "DataGridHistoryView_Next")
	private Button nextButton;

	private List<PersistedQuery> history = new ArrayList<PersistedQuery>();
	private int currentOffset;
	private HistoryQueryListener listener;

	@Override
	protected void subSubInitialize()
	{
		previousButton.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
				if (listener != null)
				{
					// move the pointer back then broadcast the query
					currentOffset--;
					listener.selectQuery(history.get(currentOffset));
				}

				updateButtonStates();
			}
		});

		nextButton.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
				if (listener != null)
				{
					currentOffset++;
					listener.selectQuery(history.get(currentOffset));
				}

				updateButtonStates();
			}
		});
	}

	@Override
	public void activate()
	{
		updateButtonStates();
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_NEXT_ENABLED:
				return !nextButton.isDisabled();
			case STATE_PREVIOUS_ENABLED:
				return !previousButton.isDisabled();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		if (message == MSG_NEXT)
		{
			EventUtils.runOnJFXEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					nextButton.fire();
					latch.countDown();
				}
			});
		}
		else if (message == MSG_PREVIOUS)
		{
			EventUtils.runOnJFXEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					previousButton.fire();
					latch.countDown();
				}
			});
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	public void addQuery(PersistedQuery q)
	{
		// when adding a new query, if there is any history beyond the current spot,
		// clear it out before adding this one
		int targetSize = currentOffset + 1;

		while (history.size() > targetSize)
		{
			history.remove(history.size() - 1);
		}

		history.add(q);

		// advance the current query pointer to the end
		currentOffset = history.size() - 1;

		updateButtonStates();
	}

	private void updateButtonStates()
	{
		EventUtils.runOnJFXEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				// disable both buttons by default so we are always starting from
				// a known state
				previousButton.setDisable(true);
				nextButton.setDisable(true);

				// if history is blank, buttons are always disabled
				if (history.size() > 0)
				{
					// if we have moved past slot 0, then previous is always an option
					if (currentOffset != 0)
					{
						previousButton.setDisable(false);
					}

					if (currentOffset != (history.size() - 1))
					{
						// next should be enabled
						nextButton.setDisable(false);
					}
				}
			}
		});
	}

	public List<PersistedQuery> history()
	{
		return Collections.unmodifiableList(history);
	}

	public void setHistoryQueryListener(HistoryQueryListener list)
	{
		listener = list;
	}

	public interface HistoryQueryListener
	{
		void selectQuery(PersistedQuery query);
	}
}
