package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractUIView extends AbstractUIController implements UIView
{
	public static final Class[] INT_TYPES = new Class[]{int.class};
	public static final Class[] STRING_TYPES = new Class[]{String.class};
	public static final Class[] STRING_OBJECT_TYPES = new Class[]{
		String.class,
		Object.class
	};
	protected UIContainer uiContainer;
	private List<UIView> viewList = new ArrayList<UIView>();

	@Override
	public final void container(UIContainer container)
	{
		this.uiContainer = container;
	}

	@Override
	public boolean checkState(int value)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String viewClass()
	{
		return getClass().getSimpleName();
	}

	@Override
	public String viewInstance()
	{
		return viewClass();
	}

	@Override
	public <T> T getModel(int id)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void activate() throws Exception
	{
	}

	protected final void addChild(UIView view)
	{
		viewList.add(view);
	}

	protected final void removeChild(UIView view)
	{
		if (viewList.size() == 0)
		{
			throw new IllegalStateException();
		}

		if (!viewList.remove(view))
		{
			throw new IllegalArgumentException("Element does not exist in child list.");
		}
	}

	protected final void clearChildViews()
	{
		viewList.clear();
	}

	@Override
	public final List<UIView> childViews()
	{
		return viewList;
	}

	@Override
	public void prepareSelf() throws Exception
	{
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
	}

	@Override
	public void deactivate()
	{
	}

	@Override
	public void stateChanged(String key, Object value)
	{
	}
}
