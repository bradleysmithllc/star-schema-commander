package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.util;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ClassNameUtils
{
	public static String removeCapsWithDashes(String word)
	{
		if (word == null)
		{
			return null;
		}

		char[] letters = word.toCharArray();

		StringBuilder stb = new StringBuilder();

		boolean lastWasUpper = false;

		for (char ch : letters)
		{
			if (Character.isUpperCase(ch))
			{
				stb.append(Character.toLowerCase(ch));
				lastWasUpper = true;
			}
			else
			{
				if (lastWasUpper)
				{
					// do > 1 to account for the upper case letter itself
					if (stb.length() > 1)
					{
						stb.insert(stb.length() - 1, '-');
					}
				}

				lastWasUpper = false;
				stb.append(ch);
			}
		}

		return stb.toString();
	}
}
