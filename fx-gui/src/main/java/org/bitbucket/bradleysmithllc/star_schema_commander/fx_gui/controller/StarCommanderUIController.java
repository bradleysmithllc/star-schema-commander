package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.controller;

/*
 * #%L
 * fx-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view.AbstractUIController;

public class StarCommanderUIController extends AbstractUIController
{
	@FXML
	private ChoiceBox<String> reportingAreaNavigation;

	@FXML
	private Label reportingAreaLabel;

	@FXML
	private ListView<ReportingArea> reportingAreaList;

	@FXML
	private Label connectionLabel;

	@FXML
	private ListView<DatabaseConnection> connectionList;

	public void build()
	{
		// set the renderer
		connectionList.setCellFactory(new Callback<ListView<DatabaseConnection>, ListCell<DatabaseConnection>>()
		{
			final ListCell<DatabaseConnection> listCell = new ListCell<DatabaseConnection>()
			{
				@Override
				protected void updateItem(DatabaseConnection item, boolean empty)
				{
					super.updateItem(item, empty);
					if (empty || item == null)
					{
						setText(null);
						setGraphic(null);
					}
					else
					{
						setText(item.name());
					}
				}
			};

			@Override
			public ListCell<DatabaseConnection> call(ListView<DatabaseConnection> param)
			{
				return listCell;
			}
		});

		// set up the connections list
		ObservableList<DatabaseConnection> items = FXCollections.observableArrayList();
		connectionList.setItems(items);

		// copy all profile connections
		for (DatabaseConnection con : uiApplicationState.profile().connections().values())
		{
			items.add(con);
		}

		// set up the reporting area list
		ObservableList<ReportingArea> ritems = FXCollections.observableArrayList();
		reportingAreaList.setItems(ritems);

		boolean mto = false;

		// copy all profile connections
		for (ReportingArea con : uiApplicationState.profile().defaultReportingArea().children().values())
		{
			mto = true;
			ritems.add(con);
		}

		if (!mto)
		{
			reportingAreaLabel.setVisible(false);
			reportingAreaList.setVisible(false);
			reportingAreaNavigation.setVisible(false);
		}
		else
		{
			ObservableList<String> oal = FXCollections.observableArrayList();
			reportingAreaNavigation.setItems(oal);

			oal.addAll(uiApplicationState.reportingArea().name());

			reportingAreaNavigation.selectionModelProperty().get().select(0);
		}
	}

	@Override
	public void stateChanged(String key, Object value)
	{
	}

	public boolean reportingAreasVisible()
	{
		boolean reportingAreaLabelVisible = reportingAreaLabel.isVisible();
		boolean reportingAreaListVisible = reportingAreaList.isVisible();
		boolean reportingAreaNavigationVisible = reportingAreaNavigation.isVisible();
		return reportingAreaLabelVisible || reportingAreaListVisible || reportingAreaNavigationVisible;
	}
}