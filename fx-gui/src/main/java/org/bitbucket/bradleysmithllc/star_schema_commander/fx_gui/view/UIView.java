package org.bitbucket.bradleysmithllc.star_schema_commander.fx_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.scene.Node;

import java.net.URL;
import java.util.List;

public interface UIView<T extends UIController> extends UIController
{
	/**
	 * Retain a reference to the container that owns this component
	 *
	 * @param container
	 */
	void container(UIContainer container);

	/**
	 * Activate this component.  Called on first activation or any
	 * time reactivate is called.
	 * Life(1)
	 */
	void activate() throws Exception;

	/**
	 * Get this objects children.
	 * Life(2)
	 *
	 * @return
	 */
	List<UIView> childViews();

	/**
	 * Prepare this object for display.  Child components are not available yet.
	 * Life(3)
	 */
	void prepareSelf() throws Exception;

	/**
	 * Prepare this object for display.  Child components are available and have been initialized.
	 * Life(4)
	 */
	void prepareWithChildren() throws Exception;

	/**
	 * Get the view object.
	 * Life(5)
	 *
	 * @return
	 */
	Node view();

	T uiController();

	URL styleSheet();


	/**
	 * Deactivate this component.
	 * Life(6)
	 */
	void deactivate();

	/**
	 * The identity type of this component.
	 *
	 * @return
	 */
	String viewClass();

	/**
	 * The identity of this component within it's type.
	 *
	 * @return
	 */
	String viewInstance();
}
