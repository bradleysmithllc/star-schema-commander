package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class CreateH2ConnectionTest extends ProfileTest
{
	@Rule
	public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void dataFileIsADirectory() throws Throwable
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());
		CreateProfile.main(new String[]{});

		try
		{
			CreateH2Connection.main(new String[]{
				"-n",
				"<none>",
				"-pa",
				temporaryFolder.newFolder().getAbsolutePath()
			});

			Assert.fail();
		}
		catch (InvocationTargetException exc)
		{
			Throwable cause = exc.getCause();
			Assert.assertSame(IllegalArgumentException.class, cause.getClass());

			Assert.assertEquals("Must use a regular file as a data source.", cause.getMessage());
		}
	}

	@Test
	public void dataFileDoesNotExist() throws Throwable
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());
		CreateProfile.main(new String[]{});

		CreateH2Connection.main(new String[]{
			"-n",
			"<none>",
			"-pa",
			new File(temporaryFolder.newFolder(), "whereever").getAbsolutePath()
		});
	}
}
