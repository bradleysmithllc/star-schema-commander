package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import net.java.truevfs.access.TFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class RemoveConnectionTest extends ProfileTest
{
	@Rule
	public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void removeDefault() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());

		CreateProfile.main(new String[]{});
		CreateSqlServerConnection.main(new String[]{
			"-n",
			"name",
			"-sn",
			"server"
		});

		// verify it was created
		Assert.assertTrue(new TFile(new TFile(new TFile(userHome, Profile.defaultProfileName()), "connections"), "name.json").exists());

		Profile p = new Profile(new File(userHome, Profile.defaultProfileName()));
		Assert.assertEquals(1, p.connections().size());

		RemoveConnection.main(new String[]{
			"-n",
			"name"
		});

		p = new Profile(new File(userHome, Profile.defaultProfileName()));
		Assert.assertEquals(0, p.connections().size());
	}

	@Test
	public void removeOverride() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFile("profile.zip");
		userHome.delete();

		CreateProfile.main(new String[]{
			"-profile",
			userHome.getAbsolutePath()
		});
		CreateSqlServerConnection.main(new String[]{
			"-profile",
			userHome.getAbsolutePath(),
			"-n",
			"name",
			"-sn",
			"server"
		});

		// verify it was created
		Assert.assertFalse(new TFile(new TFile(new TFile(userHome, Profile.defaultProfileName()), "connections"), "name.json").exists());

		Profile p = new Profile(userHome);
		Assert.assertEquals(1, p.connections().size());

		RemoveConnection.main(new String[]{
			"-profile",
			userHome.getAbsolutePath(),
			"-n",
			"name"
		});

		p = new Profile(userHome);
		Assert.assertEquals(0, p.connections().size());

		// verify it was removed
		Assert.assertFalse(new TFile(new TFile(new TFile(userHome, Profile.defaultProfileName()), "connections"), "name.json").exists());
	}

	@Test
	public void removeDoesntExist() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());

		CreateProfile.main(new String[]{});

		RemoveConnection.main(new String[]{
			"-n",
			"name"
		});

		Assert.assertEquals(-1, lastExit);
	}
}
