package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.java.truevfs.access.TFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class CreateSqlServerConnectionTest extends ProfileTest
{
	@Rule
	public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void createProfileDefault() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());
		CreateProfile.main(new String[]{});
		CreateSqlServerConnection.main(new String[]{
			"-n",
			"name",
			"-sn",
			"server"
		});

		// verify it was created
		Assert.assertTrue(new TFile(new TFile(new TFile(userHome, Profile.defaultProfileName()), "connections"), "name.json").exists());
	}

	@Test
	public void createProfileOverride() throws IOException
	{
		// create in user home
		File userHome = new File(temporaryFolder.newFolder(), "ssc.zip");

		CreateProfile.main(new String[]{
			"-profile",
			userHome.getAbsolutePath()
		});
		CreateSqlServerConnection.main(new String[]{
			"-profile",
			userHome.getAbsolutePath(),
			"-n",
			"name",
			"-sn",
			"server"
		});

		// verify it was created
		Assert.assertTrue(new TFile(new TFile(userHome, "connections"), "name.json").exists());
	}

	@Test
	public void allVariablesTrusted() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());
		CreateProfile.main(new String[]{});
		CreateSqlServerConnection.main(new String[]{
			"-n",
			"name",
			"-sn",
			"server",
			"-in",
			"instance",
			"-database",
			"database"
		});

		// verify it was created
		File connections = new TFile(new TFile(new TFile(userHome, Profile.defaultProfileName()), "connections"), "name.json");
		Assert.assertTrue(connections.exists());

		// verify the state
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();
		String json = CreateOracleConnectionTest.getString(connections);

		DatabaseConnection conn = gson.fromJson(json, DatabaseConnection.class);
		Assert.assertEquals("name", conn.name());
		Assert.assertEquals("jdbc:jtds:sqlserver://server/database;instance=instance", conn.jdbcUrl());
		Assert.assertEquals("net.sourceforge.jtds.jdbc.Driver", conn.driverClass());
		Assert.assertEquals("dbo", conn.defaultSchema());
		Assert.assertNull(conn.userName());
		Assert.assertNull(conn.password());
	}

	@Test
	public void allVariablesUser() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());
		CreateProfile.main(new String[]{});
		CreateSqlServerConnection.main(new String[]{
			"-n",
			"name",
			"-sn",
			"server",
			"-in",
			"instance",
			"-database",
			"database",
			"-un",
			"user",
			"-pwd",
			"password",
			"-dn",
			"domain"
		});

		// verify it was created
		File connections = new TFile(new TFile(new TFile(userHome, Profile.defaultProfileName()), "connections"), "name.json");
		Assert.assertTrue(connections.exists());

		// verify the state
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();
		String json = CreateOracleConnectionTest.getString(connections);

		DatabaseConnection conn = gson.fromJson(json, DatabaseConnection.class);
		conn.decryptPassword();
		Assert.assertEquals("name", conn.name());
		Assert.assertEquals("jdbc:jtds:sqlserver://server/database;domain=domain;instance=instance;useNTLMv2=true", conn.jdbcUrl());
		Assert.assertEquals("net.sourceforge.jtds.jdbc.Driver", conn.driverClass());
		Assert.assertEquals("dbo", conn.defaultSchema());
		Assert.assertEquals("user", conn.userName());
		Assert.assertEquals("password", conn.password());
	}
}
