package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class ExecuteConnectionScriptTest extends ProfileTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void scriptIsADirectory() throws Throwable
	{
		expectedException.expect(InvocationTargetException.class);
		expectedException.expectCause(new CauseMatcher(IllegalArgumentException.class, "Script \\[[^\\]]+\\] is not a file"));

		File t = temporaryFolder.newFile("profile.zip");
		FileUtils.forceDelete(t);

		CreateProfile.main(new String[]{
			"-profile",
			t.getAbsolutePath()
		});

		ExecuteConnectionScript.main(new String[]{
			"-n",
			"<none>",
			"-pa",
			temporaryFolder.newFolder().getAbsolutePath(),
			"-profile",
			t.getAbsolutePath()
		});

		Assert.fail();
	}

	@Test
	public void scriptDoesNotExist() throws Throwable
	{
		expectedException.expect(InvocationTargetException.class);
		expectedException.expectCause(new CauseMatcher(IllegalArgumentException.class, "Script \\[[^\\]]+\\] does not exist"));

		File t = temporaryFolder.newFile("profile.zip");
		FileUtils.forceDelete(t);

		CreateProfile.main(new String[]{
			"-profile",
			t.getAbsolutePath()
		});
		ExecuteConnectionScript.main(new String[]{
			"-n",
			"<none>",
			"-pa",
			new File(temporaryFolder.newFolder(), "file").getAbsolutePath(),
			"-profile",
			t.getAbsolutePath()
		});

		Assert.fail();
	}

	@Test
	public void wellDone() throws Throwable
	{
		File profile = temporaryFolder.newFile("profile.zip");
		FileUtils.forceDelete(profile);

		CreateProfile.main(new String[]{
			"-profile",
			profile.getAbsolutePath()
		});

		File f = temporaryFolder.newFile();

		FileUtils.write(f, "CREATE SCHEMA A;", true);
		FileUtils.write(f, "CREATE TABLE A.TABLE_A(ID INT NOT NULL);", true);

		for (int i = 2012; i < 3020; i++)
		{
			FileUtils.write(f, "INSERT INTO A.TABLE_A(ID) VALUES(" + i + ");", true);
		}

		CreateH2Connection.main(new String[]{
			"-profile",
			profile.getAbsolutePath(),
			"-n",
			"h2",
			"-pa",
			temporaryFolder.newFile().getAbsolutePath()
		});

		ExecuteConnectionScript.main(new String[]{
			"-n",
			"h2",
			"-pa",
			f.getAbsolutePath(),
			"-profile",
			profile.getAbsolutePath()
		});
	}
}
