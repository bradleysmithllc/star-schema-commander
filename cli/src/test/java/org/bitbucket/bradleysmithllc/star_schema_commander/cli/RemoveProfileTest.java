package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class RemoveProfileTest extends ProfileTest
{
	@Rule
	public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void removeProfileDefault() throws IOException
	{
		// create in user home
		File userHome = temporaryFolder.newFolder();

		System.setProperty("user.home", userHome.getAbsolutePath());
		// create it so it can be removed
		CreateProfile.main(new String[]{});
		RemoveProfile.main(new String[]{});

		// verify it was created
		Assert.assertFalse(new File(userHome, ".sscommander").exists());
	}

	@Test
	public void removeProfileOverride() throws IOException
	{
		// create in user home
		File userHome = new File(temporaryFolder.newFolder(), "ssc");

		CreateProfile.main(new String[]{
			"-profile",
			userHome.getAbsolutePath()
		});
		RemoveProfile.main(new String[]{
			"-profile",
			userHome.getAbsolutePath()
		});

		// verify it was created
		Assert.assertFalse(userHome.exists());
	}

	@Test
	public void removeProfileDoesntExist() throws IOException
	{
		// create in user home
		File userHome = new File(temporaryFolder.newFolder(), "ssc");

		RemoveProfile.main(new String[]{
			"-profile",
			userHome.getAbsolutePath()
		});
	}
}
