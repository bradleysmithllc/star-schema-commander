package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.SqlServerJTDSConnectionBuilder;

import java.io.File;
import java.io.IOException;

@CLIEntry(
	description = "Create a new sql-server connection in a Star(schema) Commander profile",
	nickName = "createsqlcon",
	contact = "bradleysmithllc@gmail.com",
	version = "ssc.1.D"
)
public class CreateSqlServerConnection extends ProfileBase
{
	private String name;
	private String serverName;
	private String instanceName;
	private int port = -1;
	private String domain;
	private String sqlUser;
	private String sqlPass;
	private String database;

	public static void main(String[] argv)
	{
		try
		{
			CommonsCLILauncher.main(argv);
			exit(0);
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			exit(1);
		}
	}

	@CLIOption(
		name = "n",
		longName = "name",
		required = true,
		description = "Connection name"
	)
	public void setName(String n)
	{
		name = n;
	}

	@CLIOption(
		name = "sn",
		longName = "server-name",
		required = true,
		description = "Server name"
	)
	public void setServerName(String n)
	{
		serverName = n;
	}

	@CLIOption(
		name = "in",
		longName = "instance-name",
		description = "Instance name"
	)
	public void setInstanceName(String n)
	{
		instanceName = n;
	}

	@CLIOption(
		name = "po",
		longName = "port",
		description = "Port"
	)
	public void setPort(int p)
	{
		port = p;
	}

	@CLIOption(
		name = "dn",
		longName = "domain-name",
		description = "NT Domain name to use."
	)
	public void setDomainName(String name)
	{
		domain = name;
	}

	@CLIOption(
		name = "un",
		longName = "user-name",
		description = "User name.  If domain is supplied, this will work like a Windows user account."
	)
	public void setSqlUser(String sqluser)
	{
		sqlUser = sqluser;
	}

	@CLIOption(
		name = "pwd",
		longName = "password",
		description = "Password"
	)
	public void setSqlPass(String sqlpass)
	{
		sqlPass = sqlpass;
	}

	@CLIOption(
		name = "db",
		longName = "database",
		description = "Database name"
	)
	public void setDatabase(String db)
	{
		database = db;
	}

	@CLIMain
	public void create() throws IOException
	{
		File profile = getProfileDirectory();
		Profile p = new Profile(profile);
		System.out.println("Creating sql-server connection in profile [" + profile + "]");

		SqlServerJTDSConnectionBuilder sqlServerConnectionBuilder = p.newSqlServerJTDSConnection().name(name).serverName(serverName);

		if (instanceName != null)
		{
			sqlServerConnectionBuilder.instanceName(instanceName);
		}

		if (port != -1)
		{
			sqlServerConnectionBuilder.port(port);
		}

		if (domain != null)
		{
			sqlServerConnectionBuilder.domain(domain);
		}

		if (sqlUser != null)
		{
			sqlServerConnectionBuilder.username(sqlUser);
		}

		if (sqlPass != null)
		{
			sqlServerConnectionBuilder.password(sqlPass);
		}

		if (database != null)
		{
			sqlServerConnectionBuilder.database(database);
		}

		sqlServerConnectionBuilder.create();

		p.persist();

		System.out.println("Connection created successfully");
	}
}
