package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import java.io.File;
import java.io.IOException;

@CLIEntry(
	description = "Removes a Star(schema) Commander profile after validating that it is a profile.",
	version = "ssc.1.D",
	contact = "bradleysmithllc@gmail.com",
	nickName = "removep"
)
public class RemoveProfile extends ProfileBase
{
	public static void main(String[] argv)
	{
		try
		{
			CommonsCLILauncher.main(argv);
			exit(0);
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			exit(1);
		}
	}

	@CLIMain
	public void remove() throws IOException
	{
		// initialize a profile.  Will throw an exception if the path exists
		File profile = getProfileDirectory();

		System.out.println("Validating profile in [" + profile + "]");

		Profile p = new Profile(profile);

		System.out.println("Profile validated - purging");
		FileUtils.forceDelete(profile);
		System.out.println("Profile does_not_exist.");
	}
}
