package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

@CLIEntry(
	description = "Imports a fact from a connection",
	nickName = "generate-fact",
	contact = "bradleysmithllc@gmail.com",
	version = "ssc.1.D"
)
public class GenerateFact extends ProfileBase
{
	private String name;
	private String schema;
	private String connectionName;
	private String reportingAreaName;

	public static void main(String[] argv)
	{
		try
		{
			CommonsCLILauncher.main(argv);
			exit(0);
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			exit(-1);
		}
	}

	@CLIOption(
		name = "fn",
		longName = "fact-name",
		required = true,
		description = "Fact name"
	)
	public void setName(String n)
	{
		name = n;
	}

	@CLIOption(
		name = "sch",
		longName = "schema",
		description = "Schema name"
	)
	public void setSchemaName(String n)
	{
		schema = n;
	}

	@CLIOption(
		name = "cn",
		longName = "connection-name",
		required = true,
		description = "Connection name"
	)
	public void setConnectionName(String n)
	{
		connectionName = n;
	}

	@CLIOption(
		name = "ran",
		longName = "reporting-area-name",
		required = false,
		description = "Reporting area name"
	)
	public void setReportingAreaName(String n)
	{
		reportingAreaName = n;
	}

	@CLIMain
	public void _import() throws IOException, SQLException
	{
		File profile = getProfileDirectory();
		Profile p = new Profile(profile);

		ReportingArea reportingArea = p.defaultReportingArea();

		if (reportingAreaName != null)
		{
			reportingArea = p.defaultReportingArea().reportingAreas().get(reportingAreaName);

			if (reportingArea == null)
			{
				throw new IllegalArgumentException("Reporting area [" + reportingAreaName + "] not found");
			}
		}

		System.out.println("Importing fact in profile [" + profile + "] into reporting area [" + reportingArea.qualifiedName() + "]");

		Map<String, DatabaseConnection> conns = p.connections();

		if (!conns.containsKey(connectionName))
		{
			System.out.println("ERROR: Connection name [" + connectionName + "] not found.");
			exit(-1);
		}

		DatabaseConnection conn = conns.get(connectionName);

		reportingArea.loadFactFromMetaData(conn, null, ObjectUtils.firstNonNull(schema, conn.defaultSchema()), name);

		p.persist();

		System.out.println("Fact imported successfully");
	}
}
