package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import java.io.File;

public class ProfileBase
{
	private static ExitHandler exitHandler = new SystemExitHandler();
	private File dir;

	public static void useExitHandler(ExitHandler handler)
	{
		exitHandler = handler;
	}

	static void exit(int code)
	{
		exitHandler.exit(code);
	}

	@CLIOption(
		name = "p",
		longName = "profile",
		description = "The path to initialize as a Star(schema) Commander profile.  The default is the ${user.home}/.sscommander"
	)
	public void setDirectory(String path)
	{
		dir = new File(path);
	}

	protected File getProfileDirectory()
	{
		if (dir == null)
		{
			dir = Profile.defaultProfileDir();
		}

		return dir;
	}

	public interface ExitHandler
	{
		void exit(int i);
	}

	private static class SystemExitHandler implements ExitHandler
	{
		@Override
		public void exit(int i)
		{
			System.exit(i);
		}
	}
}
