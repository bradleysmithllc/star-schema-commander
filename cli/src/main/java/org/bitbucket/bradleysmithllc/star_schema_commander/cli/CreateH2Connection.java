package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import java.io.File;
import java.io.IOException;

@CLIEntry(
	description = "Create a new H2 connection in a Star(schema) Commander profile",
	nickName = "createh2con",
	contact = "bradleysmithllc@gmail.com",
	version = "ssc.1.D"
)
public class CreateH2Connection extends ProfileBase
{
	private String name;
	private String path;

	public static void main(String[] argv) throws Throwable
	{
		try
		{
			CommonsCLILauncher.main(argv);
			exit(0);
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			exit(1);
			throw thr;
		}
	}

	@CLIOption(
		name = "n",
		longName = "name",
		required = true,
		description = "Connection name"
	)
	public void setName(String n)
	{
		name = n;
	}

	@CLIOption(
		name = "pa",
		longName = "path",
		description = "Path to database file",
		required = true
	)
	public void setPath(String p)
	{
		path = p;
	}

	@CLIMain
	public void create() throws IOException
	{
		File profile = getProfileDirectory();
		Profile p = new Profile(profile);
		System.out.println("Creating H2 connection in profile [" + profile + "]");

		p.newH2Connection().name(name).path(new File(path)).create();

		p.persist();

		System.out.println("Connection created successfully");
	}
}
