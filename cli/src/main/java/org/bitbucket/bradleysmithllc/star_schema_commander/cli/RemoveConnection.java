package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@CLIEntry(
	description = "Create a new sql-server connection in a Star(schema) Commander profile",
	nickName = "removesqlcon",
	contact = "bradleysmithllc@gmail.com",
	version = "ssc.1.D"
)
public class RemoveConnection extends ProfileBase
{
	private String name;

	public static void main(String[] argv)
	{
		try
		{
			CommonsCLILauncher.main(argv);
			exit(0);
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			exit(-1);
		}
	}

	@CLIOption(
		name = "n",
		longName = "name",
		required = true,
		description = "Connection name"
	)
	public void setName(String n)
	{
		name = n;
	}

	@CLIMain
	public void remove() throws IOException
	{
		File profile = getProfileDirectory();
		Profile p = new Profile(profile);
		System.out.println("Removing sql-server connection in profile [" + profile + "]");

		Map<String, DatabaseConnection> conns = p.connections();
		if (!conns.containsKey(name))
		{
			System.out.println("ERROR: Connection name [" + name + "] not found.");
			exit(-1);
		}

		conns.get(name).remove();

		p.persist();

		System.out.println("Connection removed successfully");
	}
}
