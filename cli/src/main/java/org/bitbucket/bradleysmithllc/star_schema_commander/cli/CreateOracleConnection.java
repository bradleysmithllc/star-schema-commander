package org.bitbucket.bradleysmithllc.star_schema_commander.cli;

/*
 * #%L
 * cli
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.OracleConnectionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import java.io.File;
import java.io.IOException;

@CLIEntry(
	description = "Create a new Oracle connection in a Star(schema) Commander profile",
	nickName = "createoracleon",
	contact = "bradleysmithllc@gmail.com",
	version = "ssc.1.D"
)
public class CreateOracleConnection extends ProfileBase
{
	private String name;
	private String serverName;
	private String serviceName;
	private int port = -1;
	private String sqlUser;
	private String sqlPass;

	public static void main(String[] argv)
	{
		try
		{
			CommonsCLILauncher.main(argv);
			exit(0);
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			exit(1);
		}
	}

	@CLIOption(
		name = "n",
		longName = "name",
		required = true,
		description = "Connection name"
	)
	public void setName(String n)
	{
		name = n;
	}

	@CLIOption(
		name = "sn",
		longName = "server-name",
		required = true,
		description = "Server name"
	)
	public void setServerName(String n)
	{
		serverName = n;
	}

	@CLIOption(
		name = "svn",
		longName = "service-name",
		description = "Service name"
	)
	public void setInstanceName(String n)
	{
		serviceName = n;
	}

	@CLIOption(
		name = "po",
		longName = "port",
		description = "Port"
	)
	public void setPort(int p)
	{
		port = p;
	}

	@CLIOption(
		name = "un",
		longName = "user-name",
		description = "User name."
	)
	public void setSqlUser(String sqluser)
	{
		sqlUser = sqluser;
	}

	@CLIOption(
		name = "pwd",
		longName = "password",
		description = "Password"
	)
	public void setSqlPass(String sqlpass)
	{
		sqlPass = sqlpass;
	}

	@CLIMain
	public void create() throws IOException
	{
		File profile = getProfileDirectory();
		Profile p = new Profile(profile);
		System.out.println("Creating Oracle connection in profile [" + profile + "]");

		OracleConnectionBuilder sqlServerConnectionBuilder = p.newOracleConnection().name(name).serverName(serverName);

		sqlServerConnectionBuilder.username(sqlUser);
		sqlServerConnectionBuilder.password(sqlPass);

		if (serviceName != null)
		{
			sqlServerConnectionBuilder.serviceName(serviceName);
		}

		if (port != -1)
		{
			sqlServerConnectionBuilder.port(port);
		}

		sqlServerConnectionBuilder.create();

		p.persist();

		System.out.println("Connection created successfully");
	}
}
