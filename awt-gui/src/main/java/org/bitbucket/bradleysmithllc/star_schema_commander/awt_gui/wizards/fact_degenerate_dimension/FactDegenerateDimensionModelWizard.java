package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_degenerate_dimension;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.ColumnProcessorModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.WizardColumnHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;

import java.util.HashMap;
import java.util.Map;

public class FactDegenerateDimensionModelWizard extends ColumnProcessorModelWizard<Fact>
{
	public FactDegenerateDimensionModelWizard(final Fact fact)
	{
		super(fact, new WizardColumnHandler<Fact>()
		{
			private Dimension degenerateDimension;
			private Map<String, String> eligibleColumns = new HashMap<String, String>();
			private Fact.DegenerateDimensionBuilder dd;

			{
				// populate all columns by default
				for (Map.Entry<String, DatabaseColumn> col : fact.columns().entrySet())
				{
					eligibleColumns.put(col.getValue().name(), "");
				}

				// remove all columns mapped as measures
				for (Map.Entry<String, FactMeasure> meas : fact.measures().entrySet())
				{
					eligibleColumns.remove(meas.getValue().column().name());
				}

				// get a list of all columns already in the DD
				if (fact.hasDegenerateDimension())
				{
					degenerateDimension = fact.degenerateDimension();

					for (Map.Entry<String, DatabaseColumn> entry : degenerateDimension.columns().entrySet())
					{
						eligibleColumns.remove(entry.getValue().name());
					}
				}
			}

			@Override
			public void beginHandling(Fact fact)
			{
				if (degenerateDimension == null)
				{
					dd = fact.newDegenerateDimension();
				}
			}

			@Override
			public void handle(Fact fact, String columnName, String logicalName)
			{
				if (degenerateDimension != null)
				{
					// create new column and attribute if the dimension already exists
					degenerateDimension.newDatabaseColumn().name(columnName).jdbcType(fact.columns().get(columnName).jdbcType()).create();
					degenerateDimension.newDimensionAttribute().column(columnName).logicalName(logicalName).create();
				}
				else
				{
					dd.column(columnName, logicalName);
				}
			}

			@Override
			public void endHandling(Fact fact)
			{
				if (dd != null)
				{
					dd.create();
				}
			}

			@Override
			public boolean include(Fact fact, DatabaseColumn value)
			{
				return eligibleColumns.containsKey(value.name());
			}
		});
	}
}
