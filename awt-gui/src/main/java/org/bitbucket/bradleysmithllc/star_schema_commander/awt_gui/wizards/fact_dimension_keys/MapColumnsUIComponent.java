package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * In:  Fact.  DimensionKey.
 * Out: Fact.Columns >> Dimension.Columns
 * FactDimensionKey.Physical Name
 * FactDimensionKey.RoleName*
 * *	If role playing.
 */
public class MapColumnsUIComponent extends WizardStateUIComponent<FactDimensionKeyRecord>
{
	public static final String MSG_SET_ROLE_NAME = "MSG_SET_ROLE_NAME";
	public static final String MSG_SET_PHYSICAL_NAME = "MSG_SET_PHYSICAL_NAME";
	public static final String MSG_MAP_COLUMN = "MSG_MAP_COLUMN";

	public static final int MOD_ROLE_NAME = 0;
	public static final int MOD_PHYSICAL_NAME = 1;
	public static final int MOD_DIMENSION_KEY = 2;
	public static final int MOD_FACT = 3;
	public static final int MOD_DIMENSION_KEY_QUALIFIED_NAME = 4;
	public static final int MOD_BUNDLE = 5;

	public static final int STATE_ROLE_NAME_VISIBLE = 0;

	private JPanel container;

	private JLabel dimensionName;
	private JLabel keyName;

	private JTextField physicalName;
	private JTextField roleName;
	private MapColumnsTableModel columnsTableModel;
	private DefaultComboBoxModel<DatabaseColumn> factColumnListModel;
	private JLabel roleNameLabel;

	@Override
	public void subInitialize() throws Exception
	{
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				container = new JPanel();
				container.setLayout(new BorderLayout());

				JPanel attrPanel = new JPanel();
				attrPanel.setLayout(new GridLayout(4, 2));

				attrPanel.add(new JLabel("Dimension Name"));
				dimensionName = new JLabel("~");
				attrPanel.add(dimensionName);
				attrPanel.add(new JLabel("Key Name"));
				keyName = new JLabel("~");
				attrPanel.add(keyName);

				physicalName = new JTextField("KEY_NAME");
				physicalName.getDocument().addDocumentListener(new DocumentListener()
				{
					@Override
					public void insertUpdate(DocumentEvent e)
					{
						String physicalNameText = physicalName.getText().trim();

						if (physicalNameText.equals(""))
						{
							physicalNameText = null;
						}

						state().model().physicalName(physicalNameText);
						checkOkayStatus();
					}

					@Override
					public void removeUpdate(DocumentEvent e)
					{
						String physicalNameText = physicalName.getText().trim();

						if (physicalNameText.equals(""))
						{
							physicalNameText = null;
						}

						state().model().physicalName(physicalNameText);
						checkOkayStatus();
					}

					@Override
					public void changedUpdate(DocumentEvent e)
					{
						String physicalNameText = physicalName.getText().trim();

						if (physicalNameText.equals(""))
						{
							physicalNameText = null;
						}

						state().model().physicalName(physicalNameText);
						checkOkayStatus();
					}
				});

				roleName = new JTextField("ROLE");
				roleName.getDocument().addDocumentListener(new DocumentListener()
				{
					@Override
					public void insertUpdate(DocumentEvent e)
					{
						String roleNameText = roleName.getText().trim();

						if (roleNameText != null && roleNameText.equals(""))
						{
							roleNameText = null;
						}

						state().model().roleName(roleNameText);
						checkOkayStatus();
					}

					@Override
					public void removeUpdate(DocumentEvent e)
					{
						String roleNameText = roleName.getText();

						if (roleNameText != null && roleNameText.equals(""))
						{
							roleNameText = null;
						}

						state().model().roleName(roleNameText);
						checkOkayStatus();
					}

					@Override
					public void changedUpdate(DocumentEvent e)
					{
						String roleNameText = roleName.getText();

						if (roleNameText != null && roleNameText.equals(""))
						{
							roleNameText = null;
						}

						state().model().roleName(roleNameText);
						checkOkayStatus();
					}
				});

				attrPanel.add(new JLabel("Physical Name"));
				attrPanel.add(physicalName);
				roleNameLabel = new JLabel("Role Name");
				attrPanel.add(roleNameLabel);
				attrPanel.add(roleName);

				container.add(attrPanel, BorderLayout.NORTH);

				columnsTableModel = new MapColumnsTableModel();

				factColumnListModel = new DefaultComboBoxModel<DatabaseColumn>();
				JComboBox<DatabaseColumn> editorComp = new JComboBox<DatabaseColumn>(factColumnListModel);
				editorComp.setRenderer(new ListCellRenderer<DatabaseColumn>()
				{
					private final JLabel label = new JLabel("~");

					@Override
					public Component getListCellRendererComponent(JList<? extends DatabaseColumn> list, DatabaseColumn value, int index, boolean isSelected, boolean cellHasFocus)
					{
						if (value == null)
						{
							label.setText("~");
						}
						else
						{
							label.setText(value.name() + ": " + value.jdbcTypeName());
						}

						return label;
					}
				});

				final JTable columns = new JTable(columnsTableModel);

				// nothing to do here but complete the model
				columnsTableModel.visit(new CoordinatedTableModel.ModelVisitor<Pair<DatabaseColumn, DatabaseColumn>>()
				{
					@Override
					public void visit(CoordinatedTableModel.UpdatableTableModel<Pair<DatabaseColumn, DatabaseColumn>> model)
					{
					}
				});

				columns.setDefaultRenderer(DatabaseColumn.class, new TableCellRenderer()
				{
					private final JLabel label = new JLabel();

					@Override
					public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
					{
						label.setText("<-- unmapped -->");

						if (value != null)
						{
							DatabaseColumn databaseColumn = (DatabaseColumn) value;
							label.setText(databaseColumn.name() + ": " + databaseColumn.jdbcTypeName());
						}

						return label;
					}
				});

				columns.setDefaultEditor(DatabaseColumn.class, new DefaultCellEditor(editorComp));

				columnsTableModel.addTableModelListener(new TableModelListener()
				{
					@Override
					public void tableChanged(TableModelEvent e)
					{
						checkOkayStatus();

						// record column mappings in model.
						Map<String, String> stringMap = state().model().columns();
						stringMap.clear();

						for (Pair<DatabaseColumn, DatabaseColumn> td : columnsTableModel.tableData())
						{
							DatabaseColumn tdRight = td.getRight();

							if (tdRight != null)
							{
								stringMap.put(td.getLeft().name(), tdRight.name());
							}
						}
					}
				});

				JScrollPane jscroll = new JScrollPane(columns);
				jscroll.setBorder(BorderFactory.createTitledBorder("Select Columns to Map"));

				container.add(jscroll, BorderLayout.CENTER);
			}
		});
	}

	private void checkOkayStatus()
	{
		boolean valid = true;

		// if rolename is required, make sure it is supplied
		if (roleName.isVisible())
		{
			String roleName = state().model().roleName();
			if (roleName == null)
			{
				valid = false;
			}
		}

		// validate physical name
		valid &= state().model().physicalName() != null;

		// check that all columns are mapped
		for (Pair<DatabaseColumn, DatabaseColumn> td : columnsTableModel.tableData())
		{
			if (td.getRight() == null)
			{
				valid = false;
			}
		}

		for (WizardStateTransition<FactDimensionKeyRecord, ? extends WizardStateUIComponent<FactDimensionKeyRecord>, ?, ?> tr : state().transitions())
		{
			tr.enable(valid);
		}
	}

	@Override
	public void activate()
	{
		columnsTableModel.visit(new CoordinatedTableModel.ModelVisitor<Pair<DatabaseColumn, DatabaseColumn>>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<Pair<DatabaseColumn, DatabaseColumn>> model)
			{
				model.clearData();
			}
		});

		try
		{
			EventUtils.runOnEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					// disable all transitions
					for (WizardStateTransition<FactDimensionKeyRecord, ? extends WizardStateUIComponent<FactDimensionKeyRecord>, ?, ?> tr : state().transitions())
					{
						tr.enable(false);
					}

					final FactDimensionKeyRecord factDimensionKeyRecord = state().model();
					Fact fact = factDimensionKeyRecord.fact();
					Dimension dimension = factDimensionKeyRecord.dimensionKey().dimension();

					Map<String, FactDimensionKey> keys = fact.keys(dimension);

					dimensionName.setText(dimension.logicalName());
					keyName.setText(factDimensionKeyRecord.dimensionKey().logicalName());

					physicalName.setText(factDimensionKeyRecord.dimensionKey().physicalName());
					roleName.setText(state().model().dimensionKey().dimension().logicalName());

					if (keys.size() != 0 || factDimensionKeyRecord.requireRole())
					{
						// assign a unique role name
						String name = dimension.logicalName() + "_" + System.currentTimeMillis();
						roleName.setVisible(true);
						roleNameLabel.setVisible(true);
						roleName.setText(name);
					}
					else
					{
						roleNameLabel.setVisible(false);
						roleName.setVisible(false);
					}

					factColumnListModel.removeAllElements();

					for (Map.Entry<String, DatabaseColumn> col : fact.columns().entrySet())
					{
						factColumnListModel.addElement(col.getValue());
					}

					columnsTableModel.visit(new CoordinatedTableModel.ModelVisitor<Pair<DatabaseColumn, DatabaseColumn>>()
					{
						@Override
						public void visit(CoordinatedTableModel.UpdatableTableModel<Pair<DatabaseColumn, DatabaseColumn>> model)
						{
							for (DatabaseColumn col : factDimensionKeyRecord.dimensionKey().columns())
							{
								model.addElement(new MutablePair<DatabaseColumn, DatabaseColumn>(col, null));
							}
						}
					});
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public <F> F getModel(int id)
	{
		switch (id)
		{
			case MOD_ROLE_NAME:
				return (F) roleName.getText();
			case MOD_PHYSICAL_NAME:
				return (F) physicalName.getText();
			case MOD_DIMENSION_KEY:
				return (F) state().model().dimensionKey();
			case MOD_DIMENSION_KEY_QUALIFIED_NAME:
				return (F) (state().model().dimensionKey().dimension().qualifiedName() + "." + state().model().dimensionKey().logicalName());
			case MOD_FACT:
				return (F) state().model().fact();
			case MOD_BUNDLE:
				FactDimensionKeyRecord fdkr = state().model().cloneMe();

				if (!roleName.isVisible())
				{
					fdkr.roleName(null);
				}
				return (F) fdkr;
		}

		throw new IllegalArgumentException();
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SET_ROLE_NAME)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						roleName.setText(String.valueOf(value));
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SET_PHYSICAL_NAME)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						physicalName.setText(String.valueOf(value));
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_MAP_COLUMN)
		{
			try
			{
				final Pair<DatabaseColumn, DatabaseColumn> inpair = (Pair<DatabaseColumn, DatabaseColumn>) value;

				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						// locate the model row
						java.util.List<Pair<DatabaseColumn, DatabaseColumn>> tableData = columnsTableModel.tableData();
						for (Pair<DatabaseColumn, DatabaseColumn> dc : tableData)
						{
							if (dc.getLeft() == inpair.getLeft())
							{
								columnsTableModel.setValueAt(inpair.getRight(), tableData.indexOf(dc), 2);
							}
						}
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_ROLE_NAME_VISIBLE:
				return roleName.isVisible() || roleNameLabel.isVisible();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}
}
