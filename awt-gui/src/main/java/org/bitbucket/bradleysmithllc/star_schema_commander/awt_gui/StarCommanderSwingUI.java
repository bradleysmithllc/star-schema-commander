package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.JFrameUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class StarCommanderSwingUI
{
	private static StarCommanderUIView starCommanderUIView;
	private static JFrameUIView frameUIView;

	public static void launch(String absolutePath) throws Exception
	{
		final Profile profile;

		File pd = new File(absolutePath);

		if (pd.exists())
		{
			profile = new Profile(pd);
		}
		else
		{
			profile = Profile.create(pd);
		}

		// set up the hub
		UIHub.init(new SystemUIController());

		UIHub.profile(profile);
		UIHub.selectReportingArea(profile.defaultReportingArea());

		// add an update listener to refresh the UI with profile changes
		UIHub.profile().addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				try
				{
					EventQueue.invokeAndWait(new Runnable()
					{
						@Override
						public void run()
						{
							UIHub.profileRefresh();

							// reselect reporting area if already working with one
							ReportingArea ra = UIHub.profile().defaultReportingArea();

							if (UIHub.selectedReportingArea() != null)
							{
								ReportingArea reportingArea = UIHub.profile().defaultReportingArea().reportingAreas().get(UIHub.selectedReportingArea().qualifiedName());

								if (reportingArea != null)
								{
									ra = reportingArea;
								}
							}

							UIHub.get().set("selectedReportingArea", ra);

							// send notification to listeners
							new Thread(new Runnable()
							{
								@Override
								public void run()
								{
									UIHub.uiRefresh();
								}
							}).start();
						}
					});
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				catch (InvocationTargetException e)
				{
					e.printStackTrace();
				}
			}

			@Override
			public void profileInitialized()
			{
			}
		});

		UIHub.selectReportingArea(UIHub.profile().defaultReportingArea());

		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				starCommanderUIView = new StarCommanderUIView();

				UIContainerImpl container = new UIContainerImpl();

				frameUIView = new JFrameUIView(starCommanderUIView, "MainUI");

				container.addView(frameUIView);

				frameUIView.frame().setTitle("Star(schema) Commander");

				frameUIView.frame().pack();
				frameUIView.frame().setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frameUIView.frame().setLocationRelativeTo(null);

				frameUIView.frame().setVisible(true);
			}
		});
	}
}
