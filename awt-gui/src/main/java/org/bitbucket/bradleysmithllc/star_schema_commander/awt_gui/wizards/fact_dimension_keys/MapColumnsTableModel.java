package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;

public class MapColumnsTableModel extends CoordinatedTableModel<Pair<DatabaseColumn, DatabaseColumn>>
{
	@Override
	protected int getCoordinatedColumnCount()
	{
		return 3;
	}

	@Override
	protected String getCoordinatedColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Key Column";
			case 1:
				return "Key Type";
			case 2:
				return "Foreign Key Column";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected boolean isCoordinatedCellEditable(int row, int column)
	{
		return column == 2;
	}

	@Override
	protected void setCoordinatedValueAt(Pair<DatabaseColumn, DatabaseColumn> pair, Object aValue, int row, int column)
	{
		if (column == 2)
		{
			// value is a database column
			((MutablePair<DatabaseColumn, DatabaseColumn>) pair).setRight((DatabaseColumn) aValue);
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	protected Class<?> getCoordinatedColumnClass(int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
				return String.class;
			case 1:
				return String.class;
			case 2:
				return DatabaseColumn.class;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected Object getCoordinatedValueAt(Pair<DatabaseColumn, DatabaseColumn> pair, int row, int column)
	{
		switch (column)
		{
			case 0:
				return pair.getLeft().name();
			case 1:
				return pair.getLeft().jdbcTypeName();
			case 2:
				return pair.getRight();
			default:
				throw new IllegalArgumentException();
		}
	}
}