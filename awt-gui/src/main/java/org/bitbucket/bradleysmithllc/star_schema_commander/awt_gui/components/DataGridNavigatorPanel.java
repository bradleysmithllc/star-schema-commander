package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class DataGridNavigatorPanel extends AbstractUIComponent implements DataGridNavigator
{
	public static final int MODEL_DIMENSIONS = 1;
	public static final int MODEL_FACT_MEASURES = 0;

	public static final String MSG_APPLY = "MSG_APPLY";
	public static final String MSG_SET_ALIAS = "MSG_SET_ALIAS";
	private final NavigatorClient client;
	private final Fact exploring;
	DataGridNavigatorOptionPanel<FactMeasure, DataGridNavigator.IncludedMeasure> factMeasureOptionPanel;
	DataGridNavigatorOptionPanel<DimensionAttribute, DataGridNavigator.IncludedAttribute> dimensionAttributeOptionPanel;
	private JPanel attributesPanel;
	private JPanel optionsPanel;
	private JPanel rootPane;
	private JButton apply;

	public DataGridNavigatorPanel(NavigatorClient client, Fact exploring)
	{
		this.client = client;
		this.exploring = exploring;
	}

	@Override
	public List<IncludedMeasure> includeMeasures()
	{
		return factMeasureOptionPanel.selectedMeasures();
	}

	@Override
	public List<IncludedAttribute> includeAttributes()
	{
		return dimensionAttributeOptionPanel.selectedMeasures();
	}

	public void applyQuery(PersistedQuery query)
	{
		// given the supplied query, update the navigator to match and apply the result
		// TODO: Update criteria
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				// begin by blanking each one out
				factMeasureOptionPanel.dispatchActionAndWait(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_INCLUDED);
				factMeasureOptionPanel.dispatchActionAndWait(DataGridNavigatorOptionPanel.MSG_REMOVE_INCLUDED_SELECTED);

				dimensionAttributeOptionPanel.dispatchActionAndWait(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_INCLUDED);
				dimensionAttributeOptionPanel.dispatchActionAndWait(DataGridNavigatorOptionPanel.MSG_REMOVE_INCLUDED_SELECTED);
			}
		}).start();
	}

	@Override
	public void subInitialize() throws Exception
	{
		factMeasureOptionPanel = new DataGridNavigatorOptionPanel<FactMeasure, IncludedMeasure>(exploring, new FactMeasureOptionPanelInnerds());
		dimensionAttributeOptionPanel = new DataGridNavigatorOptionPanel<DimensionAttribute, IncludedAttribute>(exploring, new DimensionAttributeOptionPanelInnerds());

		rootPane = new JPanel();

		rootPane.setLayout(new BorderLayout());
		apply = new JButton("Apply");
		rootPane.add(apply, BorderLayout.SOUTH);

		apply.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				client.update(DataGridNavigatorPanel.this);
			}
		});

		optionsPanel = new JPanel();
		optionsPanel.setLayout(new GridLayout(2, 0));

		rootPane.add(optionsPanel, BorderLayout.CENTER);

		addChild(factMeasureOptionPanel);
		addChild(dimensionAttributeOptionPanel);
	}

	@Override
	protected void finishBuildParent(Profile profile) throws Exception
	{
		optionsPanel.add(factMeasureOptionPanel.getComponent());
		optionsPanel.add(dimensionAttributeOptionPanel.getComponent());
	}

	@Override
	public JComponent getComponent()
	{
		return rootPane;
	}

	@Override
	public boolean checkState(int value)
	{
		return super.checkState(value);
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MODEL_FACT_MEASURES:
				return (T) includeMeasures();
			case MODEL_DIMENSIONS:
				return (T) includeAttributes();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_APPLY)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						apply.doClick();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SET_ALIAS)
		{
			try
			{
				if (true)
				{
					throw new IllegalArgumentException();
				}
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						Pair<FactMeasure, String> p = (Pair<FactMeasure, String>) value;
						String value1 = p.getValue();
						FactMeasure fm = p.getKey();

						// find the row for this fact measure
						//int row = factMeasureTableModel.locateMeasure(fm);

						//factMeasureTableModel.setValueAt(value1, row, 3);
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	public interface NavigatorClient
	{
		void update(DataGridNavigator navigator);
	}
}
