package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.StateTransitionHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;

public class WizardStateTransitionBuilder<F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>>
{
	private final WizardStateModelImpl model;
	private String id;
	private String logicalName;
	private StateTransitionHandler<F, C, T, D> handler;
	private WizardState<F, C> from;
	private WizardState<T, D> to;
	private boolean enabled = true;
	private boolean active = true;

	public WizardStateTransitionBuilder(WizardState<F, C> from, WizardState<T, D> to, WizardStateModelImpl model)
	{
		this.from = from;
		this.to = to;
		this.model = model;
	}

	public WizardStateTransition<F, C, T, D> create()
	{
		if (id == null)
		{
			throw new IllegalStateException("Id is required");
		}

		if (from == null)
		{
			throw new IllegalStateException("Transition requires a 'from' state");
		}

		// check existing transition IDs
		if (from.transitionMap().containsKey(id))
		{
			throw new IllegalStateException("Id [" + id + "] has already been used on state [" + from.id() + "]");
		}

		if (to == null)
		{
			throw new IllegalStateException("Transition requires a 'to' state");
		}

		WizardStateTransitionImpl<F, C, T, D> ftWizardStateTransition = new WizardStateTransitionImpl<F, C, T, D>(
			id,
			ObjectUtils.firstNonNull(logicalName, id),
			from,
			to,
			handler,
			model
		);

		// set defaults for enabled and active
		ftWizardStateTransition.enable(enabled);
		ftWizardStateTransition.activate(active);

		((WizardStateImpl<F, C>) from).addTransition(ftWizardStateTransition);

		return ftWizardStateTransition;
	}

	public WizardStateTransitionBuilder<F, C, T, D> id(String id)
	{
		this.id = id;
		return this;
	}

	public WizardStateTransitionBuilder<F, C, T, D> enabled(boolean st)
	{
		enabled = st;
		return this;
	}

	public WizardStateTransitionBuilder<F, C, T, D> active(boolean st)
	{
		active = st;
		return this;
	}

	public WizardStateTransitionBuilder<F, C, T, D> logicalName(String ln)
	{
		this.logicalName = ln;
		return this;
	}

	public WizardStateTransitionBuilder<F, C, T, D> handler(StateTransitionHandler<F, C, T, D> handler)
	{
		this.handler = handler;
		return this;
	}
}
