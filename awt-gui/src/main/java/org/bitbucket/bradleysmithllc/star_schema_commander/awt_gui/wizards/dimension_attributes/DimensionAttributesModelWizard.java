package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_attributes;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.ColumnProcessorModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.WizardColumnHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;

public class DimensionAttributesModelWizard extends ColumnProcessorModelWizard<Dimension>
{
	public DimensionAttributesModelWizard(Dimension dimension)
	{
		super(dimension, new WizardColumnHandler<Dimension>()
		{
			@Override
			public void beginHandling(Dimension dimension)
			{
			}

			@Override
			public void handle(Dimension dimension, String columnName, String logicalName)
			{
				dimension.newDimensionAttribute().column(columnName).logicalName(logicalName).create();
			}

			@Override
			public void endHandling(Dimension dimension)
			{
			}

			@Override
			public boolean include(Dimension fact, DatabaseColumn value)
			{
				return true;
			}
		});
	}
}
