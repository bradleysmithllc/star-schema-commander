package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;

public class UIHub
{
	private static UIHub hub;

	//private static final ThreadLocal<UIHub> threadLocal = new ThreadLocal<UIHub>();
	private final Map<String, CountDownLatch> systemLatches = new HashMap<>();
	private final UIController systemController;
	private final List<UINode> nodeList = new ArrayList<UINode>();

	private final List<UIController> controllerList = new ArrayList<UIController>();

	private final Map data = new HashMap();

	public UIHub(UIController systemController)
	{
		this.systemController = systemController;
	}

	public static void init(UIController controller)
	{
		hub = new UIHub(controller);
		//threadLocal.set(new UIHub());
	}

	public static UIHub get()
	{
		//return threadLocal.get();
		return hub;
	}

	public static ReportingArea selectedReportingArea()
	{
		UIHub uiHub = UIHub.get();
		ReportingArea area = uiHub.get("selectedReportingArea");

		if (area == null)
		{
			area = profile().defaultReportingArea();
		}

		return area;
	}

	public static void selectReportingArea(ReportingArea fact)
	{
		// select new reporting area, and blank out current fact and dimension
		UIHub.get().set("selectedReportingArea", fact);
		selectFact(null);
		selectDimension(null);
	}

	public static Fact selectedFact()
	{
		return UIHub.get().get("selectedFact");
	}

	public static void selectFact(Fact fact)
	{
		UIHub.get().set("selectedFact", fact);
	}

	public static void selectDimension(Dimension dim)
	{
		UIHub.get().set("selectedDimension", dim);
	}

	public static Dimension selectedDimension()
	{
		return UIHub.get().get("selectedDimension");
	}

	public static void profile(Profile profile)
	{
		// add a profile listener to broadcast change events
		UIHub.get().set("profile", profile);

		try
		{
			profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
			{
				@Override
				public void profileChanged()
				{
					profileRefresh();
				}

				@Override
				public void profileInitialized()
				{
					profileRefresh();
				}
			});
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static Profile profile()
	{
		return UIHub.get().get("profile");
	}

	// broadcast a profile refresh event
	public static void profileRefresh()
	{
		get().set("profileRefresh", profile());
	}

	// broadcast a UI refreshed event
	public static void uiRefresh()
	{
		get().set("uiRefresh", profile());
	}

	public static void selectConnection(DatabaseConnection selectedValue)
	{
		get().set("selectedConnection", selectedValue);
	}

	public static DatabaseConnection selectedConnection()
	{
		return get().get("selectedConnection");
	}

	public void registerSystemLatch(UIController aClass, String id, CountDownLatch latch)
	{
		systemLatches.put(aClass.controllerId() + "_" + id, latch);
	}

	public CountDownLatch retrieveSystemLatch(Class<? extends UIController> aClass, String id)
	{
		String kid = aClass.getSimpleName() + "_" + id;

		if (!systemLatches.containsKey(kid))
		{
			throw new IllegalArgumentException("Latch key {" + kid + "} does not exist.");
		}

		return systemLatches.get(kid);
	}

	public void releaseOptionalSystemLatch(Class<? extends UIController> aClass, String id)
	{
		String kid = aClass.getSimpleName() + "_" + id;

		CountDownLatch sl = systemLatches.get(kid);

		if (sl != null)
		{
			sl.countDown();
		}
	}

	public UIController getSystemController()
	{
		return systemController;
	}

	public <T> T get(String key)
	{
		return (T) data.get(key);
	}

	public void set(String key, Object val)
	{
		data.put(key, val);

		// broadcast updates to all registered components
		//if (val != prev)
		//{
		broadcast(nodeList, controllerList, key, val);
		//}
	}

	public void addUIController(UIController controller)
	{
		controllerList.add(controller);
	}

	private void broadcast(List<UINode> nodeList, List<UIController> controllerList, String key, Object val)
	{
		// dispatch to a copy of the nodeList to avoid comodification
		for (UINode node : nodeList.toArray(new UINode[nodeList.size()]))
		{
			// check if this is broadcast-worthy
			try
			{
				node.owner().stateChanged(key, val);

				broadcast(node.children(), null, key, val);
			}
			catch (IllegalArgumentException exc)
			{
				System.out.println("Logging special property: " + key);
			}
		}

		if (controllerList != null)
		{
			for (UIController controller : controllerList.toArray(new UIController[controllerList.size()]))
			{
				controller.stateChanged(key, val);
			}
		}
	}

	public void removeComponentTree(UIView comp)
	{
		nodeList.remove(locateNode(nodeList, comp.viewClass(), comp.viewInstance()));
	}

	public void removeComponentSubTree(UIView comp)
	{
		UINode uiNode = locateNode(nodeList, comp.viewClass(), comp.viewInstance());

		if (uiNode != null)
		{
			uiNode.clearChildren();
		}
	}

	public void addComponent(UIView comp)
	{
		if (comp.viewInstance() == null)
		{
			throw new IllegalArgumentException("View instance may not be null");
		}

		for (UINode node : nodeList)
		{
			if (node.owner().getClass() == comp.getClass() && node.owner().viewInstance().equals(comp.viewInstance()))
			{
				throw new IllegalArgumentException("Component already added {" + comp.getClass() + "." + comp.viewInstance() + "}");
			}
		}

		nodeList.add(new UINode(comp));
	}

	/**
	 * Add child as a new node under parent
	 *
	 * @param child
	 * @param parent
	 */
	public void addComponent(UIView child, UIView parent)
	{
		if (parent.viewInstance() == null)
		{
			throw new IllegalArgumentException("Parent view instance may not be null");
		}

		if (child.viewInstance() == null)
		{
			throw new IllegalArgumentException("Child view instance may not be null");
		}

		UINode node = locateNode(nodeList, parent.viewClass(), parent.viewInstance());

		if (node == null)
		{
			throw new IllegalArgumentException("Parent not registered.");
		}

		node.addChild(child);
	}

	private UINode locateNode(List<UINode> children, String componentType, String componentInstance)
	{
		for (UINode uin : children)
		{
			UIView owner = uin.owner();
			String ownerClass = owner.viewClass();
			String ownerComponentInstance = owner.viewInstance();

			if (ownerClass.equals(componentType) && ownerComponentInstance.equals(componentInstance))
			{
				return uin;
			}
			else
			{
				UINode ui = locateNode(uin.children(), componentType, componentInstance);

				if (ui != null)
				{
					return ui;
				}
			}
		}

		return null;
	}

	/**
	 * Locates a ui component in this tree by type, and optionally, id.
	 *
	 * @param instance the instance, if required.  Optional.
	 * @return
	 */
	public <T extends UIView> T locate(String clType, String instance)
	{
		UINode uin = locateNode(nodeList, clType, instance);

		if (uin == null)
		{
			throw new IllegalArgumentException();
		}

		return (T) uin.owner();
	}

	public <T extends UIView> T locate(String clType)
	{
		return locate(clType, clType);
	}

	public void clearChildren(UIView abstractUIComponent)
	{
		UINode uin = locateNode(nodeList, abstractUIComponent.viewClass(), abstractUIComponent.viewInstance());

		if (uin == null)
		{
			throw new IllegalArgumentException();
		}

		uin.clearChildren();
	}

	public List<UIView> children(UIView abstractUIComponent)
	{
		UINode uin = locateNode(nodeList, abstractUIComponent.viewClass(), abstractUIComponent.viewInstance());

		if (uin == null)
		{
			throw new IllegalArgumentException();
		}
		else
		{
			List<UIView> ch = new ArrayList<UIView>();

			for (UINode node : uin.children())
			{
				ch.add(node.owner());
			}

			return ch;
		}
	}

	public synchronized void persistProfile()
	{
		try
		{
			profile().persist();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}

class UINode
{
	private final UIView owner;
	private List<UINode> children;

	UINode(UIView owner)
	{
		this.owner = owner;
	}

	public UIView owner()
	{
		return owner;
	}

	public List<UINode> children()
	{
		if (children == null)
		{
			return Collections.emptyList();
		}
		else
		{
			return children;
		}
	}

	public void addChild(UIView comp)
	{
		if (children == null)
		{
			children = new ArrayList<UINode>();
		}

		children.add(new UINode(comp));
	}

	public void clearChildren()
	{
		if (children != null)
		{
			children.clear();
		}
	}
}
