package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.jgoodies.forms.builder.FormBuilder;
import com.jgoodies.forms.factories.Paddings;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.HANAConnectionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class HANAConnectionUIComponent extends WizardStateUIComponent<HANAConnectionBuilder>
{
	public static final String MSG_SET_SERVER_NAME = "MSG_SET_SERVER_NAME";
	public static final String MSG_SET_INSTANCE_NUMBER = "MSG_SET_INSTANCE_NUMBER";
	public static final String MSG_SET_USER_NAME = "MSG_SET_USER_NAME";
	public static final String MSG_SET_PASSWORD = "MSG_SET_PASSWORD";

	private JPanel form;

	private JTextField serverNameField;
	private JTextField instanceNumberField;
	private JTextField userNameField;
	private JTextField passwordField;
	private WizardStateTransition<HANAConnectionBuilder, ? extends WizardStateUIComponent<HANAConnectionBuilder>, ?, ?> confirmTransition;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		confirmTransition = state().transitionMap().get("confirm");
		confirmTransition.enable(false);

		serverNameField = new JTextField();
		serverNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkServerNameField();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkServerNameField();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkServerNameField();
			}
		});

		serverNameField.setColumns(15);

		instanceNumberField = new JTextField();
		instanceNumberField.setColumns(15);

		PlainDocument doc = (PlainDocument) instanceNumberField.getDocument();
		doc.setDocumentFilter(new IntegerDocumentFilter());

		instanceNumberField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				set(state().model(), instanceNumberField.getText());
			}

			private void set(HANAConnectionBuilder model, String text)
			{
				if (text.trim().equals(""))
				{
					model.instanceNumber(0);
				}
				else
				{
					model.instanceNumber(Integer.valueOf(text));
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				set(state().model(), instanceNumberField.getText());
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				set(state().model(), instanceNumberField.getText());
			}
		});

		userNameField = new JTextField();
		userNameField.setColumns(15);
		userNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkUserAndPassword();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkUserAndPassword();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkUserAndPassword();
			}
		});
		passwordField = new JTextField();
		passwordField.setColumns(15);
		passwordField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkUserAndPassword();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkUserAndPassword();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkUserAndPassword();
			}
		});

		form = FormBuilder.create()
			.columns("pref, $lcgap, pref, 21dlu, pref, $lcgap, pref")
			.rows("p, $lg, p, $lg, p, $lg, p, 9dlu, p, $lg, p, $lg, p")

				// Specify that columns 1 & 5 as well as 3 & 7 have equal widths.
			.columnGroups(new int[]{
				1,
				5
			}, new int[]{
				3,
				7
			})

				// Wrap the panel with a standardized whitespace.
			.padding(Paddings.DIALOG)

				// Fill the grid with components; the builder offers to create
				// frequently used components, e.g. separators and labels.

				// Add a titled separator to cell (1, 1) that spans 7 columns.
			.addSeparator("General").xyw(1, 1, 7)
			.add("&Server Name:").xy(1, 3)
			.add(serverNameField).xyw(3, 3, 5)
			.add("&Instance Number:").xy(1, 5)
			.add(instanceNumberField).xyw(3, 5, 5)

			.addSeparator("Authentication").xyw(1, 9, 7)
			.add("&User Name:").xy(1, 11)
			.add(userNameField).xy(3, 11)
			.add("&Password:").xy(1, 13)
			.add(passwordField).xy(3, 13)
			.build();
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SET_SERVER_NAME)
		{
			setField(serverNameField, value);
		}
		else if (message == MSG_SET_INSTANCE_NUMBER)
		{
			setField(instanceNumberField, value);
		}
		else if (message == MSG_SET_USER_NAME)
		{
			setField(userNameField, value);
		}
		else if (message == MSG_SET_PASSWORD)
		{
			setField(passwordField, value);
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	private void setField(final JTextField field, final Object value)
	{
		try
		{
			EventQueue.invokeAndWait(new Runnable()
			{
				@Override
				public void run()
				{
					field.setText(String.valueOf(value));
				}
			});
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
	}

	private void checkServerNameField()
	{
		checkField(serverNameField.getText(), new Fielder()
		{
			@Override
			public void set(HANAConnectionBuilder model, String text)
			{
				model.serverName(text);
			}

			@Override
			public void clear(HANAConnectionBuilder model)
			{
				model.serverName("localhost");
			}
		});
	}

	private void checkField(String text, Fielder fielder)
	{
		String trim = text.trim();
		if (trim.isEmpty())
		{
			fielder.clear(state().model());
		}
		else
		{
			fielder.set(state().model(), trim);
		}
	}

	private void checkUserAndPassword()
	{
		String text = userNameField.getText();
		String user = text.trim();
		String pass = passwordField.getText().trim();

		boolean userValid = false;
		boolean passValid = false;

		if (!user.isEmpty())
		{
			userValid = true;
			state().model().username(user);
		}

		if (!pass.isEmpty())
		{
			passValid = true;
			state().model().password(pass);
		}

		confirmTransition.enable(userValid && passValid);
	}

	@Override
	public JComponent getComponent()
	{
		return form;
	}

	static interface Fielder
	{
		void set(HANAConnectionBuilder model, String text);

		void clear(HANAConnectionBuilder model);

	}
}
