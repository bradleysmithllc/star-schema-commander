package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.scene.Node;
import javafx.scene.Parent;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.ClassNameUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.FXUtils;

import java.lang.reflect.Field;
import java.net.URL;

public abstract class FXMLUIView<T> extends AbstractUIView<Node>
{
	private Parent rootNode;
	private T controller;

	@Override
	public final void subInitialize() throws Exception
	{
		String uri = "/fxml/" + ClassNameUtils.removeCapsWithDashes(viewClass()) + ".fxml";
		Class<? extends FXMLUIView> aClass = getClass();
		URL resource = aClass.getResource(uri);

		if (resource == null)
		{
			throw new IllegalStateException("View definition not found: " + uri + " for component class " + viewClass() + ", view instance " + viewInstance());
		}

		Pair<Parent, T> p = FXUtils.loadNodeGraph(resource);
		rootNode = p.getLeft();
		controller = p.getRight();

		// now that the root node is loaded,
		// go through the any declared nodes and attach to the FX tree
		processClassDeclaredFields(aClass);

		subSubInitialize();
	}

	private void processClassDeclaredFields(Class aClass) throws IllegalAccessException
	{
		for (Field field : aClass.getDeclaredFields())
		{
			UINodeReference anno = field.getAnnotation(UINodeReference.class);

			if (anno != null)
			{
				// try to resolve
				// if not found will raise an exception
				// This used to be a node, but since not everything
				// in JFX is a node it can't be done (Menu, MenuItem).
				Object node = lookupById(anno.id());

				// try to assign to field.  Again,
				// if this does not succeed an exception will
				// be raised
				field.setAccessible(true);
				field.set(this, node);
			}
		}

		Class superclass = aClass.getSuperclass();

		if (superclass != null)
		{
			processClassDeclaredFields(superclass);
		}
	}

	protected void subSubInitialize()
	{
	}

	@Override
	public final Node view()
	{
		return rootNode;
	}

	protected <T> T lookupById(String id)
	{
		Node vRoot = view();

		T n = FXUtils.getChildByID(vRoot, id);

		if (n == null)
		{
			throw new IllegalArgumentException("Node with ID {" + id + "} not found.");
		}

		return n;
	}

	public void stateChanged(stateKey key, Object value)
	{
	}

	@Override
	public void stateChanged(String key, Object value)
	{
		try
		{
			stateChanged(stateKey.valueOf(key), value);
		}
		catch (IllegalArgumentException exc)
		{
			// not in the enum
		}
	}

	protected void runOnEventThread(Runnable runnable)
	{
		EventUtils.runOnJFXEventThread(runnable);
	}


	public enum stateKey
	{
		selectedFact,
		selectedReportingArea,
		profile,
		profileRefresh,
		selectedDimension,
		selectedConnection
	}
}
