package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class ReferenceMap
{
	private final Map<String, Reference> referenceMap = new HashMap<String, Reference>();

	public <T> Reference<T> get(String name)
	{
		return (Reference<T>) referenceMap.get(name);
	}

	public <T> T getValue(String name)
	{
		Reference<T> tReference = (Reference<T>) referenceMap.get(name);

		if (tReference == null)
		{
			tReference = new Reference<T>();
			referenceMap.put(name, tReference);
		}

		return tReference.getReference();
	}

	public void set(String name, Reference value)
	{
		referenceMap.put(name, value);
	}

	public <T> void setValue(String name, T value)
	{
		if (!referenceMap.containsKey(name))
		{
			set(name, new Reference<T>());
		}

		((Reference<T>) referenceMap.get(name)).setReference(value);
	}

	public void clearValue(String currentDimensionIndex)
	{
		referenceMap.remove(currentDimensionIndex);
	}

	public boolean containsKey(String currentDimensionIndex)
	{
		return referenceMap.containsKey(currentDimensionIndex);
	}
}
