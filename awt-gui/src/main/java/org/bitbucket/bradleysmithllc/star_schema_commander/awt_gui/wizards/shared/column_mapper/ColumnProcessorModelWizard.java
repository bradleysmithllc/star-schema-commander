package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardModelBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.NullUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ColumnStore;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ColumnProcessorModelWizard<T extends ColumnStore> extends UIWizard
{
	public ColumnProcessorModelWizard(T fact, WizardColumnHandler<T> handler)
	{
		super(createModel(fact, handler));
	}

	private static <G extends ColumnStore> WizardStateModel createModel(final G facti, final WizardColumnHandler<G> handler)
	{
		final Map<String, String> colnames = new TreeMap<String, String>();

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		WizardState<List<DatabaseColumn>, SelectColumnsUIComponent<G>> selectColumns =
			wizardModelBuilder.newState((List<DatabaseColumn>) new ArrayList<DatabaseColumn>(), new SelectColumnsUIComponent<G>(facti, handler)).id("SelectColumns").create();

		final WizardState<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> processDims = wizardModelBuilder.newState(new Reference<ColumnsSpec<G>>(new ColumnsSpec<G>()), new ProcessColumnsUIComponent<G>()).id("ProcessColumns").create();
		WizardState<Object, NullUIComponent> done = wizardModelBuilder.newState(new Object(), new NullUIComponent()).id("Done").create();

		wizardModelBuilder.newTransition(selectColumns, processDims).id("logical").logicalName("Choose Logical Names").handler(new StateTransitionHandler<List<DatabaseColumn>, SelectColumnsUIComponent<G>, Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>>()
		{
			@Override
			public void transition(WizardState<List<DatabaseColumn>, SelectColumnsUIComponent<G>> from, WizardState<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> to, WizardStateTransition<List<DatabaseColumn>, SelectColumnsUIComponent<G>, Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> transition)
			{
				to.model().getReference().columns().addAll(from.model());

				to.model().getReference().mapping(facti);
			}
		}).create();

		wizardModelBuilder.newTransition(processDims, done).id("close").logicalName("Close").handler(new StateTransitionHandler<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>, Object, NullUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> from, WizardState<Object, NullUIComponent> to, WizardStateTransition<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>, Object, NullUIComponent> transition)
			{
			}
		}).create();

		wizardModelBuilder.newTransition(processDims, done).id("process").logicalName("Process").handler(new StateTransitionHandler<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>, Object, NullUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> from, WizardState<Object, NullUIComponent> to, WizardStateTransition<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>, Object, NullUIComponent> transition)
			{
				DatabaseColumn dbc = from.component().getModel(ProcessColumnsUIComponent.MOD_DBC);

				String logicalName = from.component().getModel(ProcessColumnsUIComponent.MOD_LOGICAL_NAME);

				colnames.put(dbc.name(), logicalName);

				from.model().getReference().next();

				// iterate through the rest of the columns and map
				ColumnsSpec<G> spec = from.model().getReference();

				for (int i = spec.index(); i < spec.columns().size(); i++)
				{
					DatabaseColumn ctm = spec.columns().get(i);
					colnames.put(ctm.name(), ctm.name());
				}

				processNames(colnames, facti, handler);
			}
		}).create();

		wizardModelBuilder.newTransition(processDims, processDims).id("next").logicalName("Next").handler(new StateTransitionHandler<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>, Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>>()
		{
			@Override
			public void transition(WizardState<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> from, WizardState<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> to, WizardStateTransition<Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>, Reference<ColumnsSpec<G>>, ProcessColumnsUIComponent<G>> transition)
			{
				DatabaseColumn dbc = from.component().getModel(ProcessColumnsUIComponent.MOD_DBC);

				String logicalName = from.component().getModel(ProcessColumnsUIComponent.MOD_LOGICAL_NAME);

				colnames.put(dbc.name(), logicalName);

				to.model().getReference().next();
			}
		}).create();

		wizardModelBuilder.initial(selectColumns);

		return wizardModelBuilder.create();
	}

	private static <G extends ColumnStore> void processNames(Map<String, String> colnames, G fact, WizardColumnHandler<G> handler)
	{
		handler.beginHandling(fact);
		for (Map.Entry<String, String> entry : colnames.entrySet())
		{
			handler.handle(fact, entry.getKey(), entry.getValue());
		}
		handler.endHandling(fact);


		try
		{
			UIHub.profile().persist();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
