package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.UIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;

import javax.swing.*;

public abstract class UIComponentAdapter extends AbstractUIView<JComponent> implements UIComponent
{
	@Override
	public void subInitialize() throws Exception
	{
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					build(UIHub.profile());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void activate()
	{
		// no corelation
	}

	@Override
	public void prepareSelf() throws Exception
	{
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
	}

	@Override
	public JComponent view()
	{
		return getComponent();
	}

	@Override
	public void deactivate()
	{
	}
}