package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class WizardStateModelImpl implements WizardStateModel
{
	private WizardState initial;
	private WizardStateHandler handler;

	private Map<String, WizardState> stateMap = new HashMap<String, WizardState>();

	void privateInit(WizardState initial, WizardStateHandler handler)
	{
		this.initial = initial;
		this.handler = handler;
	}

	void addState(WizardState<?, ?> state)
	{
		stateMap.put(state.id(), state);
	}

	@Override
	public WizardState initial()
	{
		return initial;
	}

	@Override
	public WizardStateHandler handler()
	{
		return handler;
	}

	@Override
	public Map<String, WizardState> states()
	{
		return Collections.unmodifiableMap(stateMap);
	}

	<F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>> void transition(WizardState<F, C> from, WizardState<T, D> to, WizardStateTransition<F, C, T, D> transition)
	{
		if (handler != null)
		{
			handler.stateChanged(from, to, transition);
		}
	}
}