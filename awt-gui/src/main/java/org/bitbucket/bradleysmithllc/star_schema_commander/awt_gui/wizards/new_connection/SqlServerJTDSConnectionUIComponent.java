package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.jgoodies.forms.builder.FormBuilder;
import com.jgoodies.forms.factories.Paddings;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.CountDownLatch;

public class SqlServerJTDSConnectionUIComponent extends WizardStateUIComponent<SqlConnectionBuilderWrapper>
{
	public static final String MSG_SET_SERVER_NAME = "MSG_SET_SERVER_NAME";
	public static final String MSG_SET_DATABASE_NAME = "MSG_SET_DATABASE_NAME";
	public static final String MSG_SET_INSTANCE_NAME = "MSG_SET_INSTANCE_NAME";
	public static final String MSG_SET_PORT_NUMBER = "MSG_SET_PORT_NUMBER";
	public static final String MSG_SET_DOMAIN_NAME = "MSG_SET_DOMAIN_NAME";
	public static final String MSG_SET_USER_NAME = "MSG_SET_USER_NAME";
	public static final String MSG_SET_PASSWORD = "MSG_SET_PASSWORD";

	public static final int STATE_PASSWORD_ENABLED = 0;

	private JPanel form;
	private JTextField serverNameField;
	private JTextField instanceNameField;
	private JTextField domainNameField;
	private JTextField portNumberField;
	private JTextField userNameField;
	private JTextField passwordField;
	private JTextField databaseNameField;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		state().model().serverName("localhost");

		serverNameField = new JTextField();
		serverNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkServerNameField();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkServerNameField();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkServerNameField();
			}
		});

		serverNameField.setColumns(15);

		instanceNameField = new JTextField();
		instanceNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkInstanceName();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkInstanceName();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkInstanceName();
			}
		});

		if (state().model().usesDomain())
		{
			domainNameField = new JTextField();
			domainNameField.setColumns(15);
			domainNameField.getDocument().addDocumentListener(new DocumentListener()
			{
				@Override
				public void insertUpdate(DocumentEvent e)
				{
					checkDomainName();
				}

				@Override
				public void removeUpdate(DocumentEvent e)
				{
					checkDomainName();
				}

				@Override
				public void changedUpdate(DocumentEvent e)
				{
					checkDomainName();
				}
			});
		}

		portNumberField = new JTextField();
		portNumberField.setColumns(15);

		PlainDocument doc = (PlainDocument) portNumberField.getDocument();
		doc.setDocumentFilter(new IntegerDocumentFilter());

		portNumberField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				set(state().model(), portNumberField.getText());
			}

			private void set(SqlConnectionBuilderWrapper model, String text)
			{
				if (text.trim().equals(""))
				{
					model.port(1433);
				}
				else
				{
					model.port(Integer.valueOf(text));
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				set(state().model(), portNumberField.getText());
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				set(state().model(), portNumberField.getText());
			}
		});

		userNameField = new JTextField();
		userNameField.setColumns(15);
		userNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkUsername();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkUsername();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkUsername();
			}
		});
		passwordField = new JTextField();
		passwordField.setColumns(15);
		passwordField.setEnabled(false);
		passwordField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkPassword();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkPassword();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkPassword();
			}
		});
		databaseNameField = new JTextField();
		databaseNameField.setColumns(15);
		databaseNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkDatabaseName();
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkDatabaseName();
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkDatabaseName();
			}
		});

		form = FormBuilder.create()
			.columns("pref, $lcgap, pref, 21dlu, pref, $lcgap, pref")
			.rows("p, $lg, p, $lg, p, $lg, p, $lg, p, 9dlu, p, $lg, p, $lg, p, $lg, p")

				// Specify that columns 1 & 5 as well as 3 & 7 have equal widths.
			.columnGroups(new int[]{
				1,
				5
			}, new int[]{
				3,
				7
			})

				// Wrap the panel with a standardized whitespace.
			.padding(Paddings.DIALOG)

				// Fill the grid with components; the builder offers to create
				// frequently used components, e.g. separators and labels.

				// Add a titled separator to cell (1, 1) that spans 7 columns.
			.addSeparator("General").xyw(1, 1, 7)
			.add("&Server Name:").xy(1, 3)
			.add(serverNameField).xyw(3, 3, 5)
			.add("&Port Number:").xy(1, 5)
			.add(portNumberField).xyw(3, 5, 5)
			.add("&Database Name:").xy(1, 7)
			.add(databaseNameField).xyw(3, 7, 5)
			.add("&Instance Name:").xy(1, 9)
			.add(instanceNameField).xyw(3, 9, 5)

			.addSeparator("Authentication").xyw(1, 11, 7)

			.add("&Domain Name:").xy(1, 13)
			.add(domainNameField).xy(3, 13)

			.add("&User Name:").xy(1, 15)
			.add(userNameField).xy(3, 15)
			.add("&Password:").xy(1, 17)
			.add(passwordField).xy(3, 17)
			.build();
	}

	private void checkInstanceName()
	{
		checkField(instanceNameField.getText(), new Fielder()
		{
			@Override
			public void set(SqlConnectionBuilderWrapper model, String text)
			{
				model.instanceName(text);
			}

			@Override
			public void clear(SqlConnectionBuilderWrapper model)
			{
				model.instanceName(null);
			}
		});
	}

	private void checkDatabaseName()
	{
		checkField(databaseNameField.getText(), new Fielder()
		{
			@Override
			public void set(SqlConnectionBuilderWrapper model, String text)
			{
				model.database(text);
			}

			@Override
			public void clear(SqlConnectionBuilderWrapper model)
			{
				model.database(null);
			}
		});
	}

	private void checkDomainName()
	{
		checkField(domainNameField.getText(), new Fielder()
		{
			@Override
			public void set(SqlConnectionBuilderWrapper model, String text)
			{
				model.domain(text);
			}

			@Override
			public void clear(SqlConnectionBuilderWrapper model)
			{
				model.domain(null);
			}
		});
	}

	private void checkServerNameField()
	{
		checkField(serverNameField.getText(), new Fielder()
		{
			@Override
			public void set(SqlConnectionBuilderWrapper model, String text)
			{
				model.serverName(text);
			}

			@Override
			public void clear(SqlConnectionBuilderWrapper model)
			{
				model.serverName("localhost");
			}
		});
	}

	private void checkField(String text, Fielder fielder)
	{
		String trim = text.trim();
		if (trim.isEmpty())
		{
			fielder.clear(state().model());
		}
		else
		{
			fielder.set(state().model(), trim);
		}
	}

	private void checkPassword()
	{
		String tr = passwordField.getText().trim();

		state().model().password(tr);
	}

	private void checkUsername()
	{
		String text = userNameField.getText();

		String trim = text.trim();

		if (trim.isEmpty())
		{
			state().model().username(null);
			state().model().password(null);
			// invalidate password
			passwordField.setText("");
			passwordField.setEnabled(false);
		}
		else
		{
			state().model().username(trim);
			passwordField.setEnabled(true);
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message, final Object value)
	{
		try
		{
			if (message == MSG_SET_SERVER_NAME)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						serverNameField.setText(String.valueOf(value));
					}
				});
			}
			else if (message == MSG_SET_INSTANCE_NAME)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						instanceNameField.setText(String.valueOf(value));
					}
				});
			}
			else if (message == MSG_SET_DOMAIN_NAME)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						if (domainNameField != null)
						{
							domainNameField.setText(String.valueOf(value));
						}
					}
				});
			}
			else if (message == MSG_SET_PORT_NUMBER)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						portNumberField.setText(String.valueOf(value));
					}
				});
			}
			else if (message == MSG_SET_USER_NAME)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						userNameField.setText(String.valueOf(value));
					}
				});
			}
			else if (message == MSG_SET_PASSWORD)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						passwordField.setText(String.valueOf(value));
					}
				});
			}
			else if (message == MSG_SET_DATABASE_NAME)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						databaseNameField.setText(String.valueOf(value));
					}
				});
			}
			else
			{
				throw new IllegalArgumentException();
			}

			latch.countDown();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_PASSWORD_ENABLED:
				return passwordField.isEnabled();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return form;
	}

	interface Fielder
	{
		void set(SqlConnectionBuilderWrapper model, String text);

		void clear(SqlConnectionBuilderWrapper model);
	}
}
