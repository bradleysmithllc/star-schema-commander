package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class DatabaseColumnTableModel extends AbstractTableModel
{
	private final TreeMap<String, DatabaseColumn> tableMap = new TreeMap<String, DatabaseColumn>();
	private final List<DatabaseColumn> tableData = new ArrayList<DatabaseColumn>();

	@Override
	public int getRowCount()
	{
		return tableMap.size();
	}

	@Override
	public String getColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Name";
			case 1:
				return "Data Type";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getColumnCount()
	{
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		DatabaseColumn td = tableData.get(rowIndex);

		switch (columnIndex)
		{
			case 0:
				return td.name();
			case 1:
				return td.jdbcTypeName();
			default:
				throw new IllegalArgumentException();
		}
	}

	public void addColumn(DatabaseColumn dc)
	{
		tableData.add(dc);
		tableMap.put(dc.name(), dc);

		Collections.sort(tableData);

		fireTableDataChanged();
	}

	public int size()
	{
		return tableData.size();
	}

	public List<DatabaseColumn> tableData()
	{
		return tableData;
	}

	public boolean isEmpty()
	{
		return tableData.isEmpty();
	}
}
