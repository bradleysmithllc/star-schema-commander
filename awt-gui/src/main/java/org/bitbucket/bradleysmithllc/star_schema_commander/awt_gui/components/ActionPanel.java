package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.extended.button.WebSplitButton;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.managers.hotkey.Hotkey;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizardFrame;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_attributes.DimensionAttributesModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.DimensionKeyModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_degenerate_dimension.FactDegenerateDimensionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.FactDimensionKeysModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_measures.FactMeasuresModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_keys.ImportKeysModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.ImportTableModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class ActionPanel extends AbstractUIComponent
{
	public static final String MSG_IMPORT_DIMENSIONS = "MSG_IMPORT_DIMENSIONS";
	public static final String MSG_IMPORT_FACTS = "MSG_IMPORT_FACTS";
	public static final String MSG_FACT_MEASURE = "MSG_FACT_MEASURE";

	static final AtomicInteger explorerIndex = new AtomicInteger();

	private WebSplitButton webSplitButton;

	private WebPopupMenu newReportingAreaMenu;
	private WebMenuItem importDimensionAction;
	private WebMenuItem importFactAction;
	private WebMenuItem factMeasureAction;
	private WebMenuItem factKeyAction;
	private WebMenuItem factDegenerateDimensionAction;
	private WebMenuItem dimensionAttributesAction;
	private WebMenuItem dimensionKeyAction;
	private WebMenuItem importKeysAction;

	private String newReportingAreaName;
	private MySwingWorker swingWorker;

	@Override
	protected void buildParent(final Profile profile)
	{
		webSplitButton = new WebSplitButton("Actions");
		newReportingAreaMenu = new WebPopupMenu();

		importKeysAction = new WebMenuItem("Import Keys...", WebLookAndFeel.getIcon(16), Hotkey.ALT_I);
		importKeysAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new ImportKeysModelWizard());
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		newReportingAreaMenu.add(importKeysAction);

		importDimensionAction = new WebMenuItem("Import Dimension...", WebLookAndFeel.getIcon(16), Hotkey.ALT_D);
		importDimensionAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new ImportTableModelWizard(ImportTableModelWizard.target.dimension));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		newReportingAreaMenu.add(importDimensionAction);

		importFactAction = new WebMenuItem("Import Fact...", WebLookAndFeel.getIcon(16), Hotkey.ALT_F);
		importFactAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new ImportTableModelWizard(ImportTableModelWizard.target.fact));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		newReportingAreaMenu.add(importFactAction);

		factMeasureAction = new WebMenuItem("Define Measures...", WebLookAndFeel.getIcon(16), Hotkey.ALT_M);
		factMeasureAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new FactMeasuresModelWizard(UIHub.selectedFact()));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		factMeasureAction.setVisible(false);

		newReportingAreaMenu.add(factMeasureAction);

		factDegenerateDimensionAction = new WebMenuItem("Define Degenerate Dimensions...", WebLookAndFeel.getIcon(16), Hotkey.ALT_D);
		factDegenerateDimensionAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new FactDegenerateDimensionModelWizard(UIHub.selectedFact()));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		factDegenerateDimensionAction.setVisible(false);

		newReportingAreaMenu.add(factDegenerateDimensionAction);

		factKeyAction = new WebMenuItem("Define Fact Keys...", WebLookAndFeel.getIcon(16), Hotkey.ALT_F);
		factKeyAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new FactDimensionKeysModelWizard(UIHub.selectedFact()));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		factKeyAction.setVisible(false);

		newReportingAreaMenu.add(factKeyAction);

		dimensionAttributesAction = new WebMenuItem("Define Attributes...", WebLookAndFeel.getIcon(16), Hotkey.ALT_M);
		dimensionAttributesAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new DimensionAttributesModelWizard(UIHub.selectedDimension()));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		dimensionAttributesAction.setVisible(false);

		newReportingAreaMenu.add(dimensionAttributesAction);

		dimensionKeyAction = new WebMenuItem("Define Key...", WebLookAndFeel.getIcon(16), Hotkey.ALT_K);
		dimensionKeyAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					UIWizardFrame uiWizardFrame = new UIWizardFrame(new DimensionKeyModelWizard(UIHub.selectedDimension()));
					uiContainer.addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});
		dimensionKeyAction.setVisible(false);

		newReportingAreaMenu.add(dimensionKeyAction);

		webSplitButton.setPopupMenu(newReportingAreaMenu);
	}

	@Override
	public JComponent getComponent()
	{
		return webSplitButton;
	}

	@Override
	public void stateChanged(stateKey key, Object value)
	{
		boolean factSelected = UIHub.selectedFact() != null;
		boolean dimensionSelected = UIHub.selectedDimension() != null;
		boolean connectionSelected = UIHub.selectedConnection() != null;

		switch (key)
		{
			case selectedFact:
				factMeasureAction.setVisible(factSelected);
				factKeyAction.setVisible(factSelected);
				factDegenerateDimensionAction.setVisible(factSelected);
			case selectedDimension:
				dimensionAttributesAction.setVisible(dimensionSelected);
				dimensionKeyAction.setVisible(dimensionSelected);
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		if (message == MSG_IMPORT_DIMENSIONS)
		{
			try
			{
				runOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						importDimensionAction.doClick();
						latch.countDown();
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_IMPORT_FACTS)
		{
			try
			{
				runOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						importFactAction.doClick();
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	private class MySwingWorker extends SwingWorker
	{
		private final String raName;

		private final CountDownLatch latch = new CountDownLatch(1);
		private ReportingArea nra;

		public MySwingWorker(String raName)
		{
			this.raName = raName;
		}

		@Override
		protected Object doInBackground() throws Exception
		{
			try
			{
				nra = UIHub.selectedReportingArea().newReportingArea().name(raName).create();
				UIHub.get().persistProfile();
			}
			catch (Throwable thr)
			{
				thr.printStackTrace();
				throw new RuntimeException(thr);
			}

			return null;
		}

		@Override
		protected void done()
		{
			try
			{
				if (nra == null)
				{
					throw new RuntimeException("Null Reporting Area");
				}

				UIHub.selectReportingArea(nra);
			}
			finally
			{
				latch.countDown();
			}
		}

		public void waitFor()
		{
			try
			{
				latch.await();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}
