package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.QualifiedTable;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class QualifiedTableTableModel extends AbstractTableModel
{
	private final TreeMap<String, QualifiedTable> tableMap = new TreeMap<String, QualifiedTable>();
	private final List<QualifiedTable> tableData = new ArrayList<QualifiedTable>();

	@Override
	public int getRowCount()
	{
		return tableMap.size();
	}

	@Override
	public String getColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "catalog";
			case 1:
				return "schema";
			case 2:
				return "name";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getColumnCount()
	{
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		QualifiedTable td = tableData.get(rowIndex);

		switch (columnIndex)
		{
			case 0:
				return td.catalog();
			case 1:
				return td.schema();
			case 2:
				return td.name();
			default:
				throw new IllegalArgumentException();
		}
	}

	public boolean contains(String key)
	{
		return tableMap.containsKey(key);
	}

	public void addTable(QualifiedTable name)
	{
		tableData.add(name);
		tableMap.put(name.qualifiedName(), name);

		Collections.sort(tableData);

		fireTableDataChanged();
	}

	public int size()
	{
		return tableData.size();
	}

	public List<QualifiedTable> tableData()
	{
		return tableData;
	}

	public void removeTable(QualifiedTable table)
	{
		// don't need to sort when removing elements
		tableMap.remove(table.qualifiedName());
		tableData().remove(table);

		fireTableDataChanged();
	}

	public boolean isEmpty()
	{
		return tableData.isEmpty();
	}

	public int getRowIndex(String key)
	{
		return tableData.indexOf(tableMap.get(key));
	}
}
