package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardModelBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.ReferenceMap;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.QualifiedTable;

import java.util.List;
import java.util.Map;

public class ImportTableModelWizard extends UIWizard
{
	private final target _target;

	public ImportTableModelWizard(target t)
	{
		super(createModel(t));

		_target = t;
	}

	private static WizardStateModel createModel(target _target)
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		WizardState<Reference<DatabaseConnection>, SelectConnectionUIComponent> selectConnection =
			wizardModelBuilder.newState(new Reference<DatabaseConnection>(), new SelectConnectionUIComponent("tables")).id("SelectConnection").create();

		WizardState<ReferenceMap, SelectTablesUIComponent> selectDims = wizardModelBuilder.newState(new ReferenceMap(), new SelectTablesUIComponent()).id("SelectTables").create();

		WizardState<ReferenceMap, ProcessTablesUIComponent> processDims = wizardModelBuilder.newState(new ReferenceMap(), new ProcessTablesUIComponent()).id("ProcessTables").create();

		WizardState<ImportSpecification, ImportTablesUIComponent> importDims =
			wizardModelBuilder.newState(new ImportSpecification(), new ImportTablesUIComponent(_target)).id("ImportTables").create();

		WizardState<ImportSpecification, ImportTablesUIComponent> done = wizardModelBuilder.newState(new ImportSpecification(), new ImportTablesUIComponent(_target)).id("Done").create();

		wizardModelBuilder.newTransition(selectConnection, selectDims).id("tables").logicalName("Select Tables").handler(new StateTransitionHandler<Reference<DatabaseConnection>, SelectConnectionUIComponent, ReferenceMap, SelectTablesUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<DatabaseConnection>, SelectConnectionUIComponent> from, WizardState<ReferenceMap, SelectTablesUIComponent> to, WizardStateTransition<Reference<DatabaseConnection>, SelectConnectionUIComponent, ReferenceMap, SelectTablesUIComponent> transition)
			{
				to.model().setValue("SelectedDatabaseConnection", from.model().getReference());
			}
		}).create();

		wizardModelBuilder.newTransition(selectDims, processDims).id("process").logicalName("Process Tables").handler(new StateTransitionHandler<ReferenceMap, SelectTablesUIComponent, ReferenceMap, ProcessTablesUIComponent>()
		{
			@Override
			public void transition(WizardState<ReferenceMap, SelectTablesUIComponent> from, WizardState<ReferenceMap, ProcessTablesUIComponent> to, WizardStateTransition<ReferenceMap, SelectTablesUIComponent, ReferenceMap, ProcessTablesUIComponent> transition)
			{
				// grab the selected tables and stuff them into a list
				List<QualifiedTable> selMod = from.component().getModel(SelectTablesUIComponent.SELECTED_DIMENSIONS_EXPORT);

				// store in the reference map for both states
				from.model().setValue("TableList", selMod);
				to.model().setValue("TableList", selMod);

				// pass on the database connection
				to.model().setValue("SelectedDatabaseConnection", from.model().getValue("SelectedDatabaseConnection"));

				// make sure the process step gets started over
				to.model().setValue("currentTableIndex", new Integer(0));
			}
		}).create();

		wizardModelBuilder.newTransition(processDims, selectDims).id("back").logicalName("Back").create();
		wizardModelBuilder.newTransition(processDims, processDims).id("next").logicalName("Next").handler(new StateTransitionHandler<ReferenceMap, ProcessTablesUIComponent, ReferenceMap, ProcessTablesUIComponent>()
		{
			@Override
			public void transition(WizardState<ReferenceMap, ProcessTablesUIComponent> from, WizardState<ReferenceMap, ProcessTablesUIComponent> to, WizardStateTransition<ReferenceMap, ProcessTablesUIComponent, ReferenceMap, ProcessTablesUIComponent> transition)
			{
				ReferenceMap referenceMap = from.model();
				Reference<Integer> currInd = referenceMap.get("currentTableIndex");
				int currentIndex = currInd.getReference().intValue();

				// store the last physical / logical mapping
				to.model().setValue("[Table[" + from.component().getModel(ProcessTablesUIComponent.MOD_PHYSICAL_NAME) + "]]", from.component().getModel(ProcessTablesUIComponent.MOD_LOGICAL_NAME));

				// update the index
				// always advance forward one at a time
				currInd.setReference(new Integer(currentIndex + 1));
			}
		}).create();

		wizardModelBuilder.newTransition(processDims, importDims).id("done").logicalName("Done").handler(new StateTransitionHandler<ReferenceMap, ProcessTablesUIComponent, ImportSpecification, ImportTablesUIComponent>()
		{
			@Override
			public void transition(WizardState<ReferenceMap, ProcessTablesUIComponent> from, WizardState<ImportSpecification, ImportTablesUIComponent> to, WizardStateTransition<ReferenceMap, ProcessTablesUIComponent, ImportSpecification, ImportTablesUIComponent> transition)
			{
				from.model().setValue("[Table[" + from.component().getModel(ProcessTablesUIComponent.MOD_PHYSICAL_NAME) + "]]", from.component().getModel(ProcessTablesUIComponent.MOD_LOGICAL_NAME));
				ReferenceMap referenceMap = from.model();

				// pass on the database connection

				List<QualifiedTable> tables = referenceMap.getValue("TableList");
				DatabaseConnection dbc = from.model().getValue("SelectedDatabaseConnection");
				to.model().databaseConnection(dbc);

				Map<QualifiedTable, String> map = to.model().tableMap();

				for (QualifiedTable tbl : tables)
				{
					map.put(tbl, tbl.qualifiedName().replace('.', '_'));

					// check for a logical override
					String keyName = "[Table[" + tbl.qualifiedName() + "]]";

					if (referenceMap.containsKey(keyName))
					{
						map.put(tbl, (String) referenceMap.getValue(keyName));
					}
				}
			}
		}).create();

		wizardModelBuilder.newTransition(importDims, done).id("close").logicalName("Close").create();

		wizardModelBuilder.initial(selectConnection);

		return wizardModelBuilder.create();
	}

	public enum target
	{
		dimension,
		fact
	}
}
