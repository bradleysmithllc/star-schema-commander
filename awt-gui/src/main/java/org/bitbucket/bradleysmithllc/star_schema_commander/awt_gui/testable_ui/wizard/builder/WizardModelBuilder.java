package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;

import java.util.HashMap;
import java.util.Map;

public class WizardModelBuilder
{
	private final Map<String, WizardState> stateMap = new HashMap<String, WizardState>();
	private final WizardStateModelImpl wizardModel = new WizardStateModelImpl();
	private WizardState initial;
	private WizardStateHandler handler;

	public <T, C extends WizardStateUIComponent<T>> WizardStateBuilder<T, C> newState(T model, C view)
	{
		return new WizardStateBuilder<T, C>(wizardModel).model(model).component(view);
	}

	public <F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>> WizardStateTransitionBuilder<F, C, T, D> newTransition(WizardState<F, C> from, WizardState<T, D> to)
	{
		return new WizardStateTransitionBuilder<F, C, T, D>(from, to, wizardModel);
	}

	public WizardModelBuilder initial(WizardState state)
	{
		initial = state;
		return this;
	}

	public WizardModelBuilder handler(WizardStateHandler state)
	{
		handler = state;
		return this;
	}

	public WizardStateModel create()
	{
		if (initial == null)
		{
			throw new IllegalStateException("Initial state is required");
		}

		wizardModel.privateInit(initial, handler);

		return wizardModel;
	}
}
