package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

public class DimensionPanel extends AbstractUIComponent
{
	private final DefaultListModel<Dimension> factListModel = new DefaultListModel<Dimension>();

	private JList<Dimension> factList = new JList<Dimension>(factListModel);
	private JScrollPane jScrollPane;

	@Override
	protected void buildParent(Profile profile)
	{
		factListModel.clear();
		factList = new JList<Dimension>(factListModel);
		factList.setCellRenderer(new DimensionCellRenderer());

		factList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				publish();
			}
		});

		jScrollPane = new JScrollPane(factList);

		jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "Dimensions"));

		updateReportingArea(UIHub.selectedReportingArea());
	}

	private void publish()
	{
		Dimension ra = factList.getSelectedValue();
		UIHub.selectDimension(ra);
	}

	@Override
	public JComponent getComponent()
	{
		return jScrollPane;
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case DIMENSION_PANEL_DIMENSION_LIST:
				return (T) factListModel;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void stateChanged(stateKey key, final Object value)
	{
		switch (key)
		{
			case profileRefresh:
			case selectedReportingArea:
				try
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							updateReportingArea(UIHub.selectedReportingArea());
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
			case selectedFact:
				try
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							factList.revalidate();
							factList.repaint();
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
		}
	}

	private void updateReportingArea(ReportingArea value)
	{
		// populate the model with the children of this ra
		factListModel.removeAllElements();

		if (value != null)
		{
			for (Dimension rac : value.dimensions().values())
			{
				if (!rac.degenerate())
				{
					factListModel.add(factListModel.size(), rac);
				}
			}
		}
	}
}
