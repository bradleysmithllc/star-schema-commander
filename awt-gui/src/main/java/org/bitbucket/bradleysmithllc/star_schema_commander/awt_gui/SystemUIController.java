package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizardFrame;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.AbstractUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.JFrameUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.NewConnectionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class SystemUIController extends AbstractUIController implements SystemControllerConstants
{
	static final AtomicInteger explorerIndex = new AtomicInteger();

	@Override
	public boolean checkState(int value)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T getModel(int id)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		if (message.equals(MSG_NEW_REPORTING_AREA))
		{
			// Value is a pair, left = ReportingArea right = optional name.
			// If name is not supplied, the user must be prompted for it.
			final Pair<ReportingArea, String> pair = (Pair<ReportingArea, String>) value;

			final AtomicReference<String> nameRef = new AtomicReference<String>(pair.getRight());

			final CountDownLatch cdl = new CountDownLatch(1);

			if (pair.getRight() == null)
			{
				System.out.println("New Reporting Area function.");

				try
				{
					// do this later in case we are on the JFX event thread
					EventUtils.runOnEventThreadLater(new Runnable()
					{
						@Override
						public void run()
						{
							nameRef.set(JOptionPane.showInputDialog("Enter a Reporting Area name"));
							cdl.countDown();
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				cdl.countDown();
			}

			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						cdl.await();

						ReportingArea nra = pair.getLeft().newReportingArea().name(nameRef.get()).create();
						try
						{
							UIHub.profile().persist();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}

						// select it
						UIHub.get().selectReportingArea(nra);
					}
					catch (InterruptedException e)
					{
						throw new RuntimeException(e);
					}
					latch.countDown();
				}
			}).start();
		}
		else if (message == MSG_EXPLORE_FACT)
		{
			Fact pfact = UIHub.selectedFact();
			DatabaseConnection connection = UIHub.selectedConnection();

			String factId = pfact.logicalName() + " in " + connection.name();

			final JFrameUIView dataFrame = new JFrameUIView("DataExplorer." + factId + "." + explorerIndex.incrementAndGet());

			DataGridPanel dataGridPanel = new DataGridPanel(connection, pfact, new Runnable()
			{
				private final AtomicBoolean firstRun = new AtomicBoolean(true);

				@Override
				public void run()
				{
					if (firstRun.get())
					{
						dataFrame.frame().pack();
						dataFrame.frame().setLocationRelativeTo(null);
					}

					firstRun.set(false);
				}
			});

			dataFrame.setView(dataGridPanel);

			new UIContainerImpl().addView(dataFrame);

			dataFrame.frame().setTitle("Exploring Fact " + factId);

			dataFrame.frame().setLocationRelativeTo(null);

			dataFrame.frame().getContentPane().setLayout(new BorderLayout());
			dataFrame.frame().getContentPane().add(dataGridPanel.getComponent(), BorderLayout.CENTER);

			dataFrame.frame().setMinimumSize(new Dimension(100, 200));

			//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

			//height of the task bar
			//Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(dataFrame.stage().getGraphicsConfiguration());

			//available size of the screen
			//dataFrame.stage().setMaximumSize(new Dimension(screenSize.width - scnMax.bottom - scnMax.top - 100, screenSize.height - scnMax.left - scnMax.right - 100));

			dataFrame.frame().setVisible(true);
			latch.countDown();
		}
		else if (message == MSG_NEW_CONNECTION)
		{
			try
			{
				EventUtils.runOnEventThreadLater(new Runnable()
				{
					@Override
					public void run()
					{
						UIWizardFrame uiWizardFrame = new UIWizardFrame(new NewConnectionModelWizard());
						new UIContainerImpl().addView(uiWizardFrame);
						latch.countDown();
						uiWizardFrame.dialog();
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new UnsupportedOperationException();
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	public void stateChanged(String key, Object value)
	{
		throw new UnsupportedOperationException();
	}
}
