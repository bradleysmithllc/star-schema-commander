package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;

import java.util.*;

public class WizardStateImpl<T, C extends WizardStateUIComponent<T>> implements WizardState<T, C>
{
	private final T model;
	private final String id;
	private final C uiComponent;
	private final WizardStateListener<T, C> listener;

	private final List<WizardStateTransition<T, C, ?, ?>> transitions = new ArrayList<WizardStateTransition<T, C, ?, ?>>();
	private final Map<String, WizardStateTransition<T, C, ?, ?>> transitionMap = new HashMap<String, WizardStateTransition<T, C, ?, ?>>();

	public WizardStateImpl(T model, String id, C uiComponent, WizardStateListener<T, C> listener)
	{
		this.model = model;
		this.id = id;
		this.uiComponent = uiComponent;
		this.listener = listener;

		if (uiComponent != null)
		{
			uiComponent.state(this);
		}
	}

	void addTransition(WizardStateTransition<T, C, ?, ?> transition)
	{
		transitions.add(transition);
		transitionMap.put(transition.id(), transition);
	}

	@Override
	public void activate()
	{
		if (listener != null)
		{
			listener.stateActivated(this);
		}
	}

	@Override
	public void deactivate()
	{
		if (listener != null)
		{
			listener.stateDeactivated(this);
		}
	}

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public C component()
	{
		return uiComponent;
	}

	@Override
	public List<WizardStateTransition<T, C, ?, ?>> transitions()
	{
		return Collections.unmodifiableList(transitions);
	}

	@Override
	public Map<String, WizardStateTransition<T, C, ?, ?>> transitionMap()
	{
		return Collections.unmodifiableMap(transitionMap);
	}

	@Override
	public T model()
	{
		return model;
	}
}