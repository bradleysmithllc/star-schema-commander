package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.DimensionTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

public class SelectDimensionsUIComponent extends WizardStateUIComponent<Reference<List<Dimension>>>
{
	public static final int MOD_AVAILABLE_DIMENSIONS = 0;
	public static final int MOD_SELECTED_DIMENSIONS = 1;
	public static final int MOD_EFFECTIVE_REPORTING_AREA = 2;

	public static final String MSG_SELECT_DIMENSION = "MSG_SELECT_DIMENSION";
	public static final String MSG_SELECT_DIMENSIONS = "MSG_SELECT_DIMENSIONS";
	public static final String MSG_SELECT_ALL_DIMENSIONS = "MSG_SELECT_ALL_DIMENSIONS";
	public static final String MSG_CLEAR_SELECTION = "MSG_CLEAR_SELECTION";

	private final String nextTransitionId;

	DimensionTableModel dimensionTableModel;
	private JTable dimensionTable;
	private JScrollPane scrollPane;

	private WizardStateTransition<Reference<List<Dimension>>, ? extends WizardStateUIComponent<Reference<List<Dimension>>>, ?, ?> loadTrans;
	private ReportingArea effectiveReportingArea;

	public SelectDimensionsUIComponent(String nextTransitionId)
	{
		this.nextTransitionId = nextTransitionId;
	}

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		final WizardState<Reference<List<Dimension>>, ?> state = state();

		loadTrans = state.transitionMap().get(nextTransitionId);
		if (loadTrans != null)
		{
			loadTrans.enable(false);
		}

		effectiveReportingArea = UIHub.selectedReportingArea();
		if (effectiveReportingArea == null)
		{
			effectiveReportingArea = UIHub.profile().defaultReportingArea();
		}

		dimensionTableModel = new DimensionTableModel();

		dimensionTable = new JTable(dimensionTableModel);

		scrollPane = new JScrollPane(dimensionTable);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Select a Dimension"));

		dimensionTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (dimensionTable.getSelectionModel().isSelectionEmpty())
				{
					if (loadTrans != null)
					{
						loadTrans.enable(false);
					}
				}
				else
				{
					// remove prior selections
					List<Dimension> reference = state.model().getReference();
					reference.clear();

					// add all selected
					List<Dimension> td = dimensionTableModel.tableData();

					for (int i = 0; i < td.size(); i++)
					{
						if (dimensionTable.getSelectionModel().isSelectedIndex(i))
						{
							reference.add(td.get(i));
						}
					}

					if (loadTrans != null)
					{
						loadTrans.enable(true);
					}
				}
			}
		});
	}

	@Override
	public void activate()
	{
		dimensionTableModel.visit(new CoordinatedTableModel.ModelVisitor<Dimension>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<Dimension> model)
			{
				model.clearData();
				for (Map.Entry<String, Dimension> entry : effectiveReportingArea.dimensions().entrySet())
				{
					Dimension dimension = entry.getValue();
					if (!dimension.degenerate() && dimension.keys().size() != 0)
					{
						model.addElement(dimension);
					}
				}
			}
		});

		getComponent().revalidate();
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MOD_AVAILABLE_DIMENSIONS:
				return (T) dimensionTableModel.tableData();
			case MOD_SELECTED_DIMENSIONS:
				return (T) state().model().getReference();
			case MOD_EFFECTIVE_REPORTING_AREA:
				return (T) effectiveReportingArea;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_CLEAR_SELECTION)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						dimensionTable.clearSelection();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_DIMENSION)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						dimensionTable.clearSelection();

						List<Dimension> td = dimensionTableModel.tableData();

						int index = td.indexOf(value);

						if (index == -1)
						{
							throw new IllegalArgumentException();
						}

						dimensionTable.getSelectionModel().addSelectionInterval(index, index);
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_DIMENSIONS)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						List<Dimension> dlist = (List<Dimension>) value;

						dimensionTable.clearSelection();
						List<Dimension> td = dimensionTableModel.tableData();

						for (Dimension d : dlist)
						{
							int index = td.indexOf(d);
							dimensionTable.getSelectionModel().addSelectionInterval(index, index);
						}
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_ALL_DIMENSIONS)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						dimensionTable.clearSelection();
						List<Dimension> td = dimensionTableModel.tableData();

						for (Dimension d : td)
						{
							int index = td.indexOf(d);
							dimensionTable.getSelectionModel().addSelectionInterval(index, index);
						}
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return scrollPane;
	}
}