package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class UIWizardFrame extends AbstractUIComponent
{
	private final UIWizard uiWizard;
	private JFrame frame;

	public UIWizardFrame(UIWizard uiWizard)
	{
		this.uiWizard = uiWizard;

		addChild(uiWizard);

		uiWizard.observe(new UIWizard.DialogChangeListener()
		{
			@Override
			public void contentChanged()
			{
				requireEventThread();
				frame.pack();
				frame.setLocationRelativeTo(null);
			}

			@Override
			public void disposeWizard()
			{
				// remove components
				UIHub.get().removeComponentTree(UIWizardFrame.this);

				try
				{
					Runnable runnable = new Runnable()
					{
						@Override
						public void run()
						{
							// dispose stage
							frame.dispose();
						}
					};

					if (EventQueue.isDispatchThread())
					{
						runnable.run();
					}
					else
					{
						EventQueue.invokeAndWait(runnable);
					}
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				catch (InvocationTargetException e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public String viewInstance()
	{
		return String.valueOf(System.identityHashCode(this));
	}

	@Override
	protected void finishBuildParent(Profile profile) throws Exception
	{
		frame.getContentPane().add(uiWizard.getComponent(), BorderLayout.CENTER);
	}

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout());
	}

	public void dialog()
	{
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
