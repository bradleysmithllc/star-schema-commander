package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class DataGridNavigatorOptionPanel<M extends Comparable<M>, T extends Comparable<T>> extends AbstractUIComponent
{
	public static final int MOD_SELECTED_MEASURES = 0;
	public static final int MOD_AVAILABLE_AGGREGATES = 1;
	public static final int MOD_AVAILABLE_MEASURES = 2;

	public static final int ST_CONTEXT_COMBO_VISIBLE = 3;

	public static final String MSG_SELECT_AGGREGATION = "MSG_SELECT_AGGREGATION";
	public static final String MSG_SELECT_MEASURE = "MSG_SELECT_MEASURE";
	public static final String MSG_ADD_ALL = "MSG_ADD_ALL";
	public static final String MSG_SELECT_ALL_AVAILABLE = "MSG_SELECT_ALL_AVAILABLE";
	public static final String MSG_SELECT_ALL_INCLUDED = "MSG_SELECT_ALL_INCLUDED";
	public static final String MSG_ASSIGN_AGG_TO_INCLUDED = "MSG_ASSIGN_AGG_TO_INCLUDED";
	public static final String MSG_ASSIGN_ALIAS_TO_INCLUDED = "MSG_ASSIGN_ALIAS_TO_INCLUDED";
	public static final String MSG_REMOVE_INCLUDED_SELECTED = "MSG_REMOVE_INCLUDED_SELECTED";
	public static final String MSG_ADD_AVAILABLE_SELECTED = "MSG_ADD_AVAILABLE_SELECTED";
	public static final String MSG_SELECT_CONTEXT = "MSG_SELECT_CONTEXT";
	private final Fact exploring;
	// state used for keeping controls in sync
	private final Map<String, List<Query.aggregation>> includedTAggs = new HashMap<String, List<Query.aggregation>>();
	private final OptionPanelInnerds<M, T> innerds;
	private CoordinatedTableModel<T> includedTableModel;
	private JTable includedTable;
	private JTable availableTable;
	private CoordinatedTableModel<M> availableTableModel;
	private JPanel optionsPanel;
	private DefaultComboBoxModel<Query.aggregation> aggregatesListModel;
	private JComboBox<Query.aggregation> aggregatesList;
	private DefaultComboBoxModel availableContextListModel;
	private JComboBox availableContextComboBox;
	private JButton addButton;
	private JButton removeButton;

	public DataGridNavigatorOptionPanel(Fact exploring, OptionPanelInnerds<M, T> innerds)
	{
		this.innerds = innerds;
		this.exploring = exploring;
	}

	@Override
	protected void buildParent(Profile profile)
	{
		availableContextListModel = new DefaultComboBoxModel();
		innerds.initializeContextualComboBoxModel(exploring, availableContextListModel);
		availableContextComboBox = new JComboBox(availableContextListModel);
		innerds.initializeContextualComboBox(exploring, availableContextComboBox);

		optionsPanel = new JPanel();
		optionsPanel.setLayout(new GridLayout(1, 2));
		optionsPanel.setBorder(BorderFactory.createTitledBorder("Measures"));

		includedTableModel = innerds.createIncludedTableModel();

		includedTable = new JTable(includedTableModel);
		includedTableModel.addTableModelListener(new TableModelListener()
		{
			@Override
			public void tableChanged(TableModelEvent e)
			{
				// update internal state when items are added to the included list or an aggregate changes
				updateTAggMap();
				updateAvailableAggCombo();
			}
		});

		includedTableModel.addCoordinatedTableModelListener(new CoordinatedTableModel.CoordinatedTableModelListener<T>()
		{
			@Override
			public void itemAdded(T t)
			{
			}

			@Override
			public void itemRemoved(T t)
			{
				innerds.demoteToAvailable(t, availableTableModel, availableContextComboBox.getSelectedItem());
			}
		});

		DefaultComboBoxModel<Query.aggregation> stringDefaultListModel = new DefaultComboBoxModel<Query.aggregation>();
		stringDefaultListModel.addElement(null);

		for (Query.aggregation i : Query.aggregation.values())
		{
			stringDefaultListModel.addElement(i);
		}

		JComboBox<Query.aggregation> jl = new JComboBox<Query.aggregation>(stringDefaultListModel);
		jl.setRenderer(new ListCellRenderer<Query.aggregation>()
		{
			private final JLabel label = new JLabel();

			@Override
			public Component getListCellRendererComponent(JList<? extends Query.aggregation> list, Query.aggregation value, int index, boolean isSelected, boolean cellHasFocus)
			{
				if (value == null)
				{
					label.setText("exclude");
				}
				else
				{
					label.setText(value.name());
				}

				return label;
			}
		});

		includedTable.setDefaultEditor(Query.aggregation.class, new DefaultCellEditor(jl));
		includedTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				removeButton.setEnabled(!includedTable.getSelectionModel().isSelectionEmpty());
			}
		});

		aggregatesListModel = new DefaultComboBoxModel<Query.aggregation>();

		for (Query.aggregation i : Query.aggregation.values())
		{
			aggregatesListModel.addElement(i);
		}

		aggregatesList = new JComboBox<Query.aggregation>(aggregatesListModel);
		aggregatesList.setEnabled(true);

		JPanel availablePanel = new JPanel();
		availablePanel.setLayout(new BorderLayout());

		JScrollPane scrollPane = new JScrollPane(includedTable);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Included"));

		optionsPanel.add(scrollPane);

		availableTableModel = innerds.createAvailableTableModel();
		availableTable = new JTable(availableTableModel);
		availableTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		availableTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!e.getValueIsAdjusting())
				{
					updateAvailableAggCombo();
				}
			}
		});

		JPanel availableAttributesPanel = new JPanel();
		availableAttributesPanel.setLayout(new BorderLayout());
		availableAttributesPanel.add(availableContextComboBox, BorderLayout.NORTH);

		scrollPane = new JScrollPane(availableTable);
		availableAttributesPanel.add(scrollPane, BorderLayout.CENTER);

		scrollPane.setBorder(BorderFactory.createTitledBorder("Available"));

		innerds.populateIncludedTableModel(exploring, includedTableModel);

		// update internal state to reflect initial state
		updateTAggMap();

		// don't initialize this here.  Wait until the combo box selection listener is called
		availableContextComboBox.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// clear before updating
				availableTableModel.visit(new CoordinatedTableModel.ModelVisitor<M>()
				{
					@Override
					public void visit(CoordinatedTableModel.UpdatableTableModel<M> model)
					{
						model.clearData();
					}
				});

				Object selectedItem = availableContextComboBox.getSelectedItem();
				if (selectedItem != null)
				{
					innerds.populateAvailableTableModel(exploring, availableTableModel, selectedItem);
				}

				// remove all attributes that don't belong
				availableTableModel.visit(new CoordinatedTableModel.ModelVisitor<M>()
				{
					@Override
					public void visit(CoordinatedTableModel.UpdatableTableModel<M> model)
					{
						String contextId = innerds.uniquelyIdentifyContext(availableContextComboBox.getSelectedItem());

						List<M> removeList = new ArrayList<M>();

						for (M m : availableTableModel.tableData())
						{
							String key = contextId + "." + innerds.uniquelyIdentifyM(m);

							List<Query.aggregation> alist = includedTAggs.get(key);

							//if list includes all aggs, remove this entry
							if (alist != null && alist.size() == Query.aggregation.values().length)
							{
								removeList.add(m);
							}
						}

						for (M m : removeList)
						{
							model.removeElement(m);
						}
					}
				});
			}
		});

		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new BorderLayout());
		addButton = new JButton("\u2264");
		addButton.setEnabled(true);
		addButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Query.aggregation selectedItem = (Query.aggregation) aggregatesList.getSelectedItem();
				innerds.promoteSelectedToIncluded(selectedItem, availableTableModel, includedTableModel, availableTable.getSelectionModel(), availableContextComboBox.getSelectedItem());

				// update internal state when items are added to the included list
				updateTAggMap();
				updateAvailableMList();
				updateAvailableAggCombo();
			}
		});

		removeButton = new JButton(">>");
		removeButton.setEnabled(false);
		removeButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				final List<T> selected = new ArrayList<T>();

				List<T> tableData = includedTableModel.tableData();

				for (int index = 0; index < tableData.size(); index++)
				{
					if (includedTable.getSelectionModel().isSelectedIndex(index))
					{
						selected.add(tableData.get(index));
					}
				}

				// remove from available
				includedTableModel.visit(new CoordinatedTableModel.ModelVisitor<T>()
				{
					@Override
					public void visit(CoordinatedTableModel.UpdatableTableModel<T> model)
					{
						for (T t : selected)
						{
							model.removeElement(t);
						}
					}
				});

				for (T t : selected)
				{
					innerds.demoteToAvailable(t, availableTableModel, availableContextComboBox.getSelectedItem());
				}

				// update internal state when items are added to the included list
				updateTAggMap();
				updateAvailableMList();
				updateAvailableAggCombo();
			}
		});

		controlPanel.add(addButton, BorderLayout.WEST);
		controlPanel.add(aggregatesList, BorderLayout.CENTER);

		JPanel controlParent = new JPanel();

		controlParent.setLayout(new BorderLayout());

		controlParent.add(removeButton, BorderLayout.WEST);
		controlParent.add(controlPanel, BorderLayout.CENTER);

		availablePanel.add(controlParent, BorderLayout.SOUTH);

		availablePanel.add(scrollPane, BorderLayout.CENTER);
		availablePanel.add(availableContextComboBox, BorderLayout.NORTH);

		optionsPanel.add(availablePanel);

		// programmatically select the first item
		if (availableContextListModel.getSize() != 0)
		{
			availableContextComboBox.setSelectedIndex(0);
		}

		availableContextComboBox.setVisible(innerds.contextComboBoxVisible(exploring));
	}

	private void updateAvailableMList()
	{
		final List<M> tbr = new ArrayList<M>();

		for (M m : availableTableModel.tableData())
		{
			String contextId = innerds.uniquelyIdentifyContext(availableContextComboBox.getSelectedItem());
			String key = contextId + "." + innerds.uniquelyIdentifyM(m);

			if (includedTAggs.containsKey(key))
			{
				List<Query.aggregation> al = includedTAggs.get(key);

				if (al.size() == Query.aggregation.values().length)
				{
					// this one is done
					tbr.add(m);
				}
			}
		}

		availableTableModel.visit(new CoordinatedTableModel.ModelVisitor<M>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<M> model)
			{
				for (M m : tbr)
				{
					model.removeElement(m);
				}
			}
		});
	}

	private void updateAvailableAggCombo()
	{
		// preserve selected value
		Query.aggregation selectedAgg = (Query.aggregation) aggregatesList.getSelectedItem();

		// remove any aggregation level that is not eligible
		aggregatesListModel.removeAllElements();

		List<Query.aggregation> aggs = new ArrayList<Query.aggregation>();
		for (Query.aggregation agg : Query.aggregation.values())
		{
			aggs.add(agg);
		}

		ListSelectionModel factMeasureNavigatorTableSelectionModel = availableTable.getSelectionModel();
		List<M> tableData = availableTableModel.tableData();
		String contextId = innerds.uniquelyIdentifyContext(availableContextComboBox.getSelectedItem());

		for (
			int row = factMeasureNavigatorTableSelectionModel.getMinSelectionIndex();
			row <= factMeasureNavigatorTableSelectionModel.getMaxSelectionIndex();
			row++
			)
		{
			// remove all included
			if (factMeasureNavigatorTableSelectionModel.isSelectedIndex(row))
			{
				M key = tableData.get(row);
				String uid = contextId + "." + innerds.uniquelyIdentifyM(key);

				List<Query.aggregation> aggregations = includedTAggs.get(uid);

				if (aggregations != null)
				{
					for (Query.aggregation aggT : aggregations)
					{
						aggs.remove(aggT);
					}
				}
			}
		}

		for (Query.aggregation agg : aggs)
		{
			aggregatesListModel.addElement(agg);
		}

		// restore selection if it is still in there
		if (selectedAgg != null)
		{
			aggregatesList.setSelectedItem(selectedAgg);
		}
		else
		{
			aggregatesList.setSelectedIndex(0);
		}
	}

	private void updateTAggMap()
	{
		includedTAggs.clear();

		for (T t : includedTableModel.tableData())
		{
			Query.aggregation agg = innerds.aggregation(t);
			String uid = innerds.uniquelyIdentifyT(t);

			// map and count
			List<Query.aggregation> iagg = includedTAggs.get(uid);

			if (iagg == null)
			{
				iagg = new ArrayList<Query.aggregation>();
				includedTAggs.put(uid, iagg);
			}

			if (!iagg.contains(agg))
			{
				iagg.add(agg);
			}
		}
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case ST_CONTEXT_COMBO_VISIBLE:
				return availableContextComboBox.isVisible();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return optionsPanel;
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, final Object value)
	{
		if (message == MSG_SELECT_AGGREGATION)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						aggregatesList.setSelectedItem(value);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_MEASURE)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						availableTable.getSelectionModel().clearSelection();

						List<M> td = availableTableModel.tableData();

						int index = td.indexOf((M) value);

						availableTable.getSelectionModel().setSelectionInterval(index, index);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_ASSIGN_AGG_TO_INCLUDED)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						Pair<Integer, Query.aggregation> p = (Pair<Integer, Query.aggregation>) value;

						includedTableModel.setValueAt(p.getRight(), p.getLeft(), 4);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_ASSIGN_ALIAS_TO_INCLUDED)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						Pair<Integer, String> p = (Pair<Integer, String>) value;

						includedTableModel.setValueAt(p.getRight(), p.getLeft(), 3);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_ALL_AVAILABLE)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						availableTable.getSelectionModel().setSelectionInterval(0, availableTableModel.getRowCount() - 1);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_ALL_INCLUDED)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						includedTable.getSelectionModel().setSelectionInterval(0, includedTableModel.getRowCount() - 1);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_ADD_ALL)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						addButton.doClick();
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_ADD_AVAILABLE_SELECTED)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						addButton.doClick();
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_REMOVE_INCLUDED_SELECTED)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						removeButton.doClick();
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_CONTEXT)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						availableContextComboBox.setSelectedItem(value);
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public <H> H getModel(int id)
	{
		switch (id)
		{
			case MOD_SELECTED_MEASURES:
				return (H) includedTableModel.tableData();
			case MOD_AVAILABLE_MEASURES:
				return (H) availableTableModel.tableData();
			case MOD_AVAILABLE_AGGREGATES:
				List<Query.aggregation> avail = new ArrayList<Query.aggregation>();

				for (int i = 0; i < aggregatesListModel.getSize(); i++)
				{
					avail.add(aggregatesListModel.getElementAt(i));
				}

				return (H) avail;
			default:
				throw new IllegalArgumentException();
		}
	}

	public List<T> selectedMeasures()
	{
		return getModel(MOD_SELECTED_MEASURES);
	}

	public interface OptionPanelInnerds<M extends Comparable<M>, T extends Comparable<T>>
	{
		String uniquelyIdentifyContext(Object obj);

		String uniquelyIdentifyM(M m);

		String uniquelyIdentifyT(T t);

		CoordinatedTableModel<T> createIncludedTableModel();

		CoordinatedTableModel<M> createAvailableTableModel();

		Query.aggregation aggregation(T m);

		M getM(T m);

		void initializeContextualComboBoxModel(Fact exploring, DefaultComboBoxModel model);

		boolean contextComboBoxVisible(Fact exploring);

		void populateAvailableTableModel(Fact fact, CoordinatedTableModel<M> model, Object context);

		void populateIncludedTableModel(Fact fact, CoordinatedTableModel<T> model);

		void demoteToAvailable(T t, CoordinatedTableModel<M> availableModel, Object currentContext);

		void promoteSelectedToIncluded(Query.aggregation agg, CoordinatedTableModel<M> available, CoordinatedTableModel<T> included, ListSelectionModel selectionModel, Object context);

		void initializeContextualComboBox(Fact exploring, JComboBox availableContextComboBox);
	}
}
