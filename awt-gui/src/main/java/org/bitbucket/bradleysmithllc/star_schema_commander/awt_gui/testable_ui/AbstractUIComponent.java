package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIComponentAdapter;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.IProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractUIComponent extends UIComponentAdapter
{
	@Override
	public String componentInstance()
	{
		return viewInstance();
	}

	protected IProfile profile()
	{
		return UIHub.profile();
	}

	@Override
	public final void build(Profile profile) throws Exception
	{
		try
		{
			buildParent(profile);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
		finishBuildParent(UIHub.profile());
	}

	protected void finishBuildParent(Profile profile) throws Exception
	{
	}

	/**
	 * Builds the parent component and installs any children
	 *
	 * @param profile
	 */
	protected void buildParent(Profile profile) throws Exception
	{
	}

	@Override
	public JComponent getComponent()
	{
		throw new IllegalArgumentException();
	}

	@Override
	public boolean checkState(int value)
	{
		return false;
	}

	@Override
	public void dispatch(String message)
	{
		dispatch(message, null);
	}

	@Override
	public <T extends UIView> T locate(String clType, String instance)
	{
		return (T) uiContainer.locateViewGlobally(clType, instance);
	}

	@Override
	public <T extends UIView> T locate(String clType)
	{
		return locate(clType, clType);
	}

	protected void clearChildren()
	{
		UIHub.get().clearChildren(this);
	}

	protected void requireEventThread()
	{
		if (!EventQueue.isDispatchThread())
		{
			throw new RuntimeException();
		}
	}

	protected void runOnEventThread(Runnable run) throws Exception
	{
		if (EventQueue.isDispatchThread())
		{
			run.run();
		}
		else
		{
			EventQueue.invokeAndWait(run);
		}
	}

	public void stateChanged(stateKey key, Object value)
	{
	}

	@Override
	public void stateChanged(String key, Object value)
	{
		try
		{
			stateChanged(stateKey.valueOf(key), value);
		}
		catch (IllegalArgumentException exc)
		{
			// nevermind.  Doesn't map to the enum
		}
	}

	public enum stateKey
	{
		selectedFact,
		selectedReportingArea,
		profile,
		profileRefresh,
		selectedDimension,
		selectedConnection
	}
}
