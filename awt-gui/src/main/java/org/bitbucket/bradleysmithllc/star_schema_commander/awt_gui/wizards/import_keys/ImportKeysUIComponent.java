package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class ImportKeysUIComponent extends WizardStateUIComponent<Reference<DatabaseConnection>>
{
	private WizardStateTransition<Reference<DatabaseConnection>, ? extends WizardStateUIComponent<Reference<DatabaseConnection>>, ?, ?> nextTr;

	private JPanel container;

	private JLabel keyComponents;
	private JLabel keyName;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		// hide the finish step until we have visited all
		nextTr = state().transitionMap().get("done");
		nextTr.enable(false);

		container = new JPanel();
		container.setLayout(new GridLayout(2, 0));

		keyComponents = new JLabel("");
		keyName = new JLabel("");

		container.add(keyComponents);
		container.add(keyName);

		new SwingWorker()
		{
			@Override
			protected Object doInBackground() throws Exception
			{
				System.out.println("Generating keys");
				UIHub.selectedReportingArea().generateKeys(state().model().getReference(), new KeyObserver()
				{
					@Override
					public void key(final Dimension pk_dim, final Fact fk_fact, String pkName, final String fkName)
					{
						try
						{
							EventQueue.invokeAndWait(new Runnable()
							{
								@Override
								public void run()
								{
									keyComponents.setText(new StringBuilder(fk_fact.logicalName()).append(">>").append(pk_dim.logicalName()).toString());
									keyName.setText(fkName);
									container.revalidate();
								}
							});
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						catch (InvocationTargetException e)
						{
							e.printStackTrace();
						}
					}
				});

				UIHub.profile().persist();
				return null;
			}

			@Override
			protected void done()
			{
				nextTr.enable(true);
			}
		}.execute();
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}
}