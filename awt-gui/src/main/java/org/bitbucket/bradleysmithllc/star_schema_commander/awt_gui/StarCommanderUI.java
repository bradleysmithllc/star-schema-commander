package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import java.io.File;

@CLIEntry(
	version = "ssc.1.D",
	description = "UI Entry point",
	nickName = "ssc"
)
public class StarCommanderUI
{
	private File dir;

	public StarCommanderUI()
	{
	}

	public StarCommanderUI(File dir)
	{
		this(dir.getAbsolutePath());
	}

	public StarCommanderUI(String dir)
	{
		setDirectory(dir);
	}

	public static void main(String[] argv) throws Exception
	{
		// make sure that mac os can handle AWT and JavaFX
		EventUtils.enableJFXEventQueue();
		CommonsCLILauncher.main(argv);
	}

	@CLIOption(
		name = "p",
		longName = "profile",
		description = "The path to initialize as a Star(schema) Commander profile.  The default is the ${user.home}/.sscommander"
	)
	public void setDirectory(String path)
	{
		dir = new File(path);
	}

	protected File getProfileDirectory()
	{
		if (dir == null)
		{
			dir = Profile.defaultProfileDir();
		}

		return dir;
	}

	@CLIMain
	public void launchme() throws Exception
	{
		EventUtils.enableJFXEventQueue();
		StarCommanderSwingUI.launch(getProfileDirectory().getAbsolutePath());
	}

	public void shutDown()
	{
		UIHub.profile().releaseProfileChangeListeners();
		//Platform.exit();
	}

	public Profile getProfile()
	{
		return UIHub.profile();
	}
}
