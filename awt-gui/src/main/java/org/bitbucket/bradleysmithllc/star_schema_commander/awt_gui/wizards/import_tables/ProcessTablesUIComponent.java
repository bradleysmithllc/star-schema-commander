package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.laf.progressbar.WebProgressBar;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.ReferenceMap;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.QualifiedTable;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ProcessTablesUIComponent extends WizardStateUIComponent<ReferenceMap>
{
	public static final String MSG_SET_LOGICAL_NAME = "MSG_SET_LOGICAL_NAME";

	public static final int MOD_LOGICAL_NAME = 0;
	public static final int MOD_PHYSICAL_NAME = 1;

	private WizardStateTransition<ReferenceMap, ? extends WizardStateUIComponent<ReferenceMap>, ?, ?> nextTr;
	private List<QualifiedTable> tables;
	private JPanel container;

	private JLabel physicalName;
	private JTextField logicaName;

	@Override
	public void activate()
	{
		// hide the finish step until we have visited all
		nextTr = state().transitionMap().get("next");

		// deactivate next if there are no more
		ReferenceMap referenceMap = state().model();
		int currentIndex = referenceMap.getValue("currentTableIndex");
		tables = referenceMap.getValue("TableList");

		nextTr.activate(currentIndex < (tables.size() - 1));

		container = new JPanel();
		container.setLayout(new BorderLayout());

		final WebProgressBar progressBar1 = new WebProgressBar(WebProgressBar.HORIZONTAL, 1, tables.size());
		progressBar1.setValue(currentIndex + 1);
		progressBar1.setIndeterminate(false);

		container.add(progressBar1, BorderLayout.NORTH);

		QualifiedTable phName = tables.get(currentIndex);
		physicalName = new JLabel(phName.qualifiedName());

		String keyName = "[Table[" + phName + "]]";

		String logName = phName.qualifiedName().replace('.', '_');

		if (referenceMap.containsKey(keyName))
		{
			logName = referenceMap.getValue(keyName);
		}

		logicaName = new JTextField(logName);

		container.add(physicalName, BorderLayout.CENTER);
		container.add(logicaName, BorderLayout.SOUTH);
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MOD_LOGICAL_NAME:
				return (T) logicaName.getText();
			case MOD_PHYSICAL_NAME:
				return (T) physicalName.getText();
		}

		throw new IllegalArgumentException();
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SET_LOGICAL_NAME)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						logicaName.setText(String.valueOf(value));
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}
}
