package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.DatabaseColumnTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.WizardColumnHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ColumnStore;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SelectColumnsUIComponent<T extends ColumnStore> extends WizardStateUIComponent<List<DatabaseColumn>>
{
	public static final int MODEL_COLUMN_LIST = 0;
	public static final int MODEL_SELECTED_COLUMNS = 1;
	public static final String MSG_SELECT_COLUMNS = "MSG_SELECT_COLUMNS";
	public static final String MSG_SELECT_COLUMN = "MSG_SELECT_COLUMN";
	public static final String MSG_SELECT_ALL_COLUMNS = "MSG_SELECT_ALL_COLUMNS";

	private final DatabaseColumnTableModel connectionDefaultListModel = new DatabaseColumnTableModel();
	private final T fact;
	private final WizardColumnHandler handler;
	private JTable connectionJList;
	private JScrollPane scrollPane;
	private WizardStateTransition<List<DatabaseColumn>, ? extends WizardStateUIComponent<List<DatabaseColumn>>, ?, ?> loadTrans;

	public SelectColumnsUIComponent(T fact, WizardColumnHandler<T> handler)
	{
		this.fact = fact;
		this.handler = handler;
	}

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		loadTrans = state().transitionMap().get("logical");
		loadTrans.enable(false);

		// exclude columns involved in a foreign key
		for (Map.Entry<String, DatabaseColumn> col : fact.columns().entrySet())
		{
			if (handler.include(fact, col.getValue()))
			{
				connectionDefaultListModel.addColumn(col.getValue());
			}
		}

		connectionJList = new JTable(connectionDefaultListModel);
		connectionJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		scrollPane = new JScrollPane(connectionJList);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Select Columns"));

		connectionJList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				state().model().clear();

				if (!connectionJList.getSelectionModel().isSelectionEmpty())
				{
					loadTrans.enable(true);

					for (int row : connectionJList.getSelectedRows())
					{
						DatabaseColumn dbc = connectionDefaultListModel.tableData().get(row);

						state().model().add(dbc);
					}
				}
				else
				{
					loadTrans.enable(false);
				}
			}
		});
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MODEL_COLUMN_LIST:
			{
				return (T) connectionDefaultListModel;
			}
			case MODEL_SELECTED_COLUMNS:
			{
				List<DatabaseColumn> selected = new ArrayList<DatabaseColumn>();

				for (int row : connectionJList.getSelectedRows())
				{
					selected.add(connectionDefaultListModel.tableData().get(row));
				}

				return (T) selected;
			}
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SELECT_COLUMNS)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						List<DatabaseColumn> cols = (List<DatabaseColumn>) value;

						ListSelectionModel lsm = connectionJList.getSelectionModel();

						lsm.clearSelection();

						// select every requested item
						int row = 0;
						int sel = 0;

						for (DatabaseColumn dc : connectionDefaultListModel.tableData())
						{
							if (cols.contains(dc))
							{
								sel++;
								lsm.addSelectionInterval(row, row);
							}

							row++;
						}

						if (sel != cols.size())
						{
							throw new IllegalArgumentException();
						}
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_COLUMN)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						DatabaseColumn col = (DatabaseColumn) value;

						ListSelectionModel lsm = connectionJList.getSelectionModel();

						lsm.clearSelection();

						// select every requested item
						int row = 0;
						int sel = 0;

						for (DatabaseColumn dc : connectionDefaultListModel.tableData())
						{
							if (col == dc)
							{
								sel++;
								lsm.addSelectionInterval(row, row);
							}

							row++;
						}

						if (sel != 1)
						{
							throw new IllegalArgumentException();
						}
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_ALL_COLUMNS)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						ListSelectionModel lsm = connectionJList.getSelectionModel();

						lsm.setSelectionInterval(0, connectionDefaultListModel.getRowCount() - 1);
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return scrollPane;
	}
}
