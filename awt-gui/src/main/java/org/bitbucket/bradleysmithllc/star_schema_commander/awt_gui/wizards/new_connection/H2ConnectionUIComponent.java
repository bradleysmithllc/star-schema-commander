package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.extended.filechooser.DirectoryChooserListener;
import com.alee.extended.filechooser.WebDirectoryChooserPanel;
import com.alee.laf.WebLookAndFeel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.H2ConnectionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.CountDownLatch;

public class H2ConnectionUIComponent extends WizardStateUIComponent<H2ConnectionBuilder>
{
	public static final String MSG_SELECT_DIRECTORY = "MSG_SELECT_DIRECTORY";
	private JPanel panel;
	private WebDirectoryChooserPanel fileChooserField2;
	private String name;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		WebLookAndFeel.install();
		fileChooserField2 = new WebDirectoryChooserPanel();

		fileChooserField2.setSelectedDirectory(File.listRoots()[0]);

		fileChooserField2.addDirectoryChooserListener(new DirectoryChooserListener()
		{
			@Override
			public void selectionChanged(File file)
			{
				state().model().path(new File(file, name));
			}
		});

		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		panel.add(new JLabel("H2 Data File"), BorderLayout.WEST);
		panel.add(fileChooserField2, BorderLayout.CENTER);
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message, Object value)
	{
		if (message == MSG_SELECT_DIRECTORY)
		{
			final File f = (File) value;

			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						fileChooserField2.setSelectedDirectory(f);
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}

			latch.countDown();
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return panel;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
