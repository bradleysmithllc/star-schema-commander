package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.JTableUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

public class SelectDimensionKeyUIComponent extends WizardStateUIComponent<MutablePair<Dimension, Reference<List<DimensionKey>>>>
{
	public static final int MOD_AVAILABLE_KEYS_LIST = 0;
	public static final int MOD_DIMENSION = 1;

	public static final String MSG_SELECT_ALL_KEYS = "MSG_SELECT_ALL_KEYS";
	public static final String MSG_SELECT_KEYS = "MSG_SELECT_KEYS";
	public static final String MSG_SELECT_KEY = "MSG_SELECT_KEY";

	private final String nextTransitionId;

	DimensionKeyTableModel dimensionKeyTableModel;
	private JTable dimensionKeyTable;
	private JScrollPane scrollPane;
	private WizardStateTransition<MutablePair<Dimension, Reference<List<DimensionKey>>>, ? extends WizardStateUIComponent<MutablePair<Dimension, Reference<List<DimensionKey>>>>, ?, ?> loadTrans;

	public SelectDimensionKeyUIComponent(String nextTransitionId)
	{
		this.nextTransitionId = nextTransitionId;
	}

	@Override
	public void subInitialize() throws Exception
	{
		dimensionKeyTableModel = new DimensionKeyTableModel();

		dimensionKeyTable = new JTable(dimensionKeyTableModel);
		dimensionKeyTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		scrollPane = new JScrollPane(dimensionKeyTable);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Select a Key to connect"));

		dimensionKeyTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (dimensionKeyTable.getSelectionModel().isSelectionEmpty())
				{
					state().model().getRight().setReference(null);

					if (loadTrans != null)
					{
						loadTrans.enable(false);
					}
				}
				else
				{
					state().model().getRight().setReference(JTableUtils.getSelectedValues(dimensionKeyTable, DimensionKey.class));

					if (loadTrans != null)
					{
						loadTrans.enable(true);
					}
				}
			}
		});
	}

	@Override
	public void activate()
	{
		loadTrans = state().transitionMap().get(nextTransitionId);
		if (loadTrans != null)
		{
			loadTrans.enable(false);
		}

		final MutablePair<Dimension, Reference<List<DimensionKey>>> model = state().model();
		final Dimension dimension = model.getLeft();

		try
		{
			EventUtils.runOnEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					scrollPane.setBorder(BorderFactory.createTitledBorder("Select a Key to connect to " + dimension.logicalName()));
					dimensionKeyTableModel.visit(new CoordinatedTableModel.ModelVisitor<DimensionKey>()
					{
						@Override
						public void visit(CoordinatedTableModel.UpdatableTableModel<DimensionKey> tmodel)
						{
							tmodel.clearData();

							for (Map.Entry<String, DimensionKey> conn : dimension.keys().entrySet())
							{
								tmodel.addElement(conn.getValue());
							}
						}
					});
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MOD_AVAILABLE_KEYS_LIST:
				return (T) dimensionKeyTableModel.tableData();
			case MOD_DIMENSION:
				return (T) state().model().getLeft();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SELECT_ALL_KEYS)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						dimensionKeyTable.selectAll();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_KEY)
		{
			try
			{
				DesktopUtils.executeOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						int index0 = dimensionKeyTableModel.tableData().indexOf(value);
						dimensionKeyTable.getSelectionModel().setSelectionInterval(index0, index0);
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_KEYS)
		{
			throw new UnsupportedOperationException();
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return scrollPane;
	}
}