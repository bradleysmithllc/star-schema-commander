package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.SqlServerJTDSConnectionBuilder;

public class JTDSSqlConnectionBuilderWrapper implements SqlConnectionBuilderWrapper
{
	String database;
	String domain;
	String username;
	String password;
	String server;
	int port = -1;
	String instanceName;
	String connectionName;

	@Override
	public void connectionName(String name)
	{
		connectionName = name;
	}

	@Override
	public void database(String name)
	{
		database = name;
	}

	@Override
	public void domain(String name)
	{
		domain = name;
	}

	@Override
	public boolean usesDomain()
	{
		return true;
	}

	@Override
	public void username(String name)
	{
		username = name;
	}

	@Override
	public void password(String pw)
	{
		password = pw;
	}

	@Override
	public void serverName(String serverName)
	{
		server = serverName;
	}

	@Override
	public void port(int p)
	{
		port = p;
	}

	@Override
	public void instanceName(String in)
	{
		instanceName = in;
	}

	@Override
	public DatabaseConnection create()
	{
		SqlServerJTDSConnectionBuilder cb = UIHub.profile().newSqlServerJTDSConnection().serverName(server);

		cb.name(connectionName);

		if (database != null)
		{
			cb.database(database);
		}

		if (port != -1)
		{
			cb.port(port);
		}

		if (instanceName != null)
		{
			cb.instanceName(instanceName);
		}

		if (domain != null)
		{
			cb.domain(domain);
		}

		if (username != null)
		{
			cb.username(username);
		}

		if (password != null)
		{
			cb.password(password);
		}

		return cb.create();
	}
}
