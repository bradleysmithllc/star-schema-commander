package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;

import javax.swing.table.DefaultTableModel;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class CoordinatedTableModel<T extends Comparable<T>> extends DefaultTableModel
{
	private static final CoordinatedTableModelListener NullCTModelListener = new CoordinatedTableModelListener()
	{
		public void itemRemoved(Object t)
		{
		}

		public void itemAdded(Object t)
		{
		}
	};
	private final List<T> tableData = dataVector;
	private final List<T> publicTableData = Collections.unmodifiableList(tableData);
	CoordinatedTableModelListener<T> listener = NullCTModelListener;
	private boolean updating = false;
	private ModelVisitor<T> nullModelVisitor = new ModelVisitor<T>()
	{
		@Override
		public void visit(UpdatableTableModel<T> model)
		{
		}
	};

	public CoordinatedTableModel(List<T> data)
	{
		tableData.addAll(data);
	}

	public CoordinatedTableModel()
	{
	}

	public void addCoordinatedTableModelListener(CoordinatedTableModelListener<T> listen)
	{
		listener = listen;
	}

	private synchronized boolean updating()
	{
		// allow for uninitialized table data also.  This happens because the table model calls this method
		// during the super initialization.
		return updating || tableData == null;
	}

	private synchronized void beginUpdate()
	{
		updating = true;
	}

	private synchronized void finishUpdate()
	{
		updating = false;
	}

	public void visit(ModelVisitor<T> visitor)
	{
		final AtomicBoolean atb = new AtomicBoolean(false);

		try
		{
			try
			{
				beginUpdate();

				visitor.visit(new UpdatableTableModel<T>()
				{
					@Override
					public void clearData()
					{
						atb.set(true);
						tableData.clear();
					}

					@Override
					public void addElement(T dim)
					{
						atb.set(true);
						tableData.add(dim);
						listener.itemAdded(dim);
					}

					@Override
					public void removeElement(T dim)
					{
						atb.set(true);
						int index = tableData.indexOf(dim);

						if (index != -1)
						{
							tableData.remove(dim);
							listener.itemRemoved(dim);
						}
					}

					@Override
					public void removeElementAt(int row)
					{
						atb.set(true);
						listener.itemRemoved(tableData.remove(row));
					}
				});

				// may affect ordering
				if (atb.get())
				{
					Collections.sort(tableData);
				}
			}
			finally
			{
				finishUpdate();
			}
		}
		catch (Throwable thr)
		{
			thr.printStackTrace();
			throw new RuntimeException(thr);
		}
		finally
		{
			try
			{
				if (atb.get())
				{
					DesktopUtils.executeOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							fireTableStructureChanged();
						}
					});
				}
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public final int getRowCount()
	{
		if (updating())
		{
			return 0;
		}

		return tableData.size();
	}

	@Override
	public final int getColumnCount()
	{
		return getCoordinatedColumnCount();
	}

	protected abstract int getCoordinatedColumnCount();

	@Override
	public final String getColumnName(int column)
	{
		return getCoordinatedColumnName(column);
	}

	protected abstract String getCoordinatedColumnName(int column);

	@Override
	public final Object getValueAt(int row, int column)
	{
		if (updating())
		{
			return null;
		}

		T t = getRow(row);

		return getCoordinatedValueAt(t, row, column);
	}

	private T getRow(int row)
	{
		if (row >= tableData.size())
		{
			throw new IllegalArgumentException("Cannot access row [" + row + "] - table Data has [" + tableData.size() + "] rows");
		}

		return (T) tableData.get(row);
	}

	protected abstract Object getCoordinatedValueAt(T t, int row, int column);

	@Override
	public final boolean isCellEditable(int row, int column)
	{
		return isCoordinatedCellEditable(row, column);
	}

	protected boolean isCoordinatedCellEditable(int row, int column)
	{
		return false;
	}

	@Override
	public final Class<?> getColumnClass(int columnIndex)
	{
		return getCoordinatedColumnClass(columnIndex);
	}

	protected Class<?> getCoordinatedColumnClass(int columnIndex)
	{
		return super.getColumnClass(columnIndex);
	}

	@Override
	public final void setValueAt(Object aValue, int row, int column)
	{
		if (!updating())
		{
			setCoordinatedValueAt(getRow(row), aValue, row, column);

			// may affect ordering
			visit(nullModelVisitor);

			fireTableCellUpdated(row, column);
		}
	}

	protected void setCoordinatedValueAt(T t, Object aValue, int row, int column)
	{
	}

	public List<T> tableData()
	{
		return publicTableData;
	}

	public interface CoordinatedTableModelListener<T>
	{
		void itemAdded(T t);

		void itemRemoved(T t);
	}

	public interface UpdatableTableModel<T>
	{
		void clearData();

		void addElement(T dim);

		void removeElement(T dim);

		void removeElementAt(int row);
	}

	public interface ModelVisitor<T>
	{
		void visit(UpdatableTableModel<T> model);
	}
}
