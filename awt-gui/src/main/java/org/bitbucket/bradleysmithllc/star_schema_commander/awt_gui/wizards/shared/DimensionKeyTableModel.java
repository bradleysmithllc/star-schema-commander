package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;

public class DimensionKeyTableModel extends CoordinatedTableModel<DimensionKey>
{
	@Override
	protected int getCoordinatedColumnCount()
	{
		return 4;
	}

	@Override
	protected String getCoordinatedColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Key Logical Name";
			case 1:
				return "Key Physical  Name";
			case 2:
				return "Dimension Name";
			case 3:
				return "Columns";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected Object getCoordinatedValueAt(DimensionKey dk, int row, int column)
	{
		switch (column)
		{
			case 0:
				return dk.logicalName();
			case 1:
				return dk.physicalName();
			case 2:
				return dk.dimension().logicalName();
			case 3:
				StringBuilder stb = new StringBuilder();

				int pass = 0;

				for (DatabaseColumn col : dk.columns())
				{
					if (pass != 0)
					{
						stb.append("; ");
					}

					pass++;

					stb.append(col.name());
					stb.append(": ");
					stb.append(col.jdbcTypeName());
				}

				return stb.toString();
			default:
				throw new IllegalArgumentException();
		}
	}
}
