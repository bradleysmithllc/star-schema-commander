package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.extended.button.WebSplitButton;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.JTableUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.QueryBuilder;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.List;

public class DataGridPanel extends AbstractUIComponent
{
	public static final int QUERY_LIMIT = 200;
	public static final Color ON_COLOR = new Color(235, 235, 235);
	public static final Color OFF_COLOR = new Color(212, 234, 205);

	private static final UUID uuid = UUID.randomUUID();
	private final Fact fact;
	private final DatabaseConnection connection;
	private final Runnable notifier;
	private DataGridNavigatorPanel dataGridNavigatorPanel;
	private DataGridHistoryView dataGridHistoryView;
	private final Runnable wrappedNotifier;
	private QueryTableModel dataTableGrid;
	private JTable factList;
	private JScrollPane jScrollPane;
	private JPanel container;

	private PersistedQuery currentPersistedQuery;
	private Query currentQuery;

	private JProgressBar queryProgressBar;
	private JFrame navigatorFrame;

	public DataGridPanel(final DatabaseConnection connection, Fact pfact, Runnable pnotifier)
	{
		this.fact = pfact;
		this.connection = connection;
		this.notifier = pnotifier;

		wrappedNotifier = new Runnable()
		{
			@Override
			public void run()
			{
				queryProgressBar.setVisible(false);

				if (notifier != null)
				{
					notifier.run();
				}
			}
		};
	}

	private void map(Map<FactDimensionKey, List<DataGridNavigator.IncludedAttribute>> attrMap, DataGridNavigator.IncludedAttribute attr)
	{
		FactDimensionKey factDimensionKey = attr.factDimensionKey();
		List<DataGridNavigator.IncludedAttribute> list = attrMap.get(factDimensionKey);

		if (list == null)
		{
			list = new ArrayList<DataGridNavigator.IncludedAttribute>();
			attrMap.put(factDimensionKey, list);
		}

		list.add(attr);
	}

	/**
	 * Use the given query as the current query.  Updates navigator and refreshes data grid.
	 * This query is already persisted, do not requery.
	 * @param query
	 */
	public void applyQuery(final PersistedQuery query)
	{
		// update the query criteria
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				dataGridNavigatorPanel.applyQuery(query);

				// update the grid
				dataTableGrid.updateQuery(query);
			}
		});
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
		final JFXPanel jfxPanel = new JFXPanel();

		EventUtils.runOnJFXEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				Node view = dataGridHistoryView.view();

				BorderPane borderpane = new BorderPane();
				borderpane.setCenter(view);

				jfxPanel.setScene(new Scene(borderpane));
			}
		});

		container.add(
			jfxPanel, BorderLayout.NORTH
		);

		new PersistedQuerySwingWorker(connection, currentQuery).execute();
	}

	@Override
	public void subInitialize() throws Exception
	{
		dataGridNavigatorPanel = new DataGridNavigatorPanel(new DataGridNavigatorPanel.NavigatorClient()
		{
			@Override
			public void update(DataGridNavigator navigator)
			{
				// add all measures
				List<DataGridNavigator.IncludedMeasure> meas = navigator.includeMeasures();

				final QueryBuilder qb = fact.newQuery().limit(QUERY_LIMIT);

				if (meas.size() != 0)
				{
					for (DataGridNavigator.IncludedMeasure im : meas)
					{
						qb.include(im.measure(), im.aggregation(), im.alias());
					}
				}
				else
				{
					qb.reallyNoMeasures();
				}

				// add all dimension attributes
				List<DataGridNavigator.IncludedAttribute> attributes = navigator.includeAttributes();

				// group them by fact dimension key since that is how it has to be done
				Map<FactDimensionKey, List<DataGridNavigator.IncludedAttribute>> attrMap = new HashMap<FactDimensionKey, List<DataGridNavigator.IncludedAttribute>>();

				for (DataGridNavigator.IncludedAttribute attr : attributes)
				{
					map(attrMap, attr);
				}

				// now read through and add every key and attribute
				for (Map.Entry<FactDimensionKey, List<DataGridNavigator.IncludedAttribute>> keyEntry : attrMap.entrySet())
				{
					qb.join(keyEntry.getKey());

					for (DataGridNavigator.IncludedAttribute attrItem : keyEntry.getValue())
					{
						qb.include(attrItem.attribute(), attrItem.aggregation(), attrItem.alias());
					}
				}

				try
				{
					DesktopUtils.executeOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							// update progress bar
							queryProgressBar.setIndeterminate(true);
							queryProgressBar.setVisible(true);

							currentQuery = qb.create();

							new PersistedQuerySwingWorker(connection, currentQuery).execute();
						}
					});
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				catch (InvocationTargetException e)
				{
					e.printStackTrace();
				}
			}
		}, fact);

		dataGridHistoryView = new DataGridHistoryView();
		dataGridHistoryView.setHistoryQueryListener(new DataGridHistoryView.HistoryQueryListener()
		{
			@Override
			public void selectQuery(final PersistedQuery query)
			{
				// this is the JFX event thread.  Break out so it can
				// run on the AWT event thread without blocking.
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						applyQuery(query);
					}
				}).start();
			}
		});

		addChild(dataGridNavigatorPanel);
		addChild(dataGridHistoryView);

		container = new JPanel();
		container.setLayout(new BorderLayout());

		dataTableGrid = new QueryTableModel(connection);
		factList = new JTable(dataTableGrid)
		{
			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
			{
				Component c = super.prepareRenderer(renderer, row, column);

				c.setBackground(row % 2 == 0 ? Color.white : OFF_COLOR);

				return c;
			}
		};

		factList.setGridColor(Color.lightGray);

		queryProgressBar = new JProgressBar();
		queryProgressBar.setVisible(true);

		jScrollPane = new JScrollPane(factList);

		jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "Exploring " + fact.logicalName()));
		container.add(jScrollPane, BorderLayout.CENTER);

		JPanel actionPanel = new JPanel();
		actionPanel.setLayout(new BorderLayout());

		actionPanel.add(queryProgressBar, BorderLayout.NORTH);

		final JButton navigate = new JButton("Navigate");
		navigate.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (navigatorFrame != null)
				{
					navigatorFrame.toFront();
				}
				else
				{
					navigatorFrame = new JFrame();
					navigatorFrame.setTitle("Navigator for " + fact.logicalName() + " in " + connection.name());

					navigatorFrame.getContentPane().setLayout(new BorderLayout());
					navigatorFrame.getContentPane().add(dataGridNavigatorPanel.getComponent(), BorderLayout.CENTER);
					navigatorFrame.pack();
					navigatorFrame.setVisible(true);
				}
			}
		});

		actionPanel.add(navigate, BorderLayout.CENTER);

		WebSplitButton webSplitButton = new WebSplitButton("Actions");
		WebPopupMenu actionMenu = new WebPopupMenu();
		WebMenuItem copySQLAction = new WebMenuItem("Copy SQL to Clipboard...");
		copySQLAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				StringSelection stringSelection = new StringSelection(currentQuery.query());
				Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
				clpbrd.setContents(stringSelection, null);
			}
		});
		actionMenu.add(copySQLAction);

		copySQLAction = new WebMenuItem("Copy Plain text Results to Clipboard...");
		copySQLAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				List<String> col = new ArrayList();
				for (int i = 0; i < dataTableGrid.getColumnCount(); i++)
				{
					col.add(dataTableGrid.getColumnName(i));
				}

				String stb = JTableUtils.exportResultsToText(dataTableGrid.tableData(), col);

				StringSelection stringSelection = new StringSelection(stb);
				Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
				clpbrd.setContents(stringSelection, null);
			}
		});
		actionMenu.add(copySQLAction);

		webSplitButton.setPopupMenu(actionMenu);

		actionPanel.add(webSplitButton, BorderLayout.EAST);

		container.add(actionPanel, BorderLayout.SOUTH);

		currentQuery = fact.newQuery().limit(QUERY_LIMIT).create();
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case FACT_PANEL_FACT_LIST:
				return (T) dataTableGrid;
			default:
				throw new IllegalArgumentException();
		}
	}

	private class PersistedQuerySwingWorker extends SwingWorker<PersistedQuery, Object>
	{
		private final DatabaseConnection connection;
		private final Query pCurrentQuery;

		public PersistedQuerySwingWorker(DatabaseConnection connection, Query currentQuery)
		{
			this.connection = connection;
			pCurrentQuery = currentQuery;
		}

		@Override
		protected PersistedQuery doInBackground() throws Exception
		{
			currentPersistedQuery = fact.memberOf().persist(pCurrentQuery, connection);
			dataGridHistoryView.addQuery(currentPersistedQuery);

			return currentPersistedQuery;
		}

		@Override
		protected void done()
		{
			wrappedNotifier.run();

			try
			{
				dataTableGrid.updateQuery(get());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
