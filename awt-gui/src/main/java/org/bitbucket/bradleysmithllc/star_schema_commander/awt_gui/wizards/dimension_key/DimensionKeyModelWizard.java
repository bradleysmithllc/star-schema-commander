package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardModelBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.NullUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.WizardColumnHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DimensionKeyModelWizard extends UIWizard
{
	public DimensionKeyModelWizard(Dimension dimension)
	{
		super(createModel(dimension));
	}

	private static WizardStateModel createModel(final Dimension dim)
	{
		final Map<String, String> colnames = new HashMap<String, String>();

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		final ArrayList<DatabaseColumn> databaseColumns = new ArrayList<DatabaseColumn>();

		WizardState<List<DatabaseColumn>, SelectColumnsUIComponent<Dimension>> selectColumns =
			wizardModelBuilder.newState((List<DatabaseColumn>) databaseColumns, new SelectColumnsUIComponent<Dimension>(dim, new WizardColumnHandler<Dimension>()
			{
				@Override
				public void beginHandling(Dimension dimension)
				{
				}

				@Override
				public void handle(Dimension dimension, String columnName, String logicalName)
				{
				}

				@Override
				public void endHandling(Dimension dimension)
				{
				}

				@Override
				public boolean include(Dimension fact, DatabaseColumn value)
				{
					return true;
				}
			})).id("SelectColumns").create();

		WizardState<Object, LogicalPhysicalUIComponent> selectNames =
			wizardModelBuilder.newState(new Object(), new LogicalPhysicalUIComponent()).id("NameKey").create();

		WizardState<Object, NullUIComponent> close =
			wizardModelBuilder.newState(new Object(), new NullUIComponent()).id("Close").create();

		WizardState<Object, NullUIComponent> process =
			wizardModelBuilder.newState(new Object(), new NullUIComponent()).id("Process").create();

		wizardModelBuilder.newTransition(selectColumns, selectNames).id("logical").logicalName("Next").create();

		wizardModelBuilder.newTransition(selectNames, process).id("process").logicalName("Process").handler(new StateTransitionHandler<Object, LogicalPhysicalUIComponent, Object, NullUIComponent>()
		{
			@Override
			public void transition(WizardState<Object, LogicalPhysicalUIComponent> from, WizardState<Object, NullUIComponent> to, WizardStateTransition<Object, LogicalPhysicalUIComponent, Object, NullUIComponent> transition)
			{
				String logName = from.component().getModel(LogicalPhysicalUIComponent.MOD_LOGICAL_NAME);
				String physName = from.component().getModel(LogicalPhysicalUIComponent.MOD_PHYSICAL_NAME);

				// create
				DimensionKey.DimensionKeyBuilder builder = dim.newDimensionKey().logicalName(logName).physicalName(physName);

				for (DatabaseColumn i : databaseColumns)
				{
					builder.column(i.name());
				}

				builder.create();

				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							UIHub.profile().persist();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
					}
				}).start();
			}
		}).create();

		wizardModelBuilder.newTransition(selectColumns, close).id("close").logicalName("Close").create();
		wizardModelBuilder.newTransition(selectNames, close).id("close").logicalName("Close").create();

		wizardModelBuilder.initial(selectColumns);

		return wizardModelBuilder.create();
	}
}
