package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;

public class FactDimensionKeyTableModel extends CoordinatedTableModel<Dimension>
{
	@Override
	protected int getCoordinatedColumnCount()
	{
		return 3;
	}

	@Override
	protected String getCoordinatedColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Logical Name";
			case 1:
				return "Schema";
			case 2:
				return "Physical Name";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected Object getCoordinatedValueAt(Dimension dim, int row, int column)
	{
		switch (column)
		{
			case 0:
				return dim.logicalName();
			case 1:
				return dim.schema();
			case 2:
				return dim.name();
			default:
				throw new IllegalArgumentException();
		}
	}
}