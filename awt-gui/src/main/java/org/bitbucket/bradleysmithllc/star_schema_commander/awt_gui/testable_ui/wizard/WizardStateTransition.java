package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface WizardStateTransition<F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>>
{
	String id();

	String logicalName();

	/**
	 * Enabled means the user can see the transition and it may be activated
	 * Disabled means the user can see the transition but it may not be activated
	 *
	 * @return
	 */
	boolean enabled();

	void enable(boolean b);

	/**
	 * Active means the user can see the transition and it may be activated
	 * Inactive means the user cannot see the transition at all
	 *
	 * @return
	 */
	boolean active();

	void activate(boolean b);

	WizardState<F, C> from();

	WizardState<T, D> follow();

	void addStateChangeListener(TransitionStateChangeListener<F, C, T, D> list);

	void removeStateChangeListener(TransitionStateChangeListener<F, C, T, D> list);

	interface TransitionStateChangeListener<F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>>
	{
		void transitionStateChanged(WizardStateTransition<F, C, T, D> transition);
	}
}
