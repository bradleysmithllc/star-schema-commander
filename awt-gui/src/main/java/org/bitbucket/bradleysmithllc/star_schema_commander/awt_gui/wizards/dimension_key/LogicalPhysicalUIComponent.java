package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class LogicalPhysicalUIComponent extends WizardStateUIComponent<Object>
{
	public static final String MSG_SET_LOGICAL_NAME = "MSG_SET_LOGICAL_NAME";
	public static final String MSG_SET_PHYSICAL_NAME = "MSG_SET_PHYSICAL_NAME";

	public static final int MOD_LOGICAL_NAME = 0;
	public static final int MOD_PHYSICAL_NAME = 1;

	private WizardStateTransition<Object, ? extends WizardStateUIComponent<Object>, ?, ?> nextTr;
	private JPanel container;

	private JTextField physicalName;
	private JTextField logicaName;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		// hide the finish step until we have visited all
		nextTr = state().transitionMap().get("next");

		container = new JPanel();
		container.setLayout(new BorderLayout());

		physicalName = new JTextField("KEY_NAME");

		logicaName = new JTextField("Key Name");

		container.add(physicalName, BorderLayout.CENTER);
		container.add(logicaName, BorderLayout.SOUTH);
	}

	@Override
	public <F> F getModel(int id)
	{
		switch (id)
		{
			case MOD_LOGICAL_NAME:
				return (F) logicaName.getText();
			case MOD_PHYSICAL_NAME:
				return (F) physicalName.getText();
		}

		throw new IllegalArgumentException();
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SET_LOGICAL_NAME)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						logicaName.setText(String.valueOf(value));
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SET_PHYSICAL_NAME)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						physicalName.setText(String.valueOf(value));
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}
}