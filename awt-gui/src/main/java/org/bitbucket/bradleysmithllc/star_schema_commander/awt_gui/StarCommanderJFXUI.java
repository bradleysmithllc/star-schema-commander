package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.StageUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;

import java.awt.*;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Semaphore;

public class StarCommanderJFXUI extends Application
{
	Semaphore paintingSemaphore;
	private StarCommanderUIView starCommanderUIView;
	private StageUIView frameUIView;

	/**
	 * Create the application.
	 */
	public StarCommanderJFXUI() throws Exception
	{
	}

	/**
	 * Launch the application.
	 */
	public void init() throws Exception
	{
		final Profile profile;

		File pd = new File(getParameters().getNamed().get("profile"));

		if (pd.exists())
		{
			profile = new Profile(pd);
		}
		else
		{
			profile = Profile.create(pd);
		}

		// set up the hub
		UIHub.init(new SystemUIController());

		UIHub.profile(profile);
		UIHub.selectReportingArea(profile.defaultReportingArea());
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		try
		{
			starCommanderUIView = new StarCommanderUIView();

			//frameUIView = new StageUIView(starCommanderUIView);

			// create the container and start the lifecycle
			finishBuildParent();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the stage.
	 */
	protected void finishBuildParent() throws Exception
	{
		UIContainerImpl container = new UIContainerImpl();

		container.addView(frameUIView);

		frameUIView.stage().setTitle("Star(schema) Commander");

		frameUIView.stage().sizeToScene();
		frameUIView.stage().setOnCloseRequest(new EventHandler<WindowEvent>()
		{
			@Override
			public void handle(WindowEvent windowEvent)
			{
				Platform.exit();
			}
		});
		frameUIView.stage().centerOnScreen();

		frameUIView.stage().show();

		// add an update listener to refresh the UI with profile changes
		UIHub.profile().addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				try
				{
					EventQueue.invokeAndWait(new Runnable()
					{
						@Override
						public void run()
						{
							UIHub.profileRefresh();

							// reselect reporting area if already working with one
							ReportingArea ra = UIHub.profile().defaultReportingArea();

							if (UIHub.selectedReportingArea() != null)
							{
								ReportingArea reportingArea = UIHub.profile().defaultReportingArea().reportingAreas().get(UIHub.selectedReportingArea().qualifiedName());

								if (reportingArea != null)
								{
									ra = reportingArea;
								}
							}

							UIHub.get().set("selectedReportingArea", ra);

							// send notification to listeners
							new Thread(new Runnable()
							{
								@Override
								public void run()
								{
									UIHub.uiRefresh();
								}
							}).start();
						}
					});
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				catch (InvocationTargetException e)
				{
					e.printStackTrace();
				}
			}

			@Override
			public void profileInitialized()
			{
			}
		});

		UIHub.selectReportingArea(UIHub.profile().defaultReportingArea());
	}

	public void waitForNextRepaint()
	{
		// set up seamphore
		paintingSemaphore = new Semaphore(1);
		paintingSemaphore.acquireUninterruptibly(1);

		// wait until paint method releases
		try
		{
			paintingSemaphore.acquireUninterruptibly(1);
		}
		finally
		{
			paintingSemaphore = null;
		}
	}
}
