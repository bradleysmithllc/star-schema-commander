package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;

import java.util.concurrent.CountDownLatch;

public abstract class AbstractUIController implements UIController
{
	@Override
	public String controllerId()
	{
		return getClass().getSimpleName();
	}

	protected void dispatch(CountDownLatch latch, String message, Object value)
	{
		dispatch(message, value);

		try
		{
			Thread.sleep(1500L);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		latch.countDown();
	}

	protected void dispatch(String message, Object value)
	{
		throw new UnsupportedOperationException();
	}

	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(message);

		try
		{
			Thread.sleep(1500L);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		latch.countDown();
	}

	protected void dispatch(String message)
	{
		dispatch(message, null);
	}

	public final CountDownLatch dispatchAction(final String message, final Object value)
	{
		final CountDownLatch latch = new CountDownLatch(1);

		UIHub.get().registerSystemLatch(this, message, latch);

		// do not allow this to run on an event thread
		if (EventUtils.isEventThread() || EventUtils.isJFXEventThread())
		{
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					dispatch(latch, message, value);
				}
			}).start();
		}
		else
		{
			dispatch(latch, message, value);
		}

		return latch;
	}

	public final CountDownLatch dispatchAction(final String message)
	{
		final CountDownLatch latch = new CountDownLatch(1);

		UIHub.get().registerSystemLatch(this, message, latch);

		// do not allow this to run on an event thread
		if (EventUtils.isEventThread() || EventUtils.isJFXEventThread())
		{
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					dispatch(latch, message);
				}
			}).start();
		}
		else
		{
			dispatch(latch, message);
		}

		return latch;
	}

	@Override
	public final void dispatchActionAndWait(String message, Object value)
	{
		try
		{
			if (EventUtils.isEventThread() || EventUtils.isJFXEventThread())
			{
				System.out.println("Blocking event thread.");
			}

			dispatchAction(message, value).await();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public final void dispatchActionAndWait(String message)
	{
		try
		{
			if (EventUtils.isEventThread() || EventUtils.isJFXEventThread())
			{
				System.out.println("Blocking event thread.");
			}

			dispatchAction(message).await();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean checkState(int value)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T getModel(int id)
	{
		throw new UnsupportedOperationException();
	}
}
