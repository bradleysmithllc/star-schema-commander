package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DimensionAttributeNavigatorTableModel extends CoordinatedTableModel<DataGridNavigator.IncludedAttribute>
{
	@Override
	public String getCoordinatedColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Logical Name";
			case 1:
				return "Data Type";
			case 2:
				return "Dimension";
			case 3:
				return "Role";
			case 4:
				return "Alias";
			case 5:
				return "Include";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected Class<?> getCoordinatedColumnClass(int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
				return String.class;
			case 5:
				return Query.aggregation.class;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getCoordinatedColumnCount()
	{
		return 6;
	}

	@Override
	public Object getCoordinatedValueAt(DataGridNavigator.IncludedAttribute td, int rowIndex, int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
				return td.attribute().logicalName();
			case 1:
				return td.attribute().column().jdbcTypeName();
			case 2:
				return td.attribute().in().logicalName();
			case 3:
				if (td.factDimensionKey().rolePlaying())
				{
					return td.factDimensionKey().roleName();
				}
				else
				{
					return "";
				}
			case 4:
				return td.alias();
			case 5:
				return td.aggregation();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void setCoordinatedValueAt(DataGridNavigator.IncludedAttribute td, Object aValue, final int rowIndex, int columnIndex)
	{
		switch (columnIndex)
		{
			case 4:
				td.alias(String.valueOf(aValue));
				break;
			case 5:
				if (aValue == null)
				{
					// drop the row
					visit(new ModelVisitor<DataGridNavigator.IncludedAttribute>()
					{
						@Override
						public void visit(UpdatableTableModel<DataGridNavigator.IncludedAttribute> model)
						{
							model.removeElementAt(rowIndex);
						}
					});
				}
				else
				{
					td.aggregation((Query.aggregation) aValue);
				}
				break;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public boolean isCoordinatedCellEditable(int rowIndex, int columnIndex)
	{
		return columnIndex == 4 || columnIndex == 5;
	}

	public List<DataGridNavigator.IncludedAttribute> selectedMeasures()
	{
		List<DataGridNavigator.IncludedAttribute> alist = new ArrayList<DataGridNavigator.IncludedAttribute>(tableData());

		Iterator<DataGridNavigator.IncludedAttribute> it = alist.iterator();

		// remove all excluded measures
		DataGridNavigator.IncludedAttribute sm = null;

		while (it.hasNext())
		{
			sm = it.next();

			if (sm.aggregation() == null)
			{
				it.remove();
			}
		}

		return alist;
	}

	public int locateMeasure(DimensionAttribute fm)
	{
		int row = 0;

		for (DataGridNavigator.IncludedAttribute td : tableData())
		{
			if (td.attribute() == fm)
			{
				return row;
			}

			row++;
		}

		throw new IllegalArgumentException();
	}
}
