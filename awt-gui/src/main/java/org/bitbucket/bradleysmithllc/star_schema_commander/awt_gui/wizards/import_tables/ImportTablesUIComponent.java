package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.laf.progressbar.WebProgressBar;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.QualifiedTable;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class ImportTablesUIComponent extends WizardStateUIComponent<ImportSpecification>
{
	public static final String MSG_WAIT_FOR_IMPORT_PROCESS = "MSG_WAIT_FOR_IMPORT_PROCESS";
	private final CountDownLatch waiter = new CountDownLatch(1);
	ImportTableModelWizard.target _target;
	private WizardStateTransition<ImportSpecification, ? extends WizardStateUIComponent<ImportSpecification>, ?, ?> done;
	private JPanel container;
	private Map<QualifiedTable, String> tableMap;
	private JLabel tablePhysicalName;

	public ImportTablesUIComponent(ImportTableModelWizard.target target)
	{
		_target = target;
	}

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		// hide the finish step until we have visited all
		done = state().transitionMap().get("close");
		done.enable(false);

		tableMap = state().model().tableMap();

		final WebProgressBar progressBar1 = new WebProgressBar(WebProgressBar.HORIZONTAL, 1, tableMap.size());

		progressBar1.setValue(0);
		progressBar1.setIndeterminate(false);

		container = new JPanel();
		container.setLayout(new BorderLayout());

		tablePhysicalName = new JLabel("Table");

		container.add(tablePhysicalName, BorderLayout.CENTER);
		container.add(progressBar1, BorderLayout.SOUTH);

		// start background process to load the tables
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				int count = 0;

				try
				{
					for (final Map.Entry<QualifiedTable, String> tblSpec : tableMap.entrySet())
					{
						// import the dimension
						try
						{
							DatabaseConnection databaseConnection = state().model().databaseConnection();

							switch (_target)
							{
								case dimension:
									UIHub.selectedReportingArea().loadDimensionFromMetaData(databaseConnection, tblSpec.getKey().catalog(), tblSpec.getKey().schema(), tblSpec.getKey().name(), tblSpec.getValue());
									break;
								case fact:
									UIHub.selectedReportingArea().loadFactFromMetaData(databaseConnection, tblSpec.getKey().catalog(), tblSpec.getKey().schema(), tblSpec.getKey().name(), tblSpec.getValue());
									break;
							}

							EventQueue.invokeAndWait(new Runnable()
							{
								@Override
								public void run()
								{
									tablePhysicalName.setText(tblSpec.getKey().qualifiedName() + " >> " + tblSpec.getValue());
								}
							});

							Thread.sleep(200L);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}

						progressBar1.setValue(++count);
					}

					((Profile) profile()).persist();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				finally
				{
					done.enable(true);

					// signal completion
					waiter.countDown();
				}
			}
		}).start();
	}

	@Override
	public void dispatch(String message, Object value)
	{
		if (message == MSG_WAIT_FOR_IMPORT_PROCESS)
		{
			try
			{
				waiter.await();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}
}
