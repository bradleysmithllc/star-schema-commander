package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.StarCommanderUI;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ConnectionCellRenderer;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;

public class ImportDimensionWizard
{
	private final StarCommanderUI commander;

	private JFrame dialog;
	private JButton nextButton;
	private DatabaseConnection selectedDatabaseConnection;

	private int state = 0;

	public ImportDimensionWizard(StarCommanderUI commander)
	{
		this.commander = commander;
	}

	public void show()
	{
		state1();
	}

	private void state1()
	{
		state = 1;
		dialog = new JFrame();

		dialog.getContentPane().setLayout(new BorderLayout());

		DefaultListModel<DatabaseConnection> dmodel = new DefaultListModel<DatabaseConnection>();

		for (DatabaseConnection conn : commander.getProfile().connections().values())
		{
			dmodel.add(dmodel.size(), conn);
		}

		final JList<DatabaseConnection> comp = new JList<DatabaseConnection>(dmodel);
		comp.setBorder(BorderFactory.createTitledBorder("Select a connection"));
		comp.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		comp.setCellRenderer(new ConnectionCellRenderer());
		comp.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				nextButton.setEnabled(true);
			}
		});

		dialog.getContentPane().add(comp, BorderLayout.CENTER);

		nextButton = new JButton("<html><a href=''>Next</a></html>");
		nextButton.setEnabled(false);
		nextButton.setBorderPainted(false);
		nextButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (state == 1)
				{
					selectedDatabaseConnection = comp.getSelectedValue();
					dialog.getContentPane().remove(comp);
					state2();
				}
				else if (state == 2)
				{
					state3();
				}
			}
		});

		JPanel comp1 = new JPanel();
		comp1.setLayout(new BorderLayout());
		dialog.getContentPane().add(comp1, BorderLayout.SOUTH);

		comp1.add(nextButton, BorderLayout.EAST);

		dialog.pack();
		dialog.setVisible(true);
	}

	private void state2()
	{
		state = 2;
		nextButton.setEnabled(false);

		// we now have a connection.  List the objects
		final DefaultListModel<String> selectedTablesModel = new DefaultListModel<String>();
		final DefaultListModel<String> availableTablesModel = new DefaultListModel<String>();

		new SwingWorker()
		{
			private final java.util.List<String> names = new ArrayList<String>();

			@Override
			protected Object doInBackground() throws Exception
			{
				try
				{
					Connection conn = selectedDatabaseConnection.open();

					try
					{
						DatabaseMetaData dbmd = conn.getMetaData();

						ResultSet mdq = dbmd.getTables(null, null, null, null);

						try
						{
							while (mdq.next())
							{
								String schema = mdq.getString(2);
								String table = mdq.getString(3);

								names.add(schema + "." + table);
							}
						}
						finally
						{
							mdq.close();
						}
					}
					finally
					{
						conn.close();
					}
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void done()
			{
				for (String name : names)
				{
					availableTablesModel.add(availableTablesModel.size(), name);
				}
			}
		}.execute();

		final JList<String> availableTablesList = new JList<String>(availableTablesModel);
		availableTablesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		JScrollPane comp1 = new JScrollPane(availableTablesList);
		comp1.setBorder(BorderFactory.createTitledBorder("Select a table"));

		dialog.getContentPane().add(comp1, BorderLayout.WEST);

		final JList<String> selectedTablesList = new JList<String>(selectedTablesModel);
		selectedTablesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		comp1 = new JScrollPane(selectedTablesList);
		comp1.setBorder(BorderFactory.createTitledBorder("Selected tables"));

		dialog.getContentPane().add(comp1, BorderLayout.EAST);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(4, 0));

		final JButton removeSelected = new JButton("<html><a href=''>&lt;</a></html>");
		final JButton removeAll = new JButton("<html><a href=''>&lt;&lt;</a></html>");
		final JButton chooseAll = new JButton("<html><a href=''>&gt;&gt;</a></html>");
		final JButton chooseSelected = new JButton("<html><a href=''>&gt;</a></html>");

		buttonPanel.add(removeSelected);
		removeSelected.setEnabled(false);
		removeSelected.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeSelected(selectedTablesList, selectedTablesModel, availableTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAll, removeSelected);
				updateButtonStates(availableTablesModel, availableTablesList, chooseAll, chooseSelected);
				nextButton.setEnabled(selectedTablesModel.size() != 0);
			}
		});

		buttonPanel.add(removeAll);
		removeAll.setEnabled(false);
		removeAll.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeList(selectedTablesModel, availableTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAll, removeSelected);
				updateButtonStates(availableTablesModel, availableTablesList, chooseAll, chooseSelected);
				nextButton.setEnabled(selectedTablesModel.size() != 0);
			}
		});

		buttonPanel.add(chooseAll);
		chooseAll.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeList(availableTablesModel, selectedTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAll, removeSelected);
				updateButtonStates(availableTablesModel, availableTablesList, chooseAll, chooseSelected);
				nextButton.setEnabled(selectedTablesModel.size() != 0);
			}
		});

		chooseSelected.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeSelected(availableTablesList, availableTablesModel, selectedTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAll, removeSelected);
				updateButtonStates(availableTablesModel, availableTablesList, chooseAll, chooseSelected);
				nextButton.setEnabled(selectedTablesModel.size() != 0);
			}
		});
		buttonPanel.add(chooseSelected);

		// add listeners to keep the middle buttons honest
		selectedTablesList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAll, removeSelected);
			}
		});

		availableTablesList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				updateButtonStates(availableTablesModel, availableTablesList, chooseAll, chooseSelected);
			}
		});
		dialog.getContentPane().add(buttonPanel, BorderLayout.CENTER);

		// replace the action listener on the next button with this one
		dialog.invalidate();
		dialog.pack();
		dialog.repaint();
	}

	private void state3()
	{

	}

	private void updateButtonStates(DefaultListModel<String> selectedTablesModel, JList<String> selectedTablesList, JButton removeAll, JButton removeSelected)
	{
		boolean isRemoveAll = true;
		boolean isRemoveSelected = true;

		if (selectedTablesModel.size() == 0)
		{
			isRemoveAll = false;
			isRemoveSelected = false;
		}
		else
		{
			if (selectedTablesList.isSelectionEmpty())
			{
				isRemoveSelected = false;
			}
		}

		removeAll.setEnabled(isRemoveAll);
		removeSelected.setEnabled(isRemoveSelected);
	}

	private void purgeSelected(JList<String> selectedTablesList, DefaultListModel<String> selectedTablesModel, DefaultListModel<String> availableTablesModel)
	{
		for (String table : selectedTablesList.getSelectedValuesList())
		{
			selectedTablesModel.removeElement(table);
			availableTablesModel.add(availableTablesModel.size(), table);
		}
	}

	private void purgeList(DefaultListModel<String> fromModel, DefaultListModel<String> toModel)
	{
		java.util.List<String> names = new ArrayList<String>();

		Enumeration<String> enums = fromModel.elements();

		while (enums.hasMoreElements())
		{
			names.add(enums.nextElement());
		}

		for (String table : names)
		{
			fromModel.removeElement(table);
			toModel.add(toModel.size(), table);
		}
	}
}