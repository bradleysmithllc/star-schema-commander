package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.IProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class NavPanel extends AbstractUIComponent
{
	public static final String MSG_SELECT_REPORTING_AREA = "MSG_SELECT_REPORTING_AREA";
	public static final String MSG_SELECT_CONNECTION = "MSG_SELECT_CONNECTION";

	private final DefaultListModel<ReportingArea> reportingAreaListModel = new DefaultListModel<ReportingArea>();
	private final DefaultListModel<DatabaseConnection> connectionListModel = new DefaultListModel<DatabaseConnection>();

	private JPanel navPanel;
	private JList<DatabaseConnection> connectionJList;
	private JList<ReportingArea> reportingAreaJList;
	private JLabel reportingAreaLabel;
	private JScrollPane reportingAreaPane;

	@Override
	protected void buildParent(Profile profile)
	{
		navPanel = new JPanel();
		navPanel.setLayout(new BoxLayout(navPanel, BoxLayout.Y_AXIS));
		navPanel.setBackground(new Color(247, 243, 242));

		JLabel actions = new JLabel("Connections");
		actions.setFont(Font.getFont("Lucidia Grande-BOLD"));
		actions.setForeground(new Color(199, 199, 199));
		navPanel.add(actions);

		connectionListModel.clear();
		connectionJList = new JList<DatabaseConnection>(connectionListModel);
		connectionJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		connectionJList.setCellRenderer(new ConnectionCellRenderer());

		connectionJList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (connectionJList.isSelectionEmpty())
				{
					UIHub.selectConnection(null);
				}
				else
				{
					UIHub.selectConnection(connectionJList.getSelectedValue());
				}
			}
		});
		navPanel.add(new JScrollPane(connectionJList), Component.RIGHT_ALIGNMENT);

		JLabel blank = new JLabel(" ");
		navPanel.add(blank);

		reportingAreaLabel = new JLabel("Reporting Areas");
		reportingAreaLabel.setFont(Font.getFont("Lucidia Grande"));
		reportingAreaLabel.setForeground(new Color(199, 199, 199));
		reportingAreaLabel.setVisible(false);

		navPanel.add(reportingAreaLabel);

		reportingAreaJList = new JList<ReportingArea>();
		reportingAreaJList.setCellRenderer(new ReportingAreaCellRenderer());

		reportingAreaPane = new JScrollPane(reportingAreaJList);
		navPanel.add(reportingAreaPane);
		reportingAreaListModel.clear();

		reportingAreaJList.setModel(reportingAreaListModel);

		reportingAreaJList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				publish();
			}
		});

		updateReportingArea(UIHub.selectedReportingArea());
		initConnectionList(profile);
	}

	private void publish()
	{
		if (!reportingAreaJList.isSelectionEmpty())
		{
			ReportingArea ra = reportingAreaJList.getSelectedValue();
			UIHub.selectReportingArea(ra);
		}
		else
		{
			// not sure what to do here.  Don't want to select default because that will cause a LOT
			// of extra repainting
		}
	}

	@Override
	public JComponent getComponent()
	{
		return navPanel;
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case NAV_PANEL_REPORTING_AREAS_VISIBLE:
				boolean reportingAreaPaneVisible = reportingAreaPane.isVisible();
				boolean reportingAreaLabelVisible = reportingAreaLabel.isVisible();
				return reportingAreaPaneVisible || reportingAreaLabelVisible;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case NAV_PANEL_CONNECTION_LIST:
				return (T) connectionListModel;
			case NAV_PANEL_REPORTING_AREA_LIST:
				return (T) reportingAreaListModel;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		if (message == MSG_SELECT_REPORTING_AREA)
		{
			final ReportingArea area = (ReportingArea) value;

			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						// select the entry in the list
						reportingAreaJList.setSelectedValue(area, true);

						reportingAreaJList.revalidate();
						reportingAreaJList.repaint();

						publish();

						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_SELECT_CONNECTION)
		{
			final DatabaseConnection conn = (DatabaseConnection) value;

			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						// select the entry in the list
						connectionJList.setSelectedValue(conn, true);

						connectionJList.revalidate();
						connectionJList.repaint();

						publish();

						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void stateChanged(stateKey key, final Object value)
	{
		switch (key)
		{
			case selectedReportingArea:
				if (value == null)
				{
					throw new IllegalArgumentException("Null RA");
				}

				try
				{
					Runnable runnable = new Runnable()
					{
						@Override
						public void run()
						{
							updateReportingArea((ReportingArea) value);
						}
					};

					runOnEventThread(runnable);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
			case profile:
			case profileRefresh:
				Runnable runnable = new Runnable()
				{
					@Override
					public void run()
					{
						initConnectionList(UIHub.profile());
						updateReportingArea(profile().defaultReportingArea());
					}
				};

				try
				{
					runOnEventThread(runnable);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
		}
	}

	private void updateReportingArea(ReportingArea value)
	{
		requireEventThread();

		// populate the model with the children of this ra
		reportingAreaListModel.removeAllElements();

		if (value != null)
		{
			for (ReportingArea rac : value.children().values())
			{
				reportingAreaListModel.add(reportingAreaListModel.size(), rac);
			}
		}

		boolean visible = reportingAreaListModel.size() != 0;
		reportingAreaLabel.setVisible(visible);
		reportingAreaPane.setVisible(visible);
	}

	private void initConnectionList(IProfile profile)
	{
		connectionListModel.removeAllElements();
		for (Map.Entry<String, DatabaseConnection> con : profile.connections().entrySet())
		{
			connectionListModel.add(connectionListModel.size(), con.getValue());
		}
	}
}
