package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class FactMeasurePanel extends AbstractUIComponent
{
	public static final int FACT_MEASURE_PANEL_FACT_MEASURE_LIST = 0;

	private final FactMeasureTableModel factListModel = new FactMeasureTableModel();

	private JTable factList = new JTable(factListModel);
	private JScrollPane jScrollPane;

	@Override
	protected void buildParent(Profile profile)
	{
		factList = new JTable(factListModel);
		factList.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		jScrollPane = new JScrollPane(factList);

		jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "Measures"));

		updateFact();
	}

	@Override
	public JComponent getComponent()
	{
		return jScrollPane;
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case FACT_MEASURE_PANEL_FACT_MEASURE_LIST:
				return (T) factListModel;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void stateChanged(stateKey key, Object value)
	{
		try
		{
			runOnEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					updateFact();
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void updateFact()
	{
		requireEventThread();
		// populate the model with the children of this ra
		factListModel.clear();

		Fact fact = UIHub.selectedFact();
		if (fact != null)
		{
			for (Map.Entry<String, FactMeasure> rac : fact.measures().entrySet())
			{
				factListModel.addMeasure(rac.getValue());
			}
		}
	}
}