package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.swing.*;
import java.awt.*;

public class JFrameUIView extends AbstractUIView<JComponent>
{
	public static final int STATE_VISIBLE = 0;
	private final String instanceName;
	private UIView<JComponent> view;
	private JFrame frame;

	public JFrameUIView(UIView<JComponent> view, String instanceName)
	{
		this(instanceName);
		setView(view);
	}

	public JFrameUIView(String instanceName)
	{
		this.instanceName = instanceName;
	}

	public static void requireEventThread()
	{
		if (!EventQueue.isDispatchThread())
		{
			throw new RuntimeException();
		}
	}

	public static void runOnEventThread(Runnable run) throws Exception
	{
		if (EventQueue.isDispatchThread())
		{
			run.run();
		}
		else
		{
			EventQueue.invokeAndWait(run);
		}
	}

	@Override
	public String viewInstance()
	{
		return instanceName;
	}

	public void setView(UIView<JComponent> view)
	{
		this.view = view;
		clearChildViews();
		addChild(view);
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_VISIBLE:
				return frame != null && frame.isVisible();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void activate() throws Exception
	{
		runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				frame = new JFrame();
			}
		});
	}

	@Override
	public void prepareSelf()
	{
		if (frame.isVisible())
		{
			frame.setVisible(false);
		}
	}

	@Override
	public void prepareWithChildren()
	{
		// add child view to the center portion of this pane
		Container contentPane = frame.getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(view.view(), BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.pack();
	}

	@Override
	public JComponent view()
	{
		frame.setVisible(true);
		return null;
	}

	@Override
	public void deactivate()
	{
		frame.dispose();
	}

	public JFrame frame()
	{
		return frame;
	}
}
