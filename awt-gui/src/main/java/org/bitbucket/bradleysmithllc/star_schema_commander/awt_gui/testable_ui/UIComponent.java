package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIStates;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;

public interface UIComponent extends UIModels, UIStates, UIController
{
	int NAV_PANEL = 0;
	int BREADCRUMB_PANEL = 1;
	int ACTION_PANEL = 2;
	int FACT_DIM_PANEL = 3;
	int FACT_PANEL = 4;
	int DIMENSION_PANEL = 5;

	void build(Profile profile) throws Exception;

	JComponent getComponent();

	/**
	 * The identity of this component within it's type.
	 *
	 * @return
	 */
	String componentInstance();

	/**
	 * Locates a ui component in this tree by type, and optionally, id.
	 *
	 * @param instance the instance, if required.  Optional.
	 * @return
	 */
	<T extends UIView> T locate(String clType, String instance);

	/**
	 * Used when the class name and type name are the same.  Probably the case most of the time.
	 *
	 * @param clType
	 * @param <T>
	 * @return
	 */
	<T extends UIView> T locate(String clType);
}