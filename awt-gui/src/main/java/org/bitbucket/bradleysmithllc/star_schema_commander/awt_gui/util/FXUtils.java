package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.net.URL;

public class FXUtils
{

	/**
	 * Find a {@link Node} within a {@link Parent} by it's ID.
	 * <p/>
	 * This might not cover all possible {@link Parent} implementations but it's
	 * a decent crack. {@link Control} implementations all seem to have their
	 * own method of storing children along side the usual
	 * {@link Parent#getChildrenUnmodifiable()} method.
	 *
	 * @param node The parent of the node you're looking for.
	 * @param id   The ID of node you're looking for.
	 * @return The {@link Node} with a matching ID or {@code null}.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getChildByID(Node node, String id)
	{
		if (node == null)
		{
			return null;
		}

		String nodeId = node.getId();

		if (nodeId != null && nodeId.equals(id))
		{
			return (T) node;
		}

		if (node instanceof TitledPane)
		{
			TitledPane titledPane = (TitledPane) node;
			Node content = titledPane.getContent();
			nodeId = content.idProperty().get();

			if (nodeId != null && nodeId.equals(id))
			{
				return (T) content;
			}

			if (content instanceof Parent)
			{
				T child = getChildByID((Parent) content, id);

				if (child != null)
				{
					return child;
				}
			}
		}

		if (node instanceof Parent)
		{
			Parent parent = (Parent) node;

			for (Node cnode : parent.getChildrenUnmodifiable())
			{
				nodeId = cnode.idProperty().get();
				if (nodeId != null && nodeId.equals(id))
				{
					return (T) cnode;
				}

				if (cnode instanceof SplitPane)
				{
					SplitPane splitPane = (SplitPane) cnode;
					for (Node itemNode : splitPane.getItems())
					{
						nodeId = itemNode.idProperty().get();

						if (nodeId != null && nodeId.equals(id))
						{
							return (T) itemNode;
						}

						if (itemNode instanceof Parent)
						{
							T child = getChildByID((Parent) itemNode, id);

							if (child != null)
							{
								return child;
							}
						}
					}
				}
				else if (cnode instanceof Accordion)
				{
					Accordion accordion = (Accordion) cnode;
					for (TitledPane titledPane : accordion.getPanes())
					{
						nodeId = titledPane.idProperty().get();

						if (nodeId != null && nodeId.equals(id))
						{
							return (T) titledPane;
						}

						T child = getChildByID(titledPane, id);

						if (child != null)
						{
							return child;
						}
					}
				}
				else if (cnode instanceof SplitMenuButton)
				{
					SplitMenuButton splitMenuButton = (SplitMenuButton) cnode;
					for (MenuItem titledPane : splitMenuButton.getItems())
					{
						nodeId = titledPane.idProperty().get();

						if (nodeId != null && nodeId.equals(id))
						{
							return (T) titledPane;
						}
					}
				}
				else if (cnode instanceof Parent)
				{
					T child = getChildByID(cnode, id);

					if (child != null)
					{
						return child;
					}
				}
			}
		}

		return null;
	}

	public static <T, Y> Pair<Y, T> loadNodeGraph(String uri)
	{
		return loadNodeGraph(FXUtils.class.getClassLoader().getResource(uri));
	}

	public static <T, Y> Pair<Y, T> loadNodeGraph(final URL url)
	{
		try
		{
			FXMLLoader fxmlLoader = new FXMLLoader(url);
			Y l = fxmlLoader.load();
			T t = fxmlLoader.getController();

			return ImmutablePair.of(l, t);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
}