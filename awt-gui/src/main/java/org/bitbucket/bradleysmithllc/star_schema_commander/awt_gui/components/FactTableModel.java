package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Visitor;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class FactTableModel extends AbstractTableModel
{
	private final TreeMap<String, Fact> tableMap = new TreeMap<String, Fact>();
	private final List<Fact> tableData = new ArrayList<Fact>();

	private boolean updating = false;

	@Override
	public int getRowCount()
	{
		return size();
	}

	@Override
	public String getColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Logical Name";
			case 1:
				return "Schema";
			case 2:
				return "Name";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getColumnCount()
	{
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if (updating())
		{
			return null;
		}

		Fact td = tableData.get(rowIndex);

		switch (columnIndex)
		{
			case 0:
				return td.logicalName();
			case 1:
				return td.schema();
			case 2:
				return td.name();
			default:
				throw new IllegalArgumentException();
		}
	}

	public synchronized void update(Visitor<FactTableModel> visitor)
	{
		try
		{
			updating = true;

			visitor.visit(this);
		}
		finally
		{
			updating = false;
		}
	}

	private synchronized boolean updating()
	{
		return updating;
	}

	public void addFact(Fact fm)
	{
		tableData.add(fm);
		tableMap.put(fm.logicalName(), fm);

		Collections.sort(tableData);

		fireTableDataChanged();
	}

	public int size()
	{
		if (updating())
		{
			return 0;
		}

		return tableData.size();
	}

	public List<Fact> tableData()
	{
		return tableData;
	}

	public boolean isEmpty()
	{
		return tableData.isEmpty();
	}

	public void clear()
	{
		tableData().clear();
	}
}
