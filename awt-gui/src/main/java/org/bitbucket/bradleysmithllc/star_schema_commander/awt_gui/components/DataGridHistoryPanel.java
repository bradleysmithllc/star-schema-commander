package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class DataGridHistoryPanel extends AbstractUIComponent
{
	public static final int STATE_NEXT_ENABLED = 0;
	public static final int STATE_PREVIOUS_ENABLED = 1;
	public static final String MSG_NEXT = "MSG_NEXT";
	public static final String MSG_PREVIOUS = "MSG_PREVIOUS";
	private JPanel buttonPanel;
	private JButton next;
	private JButton previous;
	private List<PersistedQuery> history = new ArrayList<PersistedQuery>();
	private int currentOffset;
	private HistoryQueryListener listener;

	@Override
	public void subInitialize() throws Exception
	{
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		previous = new JButton("<<");
		previous.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (listener != null)
				{
					if (currentOffset > 0)
					{
						currentOffset--;
						listener.selectQuery(history.get(currentOffset));
					}
				}

				updateButtonStates();
			}
		});

		buttonPanel.add(previous, BorderLayout.WEST);
		next = new JButton(">>");
		next.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (listener != null)
				{
					if (currentOffset < history.size())
					{
						currentOffset++;
						listener.selectQuery(history.get(currentOffset));
					}
				}

				updateButtonStates();
			}
		});

		buttonPanel.add(next, BorderLayout.EAST);
	}

	@Override
	public void activate()
	{
		updateButtonStates();
	}

	@Override
	public JComponent getComponent()
	{
		return buttonPanel;
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_NEXT_ENABLED:
				return next.isEnabled();
			case STATE_PREVIOUS_ENABLED:
				return previous.isEnabled();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		if (message == MSG_NEXT)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						next.doClick();
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_PREVIOUS)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						previous.doClick();
						latch.countDown();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	public void addQuery(PersistedQuery q)
	{
		// when adding a new query, if there is any history beyond the current spot,
		// clear it out before adding this one
		while (currentOffset < (history.size()))
		{
			history.remove(history.size() - 1);
		}

		history.add(q);

		// advance the current query pointer to the end
		currentOffset = history.size();

		updateButtonStates();
	}

	private void updateButtonStates()
	{
		try
		{
			runOnEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					previous.setEnabled(false);
					next.setEnabled(false);

					if (history.size() > 0)
					{
						if (currentOffset != 0)
						{
							previous.setEnabled(true);
						}

						if (currentOffset < history.size())
						{
							// next should be enabled
							next.setEnabled(true);
						}
					}
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public List<PersistedQuery> history()
	{
		return Collections.unmodifiableList(history);
	}

	public void setHistoryQueryListener(HistoryQueryListener list)
	{
		listener = list;
	}

	public interface HistoryQueryListener
	{
		void selectQuery(PersistedQuery query);
	}
}
