package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.extended.breadcrumb.WebBreadcrumb;
import com.alee.extended.breadcrumb.WebBreadcrumbButton;
import com.alee.extended.button.WebSplitButton;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.menu.WebPopupMenu;
import com.alee.managers.hotkey.Hotkey;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ConnectionCellRenderer;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ReportingAreaCellRenderer;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Finder
{
	private final File profileDir;
	DefaultListModel<ReportingArea> defaultListModel;
	DefaultListModel<DatabaseConnection> connListModel;
	private JFrame frame;
	private JPanel rootPanel;
	private JPanel headerPanel;
	private JPanel navPanel;
	private Profile profile;
	private UIUpdateListener updateListener;

	/**
	 * Create the application.
	 */
	public Finder() throws IOException
	{
		this(new File(System.getProperty("user.home"), ".sscommander"));
	}

	public Finder(File profile) throws IOException
	{
		this.profileDir = profile;
		launch();
	}

	public static void main(String[] argv) throws IOException
	{
		new Finder();
	}

	/**
	 * Launch the application.
	 */
	public void launch()
	{
		final Semaphore s = new Semaphore(1);
		s.acquireUninterruptibly(1);

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					initialize();

					frame.addWindowStateListener(new WindowStateListener()
					{
						@Override
						public void windowStateChanged(WindowEvent e)
						{
							if (e.getNewState() == WindowEvent.WINDOW_OPENED)
							{
								s.release(1);
							}
						}
					});
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});

		try
		{
			s.tryAcquire(1, 10000L, TimeUnit.MILLISECONDS);
		}
		catch (InterruptedException e)
		{
			System.exit(1);
		}
	}

	/**
	 * Initialize the contents of the stage.
	 */
	private void initialize() throws IOException
	{
		profile = null;

		if (profileDir.exists())
		{
			profile = new Profile(profileDir);
		}
		else
		{
			profile = Profile.create(profileDir);
		}

		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new BorderLayout());
		rootPanel = new JPanel();
		frame.add(rootPanel, BorderLayout.CENTER);

		rootPanel.setBackground(Color.white);

		rootPanel.setLayout(new BorderLayout());
		headerPanel = new JPanel();
		rootPanel.add(headerPanel, BorderLayout.NORTH);
		headerPanel.setBackground(new Color(214, 212, 214));
		headerPanel.add(new JLabel("Header"), BorderLayout.CENTER);

		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		rootPanel.add(centerPanel, BorderLayout.CENTER);

		JPanel center2Panel = new JPanel();
		center2Panel.setLayout(new BorderLayout());
		centerPanel.add(center2Panel, BorderLayout.NORTH);

		WebBreadcrumb wbc = new WebBreadcrumb(true);
		wbc.add(new WebBreadcrumbButton("[default]"));
		wbc.add(new WebBreadcrumbButton("EDW"));
		WebBreadcrumbButton comp = new WebBreadcrumbButton("ODS");
		comp.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.out.println("3");
			}
		});
		wbc.add(comp);
		wbc.add(new WebBreadcrumbButton("PEEP"));

		center2Panel.add(wbc, BorderLayout.SOUTH);

		WebSplitButton smb = new WebSplitButton("Actions");
		final WebPopupMenu popupMenu = new WebPopupMenu();
		popupMenu.add(new WebMenuItem("Menu item 1", WebLookAndFeel.getIcon(16), Hotkey.ALT_X));
		popupMenu.add(new WebMenuItem("Menu item 2", Hotkey.D));
		popupMenu.addSeparator();
		popupMenu.add(new WebMenuItem("Menu item 3", Hotkey.ESCAPE));
		smb.setPopupMenu(popupMenu);
		headerPanel.add(smb, BorderLayout.EAST);

		navPanel = new JPanel();
		navPanel.setLayout(new BoxLayout(navPanel, BoxLayout.Y_AXIS));
		rootPanel.add(navPanel, BorderLayout.WEST);
		navPanel.setBackground(new Color(247, 243, 242));

		initNavPanel();

		// add an update listener to refresh the UI with profile changes
		profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				EventQueue.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						System.out.println("Refresh nav");
						initRaList();
						initConnectionList();
						frame.repaint();

						// send notification to listeners
						new Thread(new Runnable()
						{
							@Override
							public void run()
							{
								if (updateListener != null)
								{
									updateListener.uiUpdated();
								}
							}
						}).start();
					}
				});
			}

			@Override
			public void profileInitialized()
			{
			}
		});
	}

	private void initNavPanel()
	{
		JLabel actions = new JLabel("Connections");
		actions.setFont(Font.getFont("Lucidia Grande-BOLD"));
		actions.setForeground(new Color(199, 199, 199));
		navPanel.add(actions);

		connListModel = new DefaultListModel<DatabaseConnection>();
		final JList connList = new JList(connListModel);
		connList.setCellRenderer(new ConnectionCellRenderer());
		navPanel.add(connList, Component.RIGHT_ALIGNMENT);

		JLabel blank = new JLabel(" ");
		navPanel.add(blank);

		actions = new JLabel("Reporting Areas");
		actions.setFont(Font.getFont("Lucidia Grande"));
		actions.setForeground(new Color(199, 199, 199));
		navPanel.add(actions);

		final JList raList = new JList();
		raList.setCellRenderer(new ReportingAreaCellRenderer());
		navPanel.add(raList);
		defaultListModel = new DefaultListModel<ReportingArea>();

		raList.setModel(defaultListModel);

		raList.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseReleased(MouseEvent e)
			{
				final String res = JOptionPane.showInputDialog(e.getComponent(), "Reporting Area Name");

				final SwingWorker sw = new SwingWorker()
				{
					@Override
					protected Object doInBackground() throws Exception
					{
						profile.defaultReportingArea().newReportingArea().name(res).create();
						try
						{
							profile.persist();
							profile.invalidate();
						}
						catch (IOException e1)
						{
							e1.printStackTrace();
						}

						return null;
					}

					@Override
					protected void done()
					{
						initRaList();
					}
				};

				EventQueue.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							Thread.sleep(1000L);
						}
						catch (InterruptedException e1)
						{
							e1.printStackTrace();
						}
						sw.execute();
					}
				});
			}
		});

		initRaList();
		initConnectionList();
	}

	private void initConnectionList()
	{
		connListModel.removeAllElements();
		for (Map.Entry<String, DatabaseConnection> con : profile.connections().entrySet())
		{
			connListModel.add(connListModel.size(), con.getValue());
		}
	}

	private void initRaList()
	{
		ReportingArea dra = profile.defaultReportingArea();

		defaultListModel.removeAllElements();
		defaultListModel.add(0, dra);

		Set<Map.Entry<String, ReportingArea>> entries = dra.children().entrySet();
		for (final Map.Entry<String, ReportingArea> en : entries)
		{
			defaultListModel.add(defaultListModel.size(), en.getValue());
		}
	}

	public void setUpdateListener(UIUpdateListener list)
	{
		updateListener = list;
	}

	public void shutDown()
	{
		frame.dispose();
	}

	public static interface UIUpdateListener
	{
		void uiUpdated();
	}
}
