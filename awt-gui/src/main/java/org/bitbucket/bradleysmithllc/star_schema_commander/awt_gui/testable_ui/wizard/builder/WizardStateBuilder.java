package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;

public class WizardStateBuilder<T, C extends WizardStateUIComponent<T>>
{
	private final WizardStateModelImpl wizardStateModel;
	private T model;
	private String id;
	private C uiComponent;
	private WizardState.WizardStateListener<T, C> listener;

	public WizardStateBuilder(WizardStateModelImpl stateMap)
	{
		wizardStateModel = stateMap;
	}

	public WizardState<T, C> create()
	{
		if (model == null)
		{
			throw new IllegalStateException("Model may not be null");
		}

		if (id == null)
		{
			throw new IllegalStateException("Id may not be null");
		}

		if (wizardStateModel.states().containsKey(id))
		{
			throw new IllegalStateException("State id [" + id + "] already used.");
		}

		WizardStateImpl<T, C> wsi = new WizardStateImpl<T, C>(model, id, uiComponent, listener);

		wizardStateModel.addState(wsi);

		return wsi;
	}

	WizardStateBuilder<T, C> component(C comp)
	{
		uiComponent = comp;
		return this;
	}

	public WizardStateBuilder<T, C> id(String id)
	{
		this.id = id;
		return this;
	}

	WizardStateBuilder<T, C> model(T t)
	{
		model = t;
		return this;
	}

	public WizardStateBuilder<T, C> listener(WizardState.WizardStateListener<T, C> listener)
	{
		this.listener = listener;
		return this;
	}
}
