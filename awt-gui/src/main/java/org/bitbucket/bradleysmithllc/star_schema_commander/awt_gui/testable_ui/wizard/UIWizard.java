package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.DesktopUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;

public class UIWizard extends AbstractUIComponent implements WizardStateTransition.TransitionStateChangeListener, TransitionStates
{
	public static final String MSG_ACTIVATE_TRANSITION = "MSG_ACTIVATE_TRANSITION";

	public static final int MODEL_TRANSITION_STATES = 0;

	public static final int ST_WIZARD_COMPLETE = 0;

	private final WizardStateModel wizardStateModel;
	private final java.util.List<JButton> transitionActions = new ArrayList<JButton>();
	private WizardState<?, ?> currentState;
	private JPanel rootPanel;
	private JPanel contentPanel;
	private JPanel transitionPanel;
	private boolean wizardCompleted = false;
	private DialogChangeListener dialogChangeListener;

	public UIWizard(WizardStateModel wizardStateModel)
	{
		this.wizardStateModel = wizardStateModel;
		currentState = wizardStateModel.initial();
	}

	public void observe(DialogChangeListener dcl)
	{
		dialogChangeListener = dcl;
	}

	public WizardStateModel model()
	{
		return wizardStateModel;
	}

	@Override
	public void subInitialize() throws Exception
	{
		runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				rootPanel = new JPanel();
				rootPanel.setLayout(new BorderLayout());

				contentPanel = new JPanel();
				contentPanel.setLayout(new BorderLayout());

				// give content front and center
				rootPanel.add(contentPanel, BorderLayout.CENTER);
				contentPanel.setBorder(BorderFactory.createTitledBorder("Content"));

				// add transitions
				transitionPanel = new JPanel();
				transitionPanel.setBorder(BorderFactory.createTitledBorder("Transitions"));

				// controls go on bottom
				rootPanel.add(transitionPanel, BorderLayout.SOUTH);
			}
		});
	}

	@Override
	public void prepareSelf() throws Exception
	{
		requireEventThread();

		// grab the next ui component
		currentState.activate();
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
		requireEventThread();

		updateContent();
		addTransitionListeners();
		updateTransitions();
	}

	private void updateTransitions()
	{
		requireEventThread();

		java.util.List<? extends WizardStateTransition> trans = currentState.transitions();

		java.util.List<WizardStateTransition> activeTrans = new ArrayList<WizardStateTransition>();

		for (WizardStateTransition tran : trans)
		{
			if (tran.active())
			{
				activeTrans.add(tran);
			}
		}

		transitionActions.clear();
		transitionPanel.removeAll();

		if (!activeTrans.isEmpty())
		{
			transitionPanel.setLayout(new GridLayout(0, activeTrans.size()));

			for (WizardStateTransition tr : activeTrans)
			{
				if (!tr.active())
				{
					continue;
				}

				final WizardStateTransition thisTrans = tr;
				JButton jb = new JButton("<html><a href=''>" + tr.logicalName() + "</a></html>");
				jb.setName(tr.id());

				jb.setEnabled(tr.enabled());

				jb.setBorderPainted(false);
				jb.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						// choose this transition
						followTransition(thisTrans);

						rootPanel.updateUI();
					}
				});

				transitionActions.add(jb);
				transitionPanel.add(jb);
			}
		}

		transitionPanel.revalidate();
		transitionPanel.repaint();
	}

	@Override
	public void transitionStateChanged(WizardStateTransition transition)
	{
		try
		{
			Runnable runnable = new Runnable()
			{
				@Override
				public void run()
				{
					// update all transitions
					updateTransitions();
				}
			};

			DesktopUtils.executeOnEventThread(runnable);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		catch (InvocationTargetException e)
		{
			e.printStackTrace();
		}
	}

	private void followTransition(WizardStateTransition thisTrans)
	{
		removeTransitionListeners();

		// deactivate current state
		currentState.deactivate();
		currentState = thisTrans.follow();

		// check for terminal state
		wizardCompleted = currentState.transitionMap().isEmpty();

		if (wizardCompleted)
		{
			if (dialogChangeListener != null)
			{
				dialogChangeListener.disposeWizard();
			}
		}
		else
		{
			uiContainer.reactivate(this);
		}
	}

	private void addTransitionListeners()
	{
		updateTransitionListeners(false);
	}

	private void removeTransitionListeners()
	{
		updateTransitionListeners(true);
	}

	private void updateTransitionListeners(boolean remove)
	{
		java.util.List<? extends WizardStateTransition> trans = currentState.transitions();

		for (WizardStateTransition tran : trans)
		{
			if (remove)
			{
				tran.removeStateChangeListener(this);
			}
			else
			{
				tran.addStateChangeListener(this);
			}
		}
	}

	@Override
	public void activate()
	{
		requireEventThread();

		clearChildViews();
		addChild(currentState.component());
	}

	private void updateContent()
	{
		contentPanel.removeAll();

		WizardStateUIComponent comp = currentState.component();
		contentPanel.add(comp.getComponent(), BorderLayout.CENTER);

		contentPanel.revalidate();
		contentPanel.repaint();

		if (dialogChangeListener != null)
		{
			dialogChangeListener.contentChanged();
		}
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MODEL_TRANSITION_STATES:
				return (T) this;
		}

		throw new IllegalArgumentException();
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case ST_WIZARD_COMPLETE:
				return wizardCompleted;
			default:
				return super.checkState(value);
		}
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		if (message == MSG_ACTIVATE_TRANSITION)
		{
			// all dispatch messages here are for transitions
			String trId = (String) value;

			WizardStateTransition<?, ? extends WizardStateUIComponent<?>, ?, ?> wst = currentState.transitionMap().get(trId);

			if (wst == null)
			{
				throw new IllegalStateException("Transition [" + trId + "] does not exist");
			}
			else if (!wst.enabled())
			{
				throw new IllegalStateException("Transition [" + trId + "] is not enabled");
			}
			else if (!wst.active())
			{
				throw new IllegalStateException("Transition [" + trId + "] is not active");
			}

			for (JButton jb : transitionActions)
			{
				if (jb.getName().equals(trId))
				{
					final JButton jbInner = jb;

					// do it
					try
					{
						EventQueue.invokeAndWait(new Runnable()
						{
							@Override
							public void run()
							{
								jbInner.doClick();
							}
						});

						new Thread(new Runnable()
						{
							@Override
							public void run()
							{
								latch.countDown();
							}
						}).start();
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					catch (InvocationTargetException e)
					{
						e.printStackTrace();
					}

					return;
				}
			}

			throw new IllegalArgumentException(trId);
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return rootPanel;
	}

	@Override
	public boolean transitionActive(String id)
	{
		return getWizardStateTransition(id).active();
	}

	private WizardStateTransition<?, ? extends WizardStateUIComponent<?>, ?, ?> getWizardStateTransition(String id)
	{
		WizardStateTransition<?, ? extends WizardStateUIComponent<?>, ?, ?> wizardStateTransition = currentState.transitionMap().get(id);

		if (wizardStateTransition == null)
		{
			throw new IllegalArgumentException();
		}

		return wizardStateTransition;
	}

	@Override
	public boolean transitionEnabled(String id)
	{
		return getWizardStateTransition(id).enabled();
	}

	@Override
	public Collection<String> stateIds()
	{
		return currentState.transitionMap().keySet();
	}

	interface DialogChangeListener
	{
		void contentChanged();

		void disposeWizard();
	}
}
