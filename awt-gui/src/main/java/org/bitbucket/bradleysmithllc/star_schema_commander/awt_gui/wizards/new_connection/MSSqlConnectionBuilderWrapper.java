package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.SqlServerConnectionBuilder;

public class MSSqlConnectionBuilderWrapper extends JTDSSqlConnectionBuilderWrapper
{
	@Override
	public boolean usesDomain()
	{
		return false;
	}

	@Override
	public DatabaseConnection create()
	{
		SqlServerConnectionBuilder cb = UIHub.profile().newSqlServerConnection().serverName(server);

		cb.name(connectionName);

		if (database != null)
		{
			cb.database(database);
		}

		if (port != -1)
		{
			cb.port(port);
		}

		if (instanceName != null)
		{
			cb.instanceName(instanceName);
		}

		if (username != null)
		{
			cb.username(username);

			if (password != null)
			{
				cb.password(password);
			}
		}
		else
		{
			cb.integratedSecurity();
		}

		return cb.create();
	}
}
