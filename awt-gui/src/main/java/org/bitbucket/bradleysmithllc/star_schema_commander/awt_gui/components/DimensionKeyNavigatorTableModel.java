package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Visitor;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DimensionKeyNavigatorTableModel extends AbstractTableModel
{
	private final List<DataGridNavigator.IncludedDimension> tableData = new ArrayList<DataGridNavigator.IncludedDimension>();

	private boolean updating = false;

	@Override
	public int getRowCount()
	{
		return size();
	}

	@Override
	public String getColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Name";
			case 1:
				return "Dimension";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getColumnCount()
	{
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if (updating())
		{
			return null;
		}

		DataGridNavigator.IncludedDimension td = tableData.get(rowIndex);

		switch (columnIndex)
		{
			case 0:
				return td.dimensionKey().name();
			case 1:
				return td.dimensionKey().dimensionKey().dimension().logicalName();
			default:
				throw new IllegalArgumentException();
		}
	}

	public synchronized void update(Visitor<DimensionKeyNavigatorTableModel> visitor)
	{
		try
		{
			updating = true;

			visitor.visit(this);
		}
		finally
		{
			updating = false;
		}
	}

	private synchronized boolean updating()
	{
		return updating;
	}

	public void addDimensionKey(FactDimensionKey dk)
	{
		DataGridNavigator.IncludedDimension e = new DataGridNavigator.IncludedDimension(dk);
		tableData.add(e);

		Collections.sort(tableData);

		fireTableDataChanged();
	}

	public int size()
	{
		if (updating())
		{
			return 0;
		}
		return tableData.size();
	}

	public List<DataGridNavigator.IncludedDimension> tableData()
	{
		return tableData;
	}

	public boolean isEmpty()
	{
		return tableData.isEmpty();
	}

	public void clear()
	{
		int rowCount = tableData.size();

		if (rowCount > 0)
		{
			tableData.clear();

			fireTableDataChanged();
		}
	}

	public int locateMeasure(FactDimensionKey fm)
	{
		int row = 0;

		for (DataGridNavigator.IncludedDimension td : tableData)
		{
			if (td.dimensionKey() == fm)
			{
				return row;
			}

			row++;
		}

		throw new IllegalArgumentException();
	}
}
