package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class DimensionCellRenderer extends JLabel implements ListCellRenderer<Dimension>
{
	@Override
	public Component getListCellRendererComponent(JList<? extends Dimension> list, Dimension value, int index, boolean isSelected, boolean cellHasFocus)
	{
		// get the selected fact and highlite accordingly
		Fact fact = UIHub.selectedFact();

		String tag = "";

		if (fact != null)
		{
			Map<String, FactDimensionKey> keys = fact.keys(value);

			StringBuilder tagBuilder = new StringBuilder();

			for (Map.Entry<String, FactDimensionKey> entry : keys.entrySet())
			{
				tagBuilder.append("*");
			}

			tag = tagBuilder.toString();
		}

		setText(value.logicalName() + tag);

		setForeground(isSelected ? Color.red : Color.black);

		return this;
	}
}