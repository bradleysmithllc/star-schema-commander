package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;

import java.util.HashMap;
import java.util.Map;

public class FactDimensionKeyRecord
{
	private final Map<String, String> columns = new HashMap<String, String>();
	private Fact fact;
	private DimensionKey dimensionKey;
	private String physicalName;
	private String roleName;
	private boolean requireRole = false;

	public FactDimensionKeyRecord(Fact fact, DimensionKey dimensionKey)
	{
		this.fact = fact;
		this.dimensionKey = dimensionKey;
	}

	public void requireRole(boolean b)
	{
		requireRole = b;
	}

	public boolean requireRole()
	{
		return true;
	}

	public Map<String, String> columns()
	{
		return columns;
	}

	public Fact fact()
	{
		return fact;
	}

	public void fact(Fact f)
	{
		fact = f;
	}

	public DimensionKey dimensionKey()
	{
		return dimensionKey;
	}

	public void dimensionKey(DimensionKey dkey)
	{
		dimensionKey = dkey;
	}

	public String roleName()
	{
		return roleName;
	}

	public void roleName(String name)
	{
		roleName = name;
	}

	public String physicalName()
	{
		return physicalName;
	}

	public void physicalName(String name)
	{
		physicalName = name;
	}

	public FactDimensionKeyRecord cloneMe()
	{
		FactDimensionKeyRecord fdkr = new FactDimensionKeyRecord(fact, dimensionKey);
		fdkr.physicalName(physicalName);
		fdkr.roleName(roleName);

		fdkr.columns().putAll(columns);

		return fdkr;
	}

	@Override
	public String toString()
	{
		return "FactDimensionKeyRecord{" +
			"fact=" + fact.qualifiedName() +
			", dimensionKey=" + dimensionKey.logicalName() +
			", dimension=" + dimensionKey.dimension().logicalName() +
			", physicalName='" + physicalName + '\'' +
			", roleName='" + roleName + '\'' +
			", columns=" + columns +
			'}';
	}
}
