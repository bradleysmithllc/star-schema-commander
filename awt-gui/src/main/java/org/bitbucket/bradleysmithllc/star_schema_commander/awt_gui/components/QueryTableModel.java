package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.RowData;

import java.util.ArrayList;
import java.util.List;

public class QueryTableModel extends CoordinatedTableModel<RowData>
{
	private final DatabaseConnection connection;
	private final List<String> columns = new ArrayList<String>();
	private PersistedQuery persistedQuery;

	public QueryTableModel(final DatabaseConnection connection)
	{
		this.connection = connection;
	}

	public void updateQuery(PersistedQuery pQuery)
	{
		persistedQuery = pQuery;

		visit(new ModelVisitor<RowData>()
		{
			@Override
			public void visit(UpdatableTableModel<RowData> model)
			{
				model.clearData();

				columns.clear();
				columns.addAll(persistedQuery.columns());

				for (RowData rd : persistedQuery.data())
				{
					model.addElement(rd);
				}
			}
		});

		fireTableStructureChanged();
	}

	@Override
	public String getCoordinatedColumnName(int column)
	{
		return columns.get(column);
	}

	@Override
	public int getCoordinatedColumnCount()
	{
		return columns.size();
	}

	@Override
	public synchronized Object getCoordinatedValueAt(RowData rowData, int rowIndex, int columnIndex)
	{
		return rowData.get(columnIndex);
	}
}
