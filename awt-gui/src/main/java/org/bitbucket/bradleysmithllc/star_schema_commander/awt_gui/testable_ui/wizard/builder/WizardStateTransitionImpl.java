package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.StateTransitionHandler;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;

import java.util.ArrayList;
import java.util.List;

class WizardStateTransitionImpl<F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>> implements WizardStateTransition<F, C, T, D>
{
	private final String id;
	private final String logicalName;
	private final WizardState<F, C> from;
	private final WizardState<T, D> to;
	private final StateTransitionHandler<F, C, T, D> handler;
	private final StateTransitionHandler<F, C, T, D> modelHandler;
	List<TransitionStateChangeListener<F, C, T, D>> listeners = new ArrayList<TransitionStateChangeListener<F, C, T, D>>();
	private boolean enabled;
	private boolean active;

	WizardStateTransitionImpl(String id, String logicalName, WizardState<F, C> from, WizardState<T, D> to, StateTransitionHandler<F, C, T, D> handler, final WizardStateModelImpl model)
	{
		this.id = id;
		this.logicalName = logicalName;
		this.from = from;
		this.to = to;
		this.handler = handler;
		modelHandler = new StateTransitionHandler<F, C, T, D>()
		{
			@Override
			public void transition(WizardState<F, C> from, WizardState<T, D> to, WizardStateTransition<F, C, T, D> transition)
			{
				model.transition(from, to, transition);
			}
		};
	}

	@Override
	public String id()
	{
		return id;
	}

	@Override
	public String logicalName()
	{
		return logicalName;
	}

	@Override
	public boolean enabled()
	{
		return enabled;
	}

	@Override
	public void enable(boolean b)
	{
		enabled = b;
		broadcast();
	}

	@Override
	public boolean active()
	{
		return active;
	}

	@Override
	public void activate(boolean b)
	{
		active = b;
		broadcast();
	}

	private void broadcast()
	{
		for (TransitionStateChangeListener<F, C, T, D> list : listeners)
		{
			list.transitionStateChanged(this);
		}
	}

	@Override
	public WizardState<F, C> from()
	{
		return from;
	}

	@Override
	public WizardState<T, D> follow()
	{
		handle(handler);
		handle(modelHandler);

		return to;
	}

	@Override
	public void addStateChangeListener(TransitionStateChangeListener<F, C, T, D> list)
	{
		listeners.add(list);
	}

	@Override
	public void removeStateChangeListener(TransitionStateChangeListener<F, C, T, D> list)
	{
		listeners.remove(list);
	}

	private void handle(StateTransitionHandler<F, C, T, D> mh)
	{
		if (mh != null)
		{
			mh.transition(from, to, this);
		}
	}
}
