package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;

import java.util.ArrayList;
import java.util.List;

public class ColumnsSpec<T>
{
	List<DatabaseColumn> columnsToMap = new ArrayList<DatabaseColumn>();
	private T mapping;
	private int index = 0;

	public void mapping(T mapping)
	{
		this.mapping = mapping;
	}

	public T mapping()
	{
		return mapping;
	}

	public List<DatabaseColumn> columns()
	{
		return columnsToMap;
	}

	public int index()
	{
		return index;
	}

	public int next()
	{
		return index++;
	}
}
