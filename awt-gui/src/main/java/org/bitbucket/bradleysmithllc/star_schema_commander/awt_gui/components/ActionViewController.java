package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;

public class ActionViewController
{
	@FXML
	private MenuItem ActionView_NewConnectionMenuItem;

	@FXML
	private SplitMenuButton ActionView_ActionMenu;

	@FXML
	public void importKeys()
	{
		System.out.println("importKeys");
	}
}