package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ConnectionCellRenderer;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SelectConnectionUIComponent extends WizardStateUIComponent<Reference<DatabaseConnection>>
{
	public static final String COMPONENT_TYPE = "SelectConnectionUIComponent";

	public static final int SELECT_CONNECTION_MODEL = 0;
	public static final String MSG_SELECT_CONNECTION = "MSG_SELECT_CONNECTION";
	private final String nextTransitionId;

	DefaultListModel<DatabaseConnection> connectionDefaultListModel = new DefaultListModel<DatabaseConnection>();
	private JList<DatabaseConnection> connectionJList;
	private JScrollPane scrollPane;
	private WizardStateTransition<Reference<DatabaseConnection>, ? extends WizardStateUIComponent<Reference<DatabaseConnection>>, ?, ?> loadTrans;

	public SelectConnectionUIComponent(String nextTransitionId)
	{
		this.nextTransitionId = nextTransitionId;
	}

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		loadTrans = state().transitionMap().get(nextTransitionId);
		loadTrans.enable(false);

		for (DatabaseConnection conn : UIHub.profile().connections().values())
		{
			connectionDefaultListModel.add(connectionDefaultListModel.size(), conn);
		}

		connectionJList = new JList<DatabaseConnection>(connectionDefaultListModel);
		connectionJList.setCellRenderer(new ConnectionCellRenderer());
		connectionJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		scrollPane = new JScrollPane(connectionJList);
		scrollPane.setBorder(BorderFactory.createTitledBorder("Select a Connection"));

		connectionJList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (connectionJList.isSelectionEmpty())
				{
					loadTrans.enable(false);
				}
				else
				{
					state().model().setReference(connectionJList.getSelectedValue());
					loadTrans.enable(true);
				}
			}
		});
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case SELECT_CONNECTION_MODEL:
				return (T) connectionDefaultListModel;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, Object value)
	{
		if (message == MSG_SELECT_CONNECTION)
		{
			connectionJList.setSelectedValue(value, true);
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return scrollPane;
	}
}