package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

public class DimensionAttributePanel extends AbstractUIComponent
{
	public static final int DIMENSION_ATTRIBUTE_PANEL_ATTRIBUTE_LIST = 0;

	private final DefaultListModel<DimensionAttribute> factListModel = new DefaultListModel<DimensionAttribute>();

	private JList<DimensionAttribute> factList = new JList<DimensionAttribute>(factListModel);
	private JScrollPane jScrollPane;

	@Override
	protected void buildParent(Profile profile)
	{
		factListModel.clear();
		factList = new JList<DimensionAttribute>(factListModel);
		factList.setCellRenderer(new DimensionAttributeCellRenderer());

		jScrollPane = new JScrollPane(factList);

		jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "Attributes"));

		updateFact();
	}

	@Override
	public JComponent getComponent()
	{
		return jScrollPane;
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case DIMENSION_ATTRIBUTE_PANEL_ATTRIBUTE_LIST:
				return (T) factListModel;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void stateChanged(stateKey key, Object value)
	{
		try
		{
			runOnEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					updateFact();
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void updateFact()
	{
		// populate the model with the children of this ra
		factListModel.removeAllElements();

		Dimension dimension = UIHub.selectedDimension();
		if (dimension != null)
		{
			for (Map.Entry<String, DimensionAttribute> rac : dimension.attributes().entrySet())
			{
				factListModel.add(factListModel.size(), rac.getValue());
			}
		}

		factList.revalidate();
		jScrollPane.revalidate();
	}
}