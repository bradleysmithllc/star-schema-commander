package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.laf.progressbar.WebProgressBar;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.ReferenceMap;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.QualifiedTable;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SelectTablesUIComponent extends WizardStateUIComponent<ReferenceMap>
{
	public static final String MSG_SELECT_AVAILABLE_TABLE = "MSG_SELECT_AVAILABLE_TABLE";
	public static final String MSG_SELECT_SELECTED_TABLE = "MSG_SELECT_SELECTED_TABLE";
	public static final String MSG_WAIT_FOR_TABLE_LOAD = "MSG_WAIT_FOR_TABLE_LOAD";

	public static final String MSG_ADD_SELECTED_ACTION = "MSG_ADD_SELECTED_ACTION";
	public static final String MSG_ADD_ALL_ACTION = "MSG_ADD_ALL_ACTION";
	public static final String MSG_REMOVE_SELECTED_ACTION = "MSG_REMOVE_SELECTED_ACTION";
	public static final String MSG_REMOVE_ALL_ACTION = "MSG_REMOVE_ALL_ACTION";

	public static final int AVAILABLE_DIMENSIONS_MODEL = 0;
	public static final int SELECTED_DIMENSIONS_MODEL = 1;

	public static final int SELECTED_DIMENSIONS_EXPORT = 2;

	public static final int REMOVE_SELECTED_STATE = 0;
	public static final int REMOVE_ALL_STATE = 1;
	public static final int ADD_SELECTED_STATE = 2;
	public static final int ADD_ALL_STATE = 3;

	private JPanel rootPanel;
	private QualifiedTableTableModel availableTablesModel = new QualifiedTableTableModel();
	private QualifiedTableTableModel selectedTablesModel = new QualifiedTableTableModel();
	private JButton removeSelectedAction;
	private JButton removeAllAction;
	private JButton addAllAction;
	private JButton addSelectedAction;
	private JTable availableTablesTable;
	private JTable selectedTablesList;
	private WizardStateTransition<ReferenceMap, ? extends WizardStateUIComponent<ReferenceMap>, ?, ?> dimTrans;

	private Semaphore tableLoaded;
	private List<String> existingSelection;

	@Override
	public void activate()
	{
		dimTrans = state().transitionMap().get("process");
		dimTrans.enable(false);

		rootPanel = new JPanel();
		rootPanel.setLayout(new BorderLayout());

		final WebProgressBar progressBar1 = new WebProgressBar(WebProgressBar.HORIZONTAL, 0, 1);
		progressBar1.setValue(0);
		progressBar1.setIndeterminate(true);
		progressBar1.setStringPainted(false);

		rootPanel.add(progressBar1, BorderLayout.SOUTH);

		// we now have a connection.  List the objects
		// check for an existing table existingSelection
		existingSelection = state().model().getValue("DimensionList");

		// preacquire the semaphore
		tableLoaded = new Semaphore(1);
		tableLoaded.acquireUninterruptibly(1);

		availableTablesTable = new JTable(availableTablesModel);
		availableTablesTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		selectedTablesList = new JTable(selectedTablesModel);
		selectedTablesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		new SwingWorker()
		{
			private final java.util.List<QualifiedTable> names = new ArrayList<QualifiedTable>();

			@Override
			protected Object doInBackground() throws Exception
			{
				try
				{
					DatabaseConnection db = (DatabaseConnection) state().model().get("SelectedDatabaseConnection").getReference();

					System.out.println("Opening connection...");
					Connection conn = db.open();
					System.out.println("Done");

					try
					{
						System.out.println("Querying metadata...");
						DatabaseMetaData dbmd = conn.getMetaData();
						System.out.println("Done");

						System.out.println("Querying table metadata...");
						ResultSet mdq = dbmd.getTables(null, null, null, null);
						System.out.println("Done");

						ReportingArea selectedReportingArea1 = UIHub.selectedReportingArea();

						try
						{
							while (mdq.next())
							{
								String catalog = mdq.getString(1);
								String schema = mdq.getString(2);
								String table = mdq.getString(3);

								//System.out.println("Querying table metadata " + schema + "." + table + "...");

								// don't include dimensions we have already defined
								String key = catalog + "." + schema + "." + table;
								boolean dimensionsContainsKey = selectedReportingArea1.physicalDimensions().containsKey(key);
								boolean physicalFactsContainsKey = selectedReportingArea1.physicalFacts().containsKey(key);
								boolean tableModelCOntains = selectedTablesModel.contains(key);

								if (
									!dimensionsContainsKey
										&&
										!physicalFactsContainsKey
										&&
										!tableModelCOntains
									)
								{
									names.add(new QualifiedTable.QualifiedTableBuilder().schema(schema).name(table).catalog(catalog).create());
								}
							}
						}
						finally
						{
							System.out.println("Done with meta data...");
							mdq.close();
						}
					}
					finally
					{
						System.out.println("Done with connection...");
						conn.close();
					}
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}

				System.out.println("Sort names...");
				Collections.sort(names);
				System.out.println("Done");
				return null;
			}

			@Override
			protected void done()
			{
				for (QualifiedTable name : names)
				{
					//System.out.println("Process table " + name.qualifiedName() + "...");
					if (existingSelection != null && existingSelection.contains(name))
					{
						//selectedTablesModel.add(selectedTablesModel.size(), name);
					}
					else
					{
						availableTablesModel.addTable(name);
					}
					//System.out.println("Done");
				}

				//System.out.println("Updating button states...");
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAllAction, removeSelectedAction);
				updateButtonStates(availableTablesModel, availableTablesTable, addAllAction, addSelectedAction);
				updateNextTransition();
				//System.out.println("Done");

				progressBar1.setVisible(false);

				System.out.println("Releasing table load latch...");
				tableLoaded.release(1);
				System.out.println("Latch Released");
			}
		}.execute();

		JScrollPane comp1 = new JScrollPane(availableTablesTable);
		comp1.setBorder(BorderFactory.createTitledBorder("Select a table"));

		rootPanel.add(comp1, BorderLayout.WEST);

		comp1 = new JScrollPane(selectedTablesList);
		comp1.setBorder(BorderFactory.createTitledBorder("Selected tables"));

		rootPanel.add(comp1, BorderLayout.EAST);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(4, 0));

		removeSelectedAction = new JButton("<html><a href=''>&lt;</a></html>");
		removeSelectedAction.setBorderPainted(false);
		removeAllAction = new JButton("<html><a href=''>&lt;&lt;</a></html>");
		removeAllAction.setBorderPainted(false);
		addAllAction = new JButton("<html><a href=''>&gt;&gt;</a></html>");
		addAllAction.setBorderPainted(false);
		addSelectedAction = new JButton("<html><a href=''>&gt;</a></html>");
		addSelectedAction.setBorderPainted(false);

		buttonPanel.add(removeSelectedAction);
		removeSelectedAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeSelected(selectedTablesList, selectedTablesModel, availableTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAllAction, removeSelectedAction);
				updateButtonStates(availableTablesModel, availableTablesTable, addAllAction, addSelectedAction);
				updateNextTransition();
			}
		});

		buttonPanel.add(removeAllAction);
		removeAllAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeList(selectedTablesModel, availableTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAllAction, removeSelectedAction);
				updateButtonStates(availableTablesModel, availableTablesTable, addAllAction, addSelectedAction);
				updateNextTransition();
			}
		});

		buttonPanel.add(addAllAction);
		addAllAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeList(availableTablesModel, selectedTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAllAction, removeSelectedAction);
				updateButtonStates(availableTablesModel, availableTablesTable, addAllAction, addSelectedAction);
				updateNextTransition();
			}
		});

		addSelectedAction.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				purgeSelected(availableTablesTable, availableTablesModel, selectedTablesModel);
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAllAction, removeSelectedAction);
				updateButtonStates(availableTablesModel, availableTablesTable, addAllAction, addSelectedAction);
				updateNextTransition();
			}
		});

		buttonPanel.add(addSelectedAction);

		// add listeners to keep the middle buttons honest
		selectedTablesList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				updateButtonStates(selectedTablesModel, selectedTablesList, removeAllAction, removeSelectedAction);
			}
		});

		availableTablesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				updateButtonStates(availableTablesModel, availableTablesTable, addAllAction, addSelectedAction);
			}
		});

		rootPanel.add(buttonPanel, BorderLayout.CENTER);
	}

	private void updateNextTransition()
	{
		dimTrans.enable(selectedTablesModel.size() != 0);
	}

	@Override
	public JComponent getComponent()
	{
		return rootPanel;
	}

	private void updateButtonStates(QualifiedTableTableModel selectedTablesModel, JTable selectedTablesList, JButton removeAll, JButton removeSelected)
	{
		boolean isRemoveAll = true;
		boolean isRemoveSelected = true;

		if (selectedTablesModel.size() == 0)
		{
			isRemoveAll = false;
			isRemoveSelected = false;
		}
		else
		{
			if (selectedTablesList.getSelectedRows().length == 0)
			{
				isRemoveSelected = false;
			}
		}

		removeAll.setEnabled(isRemoveAll);
		removeSelected.setEnabled(isRemoveSelected);
	}

	private void purgeSelected(JTable selectedTablesList, QualifiedTableTableModel selectedTablesModel, QualifiedTableTableModel availableTablesModel)
	{
		List<QualifiedTable> qlist = new ArrayList<QualifiedTable>();

		for (int tableRow : selectedTablesList.getSelectedRows())
		{
			QualifiedTable qt = selectedTablesModel.tableData().get(tableRow);
			qlist.add(qt);
		}

		// remove and add
		for (QualifiedTable qt : qlist)
		{
			selectedTablesModel.removeTable(qt);
			availableTablesModel.addTable(qt);
		}
	}

	private void sortModel(DefaultListModel<String> availableTablesModel)
	{
		java.util.List<String> names = new ArrayList<String>();

		Enumeration<String> enu = availableTablesModel.elements();

		while (enu.hasMoreElements())
		{
			names.add(enu.nextElement());
		}

		Collections.sort(names, new Comparator<String>()
		{
			@Override
			public int compare(String o1, String o2)
			{
				return CompareUtils.compare(o1, o2);
			}
		});

		availableTablesModel.removeAllElements();

		for (String name : names)
		{
			availableTablesModel.add(availableTablesModel.size(), name);
		}
	}

	private void purgeList(QualifiedTableTableModel fromModel, QualifiedTableTableModel toModel)
	{
		java.util.List<QualifiedTable> names = new ArrayList<QualifiedTable>();

		for (QualifiedTable qt : fromModel.tableData())
		{
			names.add(qt);
		}

		for (QualifiedTable table : names)
		{
			fromModel.removeTable(table);
			toModel.addTable(table);
		}
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case REMOVE_ALL_STATE:
				return removeAllAction.isEnabled();
			case REMOVE_SELECTED_STATE:
				return removeSelectedAction.isEnabled();
			case ADD_ALL_STATE:
				return addAllAction.isEnabled();
			case ADD_SELECTED_STATE:
				return addSelectedAction.isEnabled();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		try
		{
			if (message == MSG_SELECT_AVAILABLE_TABLE)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						if (value == null)
						{
							availableTablesTable.clearSelection();
						}
						else
						{
							String key = String.valueOf(value);

							if (!availableTablesModel.contains(key))
							{
								throw new IllegalArgumentException();
							}

							int index = availableTablesModel.getRowIndex(key);
							availableTablesTable.getSelectionModel().setSelectionInterval(index, index);
						}
					}
				});
			}
			else if (message == MSG_SELECT_SELECTED_TABLE)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						if (value == null)
						{
							selectedTablesList.clearSelection();
						}
						else
						{
							String key = String.valueOf(value);
							if (!selectedTablesModel.contains(key))
							{
								throw new IllegalArgumentException();
							}

							int index = selectedTablesModel.getRowIndex(key);
							selectedTablesList.getSelectionModel().setSelectionInterval(index, index);
						}
					}
				});
			}
			else if (message == MSG_ADD_SELECTED_ACTION)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						addSelectedAction.doClick();
					}
				});
			}
			else if (message == MSG_REMOVE_SELECTED_ACTION)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						removeSelectedAction.doClick();
					}
				});
			}
			else if (message == MSG_REMOVE_ALL_ACTION)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						removeAllAction.doClick();
					}
				});
			}
			else if (message == MSG_ADD_ALL_ACTION)
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						addAllAction.doClick();
					}
				});
			}
			else if (message == MSG_WAIT_FOR_TABLE_LOAD)
			{
				tableLoaded.acquireUninterruptibly(1);
			}
			else
			{
				throw new IllegalArgumentException();
			}
		}
		catch (Exception exc)
		{
			throw new RuntimeException(exc);
		}
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case AVAILABLE_DIMENSIONS_MODEL:
				return (T) availableTablesModel;
			case SELECTED_DIMENSIONS_MODEL:
				return (T) selectedTablesModel;
			case SELECTED_DIMENSIONS_EXPORT:
			{
				java.util.List<QualifiedTable> tables = new ArrayList<QualifiedTable>();

				for (QualifiedTable qt : selectedTablesModel.tableData())
				{
					tables.add(qt);
				}

				return (T) tables;
			}
			default:
				throw new IllegalArgumentException();
		}
	}
}
