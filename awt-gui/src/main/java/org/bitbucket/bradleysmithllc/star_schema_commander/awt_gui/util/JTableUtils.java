package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.StringUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.RowData;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class JTableUtils
{
	public static <T extends Comparable<T>> List<T> getSelectedValues(JTable table, Class<T> cls)
	{
		List<T> values = new ArrayList<T>();

		CoordinatedTableModel<T> coordinatedTableModel = (CoordinatedTableModel<T>) table.getModel();
		List<T> tableData = coordinatedTableModel.tableData();

		for (int i : table.getSelectedRows())
		{
			values.add(tableData.get(i));
		}

		return values;
	}

	public static <T extends Comparable<T>> T getSelectedValue(JTable table, Class<T> cls)
	{
		return table.getSelectionModel().isSelectionEmpty() ? null : getSelectedValues(table, cls).get(0);
	}

	public static List<AtomicInteger> getColumnWidths(List<RowData> td)
	{
		List<AtomicInteger> atomicIntegerList = new ArrayList<AtomicInteger>();

		// initialize all to 0
		if (td.size() != 0)
		{
			for (String rd : td.get(0))
			{
				atomicIntegerList.add(new AtomicInteger(0));
			}

			for (int row = 0; row < td.size(); row++)
			{
				RowData trow = td.get(row);

				for (int tcol = 0; tcol < trow.size(); tcol++)
				{
					String col = trow.get(tcol);

					AtomicInteger atomicInteger = atomicIntegerList.get(tcol);

					atomicInteger.set(Math.max(atomicInteger.get(), col.length()));
				}
			}
		}

		return atomicIntegerList;
	}

	public static String exportResultsToText(List<RowData> td, List<String> columns)
	{
		List<AtomicInteger> widths = getColumnWidths(td);

		// calculate total width
		AtomicInteger ai = new AtomicInteger(0);

		for (int colNum = 0; colNum < widths.size(); colNum++)
		{
			AtomicInteger colWidth = widths.get(colNum);
			colWidth.set(Math.max(colWidth.get(), columns.get(colNum).length()));

			ai.addAndGet(colWidth.get());
		}

		StringBuilder stb = new StringBuilder();

		stb.append('[');
		stb.append(StringUtils.rightPad("", ai.get() + ((widths.size()) * 2) + (widths.size() - 1), '-'));
		stb.append(']').append('\n');

		String header = stb.toString();

		// header line
		for (int i = 0; i < widths.size(); i++)
		{
			AtomicInteger colName = widths.get(i);

			stb.append('|').append(' ').append(StringUtils.rightPad(columns.get(i), colName.get(), ' ')).append(' ');
		}
		stb.append("|\n");

		stb.append("|");
		for (int i = 0; i < widths.size(); i++)
		{
			stb.append(StringUtils.rightPad("", widths.get(i).get() + 2, '-'));

			if (i < (widths.size() - 1))
			{
				stb.append('+');
			}
		}
		stb.append("|\n");

		for (RowData rd : td)
		{
			stb.append("|");

			for (int col = 0; col < rd.size(); col++)
			{
				stb.append(' ').append(StringUtils.rightPad(rd.get(col), widths.get(col).get(), ' ')).append(' ');

				if (col < (rd.size() - 1))
				{
					stb.append('|');
				}
			}

			stb.append("|\n");
		}

		stb.append(header);

		return stb.toString();
	}
}