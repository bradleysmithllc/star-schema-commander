package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.util.ArrayList;
import java.util.List;

public interface DataGridNavigator
{
	List<IncludedMeasure> includeMeasures();

	List<IncludedAttribute> includeAttributes();

	class IncludedMeasure implements Comparable<IncludedMeasure>
	{
		private final FactMeasure factMeasure;
		private String alias;
		private Query.aggregation aggregation = Query.aggregation.none;

		public IncludedMeasure(FactMeasure factMeasure, String alias, Query.aggregation aggregation)
		{
			this.factMeasure = factMeasure;
			this.alias = alias;
			this.aggregation = aggregation;
		}

		public IncludedMeasure(FactMeasure factMeasure, String alias)
		{
			this.factMeasure = factMeasure;
			this.alias = alias;
		}

		public IncludedMeasure(FactMeasure factMeasure)
		{
			this.factMeasure = factMeasure;
		}

		public FactMeasure measure()
		{
			return factMeasure;
		}

		public String alias()
		{
			return alias;
		}

		public void alias(String al)
		{
			alias = al;
		}

		public Query.aggregation aggregation()
		{
			return aggregation;
		}

		public void aggregation(Query.aggregation agg)
		{
			aggregation = agg;
		}

		@Override
		public int compareTo(IncludedMeasure o)
		{
			return CompareUtils.compare(factMeasure, o.measure(), alias, o.alias, aggregation.name(), o.aggregation.name());
		}
	}

	class IncludedAttribute implements Comparable<IncludedAttribute>
	{
		private final DimensionAttribute dimensionAttribute;
		private final FactDimensionKey factDimensionKey;
		private String alias;
		private Query.aggregation aggregation = Query.aggregation.none;

		public IncludedAttribute(DimensionAttribute factMeasure, FactDimensionKey key, String alias, Query.aggregation aggregation)
		{
			this.dimensionAttribute = factMeasure;
			this.alias = alias;
			this.aggregation = aggregation;
			factDimensionKey = key;
		}

		public IncludedAttribute(DimensionAttribute factMeasure, FactDimensionKey key, String alias)
		{
			this.dimensionAttribute = factMeasure;
			this.alias = alias;
			factDimensionKey = key;
		}

		public IncludedAttribute(DimensionAttribute factMeasure, FactDimensionKey key)
		{
			factDimensionKey = key;
			this.dimensionAttribute = factMeasure;
		}

		public FactDimensionKey factDimensionKey()
		{
			return factDimensionKey;
		}

		public DimensionAttribute attribute()
		{
			return dimensionAttribute;
		}

		public String alias()
		{
			return alias;
		}

		public void alias(String al)
		{
			alias = al;
		}

		public Query.aggregation aggregation()
		{
			return aggregation;
		}

		public void aggregation(Query.aggregation agg)
		{
			aggregation = agg;
		}

		@Override
		public int compareTo(IncludedAttribute o)
		{
			return CompareUtils.compare(dimensionAttribute, o.dimensionAttribute, alias, o.alias, aggregation, o.aggregation);
		}
	}

	class IncludedDimension implements Comparable<IncludedDimension>
	{
		private final FactDimensionKey dimensionKey;
		private final List<IncludedAttribute> attributes = new ArrayList<IncludedAttribute>();

		public IncludedDimension(FactDimensionKey key)
		{
			dimensionKey = key;
		}

		public FactDimensionKey dimensionKey()
		{
			return dimensionKey;
		}

		@Override
		public int compareTo(IncludedDimension o)
		{
			return CompareUtils.compare(dimensionKey, o.dimensionKey);
		}

		public List<IncludedAttribute> attributes()
		{
			return attributes;
		}
	}
}
