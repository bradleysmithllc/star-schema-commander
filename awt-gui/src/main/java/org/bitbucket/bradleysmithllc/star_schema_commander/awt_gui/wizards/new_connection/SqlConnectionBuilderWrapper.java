package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;

/**
 * Created by bsmith on 6/10/15.
 */
public interface SqlConnectionBuilderWrapper
{
	void connectionName(String name);

	void database(String name);

	void domain(String name);

	boolean usesDomain();

	void username(String name);

	void password(String password);

	void serverName(String serverName);

	void port(int port);

	void instanceName(String instanceName);

	DatabaseConnection create();
}
