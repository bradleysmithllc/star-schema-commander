package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.SystemControllerConstants;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.SystemUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.javafx.controller.FXMLController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.FXMLUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UINodeReference;

import java.util.concurrent.CountDownLatch;

public class ActionView extends FXMLUIView<FXMLController>
{
	public static final String MSG_NEW_REPORTING_AREA = "MSG_NEW_REPORTING_AREA";
	public static final String MSG_IMPORT_DIMENSIONS = "MSG_IMPORT_DIMENSIONS";
	public static final String MSG_IMPORT_FACTS = "MSG_IMPORT_FACTS";
	public static final String MSG_FACT_MEASURE = "MSG_FACT_MEASURE";
	public static final String MSG_NEW_CONNECTION = "MSG_NEW_CONNECTION";
	public static final String MSG_EXPLORE_FACT = "MSG_EXPLORE_FACT";

	public static final int STATE_EXPLORE_VISIBLE = 5000;

	private String newReportingAreaName;

	@UINodeReference(id = "ActionView_NewConnectionMenuItem")
	protected MenuItem newConnectionNode;

	@UINodeReference(id = "ActionView_NewReportingAreaMenuItem")
	protected MenuItem newReportingAreaMenuItemNode;

	@UINodeReference(id = "ActionView_ExploreMenuItem")
	protected MenuItem explorerMenuItemNode;

	@Override
	public void subSubInitialize()
	{
		newReportingAreaMenuItemNode.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
				UIHub.get().getSystemController().dispatchAction(
					SystemControllerConstants.MSG_NEW_REPORTING_AREA, ImmutablePair.of(UIHub.selectedReportingArea(), newReportingAreaName)
				);
			}
		});

		explorerMenuItemNode.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
				UIHub.get().getSystemController().dispatchAction(
					SystemControllerConstants.MSG_EXPLORE_FACT, ImmutablePair.of(UIHub.selectedFact(), UIHub.selectedConnection())
				);
			}
		});

		newConnectionNode.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent actionEvent)
			{
				UIHub.get().getSystemController().dispatchAction(SystemControllerConstants.MSG_NEW_CONNECTION);
			}
		});
	}

	@Override
	public void stateChanged(final stateKey key, Object value)
	{
		final boolean factSelected = UIHub.selectedFact() != null;
		final boolean dimensionSelected = UIHub.selectedDimension() != null;
		final boolean connectionSelected = UIHub.selectedConnection() != null;

		EventUtils.runOnJFXEventThreadLater(new Runnable()
		{
			@Override
			public void run()
			{
				switch (key)
				{
					case selectedFact:
						//factMeasureAction.setVisible(factSelected);
						//factKeyAction.setVisible(factSelected);
						//factDegenerateDimensionAction.setVisible(factSelected);
					case selectedDimension:
						//dimensionAttributesAction.setVisible(dimensionSelected);
						//dimensionKeyAction.setVisible(dimensionSelected);
					case selectedConnection:
						explorerMenuItemNode.setVisible(factSelected && connectionSelected);
						break;
				}
			}
		});
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, Object value)
	{
		final CountDownLatch in = new CountDownLatch(1);

		if (message == MSG_NEW_REPORTING_AREA)
		{
			newReportingAreaName = value != null ? value.toString() : null;

			EventUtils.runOnJFXEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						newReportingAreaMenuItemNode.fire();
						in.countDown();
					}
					finally
					{
						newReportingAreaName = null;
					}
				}
			});

			try
			{
				in.await();
				UIHub.get().retrieveSystemLatch(SystemUIController.class, SystemUIController.MSG_NEW_REPORTING_AREA).await();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			latch.countDown();
		}
		else if (message == MSG_EXPLORE_FACT)
		{
			try
			{
				EventUtils.runOnJFXEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						explorerMenuItemNode.fire();
						in.countDown();
					}
				});

				in.await();
				UIHub.get().retrieveSystemLatch(SystemUIController.class, SystemUIController.MSG_EXPLORE_FACT).await();
				latch.countDown();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			finally
			{
				newReportingAreaName = null;
			}
		}
		else if (message == MSG_IMPORT_DIMENSIONS)
		{
			try
			{
				runOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						//importDimensionAction.doClick();
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_IMPORT_FACTS)
		{
			try
			{
				runOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						//importFactAction.doClick();
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else if (message == MSG_NEW_CONNECTION)
		{
			try
			{
				EventUtils.runOnJFXEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						newConnectionNode.fire();

						new Thread(new Runnable()
						{
							@Override
							public void run()
							{
								try
								{
									UIHub.get().retrieveSystemLatch(SystemUIController.class, SystemUIController.MSG_NEW_CONNECTION).await();
									latch.countDown();
								}
								catch (InterruptedException e)
								{
									throw new RuntimeException(e);
								}
							}
						}).start();
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	/*
	private class MySwingWorker extends SwingWorker
	{
		private final String raName;

		private final CountDownLatch latch = new CountDownLatch(1);
		private ReportingArea nra;

		public MySwingWorker(String raName)
		{
			this.raName = raName;
		}

		@Override
		protected Object doInBackground() throws Exception
		{
			try
			{
				nra = UIHub.selectedReportingArea().newReportingArea().name(raName).create();
				UIHub.get().persistProfile();
			}
			catch (Throwable thr)
			{
				thr.printStackTrace();
				throw new RuntimeException(thr);
			}

			return null;
		}

		@Override
		protected void done()
		{
			try
			{
				if (nra == null)
				{
					throw new RuntimeException("Null Reporting Area");
				}

				UIHub.selectReportingArea(nra);
			}
			finally
			{
				latch.countDown();
			}
		}

		public void waitFor()
		{
			try
			{
				latch.await();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	 */

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_EXPLORE_VISIBLE:
				return explorerMenuItemNode.isVisible();
			default:
				throw new IllegalArgumentException();
		}
	}
}
