package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardModelBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.NullUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionKeyUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class FactDimensionKeysModelWizard extends UIWizard
{
	public FactDimensionKeysModelWizard(Fact fact)
	{
		super(createModel(fact));
	}

	private static WizardStateModel createModel(final Fact fact)
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		final AtomicInteger dimensionIndex = new AtomicInteger(0);
		final List<Dimension> dimensions = new ArrayList<Dimension>();
		final List<FactDimensionKeyRecord> recordsToProcess = new ArrayList<FactDimensionKeyRecord>();

		final AtomicInteger dimensionKeyIndex = new AtomicInteger(0);
		final List<DimensionKey> dimensionKeys = new ArrayList<DimensionKey>();
		final Map<DimensionKey, String> dimensionKeyRoles = new HashMap<DimensionKey, String>();

		// states
		WizardState<Reference<List<Dimension>>, SelectDimensionsUIComponent> selectDimension =
			wizardModelBuilder.newState(new Reference<List<Dimension>>(dimensions), new SelectDimensionsUIComponent("selectKeys")).id("SelectDimension").create();

		WizardState<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> selectKey =
			wizardModelBuilder.newState(new MutablePair<Dimension, Reference<List<DimensionKey>>>(), new SelectDimensionKeyUIComponent("selectKeys")).id("SelectKeys").create();

		WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> mapColumns =
			wizardModelBuilder.newState(new FactDimensionKeyRecord(null, null), new MapColumnsUIComponent()).id("MapColumns").listener(new WizardState.WizardStateListener<FactDimensionKeyRecord, MapColumnsUIComponent>()
			{
				@Override
				public void stateActivated(WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> state)
				{
					// every activation determine the transition states
					updateMapColumnsTransitions(state, dimensionKeys, dimensionKeyIndex, dimensions, dimensionIndex);

					// check for required role playing - required always now
					//if (!dimensionKeyRoles.containsKey(state.model().dimensionKey()))
					//{
					//	state.model().requireRole(false);
					//}
					//else
					//{
					state.model().requireRole(true);
					//}
				}

				@Override
				public void stateDeactivated(WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> state)
				{
					// for each deactivation record the key information: physical name, role name, and column mappings
					MapColumnsUIComponent comp = state.component();

					recordsToProcess.add((FactDimensionKeyRecord) comp.getModel(MapColumnsUIComponent.MOD_BUNDLE));
				}
			}).create();

		WizardState<Object, NullUIComponent> close =
			wizardModelBuilder.newState(new Object(), new NullUIComponent()).id("close").create();

		// beginning here, there is a loop for processing each dimension and it's key columns
		wizardModelBuilder.newTransition(selectDimension, selectKey).id("selectKeys").logicalName("Select Keys").handler(new StateTransitionHandler<Reference<List<Dimension>>, SelectDimensionsUIComponent, MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<List<Dimension>>, SelectDimensionsUIComponent> from, WizardState<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> to, WizardStateTransition<Reference<List<Dimension>>, SelectDimensionsUIComponent, MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> transition)
			{
				// set the first dimension to inspect
				dimensionKeyRoles.clear();
				prepareSelectKeysState(to, dimensions, dimensionIndex);
			}
		}).create();

		// next, go to the map columns state
		wizardModelBuilder.newTransition(selectKey, mapColumns).id("mapColumns").logicalName("Map Columns").handler(new StateTransitionHandler<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent, FactDimensionKeyRecord, MapColumnsUIComponent>()
		{
			@Override
			public void transition(WizardState<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> from, WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> to, WizardStateTransition<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent, FactDimensionKeyRecord, MapColumnsUIComponent> transition)
			{
				// grab the keys and store in a local list
				dimensionKeyIndex.set(0);
				dimensionKeys.clear();

				List<DimensionKey> dimensionKeyList = from.model().getRight().getReference();

				// if there is more than one key, mark as role-playing.
				if (dimensionKeyList.size() > 1)
				{
					for (DimensionKey dk : dimensionKeyList)
					{
						dimensionKeyRoles.put(dk, "");
					}
				}

				dimensionKeys.addAll(dimensionKeyList);

				to.model().fact(fact);
			}
		}).create();

		// loop back to map columns for the next key if there is more than one
		wizardModelBuilder.newTransition(mapColumns, mapColumns).id("mapNextColumns").logicalName("Next Key").handler(new StateTransitionHandler<FactDimensionKeyRecord, MapColumnsUIComponent, FactDimensionKeyRecord, MapColumnsUIComponent>()
		{
			@Override
			public void transition(WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> from, WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> to, WizardStateTransition<FactDimensionKeyRecord, MapColumnsUIComponent, FactDimensionKeyRecord, MapColumnsUIComponent> transition)
			{
				dimensionKeyIndex.getAndIncrement();
			}
		}).create();

		// loop back to select keys for the next one.  This transition will only be active if there are no more keys to process for the current dimension
		wizardModelBuilder.newTransition(mapColumns, selectKey).id("nextDimension").logicalName("Next Dimension").handler(new StateTransitionHandler<FactDimensionKeyRecord, MapColumnsUIComponent, MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent>()
		{
			@Override
			public void transition(WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> from, WizardState<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> to, WizardStateTransition<FactDimensionKeyRecord, MapColumnsUIComponent, MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> transition)
			{
				dimensionIndex.incrementAndGet();
				prepareSelectKeysState(to, dimensions, dimensionIndex);
			}
		}).create();

		// for the last pass, process the keys
		WizardStateTransition<FactDimensionKeyRecord, MapColumnsUIComponent, Object, NullUIComponent> tr = wizardModelBuilder.newTransition(mapColumns, close).id("process").logicalName("Process").handler(new StateTransitionHandler<FactDimensionKeyRecord, MapColumnsUIComponent, Object, NullUIComponent>()
		{
			@Override
			public void transition(WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> from, WizardState<Object, NullUIComponent> to, WizardStateTransition<FactDimensionKeyRecord, MapColumnsUIComponent, Object, NullUIComponent> transition)
			{
				// processing all the keys
				for (FactDimensionKeyRecord fdkr : recordsToProcess)
				{
					FactDimensionKey.FactDimensionKeyBuilder kb = fact.newFactDimensionKey()
						.keyName(fdkr.dimensionKey().logicalName())
						.dimension(fdkr.dimensionKey().dimension().logicalName())
						.physicalName(fdkr.physicalName());

					if (fdkr.roleName() != null)
					{
						kb.roleName(fdkr.roleName());
					}

					// map columns
					for (Map.Entry<String, String> col : fdkr.columns().entrySet())
					{
						kb.join(col.getValue(), col.getKey());
					}

					kb.create();
				}

				// persist
				EventQueue.invokeLater(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							UIHub.profile().persist();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
					}
				});
			}
		}).create();

		// deactivate process by default
		tr.activate(false);

		wizardModelBuilder.newTransition(selectDimension, close).id("close").logicalName("Close").create();

		wizardModelBuilder.newTransition(selectKey, close).id("close").logicalName("Close").create();

		wizardModelBuilder.initial(selectDimension);

		return wizardModelBuilder.create();
	}

	private static void updateMapColumnsTransitions(WizardState<FactDimensionKeyRecord, MapColumnsUIComponent> to, List<DimensionKey> dimensionKeys, AtomicInteger dimensionKeyIndex, List<Dimension> dimensions, AtomicInteger dimensionIndex)
	{
		// assign the fact and the dimension to the model
		to.model().dimensionKey(dimensionKeys.get(dimensionKeyIndex.get()));

		to.transitionMap().get("nextDimension").activate(false);
		to.transitionMap().get("mapNextColumns").activate(false);
		to.transitionMap().get("process").activate(false);

		// check more keys
		if (dimensionKeys.size() == (dimensionKeyIndex.get() + 1))
		{
			// determine if process or next dimension are available
			if (dimensions.size() == (dimensionIndex.get() + 1))
			{
				// no more to do - activate process and deactivate next dimension
				to.transitionMap().get("process").activate(true);
			}
			else
			{
				// no more to do - activate process and deactivate next dimension
				to.transitionMap().get("nextDimension").activate(true);
			}
		}
		else
		{
			to.transitionMap().get("mapNextColumns").activate(true);
		}

		// never enabled by default.  There is always at least one mandatory action to perform.
		to.transitionMap().get("nextDimension").enable(false);
		to.transitionMap().get("mapNextColumns").enable(false);
		to.transitionMap().get("process").enable(false);
	}

	private static void prepareSelectKeysState(WizardState<MutablePair<Dimension, Reference<List<DimensionKey>>>, SelectDimensionKeyUIComponent> to, List<Dimension> dimensions, AtomicInteger dimensionIndex)
	{
		to.model().setLeft(dimensions.get(dimensionIndex.get()));
		to.model().setRight(new Reference<List<DimensionKey>>());
	}
}
