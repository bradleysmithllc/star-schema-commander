package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;

import javax.swing.*;
import java.util.List;

public class FactMeasureOptionPanelInnerds implements DataGridNavigatorOptionPanel.OptionPanelInnerds<FactMeasure, DataGridNavigator.IncludedMeasure>
{
	@Override
	public String uniquelyIdentifyContext(Object obj)
	{
		return "Fact";
	}

	@Override
	public String uniquelyIdentifyM(FactMeasure measure)
	{
		return measure.logicalName();
	}

	@Override
	public String uniquelyIdentifyT(DataGridNavigator.IncludedMeasure includedMeasure)
	{
		return "Fact." + includedMeasure.measure().logicalName();
	}

	@Override
	public CoordinatedTableModel<DataGridNavigator.IncludedMeasure> createIncludedTableModel()
	{
		return new FactMeasureNavigatorTableModel();
	}

	@Override
	public CoordinatedTableModel<FactMeasure> createAvailableTableModel()
	{
		return new AvailableFactMeasureNavigatorTableModel();
	}

	@Override
	public Query.aggregation aggregation(DataGridNavigator.IncludedMeasure m)
	{
		return m.aggregation();
	}

	@Override
	public FactMeasure getM(DataGridNavigator.IncludedMeasure m)
	{
		return m.measure();
	}

	@Override
	public void initializeContextualComboBoxModel(Fact exploring, DefaultComboBoxModel model)
	{
		model.addElement(exploring);
	}

	@Override
	public boolean contextComboBoxVisible(Fact exploring)
	{
		return false;
	}

	@Override
	public void populateAvailableTableModel(final Fact fact, CoordinatedTableModel<FactMeasure> model, Object context)
	{
		model.visit(new CoordinatedTableModel.ModelVisitor<FactMeasure>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<FactMeasure> model)
			{
				// add all measures to available as well so they can be added at different aggregation levels
				for (FactMeasure measure : fact.measures().values())
				{
					model.addElement(measure);
				}
			}
		});
	}

	@Override
	public void populateIncludedTableModel(final Fact fact, CoordinatedTableModel<DataGridNavigator.IncludedMeasure> model)
	{
		// Added back so the navigator would be in synch with the explorer.
		// Add a clear button instead.
		model.visit(new CoordinatedTableModel.ModelVisitor<DataGridNavigator.IncludedMeasure>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<DataGridNavigator.IncludedMeasure> model)
			{
				// add all measures by default
				for (FactMeasure measure : fact.measures().values())
				{
					DataGridNavigator.IncludedMeasure e = new DataGridNavigator.IncludedMeasure(measure);
					e.alias(measure.logicalName());
					model.addElement(e);
				}
			}
		});
	}

	@Override
	public void demoteToAvailable(final DataGridNavigator.IncludedMeasure includedMeasure, CoordinatedTableModel<FactMeasure> availableModel, Object currentContext)
	{
		if (!availableModel.tableData().contains(includedMeasure.measure()))
		{
			availableModel.visit(new CoordinatedTableModel.ModelVisitor<FactMeasure>()
			{
				@Override
				public void visit(CoordinatedTableModel.UpdatableTableModel<FactMeasure> model)
				{
					model.addElement(includedMeasure.measure());
				}
			});
		}
	}

	@Override
	public void promoteSelectedToIncluded(final Query.aggregation agg, final CoordinatedTableModel<FactMeasure> available, CoordinatedTableModel<DataGridNavigator.IncludedMeasure> included, final ListSelectionModel selectionModel, Object context)
	{
		included.visit(new CoordinatedTableModel.ModelVisitor<DataGridNavigator.IncludedMeasure>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<DataGridNavigator.IncludedMeasure> model)
			{
				// iterate through selected items
				List<FactMeasure> td = available.tableData();

				for (int row = 0; row < td.size(); row++)
				{
					if (selectionModel.isSelectedIndex(row))
					{
						FactMeasure tditem = td.get(row);
						DataGridNavigator.IncludedMeasure e = new DataGridNavigator.IncludedMeasure(tditem);

						String alias = (agg == Query.aggregation.none ? "" : (agg.name() + "_")) + tditem.logicalName();

						e.alias(alias);

						e.aggregation(agg);
						model.addElement(e);
					}
				}
			}
		});
	}

	@Override
	public void initializeContextualComboBox(Fact exploring, JComboBox availableContextComboBox)
	{
	}
}