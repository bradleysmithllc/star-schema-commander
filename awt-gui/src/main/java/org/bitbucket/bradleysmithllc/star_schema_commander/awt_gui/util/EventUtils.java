package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.RequiresJFXEventThread;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.RequiresSwingEventThread;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class EventUtils
{
	static
	{
		if (!Platform.isFxApplicationThread())
		{
			final CountDownLatch cdl = new CountDownLatch(1);

			PlatformImpl.startup(new Runnable()
			{
				public void run()
				{
					cdl.countDown();
				}
			});

			try
			{
				cdl.await();
			}
			catch (InterruptedException e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	public static void requireEventThread()
	{
		if (!isEventThread())
		{
			throw new RuntimeException();
		}
	}

	public static boolean isEventThread()
	{
		return EventQueue.isDispatchThread();
	}

	public static boolean isJFXEventThread()
	{
		return Platform.isFxApplicationThread();
	}

	public static void requireJFXEventThread()
	{
		if (!isJFXEventThread())
		{
			throw new RuntimeException();
		}
	}

	public static void runOnEventThread(Runnable run)
	{
		if (EventQueue.isDispatchThread())
		{
			run.run();
		}
		else
		{
			try
			{
				EventQueue.invokeAndWait(run);
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	public static void runOnEventThreadLater(Runnable run)
	{
		if (EventQueue.isDispatchThread())
		{
			run.run();
		}
		else
		{
			EventQueue.invokeLater(run);
		}
	}

	public static void runOnJFXEventThread(Runnable runnable)
	{
		if (Platform.isFxApplicationThread())
		{
			runnable.run();
		}
		else
		{
			PlatformImpl.runAndWait(runnable);
		}
	}

	public static void runOnJFXEventThreadLater(Runnable runnable)
	{
		PlatformImpl.runLater(runnable);
	}

	public static <T> Object invokeThreadwise(final Object obj, String methodName, final Class[] ptypes, final Object... parameters) throws InvocationTargetException
	{
		Class<?> aClass = obj.getClass();
		final Method m = MethodUtils.getAccessibleMethod(aClass, methodName, ptypes);

		if (m == null)
		{
			throw new IllegalArgumentException("Method {" + methodName + "} not accessible on class {" + aClass + "}");
		}

		return invokeThreadwise(obj, m, parameters);
	}

	public static <T> Object invokeThreadwise(final Object obj, final Method m, final Object... parameters) throws InvocationTargetException
	{
		final AtomicReference<T> ar = new AtomicReference<T>();

		try
		{
			boolean swingAnn = m.isAnnotationPresent(RequiresSwingEventThread.class);
			boolean jfxAnn = m.isAnnotationPresent(RequiresJFXEventThread.class);

			if (swingAnn && jfxAnn)
			{
				throw new IllegalStateException("Cannot require swing and JFX event thread");
			}

			if (swingAnn)
			{
				// cannot do it if we are on the jfx event thread
				if (isJFXEventThread())
				{
					throw new IllegalStateException("Cannot get onto swing event thread from the jfx event thread.");
				}
				else if (isEventThread())
				{
					ar.set((T) m.invoke(obj, parameters));
				}
				else
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								ar.set((T) m.invoke(obj, parameters));
							}
							catch (Exception e)
							{
								throw new RuntimeException(e);
							}
						}
					});
				}
			}
			else if (jfxAnn)
			{
				// cannot do it if we are on the jfx event thread
				if (isEventThread())
				{
					throw new IllegalStateException("Cannot get onto jfx event thread from the swing event thread.");
				}

				if (isJFXEventThread())
				{
					ar.set((T) m.invoke(obj, parameters));
				}
				else
				{
					runOnJFXEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								ar.set((T) m.invoke(obj, parameters));
							}
							catch (Exception e)
							{
								throw new RuntimeException(e);
							}
						}
					});
				}
			}
			else
			{
				ar.set((T) m.invoke(obj, parameters));
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}

		return ar.get();
	}

	public static void enableJFXEventQueue()
	{
		System.setProperty("javafx.macosx.embedded", "true");
		java.awt.Toolkit.getDefaultToolkit();
		Platform.setImplicitExit(false);
	}
}
