package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import java.awt.*;

public class ConnectionConfirmationUIComponent extends WizardStateUIComponent<Reference<DatabaseConnection>>
{
	private JPanel panel;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		panel.add(new JLabel(state().model().getReference().name()), BorderLayout.CENTER);
	}

	@Override
	public JComponent getComponent()
	{
		return panel;
	}
}