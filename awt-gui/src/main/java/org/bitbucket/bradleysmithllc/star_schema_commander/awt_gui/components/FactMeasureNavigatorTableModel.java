package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FactMeasureNavigatorTableModel extends CoordinatedTableModel<DataGridNavigator.IncludedMeasure>
{
	@Override
	public String getCoordinatedColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Logical Name";
			case 1:
				return "Data Type";
			case 2:
				return "Name";
			case 3:
				return "Alias";
			case 4:
				return "Include";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	protected Class<?> getCoordinatedColumnClass(int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
			case 1:
			case 2:
			case 3:
				return String.class;
			case 4:
				return Query.aggregation.class;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getCoordinatedColumnCount()
	{
		return 5;
	}

	@Override
	public Object getCoordinatedValueAt(DataGridNavigator.IncludedMeasure td, int rowIndex, int columnIndex)
	{
		switch (columnIndex)
		{
			case 0:
				return td.measure().logicalName();
			case 1:
				return td.measure().column().jdbcTypeName();
			case 2:
				return td.measure().column().name();
			case 3:
				return td.alias();
			case 4:
				return td.aggregation();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void setCoordinatedValueAt(DataGridNavigator.IncludedMeasure td, Object aValue, final int rowIndex, int columnIndex)
	{
		switch (columnIndex)
		{
			case 3:
				td.alias(String.valueOf(aValue));
				break;
			case 4:
				if (aValue == null)
				{
					// drop the row
					visit(new ModelVisitor<DataGridNavigator.IncludedMeasure>()
					{
						@Override
						public void visit(UpdatableTableModel<DataGridNavigator.IncludedMeasure> model)
						{
							model.removeElementAt(rowIndex);
						}
					});
				}
				else
				{
					td.aggregation((Query.aggregation) aValue);
				}
				break;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public boolean isCoordinatedCellEditable(int rowIndex, int columnIndex)
	{
		return columnIndex == 3 || columnIndex == 4;
	}

	public List<DataGridNavigator.IncludedMeasure> selectedMeasures()
	{
		List<DataGridNavigator.IncludedMeasure> alist = new ArrayList<DataGridNavigator.IncludedMeasure>(tableData());

		Iterator<DataGridNavigator.IncludedMeasure> it = alist.iterator();

		// remove all excluded measures
		DataGridNavigator.IncludedMeasure sm = null;

		while (it.hasNext())
		{
			sm = it.next();

			if (sm.aggregation() == null)
			{
				it.remove();
			}
		}

		return alist;
	}

	public int locateMeasure(FactMeasure fm)
	{
		int row = 0;

		for (DataGridNavigator.IncludedMeasure td : tableData())
		{
			if (td.measure() == fm)
			{
				return row;
			}

			row++;
		}

		throw new IllegalArgumentException();
	}
}
