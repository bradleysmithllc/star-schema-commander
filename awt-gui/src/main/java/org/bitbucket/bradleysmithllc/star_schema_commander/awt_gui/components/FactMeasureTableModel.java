package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class FactMeasureTableModel extends AbstractTableModel
{
	private final TreeMap<String, FactMeasure> tableMap = new TreeMap<String, FactMeasure>();
	private final List<FactMeasure> tableData = new ArrayList<FactMeasure>();

	@Override
	public int getRowCount()
	{
		return size();
	}

	@Override
	public String getColumnName(int column)
	{
		switch (column)
		{
			case 0:
				return "Logical Name";
			case 1:
				return "Data Type";
			case 2:
				return "Name";
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public int getColumnCount()
	{
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		FactMeasure td = tableData.get(rowIndex);

		switch (columnIndex)
		{
			case 0:
				return td.logicalName();
			case 1:
				return td.column().jdbcTypeName();
			case 2:
				return td.column().name();
			default:
				throw new IllegalArgumentException();
		}
	}

	public void addMeasure(FactMeasure fm)
	{
		tableData.add(fm);
		tableMap.put(fm.logicalName(), fm);

		Collections.sort(tableData);

		fireTableDataChanged();
	}

	public int size()
	{
		return tableData.size();
	}

	public List<FactMeasure> tableData()
	{
		return tableData;
	}

	public boolean isEmpty()
	{
		return tableData.isEmpty();
	}

	public void clear()
	{
		int rowCount = tableData.size();

		if (rowCount > 0)
		{
			tableData.clear();

			fireTableDataChanged();
		}
	}
}
