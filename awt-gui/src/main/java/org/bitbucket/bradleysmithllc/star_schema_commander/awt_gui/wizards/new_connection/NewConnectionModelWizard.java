package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardModelBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.H2ConnectionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.HANAConnectionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.OracleConnectionBuilder;

import java.io.IOException;

public class NewConnectionModelWizard extends UIWizard
{
	public NewConnectionModelWizard()
	{
		super(createModel());
	}

	private static WizardStateModel createModel()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		WizardState<Reference<String>, SelectConnectionTypeUIComponent> selectConnection =
			wizardModelBuilder.newState(new Reference<String>(), new SelectConnectionTypeUIComponent()).id("ConnectionType").create();

		WizardState<HANAConnectionBuilder, HANAConnectionUIComponent> newHANAConnection =
			wizardModelBuilder.newState(UIHub.profile().newHANAConnection(), new HANAConnectionUIComponent()).id("NewHANAConnection").create();

		WizardState<H2ConnectionBuilder, H2ConnectionUIComponent> newH2onnection =
			wizardModelBuilder.newState(UIHub.profile().newH2Connection(), new H2ConnectionUIComponent()).id("NewH2Connection").create();

		WizardState<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> newSqlJtdsConnection =
			wizardModelBuilder.newState((SqlConnectionBuilderWrapper) new JTDSSqlConnectionBuilderWrapper(), new SqlServerJTDSConnectionUIComponent()).id("NewSqlServerJtdsConnection").create();

		WizardState<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> newSqlMSConnection =
			wizardModelBuilder.newState((SqlConnectionBuilderWrapper) new MSSqlConnectionBuilderWrapper(), new SqlServerJTDSConnectionUIComponent()).id("NewSqlServerMSConnection").create();

		WizardState<OracleConnectionBuilder, OracleConnectionUIComponent> newOracleConnection =
			wizardModelBuilder.newState(UIHub.profile().newOracleConnection(), new OracleConnectionUIComponent()).id("NewOracleConnection").create();

		WizardState<Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> done =
			wizardModelBuilder.newState(new Reference<DatabaseConnection>(), new ConnectionConfirmationUIComponent()).id("confirmConnection").create();

		wizardModelBuilder.newTransition(selectConnection, newH2onnection).id("h2").logicalName("Create H2 Connection").handler(new StateTransitionHandler<Reference<String>, SelectConnectionTypeUIComponent, H2ConnectionBuilder, H2ConnectionUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<String>, SelectConnectionTypeUIComponent> from, WizardState<H2ConnectionBuilder, H2ConnectionUIComponent> to, WizardStateTransition<Reference<String>, SelectConnectionTypeUIComponent, H2ConnectionBuilder, H2ConnectionUIComponent> transition)
			{
				String name = from.model().getReference();

				to.component().setName(name);
				to.model().name(name);
			}
		}).create();

		wizardModelBuilder.newTransition(selectConnection, newHANAConnection).id("hana").logicalName("Create HANA Connection").handler(new StateTransitionHandler<Reference<String>, SelectConnectionTypeUIComponent, HANAConnectionBuilder, HANAConnectionUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<String>, SelectConnectionTypeUIComponent> from, WizardState<HANAConnectionBuilder, HANAConnectionUIComponent> to, WizardStateTransition<Reference<String>, SelectConnectionTypeUIComponent, HANAConnectionBuilder, HANAConnectionUIComponent> transition)
			{
				String name = from.model().getReference();

				to.model().name(name);
			}
		}).create();

		wizardModelBuilder.newTransition(selectConnection, newOracleConnection).id("oracle").logicalName("Create Oracle Connection").handler(new StateTransitionHandler<Reference<String>, SelectConnectionTypeUIComponent, OracleConnectionBuilder, OracleConnectionUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<String>, SelectConnectionTypeUIComponent> from, WizardState<OracleConnectionBuilder, OracleConnectionUIComponent> to, WizardStateTransition<Reference<String>, SelectConnectionTypeUIComponent, OracleConnectionBuilder, OracleConnectionUIComponent> transition)
			{
				String name = from.model().getReference();

				to.model().name(name);
			}
		}).create();
		wizardModelBuilder.newTransition(selectConnection, newSqlJtdsConnection).id("sqlserver_jtds").logicalName("Create Sql Server Connection").handler(new StateTransitionHandler<Reference<String>, SelectConnectionTypeUIComponent, SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<String>, SelectConnectionTypeUIComponent> from, WizardState<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> to, WizardStateTransition<Reference<String>, SelectConnectionTypeUIComponent, SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> transition)
			{
				String name = from.model().getReference();

				to.model().connectionName(name);
			}
		}).create();

		wizardModelBuilder.newTransition(selectConnection, newSqlMSConnection).id("sqlserver_ms").logicalName("Create Sql Server Connection").handler(new StateTransitionHandler<Reference<String>, SelectConnectionTypeUIComponent, SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<String>, SelectConnectionTypeUIComponent> from, WizardState<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> to, WizardStateTransition<Reference<String>, SelectConnectionTypeUIComponent, SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> transition)
			{
				String name = from.model().getReference();

				to.model().connectionName(name);
			}
		}).create();

		wizardModelBuilder.newTransition(selectConnection, newH2onnection).id("postgresql").logicalName("Create PostgreSQL Connection").create();

		wizardModelBuilder.newTransition(newH2onnection, done).id("confirm").logicalName("Create Connection").handler(
			new StateTransitionHandler<H2ConnectionBuilder, H2ConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent>()
			{
				@Override
				public void transition(WizardState<H2ConnectionBuilder, H2ConnectionUIComponent> from, WizardState<Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> to, WizardStateTransition<H2ConnectionBuilder, H2ConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> transition)
				{
					to.model().setReference(from.model().create());

					try
					{
						UIHub.profile().persist();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		).create();

		wizardModelBuilder.newTransition(newSqlJtdsConnection, done).id("confirm").logicalName("Create Connection").handler(new StateTransitionHandler<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent>()
																																																												{
																																																													@Override
																																																													public void transition(WizardState<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> from, WizardState<Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> to, WizardStateTransition<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> transition)
																																																													{
																																																														to.model().setReference(from.model().create());

																																																														try
																																																														{
																																																															UIHub.profile().persist();
																																																														}
																																																														catch (IOException e)
																																																														{
																																																															e.printStackTrace();
																																																														}
																																																													}
																																																												}
		).create();

		wizardModelBuilder.newTransition(newHANAConnection, done).id("confirm").logicalName("Create Connection").handler(new StateTransitionHandler<HANAConnectionBuilder, HANAConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent>()
		{
			@Override
			public void transition(WizardState<HANAConnectionBuilder, HANAConnectionUIComponent> from, WizardState<Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> to, WizardStateTransition<HANAConnectionBuilder, HANAConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> transition)
			{
				to.model().setReference(from.model().create());

				try
				{
					UIHub.profile().persist();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}).create();

		wizardModelBuilder.newTransition(newSqlMSConnection, done).id("confirm").logicalName("Create Connection").handler(new StateTransitionHandler<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent>()
		{
			@Override
			public void transition(WizardState<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent> from, WizardState<Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> to, WizardStateTransition<SqlConnectionBuilderWrapper, SqlServerJTDSConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> transition)
			{
				to.model().setReference(from.model().create());

				try
				{
					UIHub.profile().persist();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}).create();

		wizardModelBuilder.newTransition(newOracleConnection, done).id("confirm").logicalName("Create Connection").handler(
			new StateTransitionHandler<OracleConnectionBuilder, OracleConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent>()
			{
				@Override
				public void transition(WizardState<OracleConnectionBuilder, OracleConnectionUIComponent> from, WizardState<Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> to, WizardStateTransition<OracleConnectionBuilder, OracleConnectionUIComponent, Reference<DatabaseConnection>, ConnectionConfirmationUIComponent> transition)
				{
					to.model().setReference(from.model().create());

					try
					{
						UIHub.profile().persist();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		).create();

		wizardModelBuilder.initial(selectConnection);

		return wizardModelBuilder.create();
	}
}
