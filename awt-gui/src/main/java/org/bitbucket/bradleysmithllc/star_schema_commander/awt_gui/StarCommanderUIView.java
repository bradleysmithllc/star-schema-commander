package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.AbstractSwingUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.AbstractUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;

import javax.swing.*;
import java.awt.*;

public class StarCommanderUIView extends AbstractSwingUIView
{
	private NavPanel navPanel;
	private BreadcrumbPanel breadcrumbPanel;
	private ActionPanel actionPanel;
	private ActionView actionView;
	private FactDimensionPanel factDimensionPanel;

	private JPanel rootPanel;
	private JPanel headerPanel;

	@Override
	public void subInitialize() throws Exception
	{
		navPanel = new NavPanel();
		breadcrumbPanel = new BreadcrumbPanel();
		actionPanel = new ActionPanel();
		actionView = new ActionView();
		factDimensionPanel = new FactDimensionPanel();
		actionPanel = new ActionPanel();

		addChild(navPanel);
		addChild(breadcrumbPanel);
		addChild(actionPanel);
		addChild(actionView);
		addChild(factDimensionPanel);
		addChild(actionPanel);
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				rootPanel = new JPanel();

				rootPanel.setBackground(Color.white);

				rootPanel.setLayout(new BorderLayout());
				headerPanel = new JPanel();
				rootPanel.add(headerPanel, BorderLayout.NORTH);
				headerPanel.setBackground(new Color(214, 212, 214));
				headerPanel.add(new JLabel("Header"), BorderLayout.CENTER);

				JPanel centerPanel = new JPanel();
				centerPanel.setLayout(new BorderLayout());
				rootPanel.add(centerPanel, BorderLayout.CENTER);
				JPanel center2Panel = new JPanel();
				center2Panel.setLayout(new BorderLayout());
				centerPanel.add(center2Panel, BorderLayout.NORTH);
				final UIView<JComponent> breadcrumbPanel = UIHub.get().locate(BreadcrumbPanel.class.getSimpleName());
				center2Panel.add(breadcrumbPanel.view(), BorderLayout.CENTER);

				final UIView<Node> actionPanel = UIHub.get().locate(ActionView.class.getSimpleName());
				final JFXPanel jfxPanel = new JFXPanel();

				JPanel actionContainer = new JPanel();
				actionContainer.setLayout(new GridLayout(1, 2));
				actionContainer.add(
					(
						(UIView<JComponent>) UIHub.get().locate(ActionPanel.class.getSimpleName())
					).view());
				actionContainer.add(jfxPanel);

				center2Panel.add(actionContainer, BorderLayout.EAST);

				EventUtils.runOnJFXEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						BorderPane borderpane = new BorderPane();
						borderpane.setCenter(actionPanel.view());

						jfxPanel.setScene(new Scene(borderpane));
					}
				});

				final UIView<JComponent> factDimensionPanel = UIHub.get().locate(FactDimensionPanel.class.getSimpleName());
				centerPanel.add(factDimensionPanel.view(), BorderLayout.CENTER);

				final UIView<JComponent> navPanel = UIHub.get().locate(NavPanel.class.getSimpleName());
				rootPanel.add(navPanel.view(), BorderLayout.WEST);
			}
		});
	}

	public void setUpdateListener(final UIUpdateListener updateListener)
	{
		UIHub.get().addUIController(new AbstractUIController()
		{
			@Override
			public void stateChanged(String key, Object value)
			{
				if (key.equals("profileRefresh"))
				{
					updateListener.uiUpdated();
				}
			}
		});
	}


	@Override
	public JComponent view()
	{
		return rootPanel;
	}
}
