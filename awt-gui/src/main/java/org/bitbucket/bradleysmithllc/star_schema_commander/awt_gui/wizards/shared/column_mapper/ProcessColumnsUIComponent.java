package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.laf.progressbar.WebProgressBar;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class ProcessColumnsUIComponent<T> extends WizardStateUIComponent<Reference<ColumnsSpec<T>>>
{
	public static final String MSG_SET_LOGICAL_NAME = "MSG_SET_LOGICAL_NAME";

	public static final int MOD_LOGICAL_NAME = 0;
	public static final int MOD_PHYSICAL_NAME = 1;
	public static final int MOD_DBC = 2;

	private WizardStateTransition<Reference<ColumnsSpec<T>>, ? extends WizardStateUIComponent<Reference<ColumnsSpec<T>>>, ?, ?> nextTr;
	private JPanel container;

	private JLabel physicalName;
	private JTextField logicaName;
	private WebProgressBar webProgressBar;

	@Override
	public void subInitialize() throws Exception
	{
		container = new JPanel();
		container.setLayout(new BorderLayout());

		webProgressBar = new WebProgressBar(WebProgressBar.HORIZONTAL, 1, 1);
		webProgressBar.setIndeterminate(false);

		container.add(webProgressBar, BorderLayout.NORTH);

		physicalName = new JLabel();

		logicaName = new JTextField();

		container.add(physicalName, BorderLayout.CENTER);
		container.add(logicaName, BorderLayout.SOUTH);
	}

	@Override
	public void activate()
	{
		// hide the finish step until we have visited all
		nextTr = state().transitionMap().get("next");

		// deactivate next if there are no more
		ColumnsSpec<T> spec = state().model().getReference();

		nextTr.activate(spec.index() < (spec.columnsToMap.size() - 1));
		webProgressBar.setMaximum(spec.columnsToMap.size() + 1);
		webProgressBar.setValue(spec.index() + 1);

		DatabaseColumn phName = spec.columnsToMap.get(spec.index());
		physicalName.setText(phName.name());

		String logName = phName.name();

		logicaName.setText(logName);
	}

	@Override
	public <F> F getModel(int id)
	{
		switch (id)
		{
			case MOD_LOGICAL_NAME:
				return (F) logicaName.getText();
			case MOD_PHYSICAL_NAME:
				return (F) physicalName.getText();
			case MOD_DBC:
				ColumnsSpec<T> spec = state().model().getReference();
				DatabaseColumn phName = spec.columnsToMap.get(spec.index());
				return (F) phName;
		}

		throw new IllegalArgumentException();
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SET_LOGICAL_NAME)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						logicaName.setText(String.valueOf(value));
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JComponent getComponent()
	{
		return container;
	}
}