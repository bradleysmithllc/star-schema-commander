package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.alee.extended.breadcrumb.WebBreadcrumb;
import com.alee.extended.breadcrumb.WebBreadcrumbButton;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.IProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

public class BreadcrumbPanel extends AbstractUIComponent
{
	public static final int STATE_BREADCRUMB_VISIBLE = 0;

	public static final String MSG_SELECT_BREADCRUMB = "MSG_SELECT_BREADCRUMB";

	private final WebBreadcrumb wbc = new WebBreadcrumb(true);
	private String graph;

	protected void refreshVisibility(IProfile profile)
	{
		if (profile.defaultReportingArea().children().size() == 0)
		{
			wbc.setVisible(false);
		}
		else
		{
			wbc.setVisible(true);
		}
	}

	@Override
	protected void buildParent(Profile profile)
	{
		refreshVisibility(profile);
		updateReportingArea(profile.defaultReportingArea());
	}

	@Override
	public JComponent getComponent()
	{
		return wbc;
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_BREADCRUMB_VISIBLE:
				return wbc.isVisible();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case BREADCRUMB_PANEL_GRAPH:
				return (T) graph;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void dispatch(String message, final Object value)
	{
		if (message == MSG_SELECT_BREADCRUMB)
		{
			try
			{
				EventQueue.invokeAndWait(new Runnable()
				{
					@Override
					public void run()
					{
						for (int i = 0; i < wbc.getComponentCount(); i++)
						{
							WebBreadcrumbButton comp = (WebBreadcrumbButton) wbc.getComponent(i);

							String n = comp.getText();

							if (n.equals(value))
							{
								comp.doClick();
							}
						}

						wbc.revalidate();
						wbc.repaint();
					}
				});
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			throw new IllegalArgumentException();
		}
	}

	@Override
	public void stateChanged(stateKey key, final Object value)
	{
		switch (key)
		{
			case selectedReportingArea:
				if (value == null)
				{
					throw new IllegalArgumentException("Null RA");
				}

				try
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							updateReportingArea((ReportingArea) value);
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
			case profileRefresh:
				try
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							refreshVisibility((Profile) value);
							updateReportingArea(UIHub.selectedReportingArea());
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
		}
	}

	private void updateReportingArea(ReportingArea value)
	{
		requireEventThread();

		wbc.removeAll();
		graph = null;

		class Duple
		{
			String name;
			String qualifiedName;
			ReportingArea reportingArea;

			Duple(ReportingArea ra)
			{
				reportingArea = ra;
			}

			Duple(String n, String q)
			{
				name = n;
				qualifiedName = q;
			}
		}

		LinkedList<Duple> raStack = new LinkedList<Duple>();

		while (value != null)
		{
			raStack.push(new Duple(value));
			value = value.parent();
		}

		Duple name = raStack.pop();

		while (name != null)
		{
			final String text = name.reportingArea.name();
			final ReportingArea selectedRa = name.reportingArea;

			if (graph == null)
			{
				graph = text;
			}
			else
			{
				graph += "." + text;
			}

			WebBreadcrumbButton comp = new WebBreadcrumbButton(text);

			comp.addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						runOnEventThread(new Runnable()
						{
							@Override
							public void run()
							{
								UIHub.selectReportingArea(selectedRa);
							}
						});
					}
					catch (Exception e1)
					{
						e1.printStackTrace();
					}
				}
			});

			wbc.add(comp);
			name = raStack.size() == 0 ? null : raStack.pop();
		}

		wbc.revalidate();
		wbc.repaint();
	}
}
