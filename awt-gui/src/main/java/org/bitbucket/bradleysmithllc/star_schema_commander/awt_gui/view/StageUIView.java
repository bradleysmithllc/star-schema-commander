package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.embed.swing.JFXPanel;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;

public class StageUIView extends AbstractUIView<Node>
{
	public static final int STATE_VISIBLE = 0;

	private UIView<Node> view;

	private JFXPanel jfxPanel;
	private Stage stage;

	public StageUIView(UIView<Node> view)
	{
		setView(view);
	}

	public StageUIView()
	{
	}

	public static void runOnEventThread(Runnable run) throws Exception
	{
		if (EventQueue.isDispatchThread())
		{
			run.run();
		}
		else
		{
			EventQueue.invokeAndWait(run);
		}
	}

	public void setView(UIView<Node> view)
	{
		this.view = view;
		clearChildViews();
		addChild(view);
	}

	@Override
	public boolean checkState(int value)
	{
		switch (value)
		{
			case STATE_VISIBLE:
				return stage != null && stage.isShowing();
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void activate()
	{
		stage.show();
	}

	@Override
	public void subInitialize() throws Exception
	{
		stage = new Stage();
		stage.setTitle("Java FX");
	}

	@Override
	public void prepareSelf()
	{
		if (stage.isShowing())
		{
			stage.hide();
		}
	}

	@Override
	public void prepareWithChildren()
	{
		// add child view to the center portion of this pane
		BorderPane borderpane = new BorderPane();
		borderpane.setCenter(view.view());

		Scene sc = new Scene(borderpane);

		stage.setScene(sc);

		//stage.setLocationRelativeTo(null);
		//stage.pack();
	}

	@Override
	public Node view()
	{
		stage.show();
		return null;
	}

	@Override
	public void deactivate()
	{
		stage.close();
	}

	public JComponent container()
	{
		return jfxPanel;
	}

	public Stage stage()
	{
		return stage;
	}
}
