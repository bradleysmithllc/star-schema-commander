package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;

public class UIContainerImpl implements UIContainer
{
	@Override
	public void addView(final UIView view)
	{
		// stuff that should only be done once
		try
		{
			view.container(this);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		UIView proxyView = proxy(view);

		UIHub.get().addComponent(proxyView);

		// regular lifecycle
		if (!proxyView.initialized())
		{
			try
			{
				proxyView.initialize();
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}

		reactivate(proxyView);
	}

	public void addView(final UIView view, final UIView child)
	{
		// stuff that should only be done once
		try
		{
			child.container(this);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		UIView proxyView = proxy(child);

		UIHub.get().addComponent(proxyView, view);

		// regular lifecycle
		if (!proxyView.initialized())
		{
			try
			{
				proxyView.initialize();
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}

		reactivate(proxyView);
	}

	@Override
	public <T extends UIView> T locateViewGlobally(String clType)
	{
		return UIHub.get().locate(clType);
	}

	@Override
	public <T extends UIView> T locateViewGlobally(String clType, String instance)
	{
		return UIHub.get().locate(clType, instance);
	}

	@Override
	public <T extends UIView> T locateViewLocally(UIView parent, String clType)
	{
		return locateViewLocally(parent, clType, clType);
	}

	@Override
	public <T extends UIView> T locateViewLocally(UIView parent, String clType, String instance)
	{
		List<UIView> ch = UIHub.get().children(parent);

		for (UIView v : ch)
		{
			if (clType.equals(v.getClass()) && instance.equals(v.viewInstance()))
			{
				return (T) v;
			}
		}

		throw new IllegalArgumentException("View not found [" + clType + "." + instance);
	}

	private UIView proxy(final UIView view)
	{
		// don't double-proxy
		if (!Proxy.isProxyClass(view.getClass()))
		{
			return (UIView) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{
					UIView.class
				}, new InvocationHandler()
				{
					final UIView viewToProxy = view;

					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
					{
						return EventUtils.invokeThreadwise(viewToProxy, method.getName(), method.getParameterTypes(), args);
					}
				}
			);
		}
		else
		{
			return view;
		}
	}

	@Override
	public void reactivate(UIView proxyView)
	{
		// grab the proxied instance
		proxyView = UIHub.get().locate(proxyView.viewClass(), proxyView.viewInstance());

		// reset the tree
		// locate any previous children and deactivate
		List<UIView> list = UIHub.get().children(proxyView);

		deacdeeply(list);

		UIHub.get().removeComponentSubTree(proxyView);

		// lifecycle - minus initialize
		try
		{
			proxyView.activate();

			// prepare the parent
			proxyView.prepareSelf();

			// full lifecycle each child
			// This cast is stupid.  Why does the type get erased?  It's in the interface.
			for (UIView c : (List<UIView>) proxyView.childViews())
			{
				addView(proxyView, c);
			}

			// post-prepare parent
			proxyView.prepareWithChildren();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	private void deacdeeply(List<UIView> list)
	{
		for (UIView view : list)
		{
			view.deactivate();

			List<UIView> clist = UIHub.get().children(view);
			deacdeeply(clist);
		}
	}
}