package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.AbstractUIController;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class TestUIController extends AbstractUIController implements SystemControllerConstants
{
	private final LinkedList<Pair<String, Object>> dispatchQueue = new LinkedList<Pair<String, Object>>();

	@Override
	public String controllerId()
	{
		return SystemUIController.class.getSimpleName();
	}

	@Override
	public boolean checkState(int value)
	{
		return false;
	}

	@Override
	public <T> T getModel(int id)
	{
		return null;
	}

	public List<Pair<String, Object>> recentDispatches()
	{
		return dispatchQueue;
	}

	public Pair<String, Object> lastDispatch()
	{
		return dispatchQueue.peekFirst();
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message, Object value)
	{
		dispatchQueue.addFirst(new ImmutablePair<String, Object>(message, value));

		while (dispatchQueue.size() > 10)
		{
			dispatchQueue.removeLast();
		}

		latch.countDown();
	}

	@Override
	public void dispatch(String message)
	{
		dispatch(message, null);
	}

	@Override
	public void stateChanged(String key, Object value)
	{
	}
}