package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.concurrent.CountDownLatch;

public class SelectConnectionTypeUIComponent extends WizardStateUIComponent<Reference<String>>
{
	public static final String MSG_SELECT_CONNECTION_TYPE = "MSG_SELECT_CONNECTION_TYPE";
	public static final int MODEL_CONNECTION_LIST = 0;
	public static final String MSG_CONNECTION_NAME = "MSG_CONNECTION_NAME";

	private JPanel panel;
	private DefaultListModel<String> listModel;
	private JList<String> connectionTypeList;

	private WizardStateTransition<Reference<String>, ? extends WizardStateUIComponent<Reference<String>>, ?, ?> h2Trans;
	private WizardStateTransition<Reference<String>, ? extends WizardStateUIComponent<Reference<String>>, ?, ?> hanaTrans;
	private WizardStateTransition<Reference<String>, ? extends WizardStateUIComponent<Reference<String>>, ?, ?> oracleTrans;
	private WizardStateTransition<Reference<String>, ? extends WizardStateUIComponent<Reference<String>>, ?, ?> sqlServerJTDSTrans;
	private WizardStateTransition<Reference<String>, ? extends WizardStateUIComponent<Reference<String>>, ?, ?> postgreSQLTrans;
	private JTextField connectionNameField;
	private WizardStateTransition<Reference<String>, ? extends WizardStateUIComponent<Reference<String>>, ?, ?> sqlServerMSTrans;

	@Override
	protected void buildParent(Profile profile) throws Exception
	{
		h2Trans = state().transitionMap().get("h2");
		h2Trans.enable(false);
		h2Trans.activate(false);

		hanaTrans = state().transitionMap().get("hana");
		hanaTrans.enable(false);
		hanaTrans.activate(false);

		oracleTrans = state().transitionMap().get("oracle");
		oracleTrans.enable(false);
		oracleTrans.activate(false);

		sqlServerJTDSTrans = state().transitionMap().get("sqlserver_jtds");
		sqlServerJTDSTrans.enable(false);
		sqlServerJTDSTrans.activate(false);

		sqlServerMSTrans = state().transitionMap().get("sqlserver_ms");
		sqlServerMSTrans.enable(false);
		sqlServerMSTrans.activate(false);

		postgreSQLTrans = state().transitionMap().get("postgresql");
		postgreSQLTrans.enable(false);
		postgreSQLTrans.activate(false);

		panel = new JPanel();
		panel.setLayout(new BorderLayout());

		listModel = new DefaultListModel<String>();

		connectionTypeList = new JList<String>(listModel);
		connectionTypeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		connectionTypeList.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				h2Trans.activate(false);
				hanaTrans.activate(false);
				oracleTrans.activate(false);
				sqlServerJTDSTrans.activate(false);
				sqlServerMSTrans.activate(false);
				postgreSQLTrans.activate(false);

				// check selected value
				String type = connectionTypeList.getSelectedValue();

				if (type != null)
				{
					if (type.equals("H2"))
					{
						h2Trans.activate(true);
					}
					else if (type.equals("HANA"))
					{
						hanaTrans.activate(true);
					}
					else if (type.equals("Oracle"))
					{
						oracleTrans.activate(true);
					}
					else if (type.equals("PostgreSQL"))
					{
						postgreSQLTrans.activate(true);
					}
					else if (type.equals("Sql Server (jTDS)"))
					{
						sqlServerJTDSTrans.activate(true);
					}
					else if (type.equals("Sql Server (MS)"))
					{
						sqlServerMSTrans.activate(true);
					}
				}
			}
		});

		panel.add(new JScrollPane(connectionTypeList), BorderLayout.CENTER);

		final JLabel label = new JLabel("Name");
		connectionNameField = new JTextField();
		label.setLabelFor(connectionNameField);

		JPanel namePanel = new JPanel();
		namePanel.setLayout(new BorderLayout());
		namePanel.add(label, BorderLayout.WEST);
		namePanel.add(connectionNameField, BorderLayout.CENTER);

		checkValidNewConnectionName(connectionNameField.getText(), label);

		connectionNameField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e)
			{
				checkValidNewConnectionName(connectionNameField.getText(), label);
			}

			@Override
			public void removeUpdate(DocumentEvent e)
			{
				checkValidNewConnectionName(connectionNameField.getText(), label);
			}

			@Override
			public void changedUpdate(DocumentEvent e)
			{
				checkValidNewConnectionName(connectionNameField.getText(), label);
			}
		});
		panel.add(namePanel, BorderLayout.SOUTH);

		listModel.add(0, "H2");
		listModel.add(1, "HANA");
		listModel.add(2, "Oracle");
		listModel.add(3, "PostgreSQL");
		listModel.add(4, "Sql Server (jTDS)");
		listModel.add(5, "Sql Server (MS)");
	}

	@Override
	protected void dispatch(CountDownLatch latch, String message)
	{
		dispatch(latch, message, null);
	}

	@Override
	protected void dispatch(final CountDownLatch latch, String message, final Object value)
	{
		try
		{
			if (message == MSG_SELECT_CONNECTION_TYPE)
			{
				final String name = String.valueOf(value);

				if (!listModel.contains(value))
				{
					throw new IllegalArgumentException();
				}

				EventUtils.runOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						connectionTypeList.setSelectedValue(name, true);
						latch.countDown();
					}
				});
			}
			else if (message == MSG_CONNECTION_NAME)
			{
				EventUtils.runOnEventThread(new Runnable()
				{
					@Override
					public void run()
					{
						connectionNameField.setText(String.valueOf(value));
						latch.countDown();
					}
				});
			}
			else
			{
				throw new IllegalArgumentException();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case MODEL_CONNECTION_LIST:
				return (T) listModel;
		}

		throw new IllegalArgumentException();
	}

	private void checkValidNewConnectionName(String text, JLabel label)
	{
		boolean valid = !text.trim().isEmpty() && !UIHub.profile().connections().containsKey(text);

		label.setForeground(valid ? Color.black : Color.red);
		h2Trans.enable(valid);
		hanaTrans.enable(valid);
		oracleTrans.enable(valid);
		postgreSQLTrans.enable(valid);
		sqlServerJTDSTrans.enable(valid);
		sqlServerMSTrans.enable(valid);

		if (valid)
		{
			((Reference<String>) state().model()).setReference(text);
		}
	}

	@Override
	public JComponent getComponent()
	{
		return panel;
	}
}
