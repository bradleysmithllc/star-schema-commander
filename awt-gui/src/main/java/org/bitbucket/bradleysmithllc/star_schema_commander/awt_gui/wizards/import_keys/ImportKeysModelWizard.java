package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_keys;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardModelBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;

public class ImportKeysModelWizard extends UIWizard
{
	public ImportKeysModelWizard()
	{
		super(createModel());
	}

	private static WizardStateModel createModel()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		WizardState<Reference<DatabaseConnection>, SelectConnectionUIComponent> selectConnection =
			wizardModelBuilder.newState(new Reference<DatabaseConnection>(), new SelectConnectionUIComponent("next")).id("SelectConnection").create();

		WizardState<Reference<DatabaseConnection>, ImportKeysUIComponent> import_keys =
			wizardModelBuilder.newState(new Reference<DatabaseConnection>(), new ImportKeysUIComponent()).id("ImportKeys").create();

		WizardState<Reference<DatabaseConnection>, SelectConnectionUIComponent> done =
			wizardModelBuilder.newState(new Reference<DatabaseConnection>(), new SelectConnectionUIComponent("done")).id("Done").create();

		wizardModelBuilder.newTransition(selectConnection, import_keys).id("next").logicalName("Next").handler(new StateTransitionHandler<Reference<DatabaseConnection>, SelectConnectionUIComponent, Reference<DatabaseConnection>, ImportKeysUIComponent>()
		{
			@Override
			public void transition(WizardState<Reference<DatabaseConnection>, SelectConnectionUIComponent> from, WizardState<Reference<DatabaseConnection>, ImportKeysUIComponent> to, WizardStateTransition<Reference<DatabaseConnection>, SelectConnectionUIComponent, Reference<DatabaseConnection>, ImportKeysUIComponent> transition)
			{
				to.model().setReference(from.model().getReference());
			}
		}).create();
		wizardModelBuilder.newTransition(import_keys, done).id("done").logicalName("Close").create();

		wizardModelBuilder.initial(selectConnection);

		return wizardModelBuilder.create();
	}
}