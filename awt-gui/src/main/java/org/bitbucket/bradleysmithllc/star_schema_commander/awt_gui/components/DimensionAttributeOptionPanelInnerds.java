package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.CoordinatedTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Map;

public class DimensionAttributeOptionPanelInnerds implements DataGridNavigatorOptionPanel.OptionPanelInnerds<DimensionAttribute, DataGridNavigator.IncludedAttribute>
{
	@Override
	public String uniquelyIdentifyContext(Object obj)
	{
		return ((FactDimensionKey) obj).name();
	}

	@Override
	public String uniquelyIdentifyM(DimensionAttribute dimensionAttribute)
	{
		return dimensionAttribute.logicalName();
	}

	@Override
	public String uniquelyIdentifyT(DataGridNavigator.IncludedAttribute includedAttribute)
	{
		return includedAttribute.factDimensionKey().name() + "." + includedAttribute.attribute().logicalName();
	}

	@Override
	public CoordinatedTableModel<DataGridNavigator.IncludedAttribute> createIncludedTableModel()
	{
		return new DimensionAttributeNavigatorTableModel();
	}

	@Override
	public CoordinatedTableModel<DimensionAttribute> createAvailableTableModel()
	{
		return new AvailableDimensionAttributeNavigatorTableModel();
	}

	@Override
	public Query.aggregation aggregation(DataGridNavigator.IncludedAttribute m)
	{
		return m.aggregation();
	}

	@Override
	public DimensionAttribute getM(DataGridNavigator.IncludedAttribute m)
	{
		return m.attribute();
	}

	@Override
	public void initializeContextualComboBoxModel(Fact exploring, DefaultComboBoxModel model)
	{
		// add every fact dimension key
		for (Map.Entry<String, FactDimensionKey> key : exploring.keys().entrySet())
		{
			model.addElement(key.getValue());
		}
	}

	@Override
	public boolean contextComboBoxVisible(Fact exploring)
	{
		return exploring.keys().size() > 1;
	}

	@Override
	public void populateAvailableTableModel(Fact fact, CoordinatedTableModel<DimensionAttribute> model, Object context)
	{
		// context is a fact dimension key
		final FactDimensionKey factDimensionKey = (FactDimensionKey) context;
		final Dimension dimension = factDimensionKey.dimensionKey().dimension();

		model.visit(new CoordinatedTableModel.ModelVisitor<DimensionAttribute>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<DimensionAttribute> model)
			{
				// add all measures to available as well so they can be added at different aggregation levels
				for (DimensionAttribute measure : dimension.attributes().values())
				{
					model.addElement(measure);
				}
			}
		});
	}

	@Override
	public void populateIncludedTableModel(Fact fact, CoordinatedTableModel<DataGridNavigator.IncludedAttribute> model)
	{
		/*
		model.visit(new CoordinatedTableModel.ModelVisitor<DataGridNavigator.IncludedAttribute>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<DataGridNavigator.IncludedAttribute> model)
			{
				// add all measures by default
				//for (FactMeasure measure : fact.measures().values())
				//{
				//	DataGridNavigator.IncludedMeasure e = new DataGridNavigator.IncludedMeasure(measure);
				//	e.alias(measure.logicalName());
				//	model.addElement(e);
				//}
			}
		});
		 */
	}

	@Override
	public void demoteToAvailable(final DataGridNavigator.IncludedAttribute includedAttribute, CoordinatedTableModel<DimensionAttribute> availableModel, Object currentContext)
	{
		FactDimensionKey factDimensionKey = includedAttribute.factDimensionKey();
		if (factDimensionKey == currentContext && !availableModel.tableData().contains(includedAttribute.attribute()))
		{
			availableModel.visit(new CoordinatedTableModel.ModelVisitor<DimensionAttribute>()
			{
				@Override
				public void visit(CoordinatedTableModel.UpdatableTableModel<DimensionAttribute> model)
				{
					model.addElement(includedAttribute.attribute());
				}
			});
		}
	}

	@Override
	public void promoteSelectedToIncluded(final Query.aggregation agg, final CoordinatedTableModel<DimensionAttribute> available, CoordinatedTableModel<DataGridNavigator.IncludedAttribute> included, final ListSelectionModel selectionModel, Object context)
	{
		final FactDimensionKey factDimensionKey = (FactDimensionKey) context;

		included.visit(new CoordinatedTableModel.ModelVisitor<DataGridNavigator.IncludedAttribute>()
		{
			@Override
			public void visit(CoordinatedTableModel.UpdatableTableModel<DataGridNavigator.IncludedAttribute> model)
			{
				// iterate through selected items
				List<DimensionAttribute> td = available.tableData();

				for (int row = 0; row < td.size(); row++)
				{
					if (selectionModel.isSelectedIndex(row))
					{
						DimensionAttribute tditem = td.get(row);
						DataGridNavigator.IncludedAttribute e = new DataGridNavigator.IncludedAttribute(tditem, factDimensionKey);

						String alias = (agg == Query.aggregation.none ? "" : (agg.name() + "_")) + tditem.logicalName();

						e.alias(alias);
						e.aggregation(agg);
						model.addElement(e);
					}
				}
			}
		});
	}

	@Override
	public void initializeContextualComboBox(Fact exploring, JComboBox availableContextComboBox)
	{
		// add a renderer which will display the correct name
		availableContextComboBox.setRenderer(new ListCellRenderer()
		{
			private final DefaultListCellRenderer defaultListCellRenderer = new DefaultListCellRenderer();

			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
			{
				FactDimensionKey factDimensionKey = (FactDimensionKey) value;

				StringBuilder stb = new StringBuilder();
				stb.append(factDimensionKey.dimensionKey().dimension().logicalName());

				if (factDimensionKey.rolePlaying())
				{
					stb.append(" <").append(factDimensionKey.roleName()).append(">");
				}

				return defaultListCellRenderer.getListCellRendererComponent(list, stb.toString(), index, isSelected, cellHasFocus);
			}
		});
	}
}
