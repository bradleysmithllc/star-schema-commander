package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Visitor;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

public class FactPanel extends AbstractUIComponent
{
	private final FactTableModel factListModel = new FactTableModel();

	private JTable factList = new JTable(factListModel);
	private JScrollPane jScrollPane;

	@Override
	protected void buildParent(Profile profile)
	{
		factListModel.clear();
		factList = new JTable(factListModel);

		factList.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				publish();
			}
		});
		factList.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jScrollPane = new JScrollPane(factList);

		jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray), "Facts"));

		updateReportingArea();
	}

	private void publish()
	{
		if (factList.getSelectionModel().isSelectionEmpty())
		{
			UIHub.selectFact(null);
		}
		else
		{
			Fact ra = factListModel.tableData().get(factList.getSelectedRow());
			UIHub.selectFact(ra);
		}
	}

	@Override
	public JComponent getComponent()
	{
		return jScrollPane;
	}

	@Override
	public <T> T getModel(int id)
	{
		switch (id)
		{
			case FACT_PANEL_FACT_LIST:
				return (T) factListModel;
			default:
				throw new IllegalArgumentException();
		}
	}

	@Override
	public void stateChanged(stateKey key, Object value)
	{
		switch (key)
		{
			case selectedDimension:
				try
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							factList.revalidate();
							factList.repaint();
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
			case profileRefresh:
			case selectedReportingArea:
				try
				{
					runOnEventThread(new Runnable()
					{
						@Override
						public void run()
						{
							updateReportingArea();
						}
					});
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
		}
	}

	public void updateReportingArea()
	{
		factListModel.update(new Visitor<FactTableModel>()
		{
			@Override
			public void visit(FactTableModel pftm)
			{
				// populate the model with the children of this ra
				pftm.clear();

				ReportingArea reportingArea = UIHub.selectedReportingArea();
				if (reportingArea != null)
				{
					for (Fact rac : reportingArea.facts().values())
					{
						pftm.addFact(rac);
					}
				}
			}
		});
	}
}
