package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

/**
 * This tests a bug where removing a default-selected measure caused it to be duplicated
 * in the available list.
 */
public class DataGridNavigatorOptionPanelMDeleteFirstListTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void deleteFirstIncludedMeasure() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ASSIGN_AGG_TO_INCLUDED, new MutablePair<Integer, Query.aggregation>(0, null));
	}

	@UITestStep(ordinal = 1)
	public void verifySameMeasuresAvailable() throws Exception
	{
		List<FactMeasure> measures = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_MEASURES);
		Assert.assertEquals(3, measures.size());
	}
}
