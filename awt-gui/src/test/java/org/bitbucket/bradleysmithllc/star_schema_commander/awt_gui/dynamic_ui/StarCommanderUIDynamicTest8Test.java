package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dynamic_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIUpdateListener;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DimensionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.junit.Assert;

import javax.swing.*;
import java.util.concurrent.Semaphore;

public class StarCommanderUIDynamicTest8Test extends StarCommanderUIDynamicTestBase
{
	@UITestStep
	public void dimensionsAndFactsDynamic() throws Exception
	{
		final Semaphore sem = new Semaphore(1);

		uiComponent.setUpdateListener(new UIUpdateListener()
		{
			@Override
			public void uiUpdated()
			{
				sem.release(1);
			}
		});

		UIView dp = UIHub.get().locate(DimensionPanel.class.getSimpleName());
		UIView fp = UIHub.get().locate(FactPanel.class.getSimpleName());

		ListModel<Dimension> dmodel = dp.getModel(UIModels.DIMENSION_PANEL_DIMENSION_LIST);
		FactTableModel fmodel = fp.getModel(UIModels.FACT_PANEL_FACT_LIST);

		Assert.assertEquals(0, fmodel.tableData().size());
		Assert.assertEquals(0, dmodel.getSize());

		sem.acquireUninterruptibly(1);

		profile.defaultReportingArea().newFact().schema("s").name("n").logicalName("MyFact").create();
		profile.defaultReportingArea().newDimension().schema("s").name("n").logicalName("MyDim").create();

		profile.persist();

		sem.acquireUninterruptibly(1);

		Assert.assertEquals(1, fmodel.tableData().size());
		Assert.assertEquals("MyFact", fmodel.tableData().get(0).logicalName());

		Assert.assertEquals(1, dmodel.getSize());
		Assert.assertEquals("MyDim", dmodel.getElementAt(0).logicalName());
	}
}
