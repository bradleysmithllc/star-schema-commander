package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigator;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

public class DataGridNavigatorOptionPanelSetAliasTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void selectMeasures() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
	}

	@UITestStep(ordinal = 1)
	public void add() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_ALL);
	}

	@UITestStep(ordinal = 2)
	public void verifyAliasNames() throws Exception
	{
		// verify that all measures are selected by default
		List<DataGridNavigator.IncludedMeasure> selected = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_SELECTED_MEASURES);

		// verify alias names
		for (DataGridNavigator.IncludedMeasure agg : selected)
		{
			Query.aggregation ag = agg.aggregation();

			if (ag == Query.aggregation.none)
			{
				Assert.assertEquals(agg.measure().logicalName(), agg.alias());
			}
			else
			{
				Assert.assertEquals(ag.name() + "_" + agg.measure().logicalName(), agg.alias());
			}
		}
	}

	@UITestStep(ordinal = 3)
	public void setAlias() throws Exception
	{
		// verify that all measures are selected by default
		List<DataGridNavigator.IncludedMeasure> selected = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_SELECTED_MEASURES);

		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ASSIGN_ALIAS_TO_INCLUDED, new ImmutablePair<Integer, String>(1, "ALIAS"));

		// verify the aliases
		verify(selected.get(1), "measure1", "ALIAS", Query.aggregation.sum);
	}
}
