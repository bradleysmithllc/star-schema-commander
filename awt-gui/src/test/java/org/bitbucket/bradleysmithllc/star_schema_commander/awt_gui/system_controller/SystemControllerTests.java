package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.system_controller;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.SystemControllerConstants;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.SystemUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class SystemControllerTests
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	protected Profile profile;

	@Before
	public void setupStuff() throws Exception
	{
		profile = createProfile();

		UIHub.init(new SystemUIController());

		UIHub.profile(profile);
	}

	@Test
	public void newReportingArea()
	{
		UIHub.get().getSystemController().dispatchAction(SystemControllerConstants.MSG_NEW_REPORTING_AREA, new MutablePair<ReportingArea, String>(UIHub.selectedReportingArea(), "NEW"));
	}

	/* This will be destroyed each time around */
	private Profile createProfile() throws IOException
	{
		File path = temporaryFolder.newFile("profile");
		FileUtils.forceDelete(path);

		return Profile.create(path);
	}
}
