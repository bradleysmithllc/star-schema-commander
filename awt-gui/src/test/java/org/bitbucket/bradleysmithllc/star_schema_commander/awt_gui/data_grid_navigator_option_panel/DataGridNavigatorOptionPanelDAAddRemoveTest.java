package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class DataGridNavigatorOptionPanelDAAddRemoveTest extends DataGridNavigatorOptionPanelDATestBase
{
	@UITestStep(ordinal = 0)
	public void selectContext() throws Exception
	{
		Map<String, FactDimensionKey> keys = this.value.keys();
		FactDimensionKey value = keys.get("Dimension3_b_role1");

		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_CONTEXT, value);
	}

	@UITestStep(ordinal = 1)
	public void selectAttribute2() throws Exception
	{
		DimensionAttribute value = this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak2");

		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, value);
	}

	@UITestStep(ordinal = 2)
	public void selectAggSum() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, Query.aggregation.sum);
	}

	@UITestStep(ordinal = 3)
	public void addToIncluded() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);
	}

	@UITestStep(ordinal = 4)
	public void addRestToIncluded() throws Exception
	{
		for (Query.aggregation agg : Query.aggregation.values())
		{
			if (agg != Query.aggregation.sum)
			{
				uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, agg);
				uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);
			}
		}
	}

	@UITestStep(ordinal = 5)
	public void verifyAttributeIsGone() throws Exception
	{
		List<DimensionAttribute> ldi = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_MEASURES);

		Assert.assertEquals(2, ldi.size());

		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak1")));
		Assert.assertFalse(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak2")));
		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_pk")));
	}

	@UITestStep(ordinal = 6)
	public void changeContext() throws Exception
	{
		FactDimensionKey value = this.value.keys().get("Dimension3_B_role2");

		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_CONTEXT, value);
	}

	@UITestStep(ordinal = 7)
	public void attributeBackInList() throws Exception
	{
		List<DimensionAttribute> ldi = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_MEASURES);

		Assert.assertEquals(3, ldi.size());

		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak1")));
		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak2")));
		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_pk")));
	}

	@UITestStep(ordinal = 8)
	public void backToRole1() throws Exception
	{
		FactDimensionKey value = this.value.keys().get("Dimension3_b_role1");

		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_CONTEXT, value);
	}

	@UITestStep(ordinal = 9)
	public void verifyAttributeIsGoneAgain() throws Exception
	{
		List<DimensionAttribute> ldi = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_MEASURES);

		Assert.assertEquals(2, ldi.size());

		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak1")));
		Assert.assertFalse(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_ak2")));
		Assert.assertTrue(ldi.contains(this.value.keys().get("Dimension3_b_role1").dimensionKey().dimension().attributes().get("dim3_pk")));
	}

	//@UITestStep(ordinal = Integer.MAX_VALUE)
	public void wait__() throws Exception
	{
		Thread.sleep(600000L);
	}
}
