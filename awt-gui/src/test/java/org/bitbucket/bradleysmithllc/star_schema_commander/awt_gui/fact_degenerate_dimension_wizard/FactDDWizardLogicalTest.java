package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_degenerate_dimension_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.ProcessColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.junit.Assert;

import java.sql.Types;
import java.util.List;
import java.util.Map;

public class FactDDWizardLogicalTest extends FactDDWizardTestBase
{
	private Fact fact;

	@Override
	protected Fact getFact()
	{
		ReportingArea ra = profile.defaultReportingArea().newReportingArea().name(FactDDWizardLogicalTest.class.getSimpleName()).create();

		fact = ra.newFact().schema("s").name("f").create();

		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();

		return fact;
	}

	@UITestStep(ordinal = 0)
	public void selectColumns() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());
		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");
	}

	@UITestStep(ordinal = 1)
	public void selectLogicalNameCol1() throws Exception
	{
		UIView comp = uiComponent.locate(ProcessColumnsUIComponent.class.getSimpleName());
		comp.dispatchAction(ProcessColumnsUIComponent.MSG_SET_LOGICAL_NAME, "Column 1");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "next");
	}

	@UITestStep(ordinal = 2)
	public void selectLogicalNameCol2() throws Exception
	{
		UIView comp = uiComponent.locate(ProcessColumnsUIComponent.class.getSimpleName());
		comp.dispatchAction(ProcessColumnsUIComponent.MSG_SET_LOGICAL_NAME, "Column 2");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "next");
	}

	@UITestStep(ordinal = 3)
	public void selectLogicalNameCol3() throws Exception
	{
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@UITestStep(ordinal = 4)
	public void checkWizardComplete() throws Exception
	{
		Assert.assertTrue(uiComponent.checkState(UIWizard.ST_WIZARD_COMPLETE));
	}

	@UITestStep(ordinal = 5)
	public void verifyDimension() throws Exception
	{
		Map<String, FactDimensionKey> keys = fact.keys();

		Assert.assertTrue(keys.containsKey("DD_s_f_DegenerateDimension"));

		FactDimensionKey fdk = keys.get("DD_s_f_DegenerateDimension");

		List<DatabaseColumn> columns = fdk.dimensionKey().columns();

		// verify included columns
		Assert.assertEquals(3, columns.size());
		Assert.assertEquals("col1", columns.get(0).name());
		Assert.assertEquals("col2", columns.get(1).name());
		Assert.assertEquals("col3", columns.get(2).name());

		// verify dimension attributes
		Dimension d = fdk.dimensionKey().dimension();

		Map<String, DimensionAttribute> attrd = d.attributes();

		Assert.assertEquals(3, attrd.size());
		Assert.assertEquals("col1", attrd.get("Column 1").column().name());
		Assert.assertEquals("col2", attrd.get("Column 2").column().name());
		Assert.assertEquals("col3", attrd.get("col3").column().name());
	}
}
