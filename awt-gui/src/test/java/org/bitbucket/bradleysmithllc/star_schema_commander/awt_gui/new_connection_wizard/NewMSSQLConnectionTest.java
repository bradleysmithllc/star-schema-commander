package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.new_connection_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.NewConnectionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.SelectConnectionTypeUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.junit.Assert;

public class NewMSSQLConnectionTest extends AbstractUIComponentTest<NewConnectionModelWizard>
{
	public static final String CONN_TYPE_NAME = "Sql Server (MS)";
	public static final String TRANSITION_ID = "sqlserver_ms";

	@UITestStep(ordinal = 0)
	public void setType() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_SELECT_CONNECTION_TYPE, CONN_TYPE_NAME);
	}

	@UITestStep(ordinal = 1)
	public void setName() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "SQLConnection");
	}

	@UITestStep(ordinal = 2)
	public void next() throws Exception
	{
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, TRANSITION_ID);
	}

	@UITestStep(ordinal = 3)
	public void confirm() throws Exception
	{
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "confirm");
	}

	@UITestStep(ordinal = 4)
	public void reloadProfileAndVerify() throws Exception
	{
		profile.invalidate();

		Assert.assertTrue(profile.connections().containsKey("SQLConnection"));

		DatabaseConnection h2 = profile.connections().get("SQLConnection");

		Assert.assertNull(h2.userName());
		Assert.assertNull(h2.password());
		Assert.assertEquals("jdbc:sqlserver://localhost;integratedSecurity=true", h2.jdbcUrl());
	}

	@Override
	protected NewConnectionModelWizard createComponent()
	{
		return new NewConnectionModelWizard();
	}
}
