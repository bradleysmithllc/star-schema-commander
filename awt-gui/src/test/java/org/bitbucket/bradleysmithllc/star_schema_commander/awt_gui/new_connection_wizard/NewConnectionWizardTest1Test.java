package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.new_connection_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.TransitionStates;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.NewConnectionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.SelectConnectionTypeUIComponent;
import org.junit.Assert;

public class NewConnectionWizardTest1Test extends NewConnectionWizardBaseTest
{

	public static final String[][] NAMES = new String[][]{
		new String[]{
			"H2",
			"h2"
		},
		new String[]{
			"HANA",
			"hana"
		},
		new String[]{
			"Oracle",
			"oracle"
		},
		new String[]{
			"PostgreSQL",
			"postgresql"
		},
		new String[]{
			"Sql Server (jTDS)",
			"sqlserver_jtds"
		},
		new String[]{
			"Sql Server (MS)",
			"sqlserver_ms"
		}
	};

	@Override
	protected boolean useTestUIController()
	{
		return false;
	}

	@UITestStep(ordinal = 0)
	public void transitionsNewConnection() throws Exception
	{
		profile.newH2Connection().name("ABA").create();
		profile.newH2Connection().name("BAB").create();

		profile.persist();

		UIView ac = UIHub.get().locate(ActionView.class.getSimpleName());
		ac.dispatchActionAndWait(ActionView.MSG_NEW_CONNECTION);
	}

	@UITestStep(ordinal = 1)
	public void initialStates() throws Exception
	{
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());

		Assert.assertNotNull(wizard);

		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());

		// assert that all states are inactive
		assertTransStates(trModel, null, false);

		// select each connection type and verify that the transition is activated

		for (String[] name : NAMES)
		{
			scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_SELECT_CONNECTION_TYPE, name[0]);
			assertTransStates(trModel, name[1], false);
		}
	}

	@UITestStep(ordinal = 2)
	public void availableName1() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);
		// selection is left on sql server
		// select each connection type and verify that the transition is enabled with the connection name entered

		// this one is legal
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "ABAA");
		assertTransStates(trModel, "sqlserver_ms", true);
	}

	@UITestStep(ordinal = 3)
	public void takenName1() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);

		// already taken
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "ABA");
		assertTransStates(trModel, "sqlserver_ms", false);
	}

	@UITestStep(ordinal = 4)
	public void takenName2() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);

		// already taken
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "BAB");
		assertTransStates(trModel, "sqlserver_ms", false);
	}

	@UITestStep(ordinal = 5)
	public void availableName2() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);

		// available
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "BABA");
		assertTransStates(trModel, "sqlserver_ms", true);
	}

	@UITestStep(ordinal = 6)
	public void blankNotAllowed() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);

		// blank is not okay
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "");
		assertTransStates(trModel, "sqlserver_ms", false);
	}

	@UITestStep(ordinal = 7)
	public void availableName3() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);

		// available
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "VALID");
		assertTransStates(trModel, "sqlserver_ms", true);
	}

	@UITestStep(ordinal = 8)
	public void names7() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		TransitionStates trModel = wizard.getModel(UIWizard.MODEL_TRANSITION_STATES);

		for (String[] name : NAMES)
		{
			scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_SELECT_CONNECTION_TYPE, name[0]);
			assertTransStates(trModel, name[1], true);
		}
	}
}
