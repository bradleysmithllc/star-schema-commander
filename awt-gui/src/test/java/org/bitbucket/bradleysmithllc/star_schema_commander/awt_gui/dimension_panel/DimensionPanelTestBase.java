package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.DataGridNavigatorPanelTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.DimensionAttributeWizardEmptyTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.DimensionKeyWizardTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.MapColumnsUIComponentDefaultTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DimensionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard.FactKeyWizardDontShowDimsWithoutKeysTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard.FactKeyWizardLongPathTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Ignore;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.sql.Types;

@Ignore
public class DimensionPanelTestBase extends AbstractUIComponentTest<DimensionPanel>
{
	public static void prepareProfile(Profile profile, TemporaryFolder folder) throws IOException
	{
		profile.newH2Connection().name("A").path(folder.newFile()).create();
		profile.newH2Connection().name("Z").path(folder.newFile()).create();
		profile.newH2Connection().name("0").path(folder.newFile()).create();
		profile.newH2Connection().name("a").path(folder.newFile()).create();
		profile.newH2Connection().name("z").path(folder.newFile()).create();
		profile.newH2Connection().name("BootyBooty").path(folder.newFile()).create();

		Fact fact = profile.defaultReportingArea().newFact().schema("dbo").name("Fact").create();
		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newFactMeasure().column("col1").logicalName("Col 1").create();
		fact.newFactMeasure().column("col1").logicalName("Col 2").create();
		fact.newFactMeasure().column("col1").logicalName("Col 3").create();
		fact.newFactMeasure().column("col1").logicalName("Col 4").create();

		Fact fact2 = profile.defaultReportingArea().newFact().schema("dbo").name("Fact_2").create();
		fact2.newDatabaseColumn().name("2col1").jdbcType(Types.INTEGER).create();
		fact2.newFactMeasure().column("2col1").logicalName("2Col 1").create();
		fact2.newFactMeasure().column("2col1").logicalName("2Col 2").create();
		fact2.newFactMeasure().column("2col1").logicalName("2Col 3").create();
		fact2.newFactMeasure().column("2col1").logicalName("2Col 4").create();

		profile.defaultReportingArea().newDimension().schema("default").name("Dimension").create();

		profile.defaultReportingArea().newReportingArea().name("Empty").create();

		ReportingArea biRA = profile.defaultReportingArea().newReportingArea().name("BI").create();
		biRA.newDimension().schema("bi").name("Dimension").create();
		biRA.newDimension().schema("bi2").name("Dimension").create();
		biRA.newFact().schema("fact").name("FACT1").create();
		biRA.newFact().schema("fact2").name("FACT2").create();

		ReportingArea alphaRA = profile.defaultReportingArea().newReportingArea().name("Alpha").create();
		alphaRA.newDimension().schema("bi").name("0Dimension").create();
		alphaRA.newDimension().schema("bi").name("xDimension").create();
		alphaRA.newDimension().schema("bi").name("aDimension").create();
		alphaRA.newDimension().schema("bi").name("XDimension").create();
		alphaRA.newDimension().schema("bi").name("ADimension").create();
		alphaRA.newDimension().schema("0bi").name("0Dimension").create();
		alphaRA.newDimension().schema("xbi").name("xDimension").create();
		alphaRA.newDimension().schema("abi").name("aDimension").create();
		alphaRA.newDimension().schema("ybi").name("XDimension").create();
		alphaRA.newDimension().schema("Xbi").name("ADimension").create();
		alphaRA.newDimension().schema("Z").name("Z").create();
		alphaRA.newDimension().schema("A").name("AAAAAAAAAAAAAAAAAAAA").create();

		alphaRA.newFact().schema("a").name("A").create();
		alphaRA.newFact().schema("A").name("a").create();
		alphaRA.newFact().schema("z").name("a").create();
		alphaRA.newFact().schema("Z").name("Z").create();

		// create some new stuff for Data Navigator Test
		ReportingArea dntRa = profile.defaultReportingArea().newReportingArea().name(DataGridNavigatorPanelTest.class.getSimpleName()).create();
		Dimension d1 = dntRa.newDimension().schema("dbo").name("Dim1").logicalName("Dimension1").create();
		d1.newDatabaseColumn().name("dim1_pk").jdbcType(Types.INTEGER).create();
		d1.newDimensionAttribute().column("dim1_pk").logicalName("Dimension 1 PK").create();
		d1.newDimensionKey().column("dim1_pk").logicalName("pk").create();

		Dimension d2 = dntRa.newDimension().schema("dbo").name("Dim2").logicalName("dimension2").create();
		d2.newDatabaseColumn().name("dim2_pk").jdbcType(Types.INTEGER).create();
		d2.newDimensionKey().column("dim2_pk").logicalName("pk").create();
		d2.newDatabaseColumn().name("dim2_ak").jdbcType(Types.INTEGER).create();
		d2.newDimensionKey().column("dim2_ak").logicalName("ak").create();

		Dimension d3 = dntRa.newDimension().schema("dbo").name("Dim3").logicalName("Dimension3").create();
		d3.newDatabaseColumn().name("dim3_pk").jdbcType(Types.INTEGER).create();
		d3.newDimensionKey().column("dim3_pk").logicalName("pk").create();
		d3.newDatabaseColumn().name("dim3_ak1").jdbcType(Types.INTEGER).create();
		d3.newDimensionKey().column("dim3_ak1").logicalName("ak1").create();
		d3.newDatabaseColumn().name("dim3_ak2").jdbcType(Types.INTEGER).create();
		d3.newDimensionKey().column("dim3_ak2").logicalName("ak2").create();

		d3.newDimensionAttribute().column("dim3_pk").create();
		d3.newDimensionAttribute().column("dim3_ak1").create();
		d3.newDimensionAttribute().column("dim3_ak2").create();

		Fact dntfact = dntRa.newFact().schema("a").name("factOnedim").logicalName("factOneDim").create();
		dntfact.newDatabaseColumn().name("dim1_fk").jdbcType(Types.INTEGER).create();
		dntfact.newFactDimensionKey().dimension("Dimension1").keyName("pk").join("dim1_fk", "dim1_pk").create();

		dntfact = dntRa.newFact().schema("a").name("fact").logicalName("fact").create();

		dntfact.newDatabaseColumn().name("dim1_fk").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("dim2_fk").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("dim2_fk_role").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("dim3_fk").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("dim3_fk_role1").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("dim3_fk_role2").jdbcType(Types.INTEGER).create();

		dntfact.newDatabaseColumn().name("measure1").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("Measure2").jdbcType(Types.INTEGER).create();
		dntfact.newDatabaseColumn().name("measure3").jdbcType(Types.INTEGER).create();

		// measures
		dntfact.newFactMeasure().column("measure1").create();
		dntfact.newFactMeasure().column("Measure2").create();
		dntfact.newFactMeasure().column("measure3").create();

		// add fk references
		// dim1
		dntfact.newFactDimensionKey().dimension("Dimension1").keyName("pk").join("dim1_fk", "dim1_pk").create();

		// dim2
		dntfact.newFactDimensionKey().dimension("dimension2").keyName("pk").join("dim2_fk", "dim2_pk").create();
		dntfact.newFactDimensionKey().dimension("dimension2").keyName("ak").roleName("a_role").join("dim2_fk_role", "dim2_ak").create();

		// dim3
		dntfact.newFactDimensionKey().dimension("Dimension3").keyName("pk").join("dim3_fk", "dim3_pk").create();
		dntfact.newFactDimensionKey().dimension("Dimension3").keyName("ak1").roleName("b_role1").join("dim3_fk_role1", "dim3_ak1").create();
		dntfact.newFactDimensionKey().dimension("Dimension3").keyName("ak2").roleName("B_role2").join("dim3_fk_role2", "dim3_ak2").create();


		ReportingArea daaw = profile.defaultReportingArea().newReportingArea().name(DimensionAttributeWizardEmptyTest.class.getSimpleName()).create();
		daaw.newDimension().schema("dbo").name("empty").logicalName("emptydim").create();

		Dimension edim = daaw.newDimension().schema("dbo").name("dim").logicalName("dim").create();
		edim.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col4").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col5").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col6").jdbcType(Types.INTEGER).create();

		ReportingArea daw = profile.defaultReportingArea().newReportingArea().name(DimensionKeyWizardTest.class.getSimpleName()).create();

		daw.newDimension().schema("dbo").name("empty").logicalName("emptydim").create();

		edim = daw.newDimension().schema("dbo").name("dim").logicalName("dim").create();
		edim.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col4").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col5").jdbcType(Types.INTEGER).create();
		edim.newDatabaseColumn().name("col6").jdbcType(Types.INTEGER).create();

		// data for fact key wizard test
		ReportingArea fkw = profile.defaultReportingArea().newReportingArea().name(FactKeyWizardDontShowDimsWithoutKeysTest.class.getSimpleName()).create();

		// these dimensions will be excluded since they have no keys
		fkw.newDimension().schema("dbo").name("empty").logicalName("emptydim").create();
		Dimension nkd = fkw.newDimension().schema("dbo").name("nokeys").logicalName("nokeysdim").create();
		nkd.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		nkd.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		nkd.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();

		//fact to map
		fkw.newFact().schema("sch").name("fact").logicalName("fact").create();

		ReportingArea fkfw = profile.defaultReportingArea().newReportingArea().name(FactKeyWizardLongPathTest.class.getSimpleName()).create();

		fact = fkfw.newFact().schema("sch").name("fact").logicalName("fact").create();
		fact.newDatabaseColumn().name("factcol1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("factcol2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("factcol3").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("factcol4").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("factcol5").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("factcol6").jdbcType(Types.INTEGER).create();

		fkfw.newDimension().schema("dbo").name("empty").logicalName("emptydim").create();
		nkd = fkfw.newDimension().schema("dbo").name("nokeys").logicalName("nokeysdim").create();
		nkd.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		nkd.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		nkd.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();

		Dimension d11 = fkfw.newDimension().schema("dbo").name("Dim11").logicalName("Dim11").create();
		d11.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		d11.newDimensionKey().column("col1").physicalName("phys").logicalName("Logical").create();

		Dimension d12 = fkfw.newDimension().schema("dbo").name("Dim12").logicalName("Dim12").create();
		d12.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		d12.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		d12.newDimensionKey().column("col1").column("col2").physicalName("phys").logicalName("Logical").create();

		Dimension d13 = fkfw.newDimension().schema("dbo").name("Dim13").logicalName("Dim13").create();
		d13.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		d13.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		d13.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		d13.newDimensionKey().column("col1").column("col2").column("col3").physicalName("phys").logicalName("Logical").create();

		Dimension d34 = fkfw.newDimension().schema("dbo").name("Dim34").logicalName("Dim34").create();
		d34.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		d34.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		d34.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		d34.newDimensionKey().column("col1").physicalName("phys1").logicalName("Logical1").create();
		d34.newDimensionKey().column("col1").column("col2").physicalName("phys2").logicalName("Logical2").create();
		d34.newDimensionKey().column("col1").column("col2").column("col3").physicalName("phys3").logicalName("Logical3").create();
		d34.newDimensionKey().column("col1").column("col3").physicalName("phys4").logicalName("Logical4").create();

		ReportingArea mcui = profile.defaultReportingArea().newReportingArea().name(MapColumnsUIComponentDefaultTest.class.getSimpleName()).create();

		nkd = mcui.newDimension().schema("dbo").name("dim1").logicalName("Dim1").create();
		nkd.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		nkd.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		nkd.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		nkd.newDimensionKey().column("col1").column("col2").column("col3").physicalName("phys").logicalName("Logical").create();

		// not a role playing dimension
		fact = mcui.newFact().schema("sch").name("fact").logicalName("fact").create();
		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col4").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col5").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col6").jdbcType(Types.INTEGER).create();

		fact = mcui.newFact().schema("sch").name("factRole").logicalName("factRole").create();
		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col4").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col5").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col6").jdbcType(Types.INTEGER).create();

		// create connection to dim so a new connection will be role playing
		fact.newFactDimensionKey().dimension("Dim1").keyName("Logical").join("col1", "col1").join("col2", "col2").join("col3", "col3").create();
	}

	@Override
	protected DimensionPanel createComponent()
	{
		return new DimensionPanel();
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		prepareProfile(profile, temporaryFolder);
	}
}
