package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

/**
 * Measures are no longer added by default, so this test is modified to exercise add/remove
 * functions with aggregates.
 */
public class DataGridNavigatorOptionPanelMAggNoneAggListTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void noneIsAvailable() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertTrue(model.contains(Query.aggregation.none));
	}

	@UITestStep(ordinal = 10)
	public void selectARow() throws Exception
	{
		Map<String, FactMeasure> measures = this.value.measures();
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, measures.get("Measure2"));
	}

	@UITestStep(ordinal = 20)
	public void addRow() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);
	}

	@UITestStep(ordinal = 30)
	public void noneNoLongerAnOption() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(Query.aggregation.none));
	}

	@UITestStep(ordinal = 40)
	public void selectNoRow() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, null);
	}

	@UITestStep(ordinal = 50)
	public void sayHelloToNone() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertTrue(model.contains(Query.aggregation.none));
	}
}
