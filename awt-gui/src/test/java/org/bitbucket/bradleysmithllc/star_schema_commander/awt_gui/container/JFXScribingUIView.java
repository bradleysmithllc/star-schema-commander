package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.container;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.RequiresJFXEventThread;

public class JFXScribingUIView extends ScribingUIView
{
	JFXScribingUIView(StringBuilder stb, String id)
	{
		super(stb, id);
	}

	@RequiresJFXEventThread
	@Override
	public void subInitialize() throws Exception
	{
		super.subInitialize();
	}

	@RequiresJFXEventThread
	@Override
	public void activate() throws Exception
	{
		super.activate();
	}

	@RequiresJFXEventThread
	@Override
	public void prepareSelf() throws Exception
	{
		super.prepareSelf();
	}

	@RequiresJFXEventThread
	@Override
	public void prepareWithChildren() throws Exception
	{
		super.prepareWithChildren();
	}

	@RequiresJFXEventThread
	@Override
	public void deactivate()
	{
		super.deactivate();
	}

	@RequiresJFXEventThread
	@Override
	public String viewInstance()
	{
		return super.viewInstance();
	}

	@RequiresJFXEventThread
	@Override
	public String view()
	{
		return super.view();
	}
}
