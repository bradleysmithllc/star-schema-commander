package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_history_view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridHistoryView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.junit.Assert;

public class DataGridHistoryViewPreviousEnabledTest extends DataGridHistoryViewTestBase
{
	// This is the current query
	@UITestStep(ordinal = 0)
	public void addQuery() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	// Not enabled yet
	@UITestStep(ordinal = 1)
	public void previousNotYet() throws Exception
	{
		Assert.assertFalse(uiComponent.checkState(DataGridHistoryView.STATE_PREVIOUS_ENABLED));
	}

	// This is the new current query
	@UITestStep(ordinal = 2)
	public void anotherQuery() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	// Now enabled
	@UITestStep(ordinal = 3)
	public void previousNow() throws Exception
	{
		Assert.assertTrue(uiComponent.checkState(DataGridHistoryView.STATE_PREVIOUS_ENABLED));
	}
}