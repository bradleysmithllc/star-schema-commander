package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.AbstractUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainer;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by bsmith on 3/24/15.
 */
public abstract class UIComponentTest<T extends AbstractUIComponent>
{
	protected static JFrame frame;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	protected Profile profile;

	protected T uiComponent;

	@BeforeClass
	public static void createFrame()
	{
		frame = new JFrame();
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setVisible(false);
	}

	@AfterClass
	public static void dispose()
	{
		frame.dispose();
	}

	@Before
	public void setup() throws Exception
	{
		EventQueue.invokeAndWait(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					profile = createProfile();
					prepareProfile();

					UIHub.init(new TestUIController());
					prepareUIHub();

					UIHub.profile(profile);

					uiComponent = createComponent();

					UIContainer con = new UIContainerImpl();

					con.addView(uiComponent);

					prepareComponent();

					frame.setVisible(false);
					frame.getContentPane().add(uiComponent.getComponent(), BorderLayout.CENTER);
					frame.pack();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Assert.fail(e.toString());
				}
			}
		});
	}

	protected void prepareComponent()
	{

	}

	protected void prepareUIHub()
	{
	}

	protected abstract T createComponent();

	protected abstract void prepareProfile() throws IOException;

	private Profile createProfile() throws IOException
	{
		File path = temporaryFolder.newFile("profile.zip");
		FileUtils.forceDelete(path);

		return Profile.create(path);
	}
}
