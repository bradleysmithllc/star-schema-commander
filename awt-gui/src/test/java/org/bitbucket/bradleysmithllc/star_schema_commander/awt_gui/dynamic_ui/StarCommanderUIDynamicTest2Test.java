package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dynamic_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIUpdateListener;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.NavPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import javax.swing.*;
import java.util.concurrent.Semaphore;

public class StarCommanderUIDynamicTest2Test extends StarCommanderUIDynamicTestBase
{
	@UITestStep
	public void reportingAreasDynamic() throws Exception
	{
//		Assert.fail();

		final Semaphore sem = new Semaphore(1);

		uiComponent.setUpdateListener(new UIUpdateListener()
		{
			@Override
			public void uiUpdated()
			{
				sem.release(1);
			}
		});

		UIView np = UIHub.get().locate(NavPanel.class.getSimpleName());
		DefaultListModel<ReportingArea> componentModel = np.getModel(UIModels.NAV_PANEL_REPORTING_AREA_LIST);

		// allow one for default
		Assert.assertEquals(0, componentModel.size());

		sem.acquireUninterruptibly(1);

		profile.defaultReportingArea().newReportingArea().name("z").create();
		profile.persist();

		sem.acquireUninterruptibly(1);

		// again - don't forget about the default
		Assert.assertEquals(1, componentModel.size());
		Assert.assertEquals("z", componentModel.get(0).name());

		profile.defaultReportingArea().newReportingArea().name("0").create();
		profile.persist();

		sem.acquireUninterruptibly(1);

		// make sure the connections appear in alpha order
		Assert.assertEquals(2, componentModel.size());
		Assert.assertEquals("0", componentModel.get(0).name());
		Assert.assertEquals("z", componentModel.get(1).name());
	}
}
