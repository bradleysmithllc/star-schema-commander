package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigator;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DimensionKeyNavigatorTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactMeasureNavigatorTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class DataGridNavigatorPanelTest extends UIComponentTest<DataGridNavigatorPanel> implements DataGridNavigatorPanel.NavigatorClient
{

	private Fact value;
	private DataGridNavigator navigator;

	//@Test
	public void measuresAggNoneByDefault() throws Exception
	{
		// verify that all measures are selected by default
		List<DataGridNavigator.IncludedMeasure> selected = uiComponent.includeMeasures();

		Map<String, FactMeasure> available = value.measures();

		Assert.assertEquals(available.size(), selected.size());

		// verify that every measure is set to aggregation none
		for (DataGridNavigator.IncludedMeasure agg : selected)
		{
			Assert.assertEquals(Query.aggregation.none, agg.aggregation());
		}
	}

	//@Test
	public void navigator() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorPanel.MSG_APPLY);

		// interrogate the navigator
		List<DataGridNavigator.IncludedMeasure> im = navigator.includeMeasures();

		Map<String, FactMeasure> available = value.measures();

		//Thread.sleep(10000L);
		Assert.assertEquals(available.size(), im.size());

		// verify that every measure is set to aggregation none
		for (DataGridNavigator.IncludedMeasure agg : im)
		{
			Assert.assertEquals(Query.aggregation.none, agg.aggregation());
		}
	}

	//@Test
	public void measuresAlpha() throws Exception
	{
		// verify that all measures are selected by default
		FactMeasureNavigatorTableModel fmModel = uiComponent.getModel(DataGridNavigatorPanel.MODEL_FACT_MEASURES);

		List<DataGridNavigator.IncludedMeasure> selected = fmModel.selectedMeasures();

		Assert.assertEquals(3, selected.size());
		Assert.assertEquals("measure1", selected.get(0).measure().logicalName());
		Assert.assertEquals("Measure2", selected.get(1).measure().logicalName());
		Assert.assertEquals("measure3", selected.get(2).measure().logicalName());
	}

	//@Test
	public void setAlias() throws Exception
	{
		// verify that all measures are selected by default
		FactMeasureNavigatorTableModel fmModel = uiComponent.getModel(DataGridNavigatorPanel.MODEL_FACT_MEASURES);

		List<DataGridNavigator.IncludedMeasure> selected = fmModel.selectedMeasures();

		// verify alias names
		for (DataGridNavigator.IncludedMeasure agg : selected)
		{
			Assert.assertEquals(agg.measure().logicalName(), agg.alias());
		}

		// change an alias name
		FactMeasure fm = selected.get(0).measure();

		uiComponent.dispatchAction(DataGridNavigatorPanel.MSG_SET_ALIAS, new ImmutablePair<FactMeasure, String>(fm, "ALIAS"));

		uiComponent.dispatchAction(DataGridNavigatorPanel.MSG_APPLY);

		List<DataGridNavigator.IncludedMeasure> im = navigator.includeMeasures();

		// verify the aliases
		for (DataGridNavigator.IncludedMeasure agg : im)
		{
			FactMeasure factMeasure = agg.measure();

			if (factMeasure == fm)
			{
				Assert.assertEquals("ALIAS", agg.alias());
			}
			else
			{
				Assert.assertEquals(factMeasure.logicalName(), agg.alias());
			}
		}
	}

	//@Test
	public void dimensionsInOrder() throws Exception
	{
		// verify that all measures are selected by default
		DimensionKeyNavigatorTableModel fmModel = uiComponent.getModel(DataGridNavigatorPanel.MODEL_DIMENSIONS);

		List<DataGridNavigator.IncludedDimension> id = fmModel.tableData();

		Assert.assertEquals(6, id.size());

		// verify order

		Assert.assertEquals("a_role", id.get(0).dimensionKey().name());
		Assert.assertEquals("b_role1", id.get(1).dimensionKey().name());
		Assert.assertEquals("B_role2", id.get(2).dimensionKey().name());
		Assert.assertEquals("Dimension1", id.get(3).dimensionKey().name());
		Assert.assertEquals("dimension2", id.get(4).dimensionKey().name());
		Assert.assertEquals("Dimension3", id.get(5).dimensionKey().name());
	}

	//@Test
	public void dimensionsNoneByDefault() throws Exception
	{
		Assert.assertEquals(0, navigator.includeAttributes().size());
	}

	@Override
	protected DataGridNavigatorPanel createComponent()
	{
		value = profile.defaultReportingArea().reportingAreas().get(getClass().getSimpleName()).facts().get("fact");
		return new DataGridNavigatorPanel(this, value);
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@Override
	public void update(DataGridNavigator navigator)
	{
		this.navigator = navigator;
	}
}
