package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.import_dimension_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.ImportTableModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.ImportTablesUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.ProcessTablesUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.SelectTablesUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class ImportDimensionWizardTest extends AbstractUIComponentTest<UIWizard>
{
	private String h2CatalogName;

	public void prepareProfile() throws Exception
	{
		DatabaseConnection srco = profile.newH2Connection().name("src").create();

		// create some tables to list on the import
		prepareDatabase(srco);

		profile.persist();

		UIHub.init(new TestUIController());
		UIHub.profile(profile);
	}

	@UITestStep(ordinal = 0)
	public void selectConnection() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.COMPONENT_TYPE);
		scui.dispatchAction(SelectConnectionUIComponent.MSG_SELECT_CONNECTION, profile.connections().get("src"));
	}

	@UITestStep(ordinal = 1)
	public void tablesTransition() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "tables");
	}

	@UITestStep(ordinal = 2)
	public void addPublicA() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".PUBLIC.A");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
	}

	@UITestStep(ordinal = 3)
	public void addAA() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".A.A");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
	}

	@UITestStep(ordinal = 4)
	public void processTransition() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		// advance
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@UITestStep(ordinal = 5)
	public void nextTransition() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		// skip the first table (A.A)
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "next");
	}

	@UITestStep(ordinal = 6)
	public void publicAPhysicalName() throws Exception
	{
		// override the logical name for PUBLIC.A to A Prime
		UIView pdui = UIHub.get().locate(ProcessTablesUIComponent.class.getSimpleName());

		// verify that we are indeed looking at the right table
		Assert.assertEquals(h2CatalogName + ".PUBLIC.A", pdui.getModel(ProcessTablesUIComponent.MOD_PHYSICAL_NAME));
		pdui.dispatchAction(ProcessTablesUIComponent.MSG_SET_LOGICAL_NAME, "A Prime");
	}

	@UITestStep(ordinal = 7)
	public void doneTransition() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		// done
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "done");
	}

	@UITestStep(ordinal = 8)
	public void closeTransition() throws Exception
	{
		UIView idui = UIHub.get().locate(ImportTablesUIComponent.class.getSimpleName());

		idui.dispatchAction(ImportTablesUIComponent.MSG_WAIT_FOR_IMPORT_PROCESS);

		// done it
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "close");
	}

	@UITestStep(ordinal = 9)
	public void verify() throws Exception
	{
		// verify newly loaded dimensions
		ReportingArea dra = profile.defaultReportingArea();

		Map<String, Dimension> dimensions = dra.dimensions();

		Assert.assertNotNull(dimensions.get(h2CatalogName.replace(".", "_") + "_A_A"));
		Assert.assertNotNull(dimensions.get("A Prime"));
		Assert.assertNotNull(dra.physicalDimensions().get(h2CatalogName + ".A.A"));
		Assert.assertNotNull(dra.physicalDimensions().get(h2CatalogName + ".PUBLIC.A"));
	}

	private void prepareDatabase(DatabaseConnection srco) throws SQLException
	{
		Connection c = srco.open();

		try
		{
			Statement st = c.createStatement();

			try
			{
				st.execute("CREATE TABLE PUBLIC.I(ID INT);CREATE TABLE PUBLIC.A(ID INT);CREATE TABLE PUBLIC.a1(ID INT);CREATE TABLE PUBLIC.Z(ID INT);CREATE SCHEMA A;CREATE SCHEMA z;CREATE TABLE A.A(ID INT);CREATE TABLE z.Z(ID INT);");
			}
			finally
			{
				st.close();
			}

			// now get the catalog name
			ResultSet rs = c.getMetaData().getCatalogs();

			while (rs.next())
			{
				h2CatalogName = rs.getString(1);
			}

			rs.close();
		}
		finally
		{
			c.close();
		}
	}

	@Override
	protected UIWizard createComponent()
	{
		return new ImportTableModelWizard(ImportTableModelWizard.target.dimension);
	}
}
