package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dynamic_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.NavPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.junit.Assert;

import javax.swing.*;

public class StarCommanderUIDynamicTest1Test extends StarCommanderUIDynamicTestBase
{
	@UITestStep
	public void connectionsAlpha() throws Exception
	{
		// make sure the connections appear in alpha order
		UIView np = UIHub.get().locate(NavPanel.class.getSimpleName());
		DefaultListModel<DatabaseConnection> componentModel = np.getModel(UIModels.NAV_PANEL_CONNECTION_LIST);

		Assert.assertEquals("a", componentModel.get(0).name());
		Assert.assertEquals("m", componentModel.get(1).name());
		Assert.assertEquals("z", componentModel.get(2).name());
	}

	@Override
	protected void prepareProfile() throws Exception
	{
		profile.newH2Connection().name("a").create();
		profile.newH2Connection().name("m").create();
		profile.newH2Connection().name("z").create();
		profile.persist();
	}
}
