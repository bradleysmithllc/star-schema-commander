package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dynamic_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.BreadcrumbPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.NavPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

public class StarCommanderUIDynamicTest5Test extends StarCommanderUIDynamicTestBase
{
	@Override
	protected void prepareProfile() throws Exception
	{
		ReportingArea edw = profile.defaultReportingArea().newReportingArea().name("EDW").create();
		ReportingArea ods = profile.defaultReportingArea().newReportingArea().name("ODS").create();
		ods.newReportingArea().name("POS_ODS").create();
		ReportingArea edw_ods = ods.newReportingArea().name("EDW_ODS").create();
		edw_ods.newReportingArea().name("ETL").create();
		ReportingArea db2 = profile.defaultReportingArea().newReportingArea().name("APP").create();
		db2.newReportingArea().name("IFC").create();
		db2.newReportingArea().name("MSF").create();
		db2.newReportingArea().name("OUT").create();

		profile.persist();
	}

	@UITestStep
	public void reportingAreaNavigation() throws Exception
	{
		UIView breadcrumbPanel = UIHub.get().locate(BreadcrumbPanel.class.getSimpleName());

		// starting position is default reporting area
		Assert.assertEquals(
			"[default]",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);

		// select a reporting area
		UIView np = UIHub.get().locate(NavPanel.class.getSimpleName());
		ReportingArea edw1 = profile.defaultReportingArea().reportingAreas().get("EDW");
		np.dispatchAction(NavPanel.MSG_SELECT_REPORTING_AREA, edw1);

		Assert.assertEquals(
			"[default].EDW",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);

		breadcrumbPanel.dispatchAction(BreadcrumbPanel.MSG_SELECT_BREADCRUMB, profile.defaultReportingArea().name());

		Assert.assertEquals(
			"[default]",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);

		edw1 = profile.defaultReportingArea().reportingAreas().get("ODS");
		Assert.assertNotNull(edw1);
		np.dispatchAction(NavPanel.MSG_SELECT_REPORTING_AREA, edw1);

		Assert.assertEquals(
			"[default].ODS",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);

		edw1 = profile.defaultReportingArea().reportingAreas().get("ODS.POS_ODS");
		np.dispatchAction(NavPanel.MSG_SELECT_REPORTING_AREA, edw1);

		Assert.assertEquals(
			"[default].ODS.POS_ODS",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);

		breadcrumbPanel.dispatchAction(BreadcrumbPanel.MSG_SELECT_BREADCRUMB, "ODS");

		Assert.assertEquals(
			"[default].ODS",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);

		edw1 = profile.defaultReportingArea().reportingAreas().get("ODS.EDW_ODS");
		np.dispatchAction(NavPanel.MSG_SELECT_REPORTING_AREA, edw1);

		Assert.assertEquals(
			"[default].ODS.EDW_ODS",
			breadcrumbPanel.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);
	}
}
