package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.junit.Assert;

import javax.swing.*;

public class DimensionPanelDisplaySelectedReportingAreaDimsTest extends DimensionPanelTestBase
{
	@UITestStep
	public void displaySelectedReportingAreaDims() throws Exception
	{
		DefaultListModel<Dimension> model = uiComponent.getModel(UIModels.DIMENSION_PANEL_DIMENSION_LIST);

		// select a reporting area
		UIHub.selectReportingArea(profile.defaultReportingArea());
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(1, model.size());
		Assert.assertSame(profile.defaultReportingArea().dimensions().get("default_Dimension"), model.get(0));

		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("BI"));
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(2, model.size());
		Assert.assertSame(profile.defaultReportingArea().reportingAreas().get("BI").dimensions().get("bi_Dimension"), model.get(0));
		Assert.assertSame(profile.defaultReportingArea().reportingAreas().get("BI").dimensions().get("bi2_Dimension"), model.get(1));
	}
}
