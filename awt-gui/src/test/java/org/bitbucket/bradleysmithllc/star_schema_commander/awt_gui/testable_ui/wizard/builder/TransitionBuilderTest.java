package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TransitionBuilderTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void idRequired()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Id is required");

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stt = wizardModelBuilder.newState(new Integer(1), new DummyUIComp()).id("hi2").create();

		wizardModelBuilder.newTransition(stf, stt).create();
	}

	@Test
	public void fromNull()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Transition requires a 'from' state");

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();
		WizardState stt = wizardModelBuilder.newState(new Integer(1), new DummyUIComp()).id("hi").create();

		wizardModelBuilder.newTransition(null, stt).id("hi").create();
	}

	@Test
	public void toNull()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Transition requires a 'to' state");

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi").create();

		wizardModelBuilder.newTransition(stf, null).id("hi").create();
	}

	@Test
	public void to()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").create();
		WizardState nextState = tr.follow();

		Assert.assertSame(nextState, stt);
	}

	@Test
	public void idIsLogicalName()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").create();

		Assert.assertEquals(tr.id(), tr.logicalName());
	}

	@Test
	public void logicalName()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").logicalName("ln").create();

		Assert.assertEquals("ln", tr.logicalName());
		Assert.assertEquals("hi", tr.id());
	}

	@Test
	public void duplicateIdOnFromState()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Id [hi] has already been used on state [his]");

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();
		WizardState<?, ?> stf = wizardModelBuilder.newState("", new DummyUIComp()).id("his").create();
		WizardState<?, ?> stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hit").create();

		// add it once - this is fine
		wizardModelBuilder.newTransition(stf, stt).id("hi").create();

		// add a second transition with the same id - not cool.
		wizardModelBuilder.newTransition(stf, stt).id("hi").create();
	}

	@Test
	public void duplicateIdOnDifferentStates()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("his").create();
		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hit").create();

		// add it once - this is fine
		wizardModelBuilder.newTransition(stf, stt).id("hi").create();

		// use the same id on a different state - also fine.
		wizardModelBuilder.newTransition(stt, stf).id("hi").create();
	}

	@Test
	public void minaTapeDeck()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		WizardState empty = wizardModelBuilder.newState("", new DummyUIComp()).id("Empty").create();
		WizardState loaded = wizardModelBuilder.newState("", new DummyUIComp()).id("Loaded").create();
		WizardState playing = wizardModelBuilder.newState("", new DummyUIComp()).id("Playing").create();
		WizardState paused = wizardModelBuilder.newState("", new DummyUIComp()).id("Paused").create();

		// transitions
		wizardModelBuilder.newTransition(empty, loaded).id("load").create();
		wizardModelBuilder.newTransition(loaded, playing).id("play").create();
		wizardModelBuilder.newTransition(playing, paused).id("pause").create();
		wizardModelBuilder.newTransition(paused, playing).id("play").create();
		wizardModelBuilder.newTransition(playing, loaded).id("stop").create();
		wizardModelBuilder.newTransition(loaded, empty).id("eject").create();

		final StringBuilder stb = new StringBuilder();

		wizardModelBuilder.handler(new WizardStateHandler()
		{
			@Override
			public <F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>> void stateChanged(WizardState<F, C> from, WizardState<T, D> to, WizardStateTransition<F, C, T, D> transition)
			{
				stb.append("[{");
				stb.append(from.id());
				stb.append("}.{");
				stb.append(transition.id());
				stb.append("} >> {");
				stb.append(to.id());
				stb.append("}]");
			}
		});

		WizardStateModel tpm = wizardModelBuilder.initial(empty).create();

		WizardState<?, ?> currState = tpm.initial();

		Assert.assertEquals("Empty", currState.id());

		currState = currState.transitionMap().get("load").follow();
		Assert.assertEquals("Loaded", currState.id());

		currState = currState.transitionMap().get("play").follow();
		Assert.assertEquals("Playing", currState.id());

		currState = currState.transitionMap().get("pause").follow();
		Assert.assertEquals("Paused", currState.id());

		currState = currState.transitionMap().get("play").follow();
		Assert.assertEquals("Playing", currState.id());

		currState = currState.transitionMap().get("stop").follow();
		Assert.assertEquals("Loaded", currState.id());

		currState = currState.transitionMap().get("eject").follow();
		Assert.assertEquals("Empty", currState.id());

		Assert.assertEquals("[{Empty}.{load} >> {Loaded}][{Loaded}.{play} >> {Playing}][{Playing}.{pause} >> {Paused}][{Paused}.{play} >> {Playing}][{Playing}.{stop} >> {Loaded}][{Loaded}.{eject} >> {Empty}]", stb.toString());
	}

	@Test
	public void enabledDefault()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").create();

		Assert.assertTrue(tr.enabled());
	}

	@Test
	public void activeDefault()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").create();

		Assert.assertTrue(tr.active());
	}

	@Test
	public void enabledOverride()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").enabled(false).create();

		Assert.assertFalse(tr.enabled());
	}

	@Test
	public void activeOverride()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState stt = wizardModelBuilder.newState("", new DummyUIComp()).id("hi1").create();
		WizardState stf = wizardModelBuilder.newState("", new DummyUIComp()).id("hi2").create();

		WizardStateTransition tr = wizardModelBuilder.newTransition(stf, stt).id("hi").active(false).create();

		Assert.assertFalse(tr.active());
	}
}
