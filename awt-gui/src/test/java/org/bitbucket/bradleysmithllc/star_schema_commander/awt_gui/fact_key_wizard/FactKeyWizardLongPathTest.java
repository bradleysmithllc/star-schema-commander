package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.FactDimensionKeysModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.MapColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionKeyUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.junit.Assert;

import java.io.IOException;

public class FactKeyWizardLongPathTest extends FactKeyWizardTestBase
{

	private Fact fact;

	@Override
	protected FactDimensionKeysModelWizard createComponent()
	{
		ReportingArea fkw = profile.defaultReportingArea().reportingAreas().get(FactKeyWizardLongPathTest.class.getSimpleName());

		UIHub.selectReportingArea(fkw);

		return new FactDimensionKeysModelWizard(fkw.facts().get("fact"));
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@UITestStep(ordinal = 0)
	public void selectDims() throws Exception
	{
		UIView comp = uiComponent.locate(SelectDimensionsUIComponent.class.getSimpleName());
		comp.dispatchAction(SelectDimensionsUIComponent.MSG_SELECT_ALL_DIMENSIONS);
	}

	@UITestStep(ordinal = 1)
	public void selectKeysTransition() throws Exception
	{
		// verify correct keys displayed
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "selectKeys");
	}

	@UITestStep(ordinal = 2)
	public void selectKeys() throws Exception
	{
		UIView sdkcomp = uiComponent.locate(SelectDimensionKeyUIComponent.class.getSimpleName());
		Assert.assertEquals("FactKeyWizardLongPathTest.Dim11", ((Dimension) sdkcomp.getModel(SelectDimensionKeyUIComponent.MOD_DIMENSION)).qualifiedName());

		sdkcomp.dispatchAction(SelectDimensionKeyUIComponent.MSG_SELECT_ALL_KEYS);
	}

	@UITestStep(ordinal = 3)
	public void mapColumnsTransition() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapColumns");
	}

	@UITestStep(ordinal = 4)
	public void mapColumnsDim1() throws Exception
	{
		UIView mapuicomp = uiComponent.locate(MapColumnsUIComponent.class.getSimpleName());

		DimensionKey dimensionKey = mapuicomp.getModel(MapColumnsUIComponent.MOD_DIMENSION_KEY);
		Assert.assertEquals("Logical", dimensionKey.logicalName());

		fact = mapuicomp.getModel(MapColumnsUIComponent.MOD_FACT);
		Assert.assertEquals("fact", fact.logicalName());

		mapuicomp.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new ImmutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(0), fact.columns().get("factcol1")));
	}

	@UITestStep(ordinal = 5)
	public void nextDimensionTransition0() throws Exception
	{
		// this should result in a next dimension transition.  Process Dim2
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "nextDimension");
	}

	@UITestStep(ordinal = 6)
	public void selectAllKeys() throws Exception
	{
		UIView sdkcomp = uiComponent.locate(SelectDimensionKeyUIComponent.class.getSimpleName());
		sdkcomp.dispatchAction(SelectDimensionKeyUIComponent.MSG_SELECT_ALL_KEYS);
	}

	@UITestStep(ordinal = 7)
	public void mapColumnsTransition2() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapColumns");
	}

	@UITestStep(ordinal = 8)
	public void mapColumnsFactCol3() throws Exception
	{
		mapColumns(fact, "FactKeyWizardLongPathTest.Dim12", "Logical", "Physical_Key", new ImmutablePair<String, String>("col1", "factcol3"), new ImmutablePair<String, String>("col2", "factcol4"));
	}

	@UITestStep(ordinal = 9)
	public void nextDimensionTransition() throws Exception
	{
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "nextDimension");
	}

	@UITestStep(ordinal = 10)
	public void selectAllKeys2() throws Exception
	{
		UIView sdkcomp = uiComponent.locate(SelectDimensionKeyUIComponent.class.getSimpleName());
		sdkcomp.dispatchAction(SelectDimensionKeyUIComponent.MSG_SELECT_ALL_KEYS);
	}

	@UITestStep(ordinal = 11)
	public void mapColumnsTransition3() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapColumns");
	}

	@UITestStep(ordinal = 12)
	public void mapColumns() throws Exception
	{
		mapColumns(fact, "FactKeyWizardLongPathTest.Dim13", "Logical", "Physical_Key_2", new ImmutablePair<String, String>("col1", "factcol6"), new ImmutablePair<String, String>("col2", "factcol4"), new ImmutablePair<String, String>("col3", "factcol4"));
	}

	@UITestStep(ordinal = 13)
	public void nextDimensionTransition2() throws Exception
	{
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "nextDimension");
	}

	@UITestStep(ordinal = 14)
	public void selectAllKeys4() throws Exception
	{
		UIView sdkcomp = uiComponent.locate(SelectDimensionKeyUIComponent.class.getSimpleName());
		sdkcomp.dispatchAction(SelectDimensionKeyUIComponent.MSG_SELECT_ALL_KEYS);
	}

	@UITestStep(ordinal = 15)
	public void mapColumnsTransition5() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapColumns");
	}

	@UITestStep(ordinal = 16)
	public void mapColumns5() throws Exception
	{
		//Problem here.  Why is the same key repeating??
		mapColumns(fact, "FactKeyWizardLongPathTest.Dim34", "Logical1", "Physical_Key_3", new ImmutablePair<String, String>("col1", "factcol4"));
	}

	@UITestStep(ordinal = 17)
	public void mapNextColumnsTransition() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapNextColumns");
	}

	@UITestStep(ordinal = 18)
	public void mapColumns6() throws Exception
	{
		mapColumns(fact, "FactKeyWizardLongPathTest.Dim34", "Logical2", "Physical_Key_4", new ImmutablePair<String, String>("col1", "factcol3"), new ImmutablePair<String, String>("col2", "factcol2"));
	}

	@UITestStep(ordinal = 19)
	public void mapNextColumnsTransition2() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapNextColumns");
	}

	@UITestStep(ordinal = 20)
	public void mapColumns7() throws Exception
	{
		mapColumns(fact, "FactKeyWizardLongPathTest.Dim34", "Logical3", "Physical_Key_5", new ImmutablePair<String, String>("col1", "factcol2"), new ImmutablePair<String, String>("col2", "factcol1"), new ImmutablePair<String, String>("col3", "factcol6"));
	}

	@UITestStep(ordinal = 21)
	public void mapNextColumnsTransition3() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "mapNextColumns");
	}

	@UITestStep(ordinal = 22)
	public void mapColumns8() throws Exception
	{
		mapColumns(fact, "FactKeyWizardLongPathTest.Dim34", "Logical4", "Physical_Key_6", new ImmutablePair<String, String>("col1", "factcol1"), new ImmutablePair<String, String>("col3", "factcol6"));
	}

	@UITestStep(ordinal = 23)
	public void processTransition() throws Exception
	{
		// go to map the columns of the first key
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@UITestStep(ordinal = 24)
	public void verify() throws Exception
	{
		// verify keys created in the fact as required above
		Assert.assertNotNull(fact.physicalKeys().get("Physical_Key"));
		Assert.assertNotNull(fact.physicalKeys().get("Physical_Key_2"));
		Assert.assertNotNull(fact.physicalKeys().get("Physical_Key_3"));
		Assert.assertNotNull(fact.physicalKeys().get("Physical_Key_4"));
		Assert.assertNotNull(fact.physicalKeys().get("Physical_Key_5"));
		Assert.assertNotNull(fact.physicalKeys().get("Physical_Key_6"));
	}

	private void mapColumns(Fact fact, String dimensionName, String dimensionKeyName, String physicalKeyName, Pair<String, String>... columns)
	{
		UIView mapuicomp = uiComponent.locate(MapColumnsUIComponent.class.getSimpleName());

		Assert.assertEquals(dimensionName + "." + dimensionKeyName, mapuicomp.getModel(MapColumnsUIComponent.MOD_DIMENSION_KEY_QUALIFIED_NAME));

		DimensionKey dimensionKey = mapuicomp.getModel(MapColumnsUIComponent.MOD_DIMENSION_KEY);

		for (Pair<String, String> pair : columns)
		{
			mapuicomp.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new ImmutablePair<DatabaseColumn, DatabaseColumn>(getColumn(dimensionKey, pair.getLeft()), fact.columns().get(pair.getRight())));
		}

		mapuicomp.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, physicalKeyName);
	}

	private DatabaseColumn getColumn(DimensionKey dimensionKey, String left)
	{
		for (DatabaseColumn dc : dimensionKey.columns())
		{
			if (dc.name().equals(left))
			{
				return dc;
			}
		}

		throw new IllegalArgumentException();
	}
}
