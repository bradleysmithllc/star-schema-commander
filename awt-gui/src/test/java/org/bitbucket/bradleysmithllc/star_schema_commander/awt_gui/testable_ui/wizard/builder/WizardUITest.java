package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.junit.Assert;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class WizardUITest extends AbstractUIComponentTest<UIWizard>
{
	public static final String SELECT_ITEM = "SELECT_ITEM";

	@UITestStep
	public void ui() throws InterruptedException, InvocationTargetException
	{
		// create a stage to hold the wizard
		EventQueue.invokeAndWait(new Runnable()
		{
			@Override
			public void run()
			{
				UIWizardFrame uiWizardFrame = new UIWizardFrame(uiComponent);
				UIHub.get().addComponent(uiWizardFrame);

				try
				{
					new UIContainerImpl().addView(uiWizardFrame);
					uiWizardFrame.dialog();
				}
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			}
		});

		UIView emptyList = uiComponent.locate(ListUIComponent.class.getSimpleName(), "Empty");
		emptyList.dispatchAction(SELECT_ITEM, "EmptyOption [0]");
		emptyList.dispatchAction(SELECT_ITEM, "EmptyOption [10]");
		emptyList.dispatchAction(SELECT_ITEM, "EmptyOption [20]");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "load");

		UIView loadedList = uiComponent.locate(ListUIComponent.class.getSimpleName(), "Loaded");
		loadedList.dispatchAction(SELECT_ITEM, "LoadedOption [1]");
		loadedList.dispatchAction(SELECT_ITEM, "LoadedOption [11]");
		loadedList.dispatchAction(SELECT_ITEM, "LoadedOption [21]");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "play");

		UIView playingList = uiComponent.locate(ListUIComponent.class.getSimpleName(), "Playing");
		playingList.dispatchAction(SELECT_ITEM, "PlayingOption [54]");
		playingList.dispatchAction(SELECT_ITEM, "PlayingOption [43]");
		playingList.dispatchAction(SELECT_ITEM, "PlayingOption [32]");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "pause");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "play");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "stop");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "eject");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "load");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "eject");
	}

	@Override
	protected UIWizard createComponent()
	{
		// first create the wizard state model
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		// states
		WizardState<StringBuilder, ListUIComponent> empty = wizardModelBuilder.newState(new StringBuilder(), new ListUIComponent()).id("Empty").create();
		WizardState<StringBuilder, ListUIComponent> loaded = wizardModelBuilder.newState(new StringBuilder(), new ListUIComponent()).id("Loaded").create();
		WizardState<StringBuilder, ListUIComponent> playing = wizardModelBuilder.newState(new StringBuilder(), new ListUIComponent()).id("Playing").create();
		WizardState<StringBuilder, ListUIComponent> paused = wizardModelBuilder.newState(new StringBuilder(), new ListUIComponent()).id("Paused").create();

		// transitions
		wizardModelBuilder.newTransition(empty, loaded).id("load").logicalName("Load").handler(new StringBuilderStateTransitionHandler("Empty: {\"selection: \"EmptyOption [0]\"}Empty: {\"selection: \"EmptyOption [10]\"}Empty: {\"selection: \"EmptyOption [20]\"}")).create();
		wizardModelBuilder.newTransition(loaded, playing).id("play").logicalName("Play").handler(new StringBuilderStateTransitionHandler("Loaded: {\"selection: \"LoadedOption [1]\"}Loaded: {\"selection: \"LoadedOption [11]\"}Loaded: {\"selection: \"LoadedOption [21]\"}")).create();
		wizardModelBuilder.newTransition(playing, paused).id("pause").logicalName("Pause").handler(new StringBuilderStateTransitionHandler("Playing: {\"selection: \"PlayingOption [54]\"}Playing: {\"selection: \"PlayingOption [43]\"}Playing: {\"selection: \"PlayingOption [32]\"}")).create();
		wizardModelBuilder.newTransition(paused, playing).id("play").logicalName("Play").create();
		wizardModelBuilder.newTransition(playing, loaded).id("stop").logicalName("Stop").create();
		wizardModelBuilder.newTransition(loaded, empty).id("eject").logicalName("Eject").create();

		UIHub.init(new TestUIController());
		final WizardStateModel model = wizardModelBuilder.initial(empty).create();

		return new UIWizard(model);
	}

	private static class StringBuilderStateTransitionHandler implements StateTransitionHandler<StringBuilder, ListUIComponent, StringBuilder, ListUIComponent>
	{
		private final String expected;

		public StringBuilderStateTransitionHandler(String s)
		{
			expected = s;
		}

		@Override
		public void transition(WizardState<StringBuilder, ListUIComponent> from, WizardState<StringBuilder, ListUIComponent> to, WizardStateTransition<StringBuilder, ListUIComponent, StringBuilder, ListUIComponent> transition)
		{
			String values = from.model().toString();
			Assert.assertEquals(expected, values);
		}
	}
}

class ListUIComponent extends WizardStateUIComponent<StringBuilder>
{
	JList<String> list;
	DefaultListModel<String> model = new DefaultListModel<String>();
	private JScrollPane scroller;

	@Override
	public JComponent getComponent()
	{
		return scroller;
	}

	@Override
	public <T> T getModel(int id)
	{
		return (T) model;
	}

	@Override
	protected void buildParent(Profile profile)
	{
		list = new JList<String>(model);

		scroller = new JScrollPane(list);
		scroller.setBorder(BorderFactory.createTitledBorder("Select a String [" + state().id() + "]"));

		for (int i = 0; i < 100; i++)
		{
			model.add(model.size(), state().id() + "Option [" + i + "]");
		}

		list.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				stateModel().append(state().id() + ": {\"selection: \"" + list.getSelectedValue() + "\"}");
			}
		});
	}

	@Override
	public void dispatch(String message, Object value)
	{
		if (message == WizardUITest.SELECT_ITEM)
		{
			String val = (String) value;

			list.setSelectedValue(val, true);
		}
	}

	@Override
	public String viewInstance()
	{
		return state().id();
	}
}
