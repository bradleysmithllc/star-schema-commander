package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.TransitionStates;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_measures.FactMeasuresModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.DatabaseColumnTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.ProcessColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

public class FactMeasureWizardTest extends UIComponentTest<FactMeasuresModelWizard>
{

	private Fact fact;

	@Override
	protected FactMeasuresModelWizard createComponent()
	{
		Dimension dim = profile.defaultReportingArea().newDimension().schema("sch").name("d").create();
		dim.newDatabaseColumn().name("pk").jdbcType(Types.INTEGER).create();
		dim.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();

		dim.newDimensionKey().column("pk").physicalName("PK_NAME").logicalName("pk").create();

		fact = profile.defaultReportingArea().newFact().name("fact").schema("schema").create();

		fact.newDatabaseColumn().name("fk").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("Measure1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("Measure2").jdbcType(Types.NUMERIC).create();
		fact.newDatabaseColumn().name("Measure3").jdbcType(Types.DECIMAL).create();
		fact.newDatabaseColumn().name("Measure4").jdbcType(Types.BIT).create();
		fact.newDatabaseColumn().name("Measure5").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("Measure6").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("Leasure6").jdbcType(Types.INTEGER).create();

		fact.newFactDimensionKey().dimension("sch_d").keyName("pk").join("fk", "pk").create();

		return new FactMeasuresModelWizard(fact);
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@Test
	public void alpha() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		DatabaseColumnTableModel model = comp.getModel(SelectColumnsUIComponent.MODEL_COLUMN_LIST);

		Assert.assertEquals("Leasure6", model.getValueAt(0, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(0, 1));

		Assert.assertEquals("Measure1", model.getValueAt(1, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(1, 1));

		Assert.assertEquals("Measure2", model.getValueAt(2, 0));
		Assert.assertEquals("NUMERIC", model.getValueAt(2, 1));

		Assert.assertEquals("Measure3", model.getValueAt(3, 0));
		Assert.assertEquals("DECIMAL", model.getValueAt(3, 1));

		Assert.assertEquals("Measure4", model.getValueAt(4, 0));
		Assert.assertEquals("BIT", model.getValueAt(4, 1));

		Assert.assertEquals("Measure5", model.getValueAt(5, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(5, 1));

		Assert.assertEquals("Measure6", model.getValueAt(6, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(6, 1));
	}

	@Test
	public void excludeKeys() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		DatabaseColumnTableModel model = comp.getModel(SelectColumnsUIComponent.MODEL_COLUMN_LIST);

		Assert.assertEquals(7, model.size());

		Assert.assertEquals("Leasure6", model.getValueAt(0, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(0, 1));

		Assert.assertEquals("Measure1", model.getValueAt(1, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(1, 1));

		Assert.assertEquals("Measure2", model.getValueAt(2, 0));
		Assert.assertEquals("NUMERIC", model.getValueAt(2, 1));

		Assert.assertEquals("Measure3", model.getValueAt(3, 0));
		Assert.assertEquals("DECIMAL", model.getValueAt(3, 1));

		Assert.assertEquals("Measure4", model.getValueAt(4, 0));
		Assert.assertEquals("BIT", model.getValueAt(4, 1));

		Assert.assertEquals("Measure5", model.getValueAt(5, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(5, 1));

		Assert.assertEquals("Measure6", model.getValueAt(6, 0));
		Assert.assertEquals("INTEGER", model.getValueAt(6, 1));
	}

	@Test
	public void nonSelected() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());
		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_COLUMNS, new ArrayList<DatabaseColumn>());

		// verify transition is disabled
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);

		Assert.assertFalse(mod.transitionEnabled("logical"));
		Assert.assertTrue(mod.transitionActive("logical"));
	}

	@Test
	public void defaultSelected() throws Exception
	{
		// verify transition is disabled
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);

		Assert.assertFalse(mod.transitionEnabled("logical"));
		Assert.assertTrue(mod.transitionActive("logical"));
	}

	@Test
	public void trackSelected() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());
		ArrayList<DatabaseColumn> value = new ArrayList<DatabaseColumn>();
		value.add(fact.columns().get("Measure3"));

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_COLUMNS, value);

		// verify transition is enabled with selection
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);

		Assert.assertTrue(mod.transitionEnabled("logical"));
		Assert.assertTrue(mod.transitionActive("logical"));

		value.clear();

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_COLUMNS, value);

		// verify transition is disabled now
		Assert.assertFalse(mod.transitionEnabled("logical"));
		Assert.assertTrue(mod.transitionActive("logical"));
	}

	@Test
	public void processAll() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// press the logical button
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");

		// now press process
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// fact measures should be defined now
		Map<String, FactMeasure> factMeasureMap = fact.measures();

		Assert.assertEquals(7, factMeasureMap.size());

		// validate each one has a measure named after it
		Assert.assertNotNull(factMeasureMap.get("Measure1"));
		Assert.assertNotNull(factMeasureMap.get("Measure2"));
		Assert.assertNotNull(factMeasureMap.get("Measure3"));
		Assert.assertNotNull(factMeasureMap.get("Measure4"));
		Assert.assertNotNull(factMeasureMap.get("Measure5"));
		Assert.assertNotNull(factMeasureMap.get("Measure6"));
		Assert.assertNotNull(factMeasureMap.get("Leasure6"));
	}

	@Test
	public void closeDoesNothing() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// press the logical button
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "close");

		// fact measures should not be defined now
		Map<String, FactMeasure> factMeasureMap = fact.measures();

		Assert.assertEquals(0, factMeasureMap.size());
	}

	@Test
	public void closeCancels() throws Exception
	{
		UIView sccomp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		sccomp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// press the logical button
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");
		UIView pccomp = uiComponent.locate(ProcessColumnsUIComponent.class.getSimpleName());

		pccomp.dispatchAction(ProcessColumnsUIComponent.MSG_SET_LOGICAL_NAME, "Logically");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "next");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "close");

		// fact measures should be defined now
		Map<String, FactMeasure> factMeasureMap = fact.measures();

		Assert.assertEquals(0, factMeasureMap.size());
	}

	@Test
	public void stepThrough() throws Exception
	{
		UIView sccomp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		sccomp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_COLUMN, fact.columns().get("Measure1"));

		// press the logical button
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");
		UIView pccomp = uiComponent.locate(ProcessColumnsUIComponent.class.getSimpleName());

		pccomp.dispatchAction(ProcessColumnsUIComponent.MSG_SET_LOGICAL_NAME, "Logically");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// fact measures should be defined now
		Map<String, FactMeasure> factMeasureMap = fact.measures();

		Assert.assertEquals(1, factMeasureMap.size());

		Assert.assertNotNull(factMeasureMap.get("Logically"));
	}
}
