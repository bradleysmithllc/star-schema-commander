package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.breadcrumb_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.BreadcrumbPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.junit.Assert;

public class BreadcrumbPanelSelectRATriggersEventTest extends BreadcrumbPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void selectRATriggersEvent() throws Exception
	{
		UIHub.selectReportingArea(profile.defaultReportingArea());

		uiComponent.dispatchAction(BreadcrumbPanel.MSG_SELECT_BREADCRUMB, "[default]");

		// verify default is selected
		Assert.assertSame(profile.defaultReportingArea(), UIHub.selectedReportingArea());

		// select nested level
		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("Level 1.Level 2.Level 3"));
		// select breadcrumb for mid-level
		uiComponent.dispatchAction(BreadcrumbPanel.MSG_SELECT_BREADCRUMB, "Level 1");
		Assert.assertSame(profile.defaultReportingArea().reportingAreas().get("Level 1"), UIHub.selectedReportingArea());
	}
}
