package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_history_view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridHistoryView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainer;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.junit.Assert;
import org.junit.Test;
import org.loadui.testfx.GuiTest;

public class DataGridHistoryViewFXTest extends GuiTest
{
	@Test
	public void buttonsDisabledByDefault() throws InterruptedException
	{
		Button button = find("#DataGridHistoryView_Previous");
		Assert.assertNotNull(button);
		Assert.assertTrue(button.isDisabled());

		button = find("#DataGridHistoryView_Next");
		Assert.assertNotNull(button);
		Assert.assertTrue(button.isDisabled());
	}

	@Override
	protected Parent getRootNode()
	{
		UIHub.init(new TestUIController());

		DataGridHistoryView dghv = new DataGridHistoryView();

		UIContainer co = new UIContainerImpl();
		co.addView(dghv);

		return (Parent) dghv.view();
	}
}