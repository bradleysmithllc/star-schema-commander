package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardStateImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardStateModelImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardStateTransitionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.Reference;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SelectDimensionsUIComponentDefaultTest extends UIComponentTest<SelectDimensionsUIComponent>
{
	@Test
	public void selectRa() throws Exception
	{
		Assert.assertSame(profile.defaultReportingArea(), uiComponent.getModel(SelectDimensionsUIComponent.MOD_EFFECTIVE_REPORTING_AREA));
	}

	protected void prepare()
	{
	}

	@Override
	protected void prepareComponent()
	{
		uiComponent.activate();
	}

	@Override
	protected final SelectDimensionsUIComponent createComponent()
	{
		SelectDimensionsUIComponent selectDimensionsUIComponent = new SelectDimensionsUIComponent("next");
		WizardStateImpl<Reference<List<Dimension>>, SelectDimensionsUIComponent> state = new WizardStateImpl<
			Reference<List<Dimension>>,
			SelectDimensionsUIComponent
			>(
			new Reference<List<Dimension>>(new ArrayList<Dimension>()), "test", null,
			null);

		WizardStateTransition wst = new WizardStateTransitionBuilder(state, state, new WizardStateModelImpl()).id("next").create();

		selectDimensionsUIComponent.state(
			state
		);

		prepare();

		return selectDimensionsUIComponent;
	}

	@Override
	protected final void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}
}