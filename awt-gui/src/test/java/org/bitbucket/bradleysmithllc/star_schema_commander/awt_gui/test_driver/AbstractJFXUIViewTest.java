package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javafx.scene.Node;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.SystemUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.StageUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainer;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

@RunWith(
	UIComponentTestRunner.class
)
public abstract class AbstractJFXUIViewTest<T extends UIView<Node>>
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	protected StageUIView stageUIView;
	protected Profile profile;
	protected T uiComponent;
	private AtomicLong index = new AtomicLong(System.currentTimeMillis());

	@Before
	public final void setup() throws Exception
	{
		EventUtils.runOnJFXEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				stageUIView = new StageUIView();

				try
				{
					profile = createProfile();
					prepareProfile();

					UIHub.init(useTestUIController() ? new TestUIController() : new SystemUIController());
					prepareUIHub();

					UIHub.profile(profile);

					uiComponent = createComponent();

					stageUIView.setView(uiComponent);

					UIContainer con = new UIContainerImpl();
					con.addView(stageUIView);

					prepareComponent();

					stageUIView.stage().hide();
					stageUIView.stage().sizeToScene();
					stageUIView.stage().show();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Assert.fail(e.toString());
				}
			}
		});
	}

	@UITestBeforeStep
	public final void beforeStep()
	{
		EventUtils.runOnJFXEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				stageUIView.stage().sizeToScene();
				stageUIView.stage().centerOnScreen();
			}
		});
	}

	@UITestBeforeStep
	public void afterStep()
	{
	}

	@After
	public final void dispose()
	{
		EventUtils.runOnJFXEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				//stageUIView.stage().close();
			}
		});

		profile.dispose();
	}

	/* This will be destroyed each time around */
	private Profile createProfile() throws IOException
	{
		File path = temporaryFolder.newFile("profile" + index.incrementAndGet() + ".zip");
		FileUtils.forceDelete(path);

		return Profile.create(path);
	}

	protected void prepareProfile() throws Exception
	{
	}

	protected void prepareComponent()
	{
	}

	protected void prepareUIHub()
	{
	}

	protected boolean useTestUIController()
	{
		return true;
	}

	protected abstract T createComponent();

	protected Pair<String, Object> lastDispatchEvent()
	{
		TestUIController tc = (TestUIController) UIHub.get().getSystemController();

		return tc.lastDispatch();
	}
}
