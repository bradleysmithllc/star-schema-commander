package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.FactDimensionKeysModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionKeyUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class FactKeyWizardShowDimKeysTest extends FactKeyWizardTestBase
{
	@Override
	protected FactDimensionKeysModelWizard createComponent()
	{
		ReportingArea fkw = profile.defaultReportingArea().reportingAreas().get(FactKeyWizardLongPathTest.class.getSimpleName());

		UIHub.selectReportingArea(fkw);

		return new FactDimensionKeysModelWizard(fkw.facts().get("fact"));
	}

	@UITestStep
	public void showDimKeys() throws Exception
	{
		UIView comp = uiComponent.locate(SelectDimensionsUIComponent.class.getSimpleName());

		Map<String, Dimension> dims = UIHub.selectedReportingArea().dimensions();

		Dimension dim11 = dims.get("Dim34");
		comp.dispatchAction(SelectDimensionsUIComponent.MSG_SELECT_DIMENSION, dim11);

		// verify correct keys displayed
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "selectKeys");
		UIView sdkcomp = uiComponent.locate(SelectDimensionKeyUIComponent.class.getSimpleName());

		List<DimensionKey> keys = sdkcomp.getModel(SelectDimensionKeyUIComponent.MOD_AVAILABLE_KEYS_LIST);

		// verify it matches the dim exactly
		Assert.assertEquals(dim11.keys().size(), keys.size());

		// now verify each entry
		for (Map.Entry<String, DimensionKey> key : dim11.keys().entrySet())
		{
			Assert.assertTrue(keys.contains(key.getValue()));
		}
	}
}
