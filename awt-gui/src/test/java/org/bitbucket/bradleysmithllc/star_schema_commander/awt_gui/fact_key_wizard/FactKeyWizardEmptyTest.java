package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.junit.Assert;

import java.util.List;

public class FactKeyWizardEmptyTest extends FactKeyWizardTestBase
{
	@UITestStep(ordinal = 0)
	public void behave() throws Exception
	{
		UIView comp = uiComponent.locate(SelectDimensionsUIComponent.class.getSimpleName());

		List<Dimension> model = comp.getModel(SelectDimensionsUIComponent.MOD_SELECTED_DIMENSIONS);

		Assert.assertTrue(model.isEmpty());
	}
}