package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_history_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridHistoryPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.junit.Assert;

public class DataGridHistoryPanelPreviousTest extends DataGridHistoryPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void addQueries() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 1)
	public void previous() throws Exception
	{
		uiComponent.dispatchAction(DataGridHistoryPanel.MSG_PREVIOUS);
	}

	@UITestStep(ordinal = 2)
	public void previousAndNextEnabled() throws Exception
	{
		Assert.assertTrue(uiComponent.checkState(DataGridHistoryPanel.STATE_PREVIOUS_ENABLED));
		Assert.assertTrue(uiComponent.checkState(DataGridHistoryPanel.STATE_NEXT_ENABLED));
	}
}
