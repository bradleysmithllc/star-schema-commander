package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

public class ClassNameUtilsTest
{
	@Test
	public void empty()
	{
		Assert.assertEquals("", ClassNameUtils.removeCapsWithDashes(""));
	}

	@Test
	public void _null()
	{
		Assert.assertNull(ClassNameUtils.removeCapsWithDashes(null));
	}

	@Test
	public void lower()
	{
		Assert.assertEquals("hello", ClassNameUtils.removeCapsWithDashes("hello"));
	}

	@Test
	public void firstWord()
	{
		Assert.assertEquals("hello", ClassNameUtils.removeCapsWithDashes("Hello"));
	}

	@Test
	public void twoWords()
	{
		Assert.assertEquals("hello-world", ClassNameUtils.removeCapsWithDashes("HelloWorld"));
	}

	@Test
	public void acronym()
	{
		Assert.assertEquals("url-class", ClassNameUtils.removeCapsWithDashes("URLClass"));
	}

	@Test
	public void lastUpper()
	{
		Assert.assertEquals("timest", ClassNameUtils.removeCapsWithDashes("TimesT"));
	}
}