package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import java.lang.reflect.Method;
import java.util.*;

public class UIComponentTestRunner extends BlockJUnit4ClassRunner
{
	public static long NO_PAUSE = -1L;

	public static long PAUSE_BETWEEN_STEPS = NO_PAUSE;
	public static long PAUSE_WHEN_DONE = NO_PAUSE;
	public static boolean INTERACTIVE = false;

	/**
	 * Creates a BlockJUnit4ClassRunner to run {@code klass}
	 *
	 * @param klass
	 * @throws InitializationError if the test class is malformed.
	 */
	public UIComponentTestRunner(Class<?> klass) throws InitializationError
	{
		super(klass);

		// make sure that mac os can handle AWT and JavaFX
		System.setProperty("javafx.macosx.embedded", "true");
		java.awt.Toolkit.getDefaultToolkit();
	}

	@Override
	protected List<FrameworkMethod> computeTestMethods()
	{
		// grab all UITestStep methods
		Method[] methodList = getTestClass().getJavaClass().getMethods();

		List<Method> methodsToExecute = new ArrayList<Method>();

		for (Method method : methodList)
		{
			// check for the uiteststep annotation
			UITestStep annot = method.getAnnotation(UITestStep.class);

			if (annot != null)
			{
				methodsToExecute.add(method);
			}
		}

		sortByOrdinal(methodsToExecute);

		if (methodsToExecute.size() == 0)
		{
			throw new IllegalStateException("No tests to execute");
		}

		return Arrays.asList((FrameworkMethod) new UITestFrameworkMethod(getTestClass().getJavaClass(), methodsToExecute));
	}

	private void sortByOrdinal(List<Method> methodsToExecute)
	{
		Collections.sort(methodsToExecute, new Comparator<Method>()
		{
			@Override
			public int compare(Method o1, Method o2)
			{
				UITestStep lannot = o1.getAnnotation(UITestStep.class);
				UITestStep rannot = o2.getAnnotation(UITestStep.class);

				if (lannot.ordinal() < rannot.ordinal())
				{
					return -1;
				}
				else if (lannot.ordinal() == rannot.ordinal())
				{
					return 0;
				}

				return 1;
			}
		});
	}
}
