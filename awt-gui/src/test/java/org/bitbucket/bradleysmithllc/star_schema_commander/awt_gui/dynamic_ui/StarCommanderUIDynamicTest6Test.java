package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dynamic_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.BreadcrumbPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.junit.Assert;

public class StarCommanderUIDynamicTest6Test extends StarCommanderUIDynamicTestBase
{
	@UITestStep(ordinal = 0)
	public void newReportingAreaAction() throws Exception
	{
		UIView ap = UIHub.get().locate(ActionView.class.getSimpleName());
		ap.dispatchAction(ActionView.MSG_NEW_REPORTING_AREA, "SDW");
		Thread.sleep(2000L);
	}

	@Override
	protected boolean useTestUIController()
	{
		return false;
	}

	@UITestStep(ordinal = 1)
	public void validateBreadcrumb() throws Exception
	{
		UIView bcp = UIHub.get().locate(BreadcrumbPanel.class.getSimpleName());

		Assert.assertEquals(
			"[default].SDW",
			bcp.getModel(UIModels.BREADCRUMB_PANEL_GRAPH)
		);
	}
}
