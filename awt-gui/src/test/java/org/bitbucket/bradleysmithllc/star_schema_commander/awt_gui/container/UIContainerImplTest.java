package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.container;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

public class UIContainerImplTest
{
	@Test
	public void testLifecycle() throws IOException
	{
		UIHub.init(new TestUIController());

		UIContainerImpl impl = new UIContainerImpl();

		StringBuilder stb = new StringBuilder();

		ScribingUIView scribingUIView = new ScribingUIView(stb);
		impl.addView(scribingUIView);

		compareScript("testLifecycle", "activate", stb);
		impl.reactivate(scribingUIView);
		compareScript("testLifecycle", "reactivate", stb);
	}

	@Test
	public void testLifecycleTree() throws IOException
	{
		UIHub.init(new TestUIController());

		UIContainerImpl impl = new UIContainerImpl();

		StringBuilder stb = new StringBuilder();

		ScribingUIView scribingUIView = new ScribingUIView(stb);

		scribingUIView.addChildView();
		scribingUIView.addChildView();
		ScribingUIView scvi = scribingUIView.addChildView();

		scvi.addChildView();
		scvi.addChildView().addChildView();

		impl.addView(scribingUIView);

		compareScript("testLifecycleTree", "activate", stb);
		impl.reactivate(scribingUIView);
		compareScript("testLifecycleTree", "reactivate", stb);
	}

	@Test
	public void testLifecycleTreeMorphs() throws IOException
	{
		UIHub.init(new TestUIController());

		UIContainerImpl impl = new UIContainerImpl();

		StringBuilder stb = new StringBuilder();

		ScribingUIView scribingUIView = new ScribingUIView(stb);

		scribingUIView.addChildView();
		scribingUIView.addChildView();
		ScribingUIView scvi = scribingUIView.addChildView();

		scvi.addChildView();
		scvi.addChildView().addChildView();

		impl.addView(scribingUIView);

		compareScript("testLifecycleTreeMorphs", "activate", stb);

		scribingUIView.clearMyChildViews();

		stb.append(">>>>>>>>>>>>>NextPass<<<<<<<<<<<<<<<\n");
		impl.reactivate(scribingUIView);
		compareScript("testLifecycleTreeMorphs", "reactivate", stb);
	}

	@Test
	public void testLifecycleThreads() throws IOException
	{
		UIHub.init(new TestUIController());

		UIContainerImpl impl = new UIContainerImpl();

		StringBuilder stb = new StringBuilder();

		ScribingUIView scribingUIView = new ScribingUIView(stb);

		scribingUIView.addSwingChildView();
		scribingUIView.addJFXChildView();

		impl.addView(scribingUIView);

		compareScript("testLifecycleThreads", "activate", stb);

		scribingUIView.clearMyChildViews();

		stb.append(">>>>>>>>>>>>>NextPass<<<<<<<<<<<<<<<\n");
		impl.reactivate(scribingUIView);
		compareScript("testLifecycleThreads", "reactivate", stb);
	}

	@Test
	public void testComponentRegistrations() throws IOException
	{
		UIHub.init(new TestUIController());

		UIContainerImpl impl = new UIContainerImpl();

		StringBuilder stb = new StringBuilder();

		ScribingUIView scribingUIView = new ScribingUIView(stb);

		impl.addView(scribingUIView);

		Assert.assertNotNull(impl.locateViewGlobally(scribingUIView.viewClass(), scribingUIView.viewInstance()));
	}

	@Test
	public void testComponentChildRegistrations() throws IOException
	{
		UIHub.init(new TestUIController());

		UIContainerImpl impl = new UIContainerImpl();

		StringBuilder stb = new StringBuilder();

		ScribingUIView scribingUIView = new ScribingUIView(stb);

		ScribingUIView ch1 = scribingUIView.addChildView();
		ScribingUIView ch2 = scribingUIView.addChildView();
		ScribingUIView ch3 = scribingUIView.addChildView();

		ScribingUIView ch3_1 = ch3.addChildView();

		impl.addView(scribingUIView);

		Assert.assertNotNull(impl.locateViewGlobally(scribingUIView.viewClass(), scribingUIView.viewInstance()));
		Assert.assertNotNull(impl.locateViewGlobally(ch1.viewClass(), ch1.viewInstance()));
		Assert.assertNotNull(impl.locateViewGlobally(ch2.viewClass(), ch2.viewInstance()));
		Assert.assertNotNull(impl.locateViewGlobally(ch3.viewClass(), ch3.viewInstance()));
		Assert.assertNotNull(impl.locateViewGlobally(ch3_1.viewClass(), ch3_1.viewInstance()));
	}

	private void compareScript(String testLifecycle, String once, StringBuilder stb) throws IOException
	{
		String key = testLifecycle + (once != null ? ("_" + once) : "");

		String name = "/ui-container-impl/" + key + ".txt";
		URL resource = getClass().getResource(name);

		Assert.assertNotNull(name, resource);

		String script = IOUtils.toString(resource);

		Assert.assertEquals(key, script, stb.toString());
	}
}

