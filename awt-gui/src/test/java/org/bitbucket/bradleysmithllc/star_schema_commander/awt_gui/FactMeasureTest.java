package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactMeasurePanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactMeasureTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class FactMeasureTest extends UIComponentTest<FactMeasurePanel>
{
	@Override
	protected FactMeasurePanel createComponent()
	{
		return new FactMeasurePanel();
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@Test
	public void displayMeasures() throws Exception
	{
		FactMeasureTableModel model = uiComponent.getModel(FactMeasurePanel.FACT_MEASURE_PANEL_FACT_MEASURE_LIST);

		Assert.assertTrue(model.isEmpty());
		// select a fact
		UIHub.selectFact(profile.defaultReportingArea().facts().get("dbo_Fact"));
		Assert.assertFalse(model.isEmpty());

		Assert.assertEquals(4, model.size());

		Assert.assertEquals("Col 1", model.tableData().get(0).logicalName());
		Assert.assertEquals("Col 2", model.tableData().get(1).logicalName());
		Assert.assertEquals("Col 3", model.tableData().get(2).logicalName());
		Assert.assertEquals("Col 4", model.tableData().get(3).logicalName());

		// switch reporting area. Must blank panel
		UIHub.selectReportingArea(profile.defaultReportingArea().newReportingArea().name("new").create());
		Assert.assertTrue(model.isEmpty());
	}

	@Test
	public void switchFacts() throws Exception
	{
		FactMeasureTableModel model = uiComponent.getModel(FactMeasurePanel.FACT_MEASURE_PANEL_FACT_MEASURE_LIST);

		Assert.assertTrue(model.isEmpty());
		// select a fact
		UIHub.selectFact(profile.defaultReportingArea().facts().get("dbo_Fact"));
		UIHub.selectFact(profile.defaultReportingArea().facts().get("dbo_Fact_2"));

		Assert.assertEquals("2Col 1", model.tableData().get(0).logicalName());
		Assert.assertEquals("2Col 2", model.tableData().get(1).logicalName());
		Assert.assertEquals("2Col 3", model.tableData().get(2).logicalName());
		Assert.assertEquals("2Col 4", model.tableData().get(3).logicalName());

		Thread.sleep(5000L);

		UIHub.selectFact(profile.defaultReportingArea().facts().get("dbo_Fact"));

		Assert.assertEquals("Col 1", model.tableData().get(0).logicalName());
		Assert.assertEquals("Col 2", model.tableData().get(1).logicalName());
		Assert.assertEquals("Col 3", model.tableData().get(2).logicalName());
		Assert.assertEquals("Col 4", model.tableData().get(3).logicalName());

		Thread.sleep(5000L);

		// switch reporting area. Must blank panel
		UIHub.selectReportingArea(profile.defaultReportingArea().newReportingArea().name("new").create());
		Assert.assertTrue(model.isEmpty());
	}
}
