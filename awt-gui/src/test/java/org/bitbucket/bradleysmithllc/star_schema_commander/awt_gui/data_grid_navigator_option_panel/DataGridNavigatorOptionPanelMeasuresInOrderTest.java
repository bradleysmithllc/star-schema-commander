package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigator;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

/**
 * Add all measures at the 'none' agg level and verify ordering
 */
public class DataGridNavigatorOptionPanelMeasuresInOrderTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void selectAllMeasures() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
	}

	@UITestStep(ordinal = 1)
	public void addAllMeasures() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_ALL);
	}

	@UITestStep(ordinal = 2)
	public void measuresInOrder() throws Exception
	{
		// verify that all measures are selected by default
		List<DataGridNavigator.IncludedMeasure> fmMeasures = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_SELECTED_MEASURES);

		Assert.assertEquals(6, fmMeasures.size());

		// verify order
		Assert.assertEquals("measure1", fmMeasures.get(0).measure().logicalName());
		Assert.assertEquals(Query.aggregation.none, fmMeasures.get(0).aggregation());
		Assert.assertEquals("measure1", fmMeasures.get(1).measure().logicalName());
		Assert.assertEquals(Query.aggregation.sum, fmMeasures.get(1).aggregation());

		Assert.assertEquals("Measure2", fmMeasures.get(2).measure().logicalName());
		Assert.assertEquals(Query.aggregation.none, fmMeasures.get(2).aggregation());
		Assert.assertEquals("Measure2", fmMeasures.get(3).measure().logicalName());
		Assert.assertEquals(Query.aggregation.sum, fmMeasures.get(3).aggregation());

		Assert.assertEquals("measure3", fmMeasures.get(4).measure().logicalName());
		Assert.assertEquals(Query.aggregation.none, fmMeasures.get(4).aggregation());
		Assert.assertEquals("measure3", fmMeasures.get(5).measure().logicalName());
		Assert.assertEquals(Query.aggregation.sum, fmMeasures.get(5).aggregation());
	}
}
