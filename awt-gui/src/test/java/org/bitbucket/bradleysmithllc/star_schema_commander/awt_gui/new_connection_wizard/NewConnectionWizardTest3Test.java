package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.new_connection_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.H2ConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.NewConnectionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.SelectConnectionTypeUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.junit.Assert;

import java.io.File;

public class NewConnectionWizardTest3Test extends NewConnectionWizardBaseTest
{
	@UITestStep
	public void newH2Connection() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionView.class.getSimpleName());
		ac.dispatchActionAndWait(ActionView.MSG_NEW_CONNECTION);

		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_SELECT_CONNECTION_TYPE, "H2");

		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "H2Connection");

		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "h2");

		UIView newh2 = UIHub.get().locate(H2ConnectionUIComponent.class.getSimpleName());

		File value = temporaryFolder.newFolder();
		newh2.dispatchActionAndWait(H2ConnectionUIComponent.MSG_SELECT_DIRECTORY, value);

		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "confirm");

		profile.invalidate();
		Assert.assertTrue(profile.connections().containsKey("H2Connection"));

		DatabaseConnection h2 = profile.connections().get("H2Connection");

		Assert.assertNull(h2.userName());
		Assert.assertNull(h2.password());
		Assert.assertEquals("jdbc:h2:" + new File(value, h2.name()).getAbsolutePath(), h2.jdbcUrl());
	}
}
