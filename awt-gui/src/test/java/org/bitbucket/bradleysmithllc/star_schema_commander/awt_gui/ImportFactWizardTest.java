package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ImportFactWizardTest
{
	private static AtomicLong index = new AtomicLong(System.currentTimeMillis());
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	private StarCommanderUI starCommanderUI;
	private String h2CatalogName;

	@Test
	public void availableConnectionsSorted() throws Exception
	{
		Profile p = starCommanderUI.getProfile();
		p.newH2Connection().name("0src").create();
		p.newH2Connection().name("trc").create();

		p.persist();

		UIView ac = UIHub.get().locate(ActionPanel.class.getSimpleName());
		ac.dispatchAction(ActionPanel.MSG_IMPORT_FACTS);

		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.class.getSimpleName());
		DefaultListModel<DatabaseConnection> dlm = scui.getModel(SelectConnectionUIComponent.SELECT_CONNECTION_MODEL);

		Assert.assertEquals(3, dlm.size());

		Assert.assertSame(starCommanderUI.getProfile().connections().get("0src"), dlm.get(0));
		Assert.assertSame(starCommanderUI.getProfile().connections().get("src"), dlm.get(1));
		Assert.assertSame(starCommanderUI.getProfile().connections().get("trc"), dlm.get(2));
	}

	@Test
	public void availableTablesSorted() throws Exception
	{
		Profile p = starCommanderUI.getProfile();

		DatabaseConnection srco = p.newH2Connection().name("src0").create();

		// create some tables to list on the import
		prepareDatabase(srco);

		p.newH2Connection().name("0src").create();
		p.newH2Connection().name("trc").create();

		p.persist();

		UIView ac = UIHub.get().locate(ActionPanel.class.getSimpleName());
		ac.dispatchAction(ActionPanel.MSG_IMPORT_FACTS);

		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.class.getSimpleName());
		scui.dispatchAction(SelectConnectionUIComponent.MSG_SELECT_CONNECTION, starCommanderUI.getProfile().connections().get("src0"));

		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "tables");

		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		QualifiedTableTableModel dlm = scdi.getModel(SelectTablesUIComponent.AVAILABLE_DIMENSIONS_MODEL);

		// unfortunately this must include implicit objects
		Assert.assertEquals(35, dlm.size());

		// test where names are - we have to jump around the implicit crap
		Assert.assertEquals(0, dlm.getRowIndex(h2CatalogName + ".A.A"));
		Assert.assertEquals(30, dlm.getRowIndex(h2CatalogName + ".PUBLIC.A"));
		Assert.assertEquals(31, dlm.getRowIndex(h2CatalogName + ".PUBLIC.A1"));
		Assert.assertEquals(32, dlm.getRowIndex(h2CatalogName + ".PUBLIC.I"));
		Assert.assertEquals(33, dlm.getRowIndex(h2CatalogName + ".PUBLIC.Z"));
		Assert.assertEquals(34, dlm.getRowIndex(h2CatalogName + ".Z.Z"));
	}

	@Test
	public void tablesButtons() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionPanel.class.getSimpleName());
		ac.dispatchAction(ActionPanel.MSG_IMPORT_FACTS);

		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.class.getSimpleName());
		scui.dispatchAction(SelectConnectionUIComponent.MSG_SELECT_CONNECTION, starCommanderUI.getProfile().connections().get("src"));

		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "tables");

		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		// initially - The remove buttons are both disabled
		assertCheck(scdi, false, true, false, false);

		// select one entry.  No change in buttons
		String value = h2CatalogName + ".PUBLIC.A";
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, value);
		assertCheck(scdi, true, true, false, false);

		// add it to the selected list - now remove all must be enabled
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
		assertCheck(scdi, false, true, true, false);

		// add all.  Should leave both add buttons disabled
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_ALL_ACTION);
		assertCheck(scdi, false, false, true, false);

		// select one entry.  No change in buttons
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_SELECTED_TABLE, h2CatalogName + ".PUBLIC.A");
		assertCheck(scdi, false, false, true, true);

		// add it to the available list.
		scdi.dispatchAction(SelectTablesUIComponent.MSG_REMOVE_SELECTED_ACTION);
		assertCheck(scdi, false, true, true, false);

		// add it to the available list.
		scdi.dispatchAction(SelectTablesUIComponent.MSG_REMOVE_ALL_ACTION);
		assertCheck(scdi, false, true, false, false);
	}

	@Test
	public void backToDimsRemembersTables() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionPanel.class.getSimpleName());
		ac.dispatchAction(ActionPanel.MSG_IMPORT_FACTS);

		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.class.getSimpleName());
		scui.dispatchAction(SelectConnectionUIComponent.MSG_SELECT_CONNECTION, starCommanderUI.getProfile().connections().get("src"));

		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "tables");

		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		QualifiedTableTableModel slm = scdi.getModel(SelectTablesUIComponent.SELECTED_DIMENSIONS_MODEL);
		QualifiedTableTableModel alm = scdi.getModel(SelectTablesUIComponent.AVAILABLE_DIMENSIONS_MODEL);

		Assert.assertTrue(alm.contains(h2CatalogName + ".PUBLIC.A"));
		Assert.assertTrue(alm.contains(h2CatalogName + ".A.A"));

		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".PUBLIC.A");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".A.A");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);

		// advance and return
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "back");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		// verify that the two tables are still selected and that the tables are not in the available list
		Assert.assertTrue(slm.contains(h2CatalogName + ".PUBLIC.A"));
		Assert.assertTrue(slm.contains(h2CatalogName + ".A.A"));
		Assert.assertFalse(alm.contains(h2CatalogName + ".PUBLIC.A"));
		Assert.assertFalse(alm.contains(h2CatalogName + ".A.A"));

		// select all
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_ALL_ACTION);

		// advance and return
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "back");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		Assert.assertFalse(slm.isEmpty());
		Assert.assertTrue(alm.isEmpty());

		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@Test
	public void logicalPhysical() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionPanel.class.getSimpleName());
		ac.dispatchAction(ActionPanel.MSG_IMPORT_FACTS);

		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.class.getSimpleName());
		scui.dispatchAction(SelectConnectionUIComponent.MSG_SELECT_CONNECTION, starCommanderUI.getProfile().connections().get("src"));

		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "tables");

		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".PUBLIC.A");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".A.A");
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);

		// advance
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// skip the first table (A.A)
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "next");

		// override the logical name for PUBLIC.A to A Prime
		UIView pdui = UIHub.get().locate(ProcessTablesUIComponent.class.getSimpleName());

		// verify that we are indeed looking at the right table
		Assert.assertEquals(h2CatalogName + ".PUBLIC.A", pdui.getModel(ProcessTablesUIComponent.MOD_PHYSICAL_NAME));
		pdui.dispatchAction(ProcessTablesUIComponent.MSG_SET_LOGICAL_NAME, "A Prime");

		// done
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "done");

		UIView idui = UIHub.get().locate(ImportTablesUIComponent.class.getSimpleName());

		idui.dispatchAction(ImportTablesUIComponent.MSG_WAIT_FOR_IMPORT_PROCESS);

		// done it
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "close");

		// verify newly loaded dimensions
		Profile pro = starCommanderUI.getProfile();
		ReportingArea dra = pro.defaultReportingArea();

		Map<String, Fact> facts = dra.facts();

		Assert.assertNotNull(facts.get(h2CatalogName.replace(".", "_") + "_A_A"));
		Assert.assertNotNull(facts.get("A Prime"));
		Map<String, Fact> physicalFacts = dra.physicalFacts();
		Assert.assertNotNull(physicalFacts.get(h2CatalogName + ".A.A"));
		Assert.assertNotNull(physicalFacts.get(h2CatalogName + ".PUBLIC.A"));
	}

	@Before
	public void setup() throws Exception
	{
		Profile p = createProfile();

		DatabaseConnection srco = p.newH2Connection().name("src").path(temporaryFolder.newFile()).create();

		// create some tables to list on the import
		prepareDatabase(srco);

		p.persist();

		starCommanderUI = new StarCommanderUI(p.getProfileDirectory());
		starCommanderUI.launchme();
	}

	@After
	public void shutdown()
	{
		starCommanderUI.shutDown();
	}

	private void assertCheck(UIView scdi, boolean addSelected, boolean addAll, boolean removeALl, boolean removeSelected)
	{
		Assert.assertEquals(addAll, scdi.checkState(SelectTablesUIComponent.ADD_ALL_STATE));
		Assert.assertEquals(addSelected, scdi.checkState(SelectTablesUIComponent.ADD_SELECTED_STATE));
		Assert.assertEquals(removeSelected, scdi.checkState(SelectTablesUIComponent.REMOVE_SELECTED_STATE));
		Assert.assertEquals(removeALl, scdi.checkState(SelectTablesUIComponent.REMOVE_ALL_STATE));
	}

	private void prepareDatabase(DatabaseConnection srco) throws SQLException
	{
		Connection c = srco.open();

		try
		{
			Statement st = c.createStatement();

			try
			{
				st.execute("CREATE TABLE PUBLIC.I(ID INT);CREATE TABLE PUBLIC.A(ID INT);CREATE TABLE PUBLIC.a1(ID INT);CREATE TABLE PUBLIC.Z(ID INT);CREATE SCHEMA A;CREATE SCHEMA z;CREATE TABLE A.A(ID INT);CREATE TABLE z.Z(ID INT);");
			}
			finally
			{
				st.close();
			}

			// now get the catalog name
			ResultSet rs = c.getMetaData().getCatalogs();

			while (rs.next())
			{
				h2CatalogName = rs.getString(1);
			}

			rs.close();
		}
		finally
		{
			c.close();
		}
	}

	private Profile createProfile() throws IOException
	{
		File path = temporaryFolder.newFolder("profile");
		FileUtils.forceDelete(path);

		return Profile.create(path);
	}
}
