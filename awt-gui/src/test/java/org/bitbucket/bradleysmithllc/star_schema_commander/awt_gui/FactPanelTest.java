package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.FactTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class FactPanelTest extends UIComponentTest<FactPanel>
{
	@Test
	public void emptyReportingAreaSelected() throws Exception
	{
		FactTableModel model = uiComponent.getModel(UIModels.FACT_PANEL_FACT_LIST);

		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("Empty"));
		Assert.assertTrue(model.isEmpty());
	}

	@Test
	public void displaySelectedReportingAreaFacts() throws Exception
	{
		FactTableModel model = uiComponent.getModel(UIModels.FACT_PANEL_FACT_LIST);

		// select a reporting area
		ReportingArea defaultReportingArea = profile.defaultReportingArea();
		UIHub.selectReportingArea(defaultReportingArea);
		Assert.assertFalse(model.isEmpty());

		Assert.assertEquals(defaultReportingArea.facts().size(), model.size());
		Assert.assertSame(defaultReportingArea.facts().get("dbo_Fact"), model.tableData().get(0));

		UIHub.selectReportingArea(defaultReportingArea.reportingAreas().get("BI"));
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(2, model.size());
		Assert.assertSame(defaultReportingArea.reportingAreas().get("BI").facts().get("fact_FACT1"), model.tableData().get(0));
		Assert.assertSame(defaultReportingArea.reportingAreas().get("BI").facts().get("fact2_FACT2"), model.tableData().get(1));
	}

	@Test
	public void dimsAlphaOrder() throws Exception
	{
		FactTableModel model = uiComponent.getModel(UIModels.FACT_PANEL_FACT_LIST);

		// select a reporting area
		ReportingArea alpha = profile.defaultReportingArea().reportingAreas().get("Alpha");

		UIHub.selectReportingArea(alpha);
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(4, model.size());

		Assert.assertSame(alpha.facts().get("a_A"), model.tableData().get(0));
		Assert.assertSame(alpha.facts().get("A_a"), model.tableData().get(1));
		Assert.assertSame(alpha.facts().get("z_a"), model.tableData().get(2));
		Assert.assertSame(alpha.facts().get("Z_Z"), model.tableData().get(3));
	}

	@Override
	protected FactPanel createComponent()
	{
		return new FactPanel();
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}
}