package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.new_connection_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.SelectConnectionTypeUIComponent;
import org.junit.Assert;

import javax.swing.*;

/**
 * This is not a good test, but it is a hardcoded test of hardcoded values,
 * so perhaps it is not too bad . . .
 */
public class NewConnectionWizardTest2Test extends NewConnectionWizardBaseTest
{
	@UITestStep
	public void availableConnectionsSorted() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionView.class.getSimpleName());
		ac.dispatchActionAndWait(ActionView.MSG_NEW_CONNECTION);

		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		DefaultListModel<String> model = scui.getModel(SelectConnectionTypeUIComponent.MODEL_CONNECTION_LIST);

		// check connection list
		Assert.assertEquals(6, model.size());

		Assert.assertEquals("H2", model.get(0));
		Assert.assertEquals("HANA", model.get(1));
		Assert.assertEquals("Oracle", model.get(2));
		Assert.assertEquals("PostgreSQL", model.get(3));
		Assert.assertEquals("Sql Server (jTDS)", model.get(4));
		Assert.assertEquals("Sql Server (MS)", model.get(5));
	}
}
