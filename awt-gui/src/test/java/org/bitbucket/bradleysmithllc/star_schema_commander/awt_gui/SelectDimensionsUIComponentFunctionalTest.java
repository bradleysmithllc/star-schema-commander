package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class SelectDimensionsUIComponentFunctionalTest extends SelectDimensionsUIComponentDefaultTest
{
	// only here to prevent the subclass test from running
	@Test
	public void selectRa() throws Exception
	{
	}

	@Test
	public void emptyDimensions() throws Exception
	{
		Assert.assertEquals(0, ((List) uiComponent.getModel(SelectDimensionsUIComponent.MOD_SELECTED_DIMENSIONS)).size());
	}

	@Test
	public void matchSelection() throws Exception
	{
		List<Dimension> selectedList = uiComponent.getModel(SelectDimensionsUIComponent.MOD_SELECTED_DIMENSIONS);

		Dimension dim = UIHub.selectedReportingArea().dimensions().get("dbo_dim3");
		uiComponent.dispatchAction(SelectDimensionsUIComponent.MSG_SELECT_DIMENSION, dim);

		Assert.assertEquals(1, selectedList.size());
		Assert.assertSame(dim, selectedList.get(0));
	}

	@Test
	public void matchMultipleSelection() throws Exception
	{
		List<Dimension> selectedList = uiComponent.getModel(SelectDimensionsUIComponent.MOD_SELECTED_DIMENSIONS);

		List<Dimension> dlist = new ArrayList<Dimension>();

		dlist.add(UIHub.selectedReportingArea().dimensions().get("dbo_dim1"));
		dlist.add(UIHub.selectedReportingArea().dimensions().get("dbo_dim2"));
		dlist.add(UIHub.selectedReportingArea().dimensions().get("dbo_dim4"));

		uiComponent.dispatchAction(SelectDimensionsUIComponent.MSG_SELECT_DIMENSIONS, dlist);

		Assert.assertEquals(dlist.size(), selectedList.size());
		Assert.assertEquals(dlist, selectedList);
	}

	@Test
	public void transitions() throws Exception
	{
		// not enabled by default
		Assert.assertTrue(uiComponent.state().transitionMap().get("next").active());
		Assert.assertFalse(uiComponent.state().transitionMap().get("next").enabled());

		Dimension dim = UIHub.selectedReportingArea().dimensions().get("dbo_dim3");
		uiComponent.dispatchAction(SelectDimensionsUIComponent.MSG_SELECT_DIMENSION, dim);
		Assert.assertTrue(uiComponent.state().transitionMap().get("next").enabled());

		uiComponent.dispatchAction(SelectDimensionsUIComponent.MSG_CLEAR_SELECTION);
		Assert.assertFalse(uiComponent.state().transitionMap().get("next").enabled());

		uiComponent.dispatchAction(SelectDimensionsUIComponent.MSG_SELECT_ALL_DIMENSIONS);
		Assert.assertTrue(uiComponent.state().transitionMap().get("next").enabled());
	}

	@Override
	protected void prepare()
	{
		ReportingArea ra = profile.defaultReportingArea().newReportingArea().name("test").create();

		Dimension dim = ra.newDimension().name("dim1").schema("dbo").create();
		dim.newDatabaseColumn().name("a").jdbcType(Types.INTEGER).create();
		dim.newDimensionKey().logicalName("log").physicalName("phys").column("a").create();

		dim = ra.newDimension().name("dim2").schema("dbo").create();
		dim.newDatabaseColumn().name("a").jdbcType(Types.INTEGER).create();
		dim.newDimensionKey().logicalName("log").physicalName("phys").column("a").create();

		dim = ra.newDimension().name("dim3").schema("dbo").create();
		dim.newDatabaseColumn().name("a").jdbcType(Types.INTEGER).create();
		dim.newDimensionKey().logicalName("log").physicalName("phys").column("a").create();

		dim = ra.newDimension().name("dim4").schema("dbo").create();
		dim.newDatabaseColumn().name("a").jdbcType(Types.INTEGER).create();
		dim.newDimensionKey().logicalName("log").physicalName("phys").column("a").create();

		UIHub.selectReportingArea(ra);
	}
}
