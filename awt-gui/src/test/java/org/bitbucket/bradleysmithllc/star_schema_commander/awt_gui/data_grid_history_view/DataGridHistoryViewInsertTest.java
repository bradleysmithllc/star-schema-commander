package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_history_view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.junit.Assert;

import java.util.List;

public class DataGridHistoryViewInsertTest extends DataGridHistoryViewTestBase
{
	@UITestStep(ordinal = 0)

	public void addQuery0() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 1)
	public void addQuery1() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 2)
	public void addQuery2() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 3)
	public void addQuery3() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 4)
	public void addQuery4() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 5)
	public void addQuery5() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 6)
	public void checkHistorySize() throws Exception
	{
		List<PersistedQuery> hist = uiComponent.history();

		Assert.assertEquals(6, hist.size());
	}

	@UITestStep(ordinal = 7)
	public void checkHistoryItems() throws Exception
	{
		List<PersistedQuery> history = uiComponent.history();

		Assert.assertEquals(hist, history);
	}
}