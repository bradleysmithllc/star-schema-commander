package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.container;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.AbstractUIView;

public class ScribingUIView extends AbstractUIView<String>
{
	private final StringBuilder stb;
	private final String id;

	private int inc = 0;

	ScribingUIView(StringBuilder stb)
	{
		this(stb, "[root]");
	}

	ScribingUIView(StringBuilder stb, String id)
	{
		this.stb = stb;
		this.id = id;
	}

	public void clearMyChildViews()
	{
		clearChildViews();
	}

	public ScribingUIView addChildView()
	{
		ScribingUIView suiv = new ScribingUIView(stb, id + "." + inc++);

		addChild(suiv);

		return suiv;
	}

	public ScribingUIView addSwingChildView()
	{
		ScribingUIView suiv = new SwingScribingUIView(stb, id + ".(S)" + inc++);

		addChild(suiv);

		return suiv;
	}

	public ScribingUIView addJFXChildView()
	{
		ScribingUIView suiv = new JFXScribingUIView(stb, id + ".(FX)" + inc++);

		addChild(suiv);

		return suiv;
	}

	@Override
	public void subInitialize() throws Exception
	{
		message("subInitialize");
	}

	@Override
	public void activate() throws Exception
	{
		message("activate");
	}

	@Override
	public void prepareSelf() throws Exception
	{
		message("prepareSelf");
	}

	@Override
	public void prepareWithChildren() throws Exception
	{
		message("prepareWithChildren");
	}

	@Override
	public void deactivate()
	{
		message("deactivate");
	}

	private void message(String deactivate)
	{
		stb.append(id).append(": {").append(deactivate).append(": {swingEventThread: ").append(EventUtils.isEventThread()).append(", jfxEventThread: ").append(EventUtils.isJFXEventThread()).append("}}\n");
	}

	@Override
	public String viewInstance()
	{
		return id;
	}

	@Override
	public String view()
	{
		return id + ".Hello, World!";
	}
}
