package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_measures.FactMeasuresModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.DatabaseColumnTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class FactMeasureWizardEmptyTest extends UIComponentTest<FactMeasuresModelWizard>
{
	@Override
	protected FactMeasuresModelWizard createComponent()
	{
		Fact fact = profile.defaultReportingArea().newFact().name("fact").schema("schema").create();

		return new FactMeasuresModelWizard(fact);
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@Test
	public void newSQLConnection() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		DatabaseColumnTableModel model = comp.getModel(SelectColumnsUIComponent.MODEL_COLUMN_LIST);

		Assert.assertTrue(model.isEmpty());
	}
}