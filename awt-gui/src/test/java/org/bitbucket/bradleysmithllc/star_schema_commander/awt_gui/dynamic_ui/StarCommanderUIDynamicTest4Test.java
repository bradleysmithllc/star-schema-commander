package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dynamic_ui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.NavPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIStates;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import javax.swing.*;

public class StarCommanderUIDynamicTest4Test extends StarCommanderUIDynamicTestBase
{
	@Override
	protected void prepareProfile() throws Exception
	{
		profile.defaultReportingArea().newReportingArea().name("1").create();
		profile.persist();
	}

	@UITestStep
	public void reportingAreasVisibleWhenMoreThanOne() throws Exception
	{
		UIView np = UIHub.get().locate(NavPanel.class.getSimpleName());
		DefaultListModel<ReportingArea> componentModel = np.getModel(UIModels.NAV_PANEL_REPORTING_AREA_LIST);

		// make sure the areas appear in alpha order
		Assert.assertEquals("1", componentModel.get(0).name());

		Assert.assertTrue(np.checkState(UIStates.NAV_PANEL_REPORTING_AREAS_VISIBLE));
	}
}
