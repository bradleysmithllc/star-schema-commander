package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigator;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.junit.Assert;

import java.util.List;

public class DataGridNavigatorOptionPanelRemoveMeasureTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void selectAllAvailable() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
	}

	@UITestStep(ordinal = 1)
	public void add() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_ALL);
	}

	@UITestStep(ordinal = 2)
	public void validate() throws Exception
	{
		List<DataGridNavigator.IncludedMeasure> fmMeasures = uiComponent.selectedMeasures();

		Assert.assertEquals(6, fmMeasures.size());
	}

	@UITestStep(ordinal = 3)
	public void selectAllIncluded() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_INCLUDED);
	}

	@UITestStep(ordinal = 4)
	public void remove() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_REMOVE_INCLUDED_SELECTED);
	}

	@UITestStep(ordinal = 5)
	public void validateAgain() throws Exception
	{
		List<DataGridNavigator.IncludedMeasure> fmMeasures = uiComponent.selectedMeasures();

		Assert.assertTrue(fmMeasures.isEmpty());
	}
}
