package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.import_fact_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.StarCommanderUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.ImportTableModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.QualifiedTableTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.import_tables.SelectTablesUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.junit.Assert;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ImportFactWizardTest extends AbstractUIComponentTest<StarCommanderUIView>
{
	private String h2CatalogName;

	@UITestStep(ordinal = 0)
	public void selectImport() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionPanel.class.getSimpleName());
		ac.dispatchAction(ActionPanel.MSG_IMPORT_FACTS);
	}

	@UITestStep(ordinal = 1)
	public void selectConnection() throws Exception
	{
		UIView scui = UIHub.get().locate(SelectConnectionUIComponent.class.getSimpleName());
		scui.dispatchAction(SelectConnectionUIComponent.MSG_SELECT_CONNECTION, profile.connections().get("src"));
	}

	@UITestStep(ordinal = 2)
	public void tablesTransition() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "tables");
	}

	@UITestStep(ordinal = 3)
	public void waitForTableLoad() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);
	}

	@UITestStep(ordinal = 4)
	public void assertTablesAvailable() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());

		QualifiedTableTableModel alm = scdi.getModel(SelectTablesUIComponent.AVAILABLE_DIMENSIONS_MODEL);

		Assert.assertTrue(alm.contains(h2CatalogName + ".PUBLIC.A"));
		Assert.assertTrue(alm.contains(h2CatalogName + ".A.A"));
	}

	@UITestStep(ordinal = 5)
	public void selectPublicA() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".PUBLIC.A");
	}

	@UITestStep(ordinal = 6)
	public void addSelectedPublicA() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
	}

	@UITestStep(ordinal = 7)
	public void selectTableAA() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_SELECT_AVAILABLE_TABLE, h2CatalogName + ".A.A");
	}

	@UITestStep(ordinal = 8)
	public void addTableAA() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_SELECTED_ACTION);
	}

	@UITestStep(ordinal = 9)
	public void processTransition() throws Exception
	{
		// advance and return
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@UITestStep(ordinal = 10)
	public void backTransition() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "back");
	}

	@UITestStep(ordinal = 11)
	public void waitForTableLoadAgain() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);
	}

	@UITestStep(ordinal = 12)
	public void addertThatTablesAreInSelectedNotAvailable() throws Exception
	{
		// verify that the two tables are still selected and that the tables are not in the available list
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		QualifiedTableTableModel slm = scdi.getModel(SelectTablesUIComponent.SELECTED_DIMENSIONS_MODEL);
		QualifiedTableTableModel alm = scdi.getModel(SelectTablesUIComponent.AVAILABLE_DIMENSIONS_MODEL);

		Assert.assertTrue(slm.contains(h2CatalogName + ".PUBLIC.A"));
		Assert.assertTrue(slm.contains(h2CatalogName + ".A.A"));
		Assert.assertFalse(alm.contains(h2CatalogName + ".PUBLIC.A"));
		Assert.assertFalse(alm.contains(h2CatalogName + ".A.A"));
	}

	@UITestStep(ordinal = 13)
	public void addAll() throws Exception
	{
		// select all
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_ADD_ALL_ACTION);
	}

	@UITestStep(ordinal = 14)
	public void processTransitionAgain() throws Exception
	{
		// advance and return
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@UITestStep(ordinal = 15)
	public void backTransitionAgain() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "back");
	}

	@UITestStep(ordinal = 16)
	public void waitForTableLoadAndVerifyNoneAvailable() throws Exception
	{
		UIView scdi = UIHub.get().locate(SelectTablesUIComponent.class.getSimpleName());
		scdi.dispatchAction(SelectTablesUIComponent.MSG_WAIT_FOR_TABLE_LOAD);

		QualifiedTableTableModel slm = scdi.getModel(SelectTablesUIComponent.SELECTED_DIMENSIONS_MODEL);
		QualifiedTableTableModel alm = scdi.getModel(SelectTablesUIComponent.AVAILABLE_DIMENSIONS_MODEL);

		Assert.assertFalse(slm.isEmpty());
		Assert.assertTrue(alm.isEmpty());
	}

	@UITestStep(ordinal = 17)
	public void processTransitionAgain2() throws Exception
	{
		UIView uiw = UIHub.get().locate(ImportTableModelWizard.class.getSimpleName());
		uiw.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@Override
	protected void prepareProfile() throws Exception
	{
		DatabaseConnection srco = profile.newH2Connection().name("src").path(temporaryFolder.newFile()).create();
		prepareDatabase(srco);
	}

	private void prepareDatabase(DatabaseConnection srco) throws SQLException
	{
		Connection c = srco.open();

		try
		{
			Statement st = c.createStatement();

			try
			{
				st.execute("CREATE TABLE PUBLIC.I(ID INT);CREATE TABLE PUBLIC.A(ID INT);CREATE TABLE PUBLIC.a1(ID INT);CREATE TABLE PUBLIC.Z(ID INT);CREATE SCHEMA A;CREATE SCHEMA z;CREATE TABLE A.A(ID INT);CREATE TABLE z.Z(ID INT);");
			}
			finally
			{
				st.close();
			}

			// now get the catalog name
			ResultSet rs = c.getMetaData().getCatalogs();

			while (rs.next())
			{
				h2CatalogName = rs.getString(1);
			}

			rs.close();
		}
		finally
		{
			c.close();
		}
	}

	@Override
	protected StarCommanderUIView createComponent()
	{
		return new StarCommanderUIView();
	}
}
