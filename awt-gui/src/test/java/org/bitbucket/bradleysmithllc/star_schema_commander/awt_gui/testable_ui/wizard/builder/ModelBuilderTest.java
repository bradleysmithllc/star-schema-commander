package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ModelBuilderTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void initialRequired()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Initial state is required");

		new WizardModelBuilder().create();
	}

	@Test
	public void initial()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState<?, ?> ws = wizardModelBuilder.newState("", new DummyUIComp()).id("init").create();

		wizardModelBuilder.initial(ws);

		WizardStateModel wm = wizardModelBuilder.create();

		Assert.assertSame(ws, wm.initial());
		Assert.assertNull(wm.handler());
	}

	@Test
	public void handler()
	{
		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();

		WizardState<?, ?> ws = wizardModelBuilder.newState("", new DummyUIComp()).id("init").create();

		wizardModelBuilder.initial(ws);
		WizardStateHandler state = new WizardStateHandler()
		{
			@Override
			public <F, C extends WizardStateUIComponent<F>, T, D extends WizardStateUIComponent<T>> void stateChanged(WizardState<F, C> from, WizardState<T, D> to, WizardStateTransition<F, C, T, D> transition)
			{
			}
		};
		wizardModelBuilder.handler(state);

		WizardStateModel wm = wizardModelBuilder.create();

		Assert.assertSame(state, wm.handler());
	}
}
