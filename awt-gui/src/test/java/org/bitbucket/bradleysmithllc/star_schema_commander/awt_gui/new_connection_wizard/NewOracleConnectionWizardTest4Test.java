package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.new_connection_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.NewConnectionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.OracleConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.SelectConnectionTypeUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.junit.Assert;

public class NewOracleConnectionWizardTest4Test extends NewOracleConnectionWizardBaseTest
{
	@UITestStep
	public void newConnectionServiceName() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionView.class.getSimpleName());
		ac.dispatchActionAndWait(ActionView.MSG_NEW_CONNECTION);

		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_SELECT_CONNECTION_TYPE, "Oracle");

		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "OracleConnection");

		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "oracle");

		UIView newh2 = UIHub.get().locate(OracleConnectionUIComponent.class.getSimpleName());

		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_SERVICE_NAME, "service");
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_USER_NAME, "user");
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_PASSWORD, "password");

		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "confirm");

		profile.invalidate();

		Assert.assertTrue(profile.connections().containsKey("OracleConnection"));

		DatabaseConnection h2 = profile.connections().get("OracleConnection");

		Assert.assertEquals("user", h2.userName());
		Assert.assertEquals("password", h2.password());
		Assert.assertEquals("jdbc:oracle:thin:@//localhost/service", h2.jdbcUrl());
	}
}
