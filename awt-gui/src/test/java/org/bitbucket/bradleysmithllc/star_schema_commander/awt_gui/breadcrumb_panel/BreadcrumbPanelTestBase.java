package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.breadcrumb_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.BreadcrumbPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Ignore;

import java.io.IOException;

/**
 * Created by bsmith on 5/28/15.
 */
@Ignore
public class BreadcrumbPanelTestBase extends AbstractUIComponentTest<BreadcrumbPanel>
{
	@Override
	protected BreadcrumbPanel createComponent()
	{
		return new BreadcrumbPanel();
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);

		// create some nested RAs
		ReportingArea level1 = profile.defaultReportingArea().newReportingArea().name("Level 1").create();
		ReportingArea level2 = level1.newReportingArea().name("Level 2").create();
		ReportingArea level3 = level2.newReportingArea().name("Level 3").create();
	}
}
