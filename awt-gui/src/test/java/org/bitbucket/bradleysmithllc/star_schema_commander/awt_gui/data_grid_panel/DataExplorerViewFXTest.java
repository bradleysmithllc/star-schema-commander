package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_panel;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataExplorerView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainer;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.junit.Assert;
import org.junit.Test;
import org.loadui.testfx.GuiTest;

import java.util.Arrays;
import java.util.List;

/**
 * Created by bsmith on 7/31/15.
 */
public class DataExplorerViewFXTest extends GuiTest
{
	@Test
	public void buttonsDisabledByDefault() throws InterruptedException
	{
		Button button = find("#DataGridHistoryView_Previous");
		Assert.assertNotNull(button);
		Assert.assertTrue(button.isDisabled());

		button = find("#DataGridHistoryView_Next");
		Assert.assertNotNull(button);
		Assert.assertTrue(button.isDisabled());

		final TableView tv = find("#DataExplorerView_Grid");
		Assert.assertNotNull(tv);

		EventUtils.runOnJFXEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				// clear columns
				ObservableList<TableColumn> tvColumns = tv.getColumns();

				ResultSetMetaDataTableView rd = new ResultSetMetaDataTableView("First", "Last");
				rd.prepareColumns(tvColumns);

				ObservableList<Object> aol = FXCollections.observableArrayList();
				aol.add(Arrays.asList("Bradley", "Smith"));
				aol.add(Arrays.asList("Alisa", "Smith"));
				aol.add(Arrays.asList("Amber", "Smith"));
				aol.add(Arrays.asList("Andrew", "Smith"));
				aol.add(Arrays.asList("Leif", "Smith"));
				aol.add(Arrays.asList("Lochlan", "Smith"));

				tv.setItems(aol);
			}
		});

		Thread.sleep(7000L);
	}

	@Override
	protected Parent getRootNode()
	{
		UIHub.init(new TestUIController());

		DataExplorerView dghv = new DataExplorerView();

		UIContainer co = new UIContainerImpl();
		co.addView(dghv);

		return (Parent) dghv.view();
	}
}

class ResultSetMetaDataTableView
{
	private final String[] columns;

	ResultSetMetaDataTableView(String... columns)
	{
		this.columns = columns;
	}

	public void prepareColumns(ObservableList<TableColumn> oColumns)
	{
		for (int colIndex = 0; colIndex < columns.length; colIndex++)
		{
			TableColumn<List<String>, String> col = new TableColumn<List<String>, String>(columns[colIndex]);
			oColumns.add(col);

			final int myColIndex = colIndex;

			col.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<List<String>, String>, ObservableValue<String>>()
			{
				@Override
				public ObservableValue<String> call(TableColumn.CellDataFeatures<List<String>, String> param)
				{
					return new ReadOnlyStringWrapper(param.getValue().get(myColIndex));
				}
			});
		}
	}
}