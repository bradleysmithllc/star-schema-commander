package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.DataGridNavigatorPanelTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.junit.Assert;

public class DataGridNavigatorOptionPanelDAOneDimTest extends DataGridNavigatorOptionPanelDATestBase
{
	protected Fact getFact()
	{
		return profile.defaultReportingArea().reportingAreas().get(DataGridNavigatorPanelTest.class.getSimpleName()).facts().get("factOneDim");
	}

	@UITestStep
	public void measuresAggNoneByDefault() throws Exception
	{
		Assert.assertFalse(uiComponent.checkState(DataGridNavigatorOptionPanel.ST_CONTEXT_COMBO_VISIBLE));
	}
}
