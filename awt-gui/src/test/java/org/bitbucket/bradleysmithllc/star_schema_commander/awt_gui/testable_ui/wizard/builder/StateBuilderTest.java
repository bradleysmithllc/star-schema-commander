package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardState;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Profile;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StateBuilderTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void modelRequired()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Model may not be null");

		new WizardModelBuilder().newState(null, new DummyUIComp()).create();
	}

	@Test
	public void idRequired()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Id may not be null");

		new WizardModelBuilder().newState("hi", new DummyUIComp()).create();
	}

	@Test
	public void duplicateId()
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("State id [id] already used.");

		WizardModelBuilder wizardModelBuilder = new WizardModelBuilder();
		wizardModelBuilder.newState("hi", new DummyUIComp()).id("id").create();
		wizardModelBuilder.newState(new Integer(1), new DummyUIComp()).id("id").create();
	}

	@Test
	public void model()
	{
		WizardState ws = new WizardModelBuilder().newState("hi", new DummyUIComp()).id("blah").create();

		Assert.assertEquals("hi", ws.model());
	}

	@Test
	public void id()
	{
		WizardState ws = new WizardModelBuilder().newState("hi", new DummyUIComp()).id("blah").create();

		Assert.assertEquals("blah", ws.id());
	}

	@Test
	public void componentNull()
	{
		WizardState ws = new WizardModelBuilder().newState("hi", null).id("blah").create();

		Assert.assertNull(ws.component());
	}

	@Test
	public void listener()
	{
		final StringBuilder stb = new StringBuilder();

		WizardState.WizardStateListener<String, WizardStateUIComponent<String>> listener = new WizardState.WizardStateListener<String, WizardStateUIComponent<String>>()
		{
			@Override
			public void stateActivated(WizardState<String, WizardStateUIComponent<String>> state)
			{
				stb.append("|act|");
			}

			@Override
			public void stateDeactivated(WizardState<String, WizardStateUIComponent<String>> state)
			{
				stb.append("|deact|");
			}
		};

		WizardState ws = new WizardModelBuilder().newState("hi", null).id("blah").listener(listener).create();

		Assert.assertEquals("", stb.toString());

		ws.activate();

		Assert.assertEquals("|act|", stb.toString());

		ws.deactivate();

		Assert.assertEquals("|act||deact|", stb.toString());

		ws.activate();

		Assert.assertEquals("|act||deact||act|", stb.toString());

		ws.deactivate();

		Assert.assertEquals("|act||deact||act||deact|", stb.toString());

	}

	@Test
	public void componentNotNull()
	{
		WizardStateUIComponent comp = new WizardStateUIComponent()
		{
			@Override
			protected void buildParent(Profile profile)
			{
			}
		};

		WizardState ws = new WizardModelBuilder().newState("hi", comp).id("blah").create();

		Assert.assertSame(comp, ws.component());
	}
}