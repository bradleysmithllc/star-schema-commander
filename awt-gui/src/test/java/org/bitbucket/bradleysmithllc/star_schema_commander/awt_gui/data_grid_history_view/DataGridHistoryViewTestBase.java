package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_history_view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridHistoryView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractJFXUIViewTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.JsonPersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.junit.Ignore;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

@Ignore
public class DataGridHistoryViewTestBase extends AbstractJFXUIViewTest<DataGridHistoryView> implements DataGridHistoryView.HistoryQueryListener
{
	protected Fact fact;
	protected PersistedQuery lastQuery;
	protected List<PersistedQuery> hist = new ArrayList<PersistedQuery>();

	protected PersistedQuery createQ() throws SQLException
	{
		return new JsonPersistedQuery(fact.newQuery().create(), "name", null);
	}

	@Override
	protected DataGridHistoryView createComponent()
	{
		fact = profile.defaultReportingArea().newFact().schema("PUBLIC").name("FACT").create();

		fact.newDatabaseColumn().name("ID").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("ID2").jdbcType(Types.INTEGER).create();

		fact.newFactMeasure().column("ID").logicalName("Id").create();
		fact.newFactMeasure().column("ID2").logicalName("Id_2").create();

		DataGridHistoryView dataGridHistoryPanel = new DataGridHistoryView();
		dataGridHistoryPanel.setHistoryQueryListener(this);
		return dataGridHistoryPanel;
	}

	@Override
	public void selectQuery(PersistedQuery query)
	{
		lastQuery = query;
	}

	protected PersistedQuery add(PersistedQuery q)
	{
		hist.add(q);
		return q;
	}
}
