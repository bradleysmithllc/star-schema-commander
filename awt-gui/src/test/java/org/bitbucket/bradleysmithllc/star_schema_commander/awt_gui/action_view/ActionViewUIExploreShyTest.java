package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.action_view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UIComponentTestRunner;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.junit.Assert;
import org.junit.runner.RunWith;

@RunWith(
	value = UIComponentTestRunner.class
)
public class ActionViewUIExploreShyTest extends ActionViewUITestBase
{
	private DatabaseConnection selectedConnection;
	private Fact fact;

	@UITestStep(ordinal = 0)
	public void setupTest() throws Exception
	{
		selectedConnection = profile.connections().get("0");
		fact = profile.defaultReportingArea().facts().values().iterator().next();
	}

	@UITestStep(ordinal = 1)
	public void selectConnection() throws Exception
	{
		UIHub.selectConnection(selectedConnection);
		Thread.sleep(500L);
		Assert.assertFalse(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}

	@UITestStep(ordinal = 2)
	public void unselectConnection() throws Exception
	{
		UIHub.selectConnection(null);
		Thread.sleep(500L);
		Assert.assertFalse(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}

	@UITestStep(ordinal = 3)
	public void selectAgain() throws Exception
	{
		UIHub.selectConnection(selectedConnection);
		Thread.sleep(500L);
		Assert.assertFalse(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}

	@UITestStep(ordinal = 4)
	public void selectFact() throws Exception
	{
		UIHub.selectFact(fact);
		Thread.sleep(500L);
		Assert.assertTrue(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}

	@UITestStep(ordinal = 5)
	public void unselectFact() throws Exception
	{
		UIHub.selectFact(null);
		Thread.sleep(500L);
		Assert.assertFalse(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}

	@UITestStep(ordinal = 6)
	public void selectFactAgain() throws Exception
	{
		UIHub.selectFact(fact);
		Thread.sleep(500L);
		Assert.assertTrue(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}

	@UITestStep(ordinal = 7)
	public void unselectConnectionAFinalTime() throws Exception
	{
		UIHub.selectConnection(null);
		Thread.sleep(500L);
		Assert.assertFalse(uiComponent.checkState(ActionView.STATE_EXPLORE_VISIBLE));
	}
}
