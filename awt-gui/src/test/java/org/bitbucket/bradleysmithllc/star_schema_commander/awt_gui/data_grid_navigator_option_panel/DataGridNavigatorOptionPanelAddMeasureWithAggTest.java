package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigator;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

public class DataGridNavigatorOptionPanelAddMeasureWithAggTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void addMax() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, Query.aggregation.max);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_ALL);
	}

	@UITestStep(ordinal = 1)
	public void addMin() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, Query.aggregation.min);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_ALL);
	}

	@UITestStep(ordinal = 2)
	public void verify() throws Exception
	{
		// verify that all measures are selected by default
		List<DataGridNavigator.IncludedMeasure> fmMeasures = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_SELECTED_MEASURES);

		Assert.assertEquals(9, fmMeasures.size());

		// verify order
		verify(fmMeasures.get(0), "measure1", "max_measure1", Query.aggregation.max);
		verify(fmMeasures.get(1), "measure1", "measure1", Query.aggregation.none);
		verify(fmMeasures.get(2), "measure1", "min_measure1", Query.aggregation.min);

		verify(fmMeasures.get(3), "Measure2", "max_Measure2", Query.aggregation.max);
		verify(fmMeasures.get(4), "Measure2", "Measure2", Query.aggregation.none);
		verify(fmMeasures.get(5), "Measure2", "min_Measure2", Query.aggregation.min);

		verify(fmMeasures.get(6), "measure3", "max_measure3", Query.aggregation.max);
		verify(fmMeasures.get(7), "measure3", "measure3", Query.aggregation.none);
		verify(fmMeasures.get(8), "measure3", "min_measure3", Query.aggregation.min);
	}
}
