package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DimensionAttributePanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.junit.Assert;

import javax.swing.*;
import java.sql.Types;

public class DimensionAttributeTest extends AbstractUIComponentTest<DimensionAttributePanel>
{
	@UITestStep(ordinal = 0)
	public void selectDimensionShowAttributes() throws Exception
	{
		Dimension dim = profile.defaultReportingArea().newDimension().schema("dbo").name("Fact").create();
		dim.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		dim.newDimensionAttribute().column("col1").logicalName("Col 1").create();
		dim.newDimensionAttribute().column("col1").logicalName("Col 2").create();
		dim.newDimensionAttribute().column("col1").logicalName("Col 3").create();
		dim.newDimensionAttribute().column("col1").logicalName("Col 4").create();

		DefaultListModel<DimensionAttribute> model = uiComponent.getModel(DimensionAttributePanel.DIMENSION_ATTRIBUTE_PANEL_ATTRIBUTE_LIST);

		Assert.assertTrue(model.isEmpty());

		// select a fact
		UIHub.selectDimension(dim);

		Assert.assertFalse(model.isEmpty());

		Assert.assertEquals(4, model.size());

		Assert.assertEquals("Col 1", model.get(0).logicalName());
		Assert.assertEquals("Col 2", model.get(1).logicalName());
		Assert.assertEquals("Col 3", model.get(2).logicalName());
		Assert.assertEquals("Col 4", model.get(3).logicalName());

		// switch reporting area. Must blank panel
		UIHub.selectReportingArea(profile.defaultReportingArea().newReportingArea().name("new").create());
		Assert.assertTrue(model.isEmpty());
	}

	@Override
	protected DimensionAttributePanel createComponent()
	{
		return new DimensionAttributePanel();
	}
}