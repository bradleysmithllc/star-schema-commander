package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.breadcrumb_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.BreadcrumbPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.junit.Assert;

import java.io.IOException;

public class BreadcrumbPanelVisibleAfterRACreatedTest extends BreadcrumbPanelTestBase
{
	@Override
	protected void prepareProfile() throws IOException
	{
	}

	@UITestStep(ordinal = 0)
	public void visibleAfterRACreated() throws Exception
	{
		Assert.assertFalse(uiComponent.checkState(BreadcrumbPanel.STATE_BREADCRUMB_VISIBLE));

		profile.defaultReportingArea().newReportingArea().name("ra").create();

		UIHub.profileRefresh();

		Assert.assertTrue(uiComponent.checkState(BreadcrumbPanel.STATE_BREADCRUMB_VISIBLE));
	}
}
