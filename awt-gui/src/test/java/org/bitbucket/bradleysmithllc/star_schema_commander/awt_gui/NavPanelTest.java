package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.NavPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;
import java.io.IOException;
import java.util.Map;

public class NavPanelTest extends UIComponentTest<NavPanel>
{
	@Test
	public void reportingAreaLabelVisible() throws InterruptedException
	{
		Assert.assertTrue(uiComponent.checkState(NavPanel.NAV_PANEL_REPORTING_AREAS_VISIBLE));
	}

	@Test
	public void emptyConnections() throws Exception
	{
		for (Map.Entry<String, DatabaseConnection> conn : profile.connections().entrySet())
		{
			conn.getValue().remove();
		}

		profile.persist();
		UIHub.profile(profile);

		DefaultListModel<Fact> model = uiComponent.getModel(UIModels.NAV_PANEL_CONNECTION_LIST);
		Assert.assertTrue(model.isEmpty());
	}

	@Test
	public void connectionsAlphaOrder() throws Exception
	{
		DefaultListModel<DatabaseConnection> model = uiComponent.getModel(UIModels.NAV_PANEL_CONNECTION_LIST);
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(6, model.size());

		Assert.assertEquals("0", model.get(0).name());
		Assert.assertEquals("A", model.get(1).name());
		Assert.assertEquals("BootyBooty", model.get(2).name());
		Assert.assertEquals("Z", model.get(3).name());
		Assert.assertEquals("a", model.get(4).name());
		Assert.assertEquals("z", model.get(5).name());
	}

	@Test
	public void selectConnection() throws Exception
	{
		DefaultListModel<DatabaseConnection> model = uiComponent.getModel(UIModels.NAV_PANEL_CONNECTION_LIST);

		DatabaseConnection databaseConnection = profile.connections().get("BootyBooty");
		uiComponent.dispatchAction(NavPanel.MSG_SELECT_CONNECTION, databaseConnection);

		Assert.assertSame(databaseConnection, UIHub.selectedConnection());

		databaseConnection = profile.connections().get("0");
		uiComponent.dispatchAction(NavPanel.MSG_SELECT_CONNECTION, databaseConnection);

		Assert.assertSame(databaseConnection, UIHub.selectedConnection());
	}

	@Test
	public void connectionsUpdate() throws Exception
	{
		DefaultListModel<DatabaseConnection> model = uiComponent.getModel(UIModels.NAV_PANEL_CONNECTION_LIST);
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(6, model.size());

		profile.newH2Connection().name("newH2").path(temporaryFolder.newFile()).create();

		UIHub.profileRefresh();

		Assert.assertEquals(7, model.size());
	}

	@Override
	protected NavPanel createComponent()
	{
		return new NavPanel();
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}
}
