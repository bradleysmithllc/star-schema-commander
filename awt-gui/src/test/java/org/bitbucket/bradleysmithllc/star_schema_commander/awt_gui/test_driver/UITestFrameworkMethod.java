package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.EventUtils;
import org.junit.internal.runners.model.ReflectiveCallable;
import org.junit.runners.model.FrameworkMethod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class UITestFrameworkMethod extends FrameworkMethod
{
	private final List<Method> methodList;
	private final List<Method> beforeStepMethods = new ArrayList<Method>();
	private final List<Method> afterStepMethods = new ArrayList<Method>();
	private final interactor ia = new interactor(UIComponentTestRunner.INTERACTIVE);

	/**
	 * Returns a new {@code FrameworkMethod} for {@code method}
	 *
	 * @param methodList
	 */
	public UITestFrameworkMethod(Class testing, List<Method> methodList)
	{
		super(methodList.get(0));

		this.methodList = methodList;

		// pull out before / after test methods
		for (Method method : testing.getMethods())
		{
			UITestBeforeStep bannotationsByType = method.getAnnotation(UITestBeforeStep.class);
			if (bannotationsByType != null)
			{
				beforeStepMethods.add(method);
			}

			UITestAfterStep aannotationsByType = method.getAnnotation(UITestAfterStep.class);
			if (aannotationsByType != null)
			{
				afterStepMethods.add(method);
			}
		}
	}

	@Override
	public Object invokeExplosively(final Object target, final Object... params) throws Throwable
	{
		return new ReflectiveCallable()
		{
			@Override
			protected Object runReflectiveCall() throws Throwable
			{
				if (UIComponentTestRunner.PAUSE_BETWEEN_STEPS != UIComponentTestRunner.NO_PAUSE)
				{
					ia.pause();
					Thread.sleep(UIComponentTestRunner.PAUSE_BETWEEN_STEPS);
				}

				for (final Method step : methodList)
				{
					ia.startTest(step);

					ia.before();
					// before each step, invoke any UITestBeforeStep methods
					for (Method beforeMethod : beforeStepMethods)
					{
						beforeMethod.invoke(target);
					}

					ia.during();

					step.invoke(target, params);

					ia.after();

					// after each step, invoke any UITestAfterStep methods
					for (Method beforeMethod : afterStepMethods)
					{
						beforeMethod.invoke(target);
					}

					if (UIComponentTestRunner.PAUSE_BETWEEN_STEPS != UIComponentTestRunner.NO_PAUSE)
					{
						ia.pause();
						Thread.sleep(UIComponentTestRunner.PAUSE_BETWEEN_STEPS);
					}
				}

				if (UIComponentTestRunner.PAUSE_WHEN_DONE != UIComponentTestRunner.NO_PAUSE)
				{
					Thread.sleep(UIComponentTestRunner.PAUSE_WHEN_DONE);
				}

				ia.dispose();
				return null;
			}
		}.run();
	}
}

class interactor
{
	JFrame status;
	JLabel stepLabel;
	JButton next;
	Semaphore semaphore = new Semaphore(1);
	String testName;

	interactor(boolean interactive)
	{
		if (!interactive)
		{
			return;
		}

		// pop up a status window
		try
		{
			EventUtils.runOnEventThread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						status = new JFrame();
						status.setTitle("Interactor");
						status.setAlwaysOnTop(true);

						status.getContentPane().setLayout(new BorderLayout());
						status.getContentPane().add(new JLabel(getClass().getSimpleName()), BorderLayout.NORTH);

						stepLabel = new JLabel("Initializing . . .");

						status.getContentPane().add(stepLabel, BorderLayout.CENTER);

						status.setMinimumSize(new Dimension(200, 50));

						next = new JButton("Next");
						next.addActionListener(new ActionListener()
						{
							@Override
							public void actionPerformed(ActionEvent e)
							{
								next.setForeground(Color.black);
								semaphore.release();
							}
						});

						status.getContentPane().add(next, BorderLayout.EAST);

						status.pack();
						status.setLocationRelativeTo(null);
						status.setVisible(true);
					}
					catch (Exception exc)
					{
						throw new RuntimeException(exc);
					}
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void startTest(final Method step) throws Exception
	{
		if (status == null)
		{
			return;
		}

		testName = step.getName();

		semaphore.drainPermits();
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				next.setForeground(Color.red);
				stepLabel.setForeground(Color.black);
				stepLabel.setText(testName);
			}
		});
		semaphore.acquire();
	}

	public void before() throws Exception
	{
		if (status == null)
		{
			return;
		}

		semaphore.drainPermits();
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				next.setForeground(Color.red);
				stepLabel.setText(testName + "-->BeforeTest");
				stepLabel.setForeground(Color.yellow);
			}
		});
		semaphore.acquire();
	}

	public void during() throws Exception
	{
		if (status == null)
		{
			return;
		}

		semaphore.drainPermits();
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				next.setForeground(Color.red);
				stepLabel.setText(testName);
				stepLabel.setForeground(Color.green);
			}
		});
		semaphore.acquire();
	}

	public void after() throws Exception
	{
		if (status == null)
		{
			return;
		}

		semaphore.drainPermits();
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				next.setForeground(Color.red);
				stepLabel.setText(testName + "-->AfterTest");
				stepLabel.setForeground(Color.magenta);
			}
		});
		semaphore.acquire();
	}

	public void pause() throws Exception
	{
		if (status == null)
		{
			return;
		}

		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				next.setForeground(Color.red);
				stepLabel.setText(testName + ". . .");
				stepLabel.setForeground(Color.blue);
			}
		});
		semaphore.acquire();
	}

	public void dispose() throws Exception
	{
		if (status == null)
		{
			return;
		}

		semaphore.drainPermits();
		EventUtils.runOnEventThread(new Runnable()
		{
			@Override
			public void run()
			{
				status.dispose();
			}
		});
	}
}