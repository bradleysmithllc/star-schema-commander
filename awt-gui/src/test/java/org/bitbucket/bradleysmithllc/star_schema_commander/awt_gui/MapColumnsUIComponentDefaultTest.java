package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateTransition;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.WizardStateUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardStateImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardStateModelImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.builder.WizardStateTransitionBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.FactDimensionKeyRecord;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.MapColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class MapColumnsUIComponentDefaultTest extends UIComponentTest<MapColumnsUIComponent>
{
	private Fact fact;
	private DimensionKey dimensionKey;
	private Dimension dimension;

	@Test
	public void notRolePlaying() throws Exception
	{
		activate("fact", "Dim1", "Logical", true);

		// verify role playing option is invisible
		Assert.assertTrue(uiComponent.checkState(MapColumnsUIComponent.STATE_ROLE_NAME_VISIBLE));
	}

	private void activate(String factName, String dimensionName, String dimensionKeyName, boolean shouldActivate)
	{
		ReportingArea reportingArea = profile.defaultReportingArea().reportingAreas().get(getClass().getSimpleName());

		fact = reportingArea.facts().get(factName);

		uiComponent.state().model().fact(fact);
		dimension = reportingArea.dimensions().get(dimensionName);
		dimensionKey = dimension.keys().get(dimensionKeyName);
		uiComponent.state().model().dimensionKey(dimensionKey);

		if (shouldActivate)
		{
			uiComponent.activate();
		}
	}

	@Test
	public void rolePlaying() throws Exception
	{
		activate("factRole", "Dim1", "Logical", true);

		// verify role playing option is visible
		Assert.assertTrue(uiComponent.checkState(MapColumnsUIComponent.STATE_ROLE_NAME_VISIBLE));
	}

	@Test
	public void combinedValidation() throws Exception
	{
		activate("factRole", "Dim1", "Logical", true);

		WizardStateTransition<FactDimensionKeyRecord, ? extends WizardStateUIComponent<FactDimensionKeyRecord>, ?, ?> transition = uiComponent.state().transitionMap().get("next");

		// map the key columns so that part is complete
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(0), fact.columns().get("col1")));
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(1), fact.columns().get("col2")));
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(2), fact.columns().get("col3")));
		Assert.assertTrue(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, "  ");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "R");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, "L");
		Assert.assertTrue(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "");
		Assert.assertFalse(transition.enabled());
	}

	@Test
	public void rolePlayingValidation() throws Exception
	{
		activate("factRole", "Dim1", "Logical", true);

		WizardStateTransition<FactDimensionKeyRecord, ? extends WizardStateUIComponent<FactDimensionKeyRecord>, ?, ?> transition = uiComponent.state().transitionMap().get("next");

		// map the key columns so that part is complete
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(0), fact.columns().get("col1")));
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(1), fact.columns().get("col2")));
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(2), fact.columns().get("col3")));
		Assert.assertTrue(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "  ");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "\t");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_ROLE_NAME, "H");
		Assert.assertTrue(transition.enabled());
	}

	@Test
	public void physicalValidation() throws Exception
	{
		activate("fact", "Dim1", "Logical", true);

		WizardStateTransition<FactDimensionKeyRecord, ? extends WizardStateUIComponent<FactDimensionKeyRecord>, ?, ?> transition = uiComponent.state().transitionMap().get("next");

		// map the key columns so that part is complete
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(0), fact.columns().get("col1")));
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(1), fact.columns().get("col2")));
		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_MAP_COLUMN, new MutablePair<DatabaseColumn, DatabaseColumn>(dimensionKey.columns().get(2), fact.columns().get("col3")));
		Assert.assertTrue(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, "");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, "  ");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, "\t");
		Assert.assertFalse(transition.enabled());

		uiComponent.dispatchAction(MapColumnsUIComponent.MSG_SET_PHYSICAL_NAME, "H");
		Assert.assertTrue(transition.enabled());
	}

	@Override
	protected final MapColumnsUIComponent createComponent()
	{
		MapColumnsUIComponent mapColumnsUIComponent = new MapColumnsUIComponent();

		WizardStateImpl<FactDimensionKeyRecord, MapColumnsUIComponent> state = new WizardStateImpl<
			FactDimensionKeyRecord,
			MapColumnsUIComponent
			>(
			new FactDimensionKeyRecord(null, null), "test", null,
			null);

		new WizardStateTransitionBuilder(state, state, new WizardStateModelImpl()).id("next").create();

		mapColumnsUIComponent.state(state);

		return mapColumnsUIComponent;
	}

	@Override
	protected final void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}
}
