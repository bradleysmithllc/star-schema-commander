package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

/**
 * Since all measures are selected and added as 'none' by default, the agg list at the bottom
 * must always exclude none when any are selected, but show it when they are not.
 */
public class DataGridNavigatorOptionPanelMAggAddAggListTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void noneIsAvailable() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertTrue(model.contains(Query.aggregation.none));
	}

	@UITestStep(ordinal = 1)
	public void selectARow() throws Exception
	{
		Map<String, FactMeasure> measures = this.value.measures();
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, measures.get("Measure2"));
	}

	@UITestStep(ordinal = 2)
	public void selectAggAndAdd() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 3)
	public void selectAggAndAdd2() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 4)
	public void selectAggAndAdd3() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 5)
	public void selectAggAndAdd4() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 6)
	public void selectAggAndAdd5() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 7)
	public void selectAggAndAdd6() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 8)
	public void selectAggAndAdd7() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);

		model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Assert.assertFalse(model.contains(aggregation));
	}

	@UITestStep(ordinal = 9)
	public void selectAggAndAdd8() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);

		Query.aggregation aggregation = model.get(0);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_AGGREGATION, aggregation);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);
	}

	@UITestStep(ordinal = 10)
	public void measureNoLongerAvailable() throws Exception
	{
		List<FactMeasure> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_MEASURES);

		Assert.assertEquals(2, model.size());
	}
}
