package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigator;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

public class DataGridNavigatorOptionPanelNullAggRemovesMeasureTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void selectMeasures() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
	}

	@UITestStep(ordinal = 1)
	public void add() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_ALL);
	}

	@UITestStep(ordinal = 10)
	public void nullAggRemovesMeasure() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ASSIGN_AGG_TO_INCLUDED, new ImmutablePair<Integer, Query.aggregation>(1, null));

		List<DataGridNavigator.IncludedMeasure> fmMeasures = uiComponent.selectedMeasures();

		Assert.assertEquals(5, fmMeasures.size());

		// verify order
		Assert.assertEquals("measure1", fmMeasures.get(0).measure().logicalName());
		Assert.assertEquals("Measure2", fmMeasures.get(1).measure().logicalName());
		Assert.assertEquals("Measure2", fmMeasures.get(2).measure().logicalName());
		Assert.assertEquals("measure3", fmMeasures.get(3).measure().logicalName());
		Assert.assertEquals("measure3", fmMeasures.get(4).measure().logicalName());
	}
}
