package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.UIModels;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import javax.swing.*;

public class DimensionPanelDimsAlphaOrderTest extends DimensionPanelTestBase
{
	@UITestStep
	public void displaySelectedReportingAreaDims() throws Exception
	{
		DefaultListModel<Dimension> model = uiComponent.getModel(UIModels.DIMENSION_PANEL_DIMENSION_LIST);

		// select a reporting area
		ReportingArea alpha = profile.defaultReportingArea().reportingAreas().get("Alpha");

		UIHub.selectReportingArea(alpha);
		Assert.assertFalse(model.isEmpty());
		Assert.assertEquals(12, model.size());

		Assert.assertSame(alpha.dimensions().get("0bi_0Dimension"), model.get(0));
		Assert.assertSame(alpha.dimensions().get("A_AAAAAAAAAAAAAAAAAAAA"), model.get(1));
		Assert.assertSame(alpha.dimensions().get("abi_aDimension"), model.get(2));
		Assert.assertSame(alpha.dimensions().get("bi_0Dimension"), model.get(3));
		Assert.assertSame(alpha.dimensions().get("bi_aDimension"), model.get(4));
		Assert.assertSame(alpha.dimensions().get("bi_ADimension"), model.get(5));
		Assert.assertSame(alpha.dimensions().get("bi_xDimension"), model.get(6));
		Assert.assertSame(alpha.dimensions().get("bi_XDimension"), model.get(7));
		Assert.assertSame(alpha.dimensions().get("Xbi_ADimension"), model.get(8));
		Assert.assertSame(alpha.dimensions().get("xbi_xDimension"), model.get(9));
		Assert.assertSame(alpha.dimensions().get("ybi_XDimension"), model.get(10));
		Assert.assertSame(alpha.dimensions().get("Z_Z"), model.get(11));
	}
}