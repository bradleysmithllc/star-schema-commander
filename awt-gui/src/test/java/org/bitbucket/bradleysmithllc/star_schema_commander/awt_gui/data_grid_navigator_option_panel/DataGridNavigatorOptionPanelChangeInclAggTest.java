package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

/**
 * Select a mesaure and add at 'none' agg level, then change to 'sum' and
 * verify that sum is no longer an option.
 */
public class DataGridNavigatorOptionPanelChangeInclAggTest extends DataGridNavigatorOptionPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void noneIsAvailable() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);
		Assert.assertTrue(model.contains(Query.aggregation.none));
	}

	@UITestStep(ordinal = 1)
	public void selectMeasure2() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, value.measures().get("Measure2"));
	}

	@UITestStep(ordinal = 2)
	public void addToIncluded() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ADD_AVAILABLE_SELECTED);
	}

	@UITestStep(ordinal = 3)
	public void changeIncludedAggLevelToSum() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ASSIGN_AGG_TO_INCLUDED, new MutablePair<Integer, Query.aggregation>(0, Query.aggregation.sum));
	}

	@UITestStep(ordinal = 4)
	public void selectMeasure2Available() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, value.measures().get("Measure2"));
	}

	@UITestStep(ordinal = 5)
	public void sumIsNotAvailable() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);
		Assert.assertFalse(model.contains(Query.aggregation.sum));
	}

	@UITestStep(ordinal = 6)
	public void selectMeasure3Available() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, value.measures().get("measure3"));
	}

	@UITestStep(ordinal = 7)
	public void sumIsAvailable() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);
		Assert.assertTrue(model.contains(Query.aggregation.sum));
	}

	@UITestStep(ordinal = 8)
	public void selectAllAvailable() throws Exception
	{
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_ALL_AVAILABLE);
	}

	@UITestStep(ordinal = 9)
	public void sumIsNotAvailableAgain() throws Exception
	{
		List<Query.aggregation> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);
		Assert.assertFalse(model.contains(Query.aggregation.sum));
	}
}
