package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.breadcrumb_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.BreadcrumbPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.junit.Assert;

public class BreadcrumbPanelDisplaysSelectedRATest extends BreadcrumbPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void displaysSelectedRA() throws Exception
	{
		Assert.assertTrue(uiComponent.checkState(BreadcrumbPanel.STATE_BREADCRUMB_VISIBLE));

		UIHub.selectReportingArea(profile.defaultReportingArea());
		Assert.assertEquals("[default]", uiComponent.getModel(BreadcrumbPanel.BREADCRUMB_PANEL_GRAPH));

		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("Alpha"));
		Assert.assertEquals("[default].Alpha", uiComponent.getModel(BreadcrumbPanel.BREADCRUMB_PANEL_GRAPH));

		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("BI"));
		Assert.assertEquals("[default].BI", uiComponent.getModel(BreadcrumbPanel.BREADCRUMB_PANEL_GRAPH));

		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("Level 1.Level 2"));
		Assert.assertEquals("[default].Level 1.Level 2", uiComponent.getModel(BreadcrumbPanel.BREADCRUMB_PANEL_GRAPH));

		UIHub.selectReportingArea(profile.defaultReportingArea().reportingAreas().get("Level 1.Level 2.Level 3"));
		Assert.assertEquals("[default].Level 1.Level 2.Level 3", uiComponent.getModel(BreadcrumbPanel.BREADCRUMB_PANEL_GRAPH));
	}
}
