package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_navigator_option_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.MutablePair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridNavigatorOptionPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.junit.Assert;

import java.util.List;

/**
 * Since all measures are selected and added as 'none' by default, the agg list at the bottom
 * must always exclude none when any are selected, but show it when they are not.
 */
public class DataGridNavigatorOptionPanelMAggNoneAggListTest2 extends DataGridNavigatorOptionPanelMAggAddAggListTest
{
	@UITestStep(ordinal = 1000)
	public void removeFromIncluded() throws Exception
	{
		MutablePair<Integer, Query.aggregation> pair = new MutablePair<Integer, Query.aggregation>();
		pair.setLeft(1);
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_ASSIGN_AGG_TO_INCLUDED, pair);
	}

	@UITestStep(ordinal = 1001)
	public void measure2AgainAvailable()
	{
		// verify that measure 2 is back on the available list
		List<FactMeasure> model = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_MEASURES);
		Assert.assertTrue(model.contains(value.measures().get("Measure2")));
	}

	@UITestStep(ordinal = 1002)
	public void selectMeasure2()
	{
		// verify that measure 2 is back on the available list
		uiComponent.dispatchAction(DataGridNavigatorOptionPanel.MSG_SELECT_MEASURE, value.measures().get("Measure2"));
	}

	@UITestStep(ordinal = 1003)
	public void onlyAferageDistinctOption()
	{
		// verify that measure 2 is back on the available list
		List<Query.aggregation> aggs = uiComponent.getModel(DataGridNavigatorOptionPanel.MOD_AVAILABLE_AGGREGATES);
		Assert.assertEquals(1, aggs.size());
		Assert.assertEquals(aggs.get(0), Query.aggregation.average_distinct);
	}
}
