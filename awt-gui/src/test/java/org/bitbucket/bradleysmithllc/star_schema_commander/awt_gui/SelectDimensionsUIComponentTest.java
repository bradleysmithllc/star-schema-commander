package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SelectDimensionsUIComponentTest extends SelectDimensionsUIComponentDefaultTest
{
	@Test
	public void selectRa() throws Exception
	{
		Assert.assertSame(profile.defaultReportingArea().reportingAreas().get("test"), uiComponent.getModel(SelectDimensionsUIComponent.MOD_EFFECTIVE_REPORTING_AREA));
	}

	@Test
	public void emptyDimensions() throws Exception
	{
		Assert.assertEquals(0, ((List) uiComponent.getModel(SelectDimensionsUIComponent.MOD_SELECTED_DIMENSIONS)).size());
	}

	@Override
	protected void prepare()
	{
		ReportingArea ra = profile.defaultReportingArea().newReportingArea().name("test").create();

		UIHub.selectReportingArea(ra);
	}
}
