package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util.JTableUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.RowData;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class JTableUtilsTest
{
	@Test
	public void emptyInput()
	{
		Assert.assertEquals(0, JTableUtils.getColumnWidths(Collections.<RowData>emptyList()).size());
	}

	@Test
	public void rows()
	{
		Assert.assertEquals(0, JTableUtils.getColumnWidths(Arrays.asList(new RowData(0), new RowData(0), new RowData(0), new RowData(0))).size());
	}

	@Test
	public void columns()
	{
		ArrayList<RowData> rowDataList = new ArrayList<RowData>();
		RowData rowData = new RowData(0);
		rowData.add("");
		rowData.add("1");
		rowData.add("");
		rowData.add("");

		rowDataList.add(rowData);

		List<AtomicInteger> columnWidths = JTableUtils.getColumnWidths(rowDataList);

		Assert.assertEquals(4, columnWidths.size());

		Assert.assertEquals(0, columnWidths.get(0).get());
		Assert.assertEquals(1, columnWidths.get(1).get());
		Assert.assertEquals(0, columnWidths.get(2).get());
		Assert.assertEquals(0, columnWidths.get(3).get());
	}

	@Test
	public void manyColumns()
	{
		ArrayList<RowData> rowDataList = new ArrayList<RowData>();

		RowData rowData = new RowData(0);
		rowData.add("");
		rowData.add("1");
		rowData.add("");
		rowData.add("11");
		rowDataList.add(rowData);

		rowData = new RowData(0);
		rowData.add("111");
		rowData.add("11");
		rowData.add("1111");
		rowData.add("1");
		rowDataList.add(rowData);

		List<AtomicInteger> columnWidths = JTableUtils.getColumnWidths(rowDataList);

		Assert.assertEquals(4, columnWidths.size());

		Assert.assertEquals(3, columnWidths.get(0).get());
		Assert.assertEquals(2, columnWidths.get(1).get());
		Assert.assertEquals(4, columnWidths.get(2).get());
		Assert.assertEquals(2, columnWidths.get(3).get());
	}

	@Test
	public void export()
	{
		ArrayList<RowData> rowDataList = new ArrayList<RowData>();

		RowData rowData = new RowData(0);
		rowData.add("");
		rowData.add("1");
		rowData.add("");
		rowData.add("11");
		rowDataList.add(rowData);

		rowData = new RowData(0);
		rowData.add("111");
		rowData.add("11");
		rowData.add("1111");
		rowData.add("1");
		rowDataList.add(rowData);

		System.out.println(JTableUtils.exportResultsToText(rowDataList, Arrays.asList("ID1", "COLUMN2", "COL3", "C4")));
	}
}