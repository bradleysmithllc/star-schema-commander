package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.FactDimensionKeysModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectDimensionsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class FactKeyWizardShowDimsWithKeysTest extends FactKeyWizardTestBase
{
	@Override
	protected FactDimensionKeysModelWizard createComponent()
	{
		ReportingArea fkw = profile.defaultReportingArea().reportingAreas().get(FactKeyWizardLongPathTest.class.getSimpleName());

		UIHub.selectReportingArea(fkw);

		return new FactDimensionKeysModelWizard(fkw.facts().get("fact"));
	}

	@UITestStep
	public void showDimsWithKeys() throws Exception
	{
		UIView comp = uiComponent.locate(SelectDimensionsUIComponent.class.getSimpleName());

		Map<String, Dimension> dims = UIHub.selectedReportingArea().dimensions();

		List<Dimension> model = comp.getModel(SelectDimensionsUIComponent.MOD_AVAILABLE_DIMENSIONS);

		for (Map.Entry<String, Dimension> entry : dims.entrySet())
		{
			Dimension dimension = entry.getValue();

			if (dimension.keys().size() != 0)
			{
				Assert.assertTrue(model.contains(dimension));
			}
			else
			{
				Assert.assertFalse(model.contains(dimension));
			}
		}
	}
}