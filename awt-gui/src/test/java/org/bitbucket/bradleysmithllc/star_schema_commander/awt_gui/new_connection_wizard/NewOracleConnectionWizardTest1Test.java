package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.new_connection_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.TransitionStates;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.NewConnectionModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.OracleConnectionUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.new_connection.SelectConnectionTypeUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.junit.Assert;

public class NewOracleConnectionWizardTest1Test extends NewOracleConnectionWizardBaseTest
{
	@UITestStep
	public void newConnection() throws Exception
	{
		UIView ac = UIHub.get().locate(ActionView.class.getSimpleName());
		ac.dispatchActionAndWait(ActionView.MSG_NEW_CONNECTION);

		UIView scui = UIHub.get().locate(SelectConnectionTypeUIComponent.class.getSimpleName());
		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_SELECT_CONNECTION_TYPE, "Oracle");

		scui.dispatchActionAndWait(SelectConnectionTypeUIComponent.MSG_CONNECTION_NAME, "OracleConnection");

		UIView wizard = UIHub.get().locate(NewConnectionModelWizard.class.getSimpleName());
		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "oracle");

		UIView newh2 = UIHub.get().locate(OracleConnectionUIComponent.class.getSimpleName());

		Assert.assertFalse(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));

		// verify required states
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_USER_NAME, "user2");
		Assert.assertFalse(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_PASSWORD, "password2");
		Assert.assertTrue(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_USER_NAME, "");
		Assert.assertFalse(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_USER_NAME, "username");
		Assert.assertTrue(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_PASSWORD, "");
		Assert.assertFalse(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));
		newh2.dispatchActionAndWait(OracleConnectionUIComponent.MSG_SET_PASSWORD, "password1");
		Assert.assertTrue(((TransitionStates) wizard.getModel(UIWizard.MODEL_TRANSITION_STATES)).transitionEnabled("confirm"));

		wizard.dispatchActionAndWait(UIWizard.MSG_ACTIVATE_TRANSITION, "confirm");

		profile.invalidate();

		Assert.assertTrue(profile.connections().containsKey("OracleConnection"));

		DatabaseConnection h2 = profile.connections().get("OracleConnection");

		Assert.assertEquals("username", h2.userName());
		Assert.assertEquals("password1", h2.password());
		Assert.assertEquals("jdbc:oracle:thin:@//localhost", h2.jdbcUrl());
	}
}
