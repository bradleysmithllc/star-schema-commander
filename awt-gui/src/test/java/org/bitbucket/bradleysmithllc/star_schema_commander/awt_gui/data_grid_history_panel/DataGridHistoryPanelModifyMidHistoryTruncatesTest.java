package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_history_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridHistoryPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.junit.Assert;

import java.util.List;

/**
 * This test is to assert that once history is established, going back in history
 * and then adding new queries will truncate the rest of the history and
 * add the new mid stream.
 */
public class DataGridHistoryPanelModifyMidHistoryTruncatesTest extends DataGridHistoryPanelTestBase
{
	@UITestStep(ordinal = 0)
	public void addQueries() throws Exception
	{
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 1)
	public void movePointerBackABit() throws Exception
	{
		Assert.assertFalse(uiComponent.checkState(DataGridHistoryPanel.STATE_NEXT_ENABLED));
		Assert.assertTrue(uiComponent.checkState(DataGridHistoryPanel.STATE_PREVIOUS_ENABLED));
		uiComponent.dispatchAction(DataGridHistoryPanel.MSG_PREVIOUS);
		uiComponent.dispatchAction(DataGridHistoryPanel.MSG_PREVIOUS);
		uiComponent.dispatchAction(DataGridHistoryPanel.MSG_PREVIOUS);
		Assert.assertTrue(uiComponent.checkState(DataGridHistoryPanel.STATE_NEXT_ENABLED));
	}

	@UITestStep(ordinal = 2)
	public void addAnotherQuery() throws Exception
	{
		// truncate local list to match what the history should do
		hist.remove(hist.size() - 1);
		hist.remove(hist.size() - 1);
		hist.remove(hist.size() - 1);

		uiComponent.addQuery(add(createQ()));
	}

	@UITestStep(ordinal = 3)
	public void noMoreNext() throws Exception
	{
		Assert.assertFalse(uiComponent.checkState(DataGridHistoryPanel.STATE_NEXT_ENABLED));
	}

	@UITestStep(ordinal = 4)
	public void verify() throws Exception
	{
		List<PersistedQuery> history = uiComponent.history();

		Assert.assertEquals(hist, history);
	}
}
