package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_attributes.DimensionAttributesModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class DimensionAttributeWizardTest extends UIComponentTest<DimensionAttributesModelWizard>
{
	private Dimension dimension;

	@Override
	protected DimensionAttributesModelWizard createComponent()
	{
		dimension = profile.defaultReportingArea().reportingAreas().get(DimensionAttributeWizardEmptyTest.class.getSimpleName()).dimensions().get("dim");

		return new DimensionAttributesModelWizard(dimension);
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@Test
	public void mapAll() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// go to logical
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		Assert.assertEquals(dimension.columns().size(), dimension.attributes().size());

		// verify attributes
		for (Map.Entry<String, DimensionAttribute> attrr : dimension.attributes().entrySet())
		{
			Assert.assertEquals(attrr.getValue().logicalName(), attrr.getValue().column().name());
		}
	}
}
