package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.action_view;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.SystemControllerConstants;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.ActionView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UIComponentTestRunner;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;
import org.junit.runner.RunWith;

@RunWith(
	value = UIComponentTestRunner.class
)
public class ActionViewUINewReportingAreaTest extends ActionViewUITestBase
{
	@UITestStep(ordinal = 0)
	public void newReportingAreaFiresEvent() throws Exception
	{
		uiComponent.dispatchActionAndWait(ActionView.MSG_NEW_REPORTING_AREA);

		// verify
		Assert.assertEquals(SystemControllerConstants.MSG_NEW_REPORTING_AREA, lastDispatchEvent().getLeft());

		// test mode
		uiComponent.dispatchActionAndWait(ActionView.MSG_NEW_REPORTING_AREA, "Blahy");

		// verify
		Pair<String, Object> lastDispatchEvent = lastDispatchEvent();
		Assert.assertEquals(SystemControllerConstants.MSG_NEW_REPORTING_AREA, lastDispatchEvent.getLeft());
		Pair<ReportingArea, String> right = (Pair<ReportingArea, String>) lastDispatchEvent.getRight();

		String raName = right.getRight();
		Assert.assertEquals("Blahy", raName);
		Assert.assertSame(profile.defaultReportingArea(), right.getLeft());
	}
}
