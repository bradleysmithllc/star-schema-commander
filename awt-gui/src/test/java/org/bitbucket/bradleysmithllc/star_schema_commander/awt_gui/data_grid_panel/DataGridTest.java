package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.data_grid_panel;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.components.DataGridPanel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseConnection;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.junit.Assert;

import java.sql.*;

public class DataGridTest extends AbstractUIComponentTest<DataGridPanel>
{
	@UITestStep(ordinal = 0)
	public void addMax() throws Exception
	{
		Thread.sleep(10000000L);
	}

	@Override
	protected DataGridPanel createComponent()
	{
		DatabaseConnection h2conn = profile.newH2Connection().name("billy").create();

		String catalog = null;

		try
		{
			Connection connection = h2conn.open();

			ResultSet mdrs = connection.getMetaData().getCatalogs();

			try
			{
				if (mdrs.next())
				{
					catalog = mdrs.getString(1);
				}
			}
			finally
			{
				mdrs.close();
			}

			try
			{
				Statement statement = connection.createStatement();

				try
				{
					statement.execute("CREATE TABLE FACT(ID INT, ID2 INT); INSERT INTO FACT(ID, ID2) VALUES(1, 2), (2,3), (3,4), (4,5);");
				}
				finally
				{
					statement.close();
				}
			}
			finally
			{
				connection.close();
			}
		}
		catch (SQLException e)
		{
			Assert.fail(e.toString());
		}

		Fact f = profile.defaultReportingArea().newFact().schema("PUBLIC").catalog(catalog).name("FACT").create();

		f.newDatabaseColumn().name("ID").jdbcType(Types.INTEGER).create();
		f.newDatabaseColumn().name("ID2").jdbcType(Types.INTEGER).create();

		f.newFactMeasure().column("ID").logicalName("Id").create();
		f.newFactMeasure().column("ID2").logicalName("Id_2").create();

		f.newDegenerateDimension().column("ID").create();

		return new DataGridPanel(h2conn, f, null);
	}
}