package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.RequiresJFXEventThread;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.RequiresSwingEventThread;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.InvocationTargetException;

public class EventUtilsInvokerTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@BeforeClass
	public static void enable()
	{
		EventUtils.enableJFXEventQueue();
	}

	@Test
	public void missingMethod() throws InvocationTargetException
	{
		expectedException.expect(RuntimeException.class);

		EventUtils.invokeThreadwise(this, "methodNotExists", null);
	}

	@Test
	public void noThreads() throws InvocationTargetException
	{
		EventUtils.invokeThreadwise(this, "noThreadsTest", null);
	}

	public void noThreadsTest()
	{
		require(false, false);
	}

	@Test
	public void requireSwing() throws InvocationTargetException
	{
		EventUtils.invokeThreadwise(this, "requireSwingTest", null);
	}

	@RequiresSwingEventThread
	public void requireSwingTest()
	{
		require(true, false);
	}

	@Test
	public void requireJFX() throws InvocationTargetException
	{
		EventUtils.invokeThreadwise(this, "requireJFXTest", null);
	}

	@RequiresJFXEventThread
	public void requireJFXTest()
	{
		require(false, true);
	}

	@Test
	public void returnObject() throws InvocationTargetException
	{
		Assert.assertEquals("Hi", EventUtils.invokeThreadwise(this, "returnObjectTest", null));
	}

	public Object returnObjectTest()
	{
		return "Hi";
	}

	@Test
	public void returnObjectSwing() throws InvocationTargetException
	{
		Assert.assertEquals("Hi", EventUtils.invokeThreadwise(this, "returnObjectSwingTest", null));
	}

	@RequiresSwingEventThread
	public Object returnObjectSwingTest()
	{
		return "Hi";
	}

	@Test
	public void returnObjectJFX() throws InvocationTargetException
	{
		Assert.assertEquals("Hi", EventUtils.invokeThreadwise(this, "returnObjectJFXTest", null));
	}

	@RequiresJFXEventThread
	public Object returnObjectJFXTest()
	{
		return "Hi";
	}

	private void require(boolean swing, boolean jfx)
	{
		if (swing)
		{
			Assert.assertTrue(EventUtils.isEventThread());
		}

		if (!swing)
		{
			Assert.assertFalse(EventUtils.isEventThread());
		}

		if (jfx)
		{
			Assert.assertTrue(EventUtils.isJFXEventThread());
		}

		if (!jfx)
		{
			Assert.assertFalse(EventUtils.isJFXEventThread());
		}
	}
}