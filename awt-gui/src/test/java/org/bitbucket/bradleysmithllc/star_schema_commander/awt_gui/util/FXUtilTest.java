package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

public class FXUtilTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void _nullNode()
	{
		FXUtils.getChildByID(null, "");
	}

	@Test
	public void _nullId()
	{
		FXUtils.getChildByID(new Button(), null);
	}

	@Test
	public void _nullBoth()
	{
		FXUtils.getChildByID(null, null);
	}

	@Test
	public void matchButton()
	{
		Button button = new Button();
		button.idProperty().setValue("idb");

		Button b2 = FXUtils.getChildByID(button, "idb");

		Assert.assertNotNull(b2);
	}

	@Test
	public void matchMenu() throws IOException
	{
		Pair<Node, Object> nodePair = FXUtils.loadNodeGraph("fxml/menu.fxml");
		MenuItem mi = FXUtils.getChildByID(nodePair.getLeft(), "idmi");

		Assert.assertNotNull(mi);
	}
}