package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.util;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.TestUIController;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.UIHub;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.FXMLUIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIContainerImpl;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UINodeReference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.concurrent.CountDownLatch;

public class AbstractFXViewTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Before
	public void init()
	{
		UIHub.init(new TestUIController());
	}

	@Test
	public void missingNode()
	{
		expectedException.expect(RuntimeException.class);

		FXMLUIView view = new FXMLUIView()
		{
			@UINodeReference(id = "id")
			public Node node;

			@Override
			public String viewClass()
			{
				return "missing-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}
		};

		new UIContainerImpl().addView(view);
	}

	@Test
	public void node()
	{
		class viewn extends FXMLUIView
		{
			@UINodeReference(id = "node-id")
			protected Node node;

			@Override
			public String viewClass()
			{
				return "valid-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}
		}

		viewn view = new viewn();

		new UIContainerImpl().addView(view);

		Assert.assertNotNull(view.node);
	}

	@Test
	public void wrongNodeType()
	{
		expectedException.expect(RuntimeException.class);

		class viewn extends FXMLUIView
		{
			@UINodeReference(id = "node-id")
			protected Label node;

			@Override
			public String viewClass()
			{
				return "valid-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}
		}

		viewn view = new viewn();

		new UIContainerImpl().addView(view);

		Assert.assertNotNull(view.node);
	}

	@Test
	public void nodeType() throws InterruptedException
	{
		class viewn extends FXMLUIView
		{
			@UINodeReference(id = "node-id")
			protected Button node;

			@Override
			public String viewClass()
			{
				return "valid-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}
		}

		viewn view = new viewn();

		new UIContainerImpl().addView(view);

		Assert.assertNotNull(view.node);
		Assert.assertEquals("node-id", view.node.getId());
	}

	@Test
	public void nodesAvailableBeforeInit() throws InterruptedException
	{
		final CountDownLatch cdl = new CountDownLatch(2);

		class viewn extends FXMLUIView
		{
			@UINodeReference(id = "node-id")
			protected Button node;

			@Override
			public String viewClass()
			{
				return "valid-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}

			@Override
			protected void subSubInitialize()
			{
				Assert.assertNotNull(node);
				Assert.assertEquals("node-id", node.getId());
				cdl.countDown();
			}

			@Override
			public void activate() throws Exception
			{
				Assert.assertNotNull(node);
				Assert.assertEquals("node-id", node.getId());
				cdl.countDown();
			}
		}

		viewn view = new viewn();

		new UIContainerImpl().addView(view);

		cdl.await();

		Assert.assertNotNull(view.node);
		Assert.assertEquals("node-id", view.node.getId());
	}

	@Test
	public void inheritedNodes()
	{
		class viewn extends FXMLUIView
		{
			@UINodeReference(id = "node-id")
			protected Button node;

			@Override
			public String viewClass()
			{
				return "valid-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}
		}

		class viewo extends viewn
		{
			@Override
			public String viewClass()
			{
				return "valid-node";
			}

			@Override
			public String viewInstance()
			{
				return viewClass();
			}
		}

		viewo view = new viewo();

		new UIContainerImpl().addView(view);

		Assert.assertNotNull(view.node);
		Assert.assertEquals("node-id", view.node.getId());
	}
}