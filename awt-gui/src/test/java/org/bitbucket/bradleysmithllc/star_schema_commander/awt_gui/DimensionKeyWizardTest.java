package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.TransitionStates;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.DimensionKeyModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.dimension_key.LogicalPhysicalUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionKey;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class DimensionKeyWizardTest extends UIComponentTest<DimensionKeyModelWizard>
{
	private Dimension dimension;

	@Override
	protected DimensionKeyModelWizard createComponent()
	{
		dimension = profile.defaultReportingArea().reportingAreas().get(DimensionKeyWizardTest.class.getSimpleName()).dimensions().get("dim");

		return new DimensionKeyModelWizard(dimension);
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}

	@Test
	public void mapAll() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// go to logical
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// verify key named KEY_NAME and logical Key Name with all columns
		DimensionKey key = dimension.keys().get("Key Name");

		Assert.assertNotNull(key);

		Assert.assertEquals("Key Name", key.logicalName());
		Assert.assertEquals("KEY_NAME", key.physicalName());

		List<DatabaseColumn> cols = key.columns();

		Assert.assertEquals("col1", cols.get(0).name());
		Assert.assertEquals("col2", cols.get(1).name());
		Assert.assertEquals("col3", cols.get(2).name());
		Assert.assertEquals("col4", cols.get(3).name());
		Assert.assertEquals("col5", cols.get(4).name());
		Assert.assertEquals("col6", cols.get(5).name());
	}

	@Test
	public void logical() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// go to logical
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");

		UIView lp = uiComponent.locate(LogicalPhysicalUIComponent.class.getSimpleName());
		lp.dispatchAction(LogicalPhysicalUIComponent.MSG_SET_LOGICAL_NAME, "Logical");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// verify key named KEY_NAME and logical Key Name with all columns
		DimensionKey key = dimension.keys().get("Logical");

		Assert.assertNotNull(key);

		Assert.assertEquals("Logical", key.logicalName());
		Assert.assertEquals("KEY_NAME", key.physicalName());

		List<DatabaseColumn> cols = key.columns();

		Assert.assertEquals("col1", cols.get(0).name());
		Assert.assertEquals("col2", cols.get(1).name());
		Assert.assertEquals("col3", cols.get(2).name());
		Assert.assertEquals("col4", cols.get(3).name());
		Assert.assertEquals("col5", cols.get(4).name());
		Assert.assertEquals("col6", cols.get(5).name());
	}

	@Test
	public void physical() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// go to logical
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");

		UIView lp = uiComponent.locate(LogicalPhysicalUIComponent.class.getSimpleName());
		lp.dispatchAction(LogicalPhysicalUIComponent.MSG_SET_PHYSICAL_NAME, "Physical");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// verify key named KEY_NAME and logical Key Name with all columns
		DimensionKey key = dimension.keys().get("Key Name");

		Assert.assertNotNull(key);

		Assert.assertEquals("Key Name", key.logicalName());
		Assert.assertEquals("Physical", key.physicalName());

		List<DatabaseColumn> cols = key.columns();

		Assert.assertEquals("col1", cols.get(0).name());
		Assert.assertEquals("col2", cols.get(1).name());
		Assert.assertEquals("col3", cols.get(2).name());
		Assert.assertEquals("col4", cols.get(3).name());
		Assert.assertEquals("col5", cols.get(4).name());
		Assert.assertEquals("col6", cols.get(5).name());
	}

	@Test
	public void both() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_ALL_COLUMNS);

		// go to logical
		TransitionStates mod = uiComponent.getModel(UIWizard.MODEL_TRANSITION_STATES);
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");

		UIView lp = uiComponent.locate(LogicalPhysicalUIComponent.class.getSimpleName());
		lp.dispatchAction(LogicalPhysicalUIComponent.MSG_SET_PHYSICAL_NAME, "Physical");
		lp.dispatchAction(LogicalPhysicalUIComponent.MSG_SET_LOGICAL_NAME, "Logical");

		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");

		// verify key named KEY_NAME and logical Key Name with all columns
		DimensionKey key = dimension.keys().get("Logical");

		Assert.assertNotNull(key);

		Assert.assertEquals("Logical", key.logicalName());
		Assert.assertEquals("Physical", key.physicalName());

		List<DatabaseColumn> cols = key.columns();

		Assert.assertEquals("col1", cols.get(0).name());
		Assert.assertEquals("col2", cols.get(1).name());
		Assert.assertEquals("col3", cols.get(2).name());
		Assert.assertEquals("col4", cols.get(3).name());
		Assert.assertEquals("col5", cols.get(4).name());
		Assert.assertEquals("col6", cols.get(5).name());
	}
}
