package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_degenerate_dimension_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.UITestStep;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.testable_ui.wizard.UIWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.view.UIView;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.SelectColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.DatabaseColumnTableModel;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.shared.column_mapper.ProcessColumnsUIComponent;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.DatabaseColumn;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Dimension;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.ReportingArea;
import org.junit.Assert;

import java.sql.Types;
import java.util.List;

public class FactDDWizardExcludeExistingDDTest extends FactDDWizardTestBase
{
	private Fact fact;

	@Override
	protected Fact getFact()
	{
		ReportingArea ra = profile.defaultReportingArea().newReportingArea().name(FactDDWizardExcludeExistingDDTest.class.getSimpleName()).create();

		fact = ra.newFact().schema("s").name("f").create();

		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col4").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col5").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col6").jdbcType(Types.INTEGER).create();

		fact.newFactMeasure().column("col2").create();

		fact.newDegenerateDimension().column("col3").column("col4").create();

		return fact;
	}

	@UITestStep(ordinal = 0)
	public void selectColumns() throws Exception
	{
		UIView comp = uiComponent.locate(SelectColumnsUIComponent.class.getSimpleName());
		DatabaseColumnTableModel model = comp.getModel(SelectColumnsUIComponent.MODEL_COLUMN_LIST);

		List<DatabaseColumn> td = model.tableData();

		Assert.assertEquals(3, td.size());

		Assert.assertTrue(td.contains(fact.columns().get("col1")));
		Assert.assertFalse(td.contains(fact.columns().get("col2")));
		Assert.assertFalse(td.contains(fact.columns().get("col3")));
		Assert.assertFalse(td.contains(fact.columns().get("col4")));
		Assert.assertTrue(td.contains(fact.columns().get("col5")));
		Assert.assertTrue(td.contains(fact.columns().get("col6")));

		comp.dispatchAction(SelectColumnsUIComponent.MSG_SELECT_COLUMN, fact.columns().get("col6"));
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "logical");
	}

	@UITestStep(ordinal = 1)
	public void process() throws Exception
	{
		UIView comp = uiComponent.locate(ProcessColumnsUIComponent.class.getSimpleName());

		comp.dispatchAction(ProcessColumnsUIComponent.MSG_SET_LOGICAL_NAME, "LogCol6");
		uiComponent.dispatchAction(UIWizard.MSG_ACTIVATE_TRANSITION, "process");
	}

	@UITestStep(ordinal = 2)
	public void validateDD() throws Exception
	{
		Dimension dim = fact.degenerateDimension();

		Assert.assertEquals(3, dim.columns().size());

		// verify that the attribute was created as well.
		Assert.assertEquals(3, dim.attributes().size());
		Assert.assertEquals("col6", dim.attributes().get("LogCol6").column().name());
	}
}
