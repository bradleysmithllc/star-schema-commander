package org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.fact_key_wizard;

/*
 * #%L
 * awt-gui
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.dimension_panel.DimensionPanelTestBase;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.test_driver.AbstractUIComponentTest;
import org.bitbucket.bradleysmithllc.star_schema_commander.awt_gui.wizards.fact_dimension_keys.FactDimensionKeysModelWizard;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.junit.Ignore;

import java.io.IOException;

@Ignore
public class FactKeyWizardTestBase extends AbstractUIComponentTest<FactDimensionKeysModelWizard>
{
	@Override
	protected FactDimensionKeysModelWizard createComponent()
	{
		Fact fact = profile.defaultReportingArea().reportingAreas().get("Empty").newFact().name("fact").schema("schema").create();

		return new FactDimensionKeysModelWizard(fact);
	}

	@Override
	protected void prepareProfile() throws IOException
	{
		DimensionPanelTestBase.prepareProfile(profile, temporaryFolder);
	}
}
