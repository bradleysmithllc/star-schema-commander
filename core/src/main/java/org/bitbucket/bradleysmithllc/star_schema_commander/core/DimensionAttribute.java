package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedObject;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class DimensionAttribute extends Removable implements Comparable<DimensionAttribute>, PersistedObject
{
	private final Dimension in;
	private DatabaseColumn column;
	private String logicalName;

	public DimensionAttribute(Dimension in, DatabaseColumn column, String logicalName)
	{
		this.in = in;
		this.column = column;
		this.logicalName = logicalName;
	}

	public DimensionAttribute(Dimension in)
	{
		this.in = in;
	}

	public static void read(PersistedReadableFile file, Dimension dimension) throws IOException
	{
		BufferedReader reader = new BufferedReader(file.reader());

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			JsonNode logicalName = node.get("logical-name");
			String columnName = node.get("column-name").asText();

			DimensionAttributeBuilder attributeBuilder = dimension.newDimensionAttribute().column(columnName);

			if (logicalName != null && !logicalName.isNull())
			{
				attributeBuilder.logicalName(logicalName.asText());
			}

			attributeBuilder.create();
		}
		finally
		{
			reader.close();
		}
	}

	public Dimension in()
	{
		return in;
	}

	public DatabaseColumn column()
	{
		return column;
	}

	public String logicalName()
	{
		return logicalName;
	}

	public String qualifiedName()
	{
		return in.qualifiedName() + "." + logicalName();
	}

	@Override
	public int compareTo(DimensionAttribute o)
	{
		return CompareUtils.compare(in, o.in, logicalName, o.logicalName);
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		if (removed())
		{
			file.delete();
		}
		else
		{
			// write the dimension attribute object
			BufferedWriter writer = new BufferedWriter(file.writer());

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("logical-name").value(logicalName);
				jsonWriter.name("column-name").value(column.name());

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{

	}

	public static class DimensionAttributeBuilder
	{
		private final Dimension in;
		private String column;
		private String logicalName;

		public DimensionAttributeBuilder(Dimension profile)
		{
			this.in = profile;
		}

		public DimensionAttributeBuilder logicalName(String n)
		{
			logicalName = n;
			return this;
		}

		public DimensionAttributeBuilder column(String name)
		{
			column = name;
			return this;
		}

		public DimensionAttribute create()
		{
			if (column == null)
			{
				throw new IllegalStateException("Column must be supplied");
			}

			DatabaseColumn dc = in.columns().get(column);

			if (dc == null)
			{
				throw new IllegalStateException("Column [" + column + "] not found");
			}

			DimensionAttribute dimensionAttribute = new DimensionAttribute(in, dc, ObjectUtils.firstNonNull(logicalName, dc.name()));
			in.addDimensionAttribute(dimensionAttribute);

			return dimensionAttribute;
		}
	}
}
