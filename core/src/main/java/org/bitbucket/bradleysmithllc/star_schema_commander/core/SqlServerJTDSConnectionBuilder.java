package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class SqlServerJTDSConnectionBuilder extends BaseConnectionBuilder<SqlServerJTDSConnectionBuilder>
{
	private String database;

	public SqlServerJTDSConnectionBuilder(Profile profile)
	{
		super(profile);

		port(1433);
	}

	@Override
	public SqlServerJTDSConnectionBuilder serverName(String server)
	{
		return super.serverName(server);
	}

	@Override
	protected propertyRequired requiresServerName()
	{
		return propertyRequired.required;
	}

	@Override
	public SqlServerJTDSConnectionBuilder port(int p)
	{
		return super.port(p);
	}

	@Override
	protected propertyRequired requiresPort()
	{
		return propertyRequired.optional;
	}

	@Override
	public SqlServerJTDSConnectionBuilder username(String user)
	{
		return super.username(user);
	}

	@Override
	protected propertyRequired requiresUserName()
	{
		return propertyRequired.optional;
	}

	@Override
	public SqlServerJTDSConnectionBuilder password(String pass)
	{
		return super.password(pass);
	}

	@Override
	protected propertyRequired requiresPassword()
	{
		return propertyRequired.optional;
	}

	public SqlServerJTDSConnectionBuilder database(String d)
	{
		database = d;
		return this;
	}

	public SqlServerJTDSConnectionBuilder instanceName(String ins)
	{
		if (ins == null)
		{
			clearProperty("instance");
		}
		else
		{
			property("instance", ins);
		}
		return this;
	}

	public SqlServerJTDSConnectionBuilder domain(String d)
	{
		if (d != null)
		{
			property("useNTLMv2", "true");
			return property("domain", d);
		}
		else
		{
			clearProperty("useNTLMv2");
			clearProperty("domain");

			return this;
		}
	}

	protected DatabaseConnection createSub()
	{
		DatabaseConnection dbo = new DatabaseConnection(name, properties("jdbc:jtds:sqlserver://" + serverName + (port != 1433 ? (":" + port) : "") + (database != null ? ("/" + database) : "")), "net.sourceforge.jtds.jdbc.Driver", "dbo", username, password);
		return profile.addDatabaseConnection(dbo);
	}

	@Override
	protected String getVendorName()
	{
		return "SqlServer";
	}
}