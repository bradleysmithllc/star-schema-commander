package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class H2ConnectionBuilder extends BaseConnectionBuilder<H2ConnectionBuilder>
{
	private File path;

	public H2ConnectionBuilder(Profile profile)
	{
		super(profile);
	}

	public H2ConnectionBuilder path(File path)
	{
		this.path = path;
		return this;
	}

	protected DatabaseConnection createSub()
	{
		if (path == null)
		{
			// create a temp file
			try
			{
				path = File.createTempFile("H2H", "2H2");
			}
			catch (IOException e)
			{
				throw new IllegalArgumentException(e);
			}
		}
		else
		{
			try
			{
				if (!path.exists())
				{
					FileUtils.forceMkdir(path.getParentFile());
					FileUtils.touch(path);
				}
				else
				{
					if (!path.isFile())
					{
						throw new IllegalArgumentException("Must use a regular file as a data source.");
					}
				}
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}
		}

		DatabaseConnection dbo = new DatabaseConnection(
			name, properties("jdbc:h2:" + path.getAbsolutePath()), "org.h2.Driver", "PUBLIC", username, password);

		return profile.addDatabaseConnection(dbo);
	}

	@Override
	protected String getVendorName()
	{
		return "H2";
	}
}
