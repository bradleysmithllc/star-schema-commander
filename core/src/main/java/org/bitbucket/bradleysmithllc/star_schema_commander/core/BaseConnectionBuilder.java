package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Map;
import java.util.TreeMap;

public abstract class BaseConnectionBuilder<T extends BaseConnectionBuilder>
{
	protected final Profile profile;
	private final Map<String, String> properties = new TreeMap<String, String>();
	protected String name;
	protected String serverName;
	protected int port = -1;

	protected String username;
	protected String password;

	public BaseConnectionBuilder(Profile profile)
	{
		this.profile = profile;
	}

	public T name(String name)
	{
		this.name = name;
		return (T) this;
	}

	public T property(String key, String name)
	{
		properties.put(key, name);
		return (T) this;
	}

	public T clearProperty(String key)
	{
		properties.remove(key);
		return (T) this;
	}

	public final DatabaseConnection create()
	{
		if (name == null || name.trim().equals(""))
		{
			throw new IllegalStateException(getVendorName() + " Connection requires a name");
		}

		// check optional stuff
		if (requiresServerName() == propertyRequired.required && serverName == null)
		{
			throw new IllegalStateException(getVendorName() + " Connection requires a server name");
		}

		if (requiresUserName() == propertyRequired.required && username == null)
		{
			throw new IllegalStateException(getVendorName() + " Connection requires a user name");
		}

		if (requiresPassword() == propertyRequired.required && password == null)
		{
			throw new IllegalStateException(getVendorName() + " Connection requires a password");
		}

		return createSub();
	}

	protected T serverName(String server)
	{
		serverName = server;
		return (T) this;
	}

	protected propertyRequired requiresServerName()
	{
		return propertyRequired.not_allowed;
	}

	protected T port(int p)
	{
		port = p;
		return (T) this;
	}

	protected propertyRequired requiresPort()
	{
		return propertyRequired.not_allowed;
	}

	protected T username(String user)
	{
		username = user;
		return (T) this;
	}

	protected propertyRequired requiresUserName()
	{
		return propertyRequired.not_allowed;
	}

	protected T password(String pass)
	{
		password = pass;
		return (T) this;
	}

	protected propertyRequired requiresPassword()
	{
		return propertyRequired.not_allowed;
	}

	protected abstract DatabaseConnection createSub();

	protected String properties(String url)
	{
		StringBuilder stb = new StringBuilder(url);

		for (Map.Entry<String, String> entry : properties.entrySet())
		{
			stb.append(";");
			stb.append(entry.getKey());
			stb.append("=");
			stb.append(entry.getValue());
		}

		return stb.toString();
	}

	protected abstract String getVendorName();

	enum propertyRequired
	{
		required,
		optional,
		not_allowed
	}
}
