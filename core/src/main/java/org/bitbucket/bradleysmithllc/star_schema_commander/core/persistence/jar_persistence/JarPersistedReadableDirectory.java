package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.jar_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableDirectory;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableEntry;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JarPersistedReadableDirectory implements PersistedReadableDirectory
{
	private final JarPersistedProfile jarPersistedProfile;
	private final JarPersistedReadableDirectory parentDirectory;
	private final String name;

	public JarPersistedReadableDirectory(JarPersistedProfile jarPersistedProfile)
	{
		this.jarPersistedProfile = jarPersistedProfile;
		name = null;
		parentDirectory = null;
	}

	public JarPersistedReadableDirectory(JarPersistedProfile jarPersistedProfile, JarPersistedReadableDirectory jarPersistedReadableDirectory, String name)
	{
		this.jarPersistedProfile = jarPersistedProfile;
		this.parentDirectory = jarPersistedReadableDirectory;
		this.name = name;
	}

	String path()
	{
		return parentDirectory == null ? "" : (parentDirectory.path() + name + "/");
	}

	@Override
	public List<PersistedReadableEntry> listReadable()
	{
		List<String> names = jarPersistedProfile.listNames(path());

		if (names != null)
		{
			List<PersistedReadableEntry> pelist = new ArrayList<PersistedReadableEntry>();

			for (String name : names)
			{
				String pathName = path() + name;
				if (jarPersistedProfile.isDirectoryPath(pathName))
				{
					pelist.add(new JarPersistedReadableDirectory(jarPersistedProfile, this, name));
				}
				else
				{
					pelist.add(new JarPersistedReadableFile(jarPersistedProfile, this, name));
				}
			}

			return pelist;
		}
		return Collections.emptyList();
	}

	@Override
	public PersistedReadableFile openReadableFile(String name) throws IOException
	{
		return new JarPersistedReadableFile(jarPersistedProfile, this, name);
	}

	@Override
	public PersistedReadableDirectory openReadableDirectory(String name) throws IOException
	{
		return new JarPersistedReadableDirectory(jarPersistedProfile, this, name);
	}

	@Override
	public boolean isDirectory()
	{
		return true;
	}

	@Override
	public PersistedReadableFile readableFile()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PersistedReadableDirectory readableDirectory()
	{
		return this;
	}

	@Override
	public String name()
	{
		return name;
	}
}
