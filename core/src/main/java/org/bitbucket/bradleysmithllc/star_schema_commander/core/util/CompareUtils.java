package org.bitbucket.bradleysmithllc.star_schema_commander.core.util;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.text.Collator;
import java.util.Locale;

public class CompareUtils
{
	private static final Collator collator = Collator.getInstance(Locale.ENGLISH);

	public static int compare(Comparable... objects)
	{
		if (objects.length % 2 != 0)
		{
			throw new IllegalArgumentException("Mismatched argument pair.");
		}

		if (objects.length == 0)
		{
			throw new IllegalArgumentException("Empty argument array.");
		}

		for (int i = 0; i < objects.length; i += 2)
		{
			Comparable l = objects[i];
			Comparable r = objects[i + 1];

			if (l == null && r != null)
			{
				return -1;
			}
			else if (r == null && l != null)
			{
				return 1;
			}

			int comp = 0;

			if (l != null && r != null)
			{
				if (l instanceof String && r instanceof String)
				{
					comp = collator.compare(((String) l).toLowerCase(), ((String) r).toLowerCase());
				}
				else
				{
					comp = l.compareTo(r);
				}
			}

			if (comp != 0)
			{
				return comp;
			}
		}

		return 0;
	}
}
