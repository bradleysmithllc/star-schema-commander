package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.file_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FilePersistedProfile implements PersistedProfile, FileChangeWatcher.FileChangeListener
{
	private final File fsRoot;
	private List<ProfileChangeListener> updateListenerMap = new ArrayList<ProfileChangeListener>();
	private FileChangeWatcher profileChangeWatcher;

	public FilePersistedProfile(File fsRoot)
	{
		this.fsRoot = fsRoot;

		profileChangeWatcher = new FileChangeWatcher(new File(fsRoot, "profile.json"));
	}

	@Override
	public File filesystemLocation()
	{
		return fsRoot;
	}

	@Override
	public synchronized void addProfileChangeListener(ProfileChangeListener listener)
	{
		updateListenerMap.add(listener);

		if (updateListenerMap.size() != 0)
		{
			profileChangeWatcher.watch(this);
		}
	}

	@Override
	public synchronized void removeProfileChangeListener(ProfileChangeListener listener) throws IOException
	{
		if (!updateListenerMap.remove(listener))
		{
			throw new IllegalArgumentException("Listener not registered");
		}

		if (updateListenerMap.isEmpty())
		{
			releaseProfileChangeListeners();
		}
	}

	@Override
	public int listenerCount()
	{
		return updateListenerMap.size();
	}

	@Override
	public synchronized void releaseProfileChangeListeners()
	{
		if (profileChangeWatcher.watching())
		{
			//profileChangeWatcher.stopWatching();
		}
	}

	@Override
	public void read(ReadVisitor visitor) throws IOException
	{
		visitor.visitReader(this, (PersistedReadableDirectory) wrap(fsRoot));
	}

	@Override
	public void write(WriteVisitor visitor) throws IOException
	{
		visitor.visitWriter(this, (PersistedWritableDirectory) wrap(fsRoot));
	}

	private PersistedEntry wrap(final File fsRoot)
	{
		if (fsRoot.isDirectory())
		{
			return new FilePersistedDirectory(fsRoot);
		}
		else if (fsRoot.isFile())
		{
			return new FilePersistedFile(fsRoot);
		}

		throw new IllegalArgumentException(fsRoot.getAbsolutePath());
	}

	@Override
	public void pathInitialized(File path)
	{
		System.out.println("Profile watcher initialized");

		for (ProfileChangeListener list : updateListenerMap)
		{
			list.profileInitialized();
		}
	}

	@Override
	public void pathCreated(File path)
	{
		System.out.println("Profile created");

		for (ProfileChangeListener list : updateListenerMap)
		{
			list.profileChanged();
		}
	}

	@Override
	public void pathDeleted(File path)
	{
	}

	@Override
	public void pathModified(File path)
	{
		System.out.println("Profile updated externally");

		for (ProfileChangeListener list : updateListenerMap)
		{
			list.profileChanged();
		}
	}

	private static class FilePersistedFile implements PersistedReadableFile, PersistedWritableFile
	{
		private final File fsRoot;

		public FilePersistedFile(File fsRoot)
		{
			this.fsRoot = fsRoot;
		}

		@Override
		public InputStream read() throws IOException
		{
			return new FileInputStream(fsRoot);
		}

		@Override
		public Reader reader() throws FileNotFoundException
		{
			return new FileReader(fsRoot);
		}

		@Override
		public OutputStream write() throws IOException
		{
			return new FileOutputStream(fsRoot);
		}

		@Override
		public Writer writer() throws IOException
		{
			return new FileWriter(fsRoot);
		}

		@Override
		public boolean isDirectory()
		{
			return fsRoot.isDirectory();
		}

		@Override
		public PersistedReadableFile readableFile()
		{
			return this;
		}

		@Override
		public PersistedReadableDirectory readableDirectory()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public PersistedWritableFile writableFile()
		{
			return this;
		}

		@Override
		public PersistedWritableDirectory writableDirectory()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public String name()
		{
			return fsRoot.getName();
		}

		@Override
		public void delete() throws IOException
		{
			FileUtils.forceDelete(fsRoot);
		}
	}

	private class FilePersistedDirectory implements PersistedReadableDirectory, PersistedWritableDirectory
	{
		private final File fsRoot;

		public FilePersistedDirectory(File fsRoot)
		{
			this.fsRoot = fsRoot;
		}

		@Override
		public List<PersistedReadableEntry> listReadable()
		{
			List<PersistedReadableEntry> pelist = new ArrayList<PersistedReadableEntry>();

			File[] files = fsRoot.listFiles();

			for (File file : files)
			{
				pelist.add((PersistedReadableEntry) wrap(file));
			}

			return pelist;
		}

		@Override
		public FilePersistedFile openReadableFile(String name) throws IOException
		{
			File target = new File(fsRoot, name);

			if (target.exists())
			{
				if (!target.isFile())
				{
					throw new IOException();
				}
			}
			else if (!target.exists())
			{
				FileUtils.touch(target);
			}

			return (FilePersistedFile) wrap(target);
		}


		@Override
		public FilePersistedFile openWritableFile(String name) throws IOException
		{
			return openReadableFile(name);
		}

		@Override
		public FilePersistedDirectory openReadableDirectory(String name) throws IOException
		{
			File target = new File(fsRoot, name);

			if (!target.exists())
			{
				target.mkdirs();
			}

			if (!target.isDirectory())
			{
				throw new IOException();
			}

			return (FilePersistedDirectory) wrap(target);
		}

		@Override
		public FilePersistedDirectory openWritableDirectory(String name) throws IOException
		{
			return openReadableDirectory(name);
		}

		@Override
		public boolean isDirectory()
		{
			return fsRoot.isDirectory();
		}

		@Override
		public PersistedReadableFile readableFile()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public PersistedReadableDirectory readableDirectory()
		{
			return this;
		}

		@Override
		public PersistedWritableFile writableFile()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public PersistedWritableDirectory writableDirectory()
		{
			return this;
		}

		@Override
		public String name()
		{
			return fsRoot.getName();
		}

		@Override
		public void delete() throws IOException
		{
			FileUtils.forceDelete(fsRoot);
		}
	}
}
