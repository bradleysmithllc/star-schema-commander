package org.bitbucket.bradleysmithllc.star_schema_commander.core.query;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Collections;
import java.util.List;

public class QueryDataContainer
{
	private List<String> columns;
	private List<RowData> rowData;

	public QueryDataContainer(List<String> columns, List<RowData> rowData)
	{
		this.columns = columns;
		this.rowData = rowData;
	}

	public List<String> getColumns()
	{
		return Collections.unmodifiableList(columns);
	}

	public List<RowData> getRowData()
	{
		return Collections.unmodifiableList(rowData);
	}
}
