package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.file_persistence.FilePersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;

import java.io.*;
import java.util.*;

public class Profile implements IProfile, PersistedProfile.ProfileChangeListener, ReadVisitor, WriteVisitor
{
	private final File root;
	private final PersistedProfile persistedProfile;
	private final Map<String, DatabaseConnection> connectionList = new TreeMap<String, DatabaseConnection>();
	private final Map<String, Query> queryHistory = new TreeMap<String, Query>();
	private final ReportingArea defaultReportingArea;
	private final List<PersistedProfile.ProfileChangeListener> listeners = new ArrayList<PersistedProfile.ProfileChangeListener>();
	private Version version;

	public Profile(File root) throws IOException
	{
		this.root = root;
		this.persistedProfile = new FilePersistedProfile(root);

		if (!root.exists())
		{
			throw new IllegalArgumentException("Profile path does not exist [" + root + "]");
		}

		defaultReportingArea = new ReportingArea.ReportingAreaBuilder(this, null).name("[default]").create();

		reload();
	}

	public static File defaultProfileDir()
	{
		return new File(System.getProperty("user.home"), defaultProfileName());
	}

	public static String defaultProfileName()
	{
		return ".sscommander";
	}

	public static Profile create(File path) throws IOException
	{
		if (path.exists())
		{
			throw new IllegalStateException("Path already exists [" + path + "]");
		}

		FileUtils.forceMkdir(path);

		FilePersistedProfile jpp = new FilePersistedProfile(path);
		jpp.write(new WriteVisitor()
		{
			@Override
			public void visitWriter(PersistedProfile profile, PersistedWritableDirectory root) throws IOException
			{
				// write out the version file
				Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

				PersistedWritableFile pwf = root.openWritableFile("profile.json");

				Writer writer = pwf.writer();

				try
				{
					IOUtils.write(gson.toJson(new Version(Version._CURRENT_VERSION)), writer);
				}
				finally
				{
					writer.close();
				}
			}
		});

		return new Profile(path);
	}

	static <T> T loadFromJson(PersistedReadableFile pathname, Class<T> cls) throws IOException
	{
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();
		InputStream inputStream = pathname.read();

		try
		{
			String json = IOUtils.toString(inputStream);
			return gson.fromJson(json, cls);
		}
		finally
		{
			inputStream.close();
		}
	}

	static JsonNode loadJson(PersistedReadableFile pathname) throws IOException
	{
		InputStream read = pathname.read();
		try
		{
			String json = IOUtils.toString(read);
			return JsonLoader.fromString(json);
		}
		finally
		{
			read.close();
		}
	}

	private void reload() throws IOException
	{
		persistedProfile.read(this);
	}

	@Override
	public File getProfileDirectory()
	{
		return root;
	}

	public SqlServerJTDSConnectionBuilder newSqlServerJTDSConnection()
	{
		return new SqlServerJTDSConnectionBuilder(this);
	}

	public SqlServerConnectionBuilder newSqlServerConnection()
	{
		return new SqlServerConnectionBuilder(this);
	}

	public OracleConnectionBuilder newOracleConnection()
	{
		return new OracleConnectionBuilder(this);
	}

	public H2ConnectionBuilder newH2Connection()
	{
		return new H2ConnectionBuilder(this);
	}

	public HANAConnectionBuilder newHANAConnection()
	{
		return new HANAConnectionBuilder(this);
	}

	public PostgreSQLConnectionBuilder newPostgreSQLConnection()
	{
		return new PostgreSQLConnectionBuilder(this);
	}

	@Override
	public ReportingArea defaultReportingArea()
	{
		return defaultReportingArea;
	}

	synchronized DatabaseConnection addDatabaseConnection(DatabaseConnection con)
	{
		if (connectionList.containsKey(con.name()))
		{
			throw new IllegalArgumentException("Duplicate connection name [" + con.name() + "]");
		}

		connectionList.put(con.name(), con);
		return con;
	}

	/**
	 * Reset profile caches to force it to reload configuration from the filesystem.
	 */
	public synchronized void invalidate() throws IOException
	{
		persistedProfile.read(this);
	}

	public synchronized void persist() throws IOException
	{
		// lame fix for the file monitor.  Make sure to advance the current timestamp at least
		// 1 millisecond so that the resulting file will change in a significant way
		try
		{
			long currentTime = System.currentTimeMillis();

			Thread.sleep(1000L - (currentTime % 1000L));
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		persistedProfile.write(this);
	}

	@Override
	public Map<String, DatabaseConnection> connections()
	{
		return Collections.unmodifiableMap(connectionList);
	}

	@Override
	public synchronized void addProfileChangeListener(PersistedProfile.ProfileChangeListener listener) throws IOException
	{
		listeners.add(listener);

		if (persistedProfile.listenerCount() == 0)
		{
			persistedProfile.addProfileChangeListener(this);
		}
	}

	@Override
	public synchronized void removeProfileChangeListener(PersistedProfile.ProfileChangeListener listener) throws IOException
	{
		if (!listeners.remove(listener))
		{
			throw new IllegalArgumentException("Listener not registered");
		}

		if (persistedProfile.listenerCount() == 1)
		{
			releaseProfileChangeListeners();
		}
	}

	@Override
	public synchronized void releaseProfileChangeListeners()
	{
		listeners.clear();

		persistedProfile.releaseProfileChangeListeners();
	}

	File profileDir(String name)
	{
		return new File(root, name);
	}

	@Override
	public void profileChanged()
	{
		try
		{
			persistedProfile.read(this);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		for (PersistedProfile.ProfileChangeListener listener : listeners)
		{
			listener.profileChanged();
		}
	}

	@Override
	public void profileInitialized()
	{
		for (PersistedProfile.ProfileChangeListener listener : listeners.toArray(new PersistedProfile.ProfileChangeListener[listeners.size()]))
		{
			listener.profileInitialized();
		}
	}

	@Override
	public void visitReader(PersistedProfile profile, PersistedReadableDirectory root) throws IOException
	{
		// load the version
		try
		{
			version = loadFromJson(root.openReadableFile("profile.json"), Version.class);
		}
		catch (IOException e)
		{
			throw new IllegalStateException("Error loading profile descriptor", e);
		}

		// read in all connections
		connectionList.clear();

		// load from json
		for (PersistedReadableEntry pe : root.openReadableDirectory("connections").listReadable())
		{
			if (!pe.isDirectory())
			{
				PersistedReadableFile pf = pe.readableFile();

				try
				{
					DatabaseConnection con = new DatabaseConnection();
					con.read(persistedProfile, pf);
					addDatabaseConnection(con);
				}
				catch (IOException exc)
				{
					System.out.println("Bad connection found in profile: " + pe.name());
					exc.printStackTrace(System.out);
				}
			}
		}

		// now reporting areas
		defaultReportingArea.read(persistedProfile, root.openReadableDirectory("reportingAreas").openReadableDirectory(defaultReportingArea.name()));

		queryHistory.clear();

		// read in the query history
		PersistedReadableDirectory qrd = root.openReadableDirectory("history").openReadableDirectory("queries");

		for (PersistedReadableEntry qre : qrd.listReadable())
		{
			PersistedReadableFile qrf = qre.readableFile();

			try
			{
				Query q = Query.read(this, qrf);

				String name = qrf.name();
				// strip off the .json
				name = name.substring(0, name.length() - 5);

				queryHistory.put(name, q);
			}
			catch (IllegalArgumentException exc)
			{
				System.out.println("Bad query found in history [" + qrf.name() + "] {" + exc.toString() + "}");
			}
		}
	}

	@Override
	public void visitWriter(PersistedProfile profile, PersistedWritableDirectory root) throws IOException
	{
		// write out the version file
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

		// persist connections
		Iterator<DatabaseConnection> values = connectionList.values().iterator();

		while (values.hasNext())
		{
			DatabaseConnection conn = values.next();

			PersistedWritableFile target = root.openWritableDirectory("connections").openWritableFile(conn.name() + ".json");

			conn.persist(persistedProfile, target);

			// check for deletion
			if (conn.removed())
			{
				values.remove();
			}
		}

		// write out query history
		PersistedWritableDirectory qwd = root.openWritableDirectory("history").openWritableDirectory("queries");

		for (Map.Entry<String, Query> qentry : queryHistory.entrySet())
		{
			PersistedWritableFile qwf = qwd.openWritableFile(qentry.getKey() + ".json");
			qentry.getValue().persist(profile, qwf);
		}

		// persist reporting areas
		defaultReportingArea.persist(persistedProfile, root.openWritableDirectory("reportingAreas").openWritableDirectory(defaultReportingArea.name()));

		// write out profile last - that is the trigger for modifications.
		PersistedWritableFile persistedFile = root.openWritableFile("profile.json");

		OutputStream outputStream = new BufferedOutputStream(persistedFile.write());

		try
		{
			IOUtils.write(gson.toJson(new Version(Version._CURRENT_VERSION)), outputStream);
		}
		finally
		{
			outputStream.close();
		}
	}

	public void dispose()
	{
		releaseProfileChangeListeners();
	}

	public Map<String, Query> namedQueries()
	{
		return Collections.unmodifiableMap(queryHistory);
	}

	public Query namedQuery(String name)
	{
		return queryHistory.get(name);
	}

	public void registerNamedQuery(String name, Query q)
	{
		if (queryHistory.containsKey(name))
		{
			throw new IllegalArgumentException("Query name [" + name + "] has already been used.");
		}

		queryHistory.put(name, q);
	}
}
