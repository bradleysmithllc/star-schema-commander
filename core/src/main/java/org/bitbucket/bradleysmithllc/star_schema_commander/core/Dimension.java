package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.*;
import java.text.Collator;
import java.util.*;

public class Dimension extends Removable implements ColumnStore, Comparable<Dimension>, PersistedCollection
{
	private final String logicalName;
	private final String name;
	private final String schema;
	private final String catalog;
	private final boolean degenerate;
	private final Map<String, DatabaseColumn> columnList = new TreeMap<String, DatabaseColumn>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, DimensionAttribute> attributes = new TreeMap<String, DimensionAttribute>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, DimensionKey> keys = new TreeMap<String, DimensionKey>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, DimensionKey> physicalKeys = new TreeMap<String, DimensionKey>(Collator.getInstance(Locale.ENGLISH));
	private transient ReportingArea reportingArea;

	public Dimension(ReportingArea reportingArea, String logicalName, String name, String schema, String catalog, boolean degenerate)
	{
		this.reportingArea = reportingArea;
		this.name = name;
		this.schema = schema;
		this.logicalName = logicalName;
		this.degenerate = degenerate;
		this.catalog = catalog;
	}

	void enter(ReportingArea area)
	{
		reportingArea = area;
	}

	@Override
	public void remove()
	{
		super.remove();

		// remove all keys so the facts get notified
		for (Map.Entry<String, DimensionKey> dk : keys().entrySet())
		{
			DimensionKey value = dk.getValue();
			if (!value.removed())
			{
				value.remove();
			}
		}
	}

	public String logicalName()
	{
		return logicalName;
	}

	public String name()
	{
		return name;
	}

	public String schema()
	{
		return schema;
	}

	public String catalog()
	{
		return catalog;
	}

	public boolean degenerate()
	{
		return degenerate;
	}

	void addDimensionKey(DimensionKey key)
	{
		if (keys.containsKey(key.logicalName()))
		{
			throw new IllegalArgumentException("Duplicate key [" + key.logicalName() + "]");
		}

		if (physicalKeys.containsKey(key.logicalName()))
		{
			throw new IllegalArgumentException("Duplicate key [" + key.physicalName() + "]");
		}

		keys.put(key.logicalName(), key);
		physicalKeys.put(key.physicalName(), key);
	}

	void addDimensionAttribute(DimensionAttribute attr)
	{
		if (attributes.containsKey(attr.logicalName()))
		{
			throw new IllegalStateException("Attribute already added [" + attr.logicalName() + "]");
		}

		attributes.put(attr.logicalName(), attr);
	}

	public Map<String, DatabaseColumn> columns()
	{
		return Collections.unmodifiableMap(columnList);
	}

	@Override
	public Map<String, DatabaseColumn> mutableColumns()
	{
		return columnList;
	}

	@Override
	public void add(String name, String typeName)
	{
		newDatabaseColumn().name(name).jdbcTypeName(typeName).create();
	}

	public Map<String, DimensionAttribute> attributes()
	{
		return Collections.unmodifiableMap(attributes);
	}

	public Map<String, DimensionKey> keys()
	{
		return Collections.unmodifiableMap(keys);
	}

	public Map<String, DimensionKey> physicalKeys()
	{
		return Collections.unmodifiableMap(physicalKeys);
	}

	public synchronized DatabaseColumn addDatabaseColumn(DatabaseColumn column)
	{
		if (columnList.containsKey(column.name()))
		{
			throw new IllegalArgumentException("Duplicate column name [" + column.name() + "]");
		}

		columnList.put(column.name(), column);
		return column;
	}

	public DatabaseColumn.DatabaseColumnBuilder newDatabaseColumn()
	{
		return new DatabaseColumn.DatabaseColumnBuilder(this);
	}

	public DimensionAttribute.DimensionAttributeBuilder newDimensionAttribute()
	{
		return new DimensionAttribute.DimensionAttributeBuilder(this);
	}

	public DimensionKey.DimensionKeyBuilder newDimensionKey()
	{
		return new DimensionKey.DimensionKeyBuilder(this);
	}

	public List<Fact> facts()
	{
		List<Fact> facts = new ArrayList<Fact>();

		for (Fact fact : reportingArea.facts().values())
		{
			if (fact.keys(this).size() != 0)
			{
				facts.add(fact);
			}
		}

		return facts;
	}

	@Override
	public int compareTo(Dimension o)
	{
		return CompareUtils.compare(reportingArea, o.reportingArea, logicalName, o.logicalName);
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableDirectory root) throws IOException
	{
		if (removed())
		{
			root.delete();
		}
		else
		{
			PersistedWritableFile target = root.openWritableFile("dimension.json");

			// write the dimension object
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(target.write()));

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("name").value(name);
				jsonWriter.name("logical-name").value(logicalName);
				jsonWriter.name("schema").value(schema);

				jsonWriter.name("catalog").value(catalog);

				jsonWriter.name("degenerate").value(degenerate);

				jsonWriter.name("column-list").beginObject().endObject();
				jsonWriter.name("attributes").beginObject().endObject();
				jsonWriter.name("keys").beginObject().endObject();
				jsonWriter.name("physical-keys").beginObject().endObject();

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}

			// now write each column
			DatabaseColumn.writeToContainer(root, this);

			// attributes
			PersistedWritableFile attrs = root.openWritableFile("attributes.json");

			Writer fwriter = attrs.writer();

			try
			{
				JsonWriter jwriter = new JsonWriter(fwriter);
				jwriter.beginObject();

				Iterator<DimensionAttribute> ait = attributes.values().iterator();

				while (ait.hasNext())
				{
					DimensionAttribute col = ait.next();

					if (col.removed())
					{
						ait.remove();
					}
					else
					{
						jwriter.name(col.logicalName());
						jwriter.value(col.column().name());
					}
				}

				jwriter.endObject();
				jwriter.flush();
				jwriter.close();
			}
			finally
			{
				fwriter.close();
			}

			// keys
			PersistedWritableDirectory keysDir = root.openWritableDirectory("keys");

			Iterator<DimensionKey> kit = keys.values().iterator();

			while (kit.hasNext())
			{
				DimensionKey col = kit.next();
				col.persist(profile, keysDir.openWritableFile(col.logicalName() + ".json"));

				if (col.removed())
				{
					kit.remove();
				}
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableDirectory root) throws IOException
	{
		// load columns from json
		DatabaseColumn.readFromContainer(root, this);

		// read attributes
		PersistedReadableFile rf = root.openReadableFile("attributes.json");

		Reader reader = rf.reader();

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			Iterator<Map.Entry<String, JsonNode>> fn = node.fields();

			while (fn.hasNext())
			{
				Map.Entry<String, JsonNode> jnode = fn.next();

				// create attribute
				newDimensionAttribute().logicalName(jnode.getKey()).column(jnode.getValue().asText()).create();
			}
		}
		finally
		{
			reader.close();
		}

		// read keys
		// load from json
		for (PersistedReadableEntry pe : root.openReadableDirectory("keys").listReadable())
		{
			try
			{
				DimensionKey.read(pe.readableFile(), Dimension.this);
			}
			catch (IOException exc)
			{
				System.out.println("Bad key found in profile: " + pe.name());
				exc.printStackTrace(System.out);
			}
		}
	}

	public String qualifiedName()
	{
		return reportingArea.qualifiedName() + "." + logicalName();
	}

	public String sqlQualifiedName()
	{
		return (catalog == null ? "" : (catalog + ".")) + schema + "." + name;
	}

	@Override
	public String toString()
	{
		return "Dimension{" +
			"logicalName='" + logicalName + '\'' +
			", name='" + name + '\'' +
			", schema='" + schema + '\'' +
			", degenerate=" + degenerate +
			'}';
	}

	public static class DimensionBuilder
	{
		private final ReportingArea profile;
		private String name;
		private String logicalName;
		private String schema;
		private String catalog;
		private boolean degenerate;

		public DimensionBuilder(ReportingArea profile)
		{
			this.profile = profile;
		}

		public DimensionBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public DimensionBuilder catalog(String name)
		{
			this.catalog = name;
			return this;
		}

		public DimensionBuilder degenerate()
		{
			degenerate = true;
			return this;
		}

		public DimensionBuilder logicalName(String logicalName)
		{
			this.logicalName = logicalName;
			return this;
		}

		public DimensionBuilder schema(String sch)
		{
			schema = sch;
			return this;
		}

		public Dimension create()
		{
			if (name == null)
			{
				throw new IllegalStateException("Name must be specified");
			}

			if (schema == null)
			{
				throw new IllegalStateException("Schema must be specified");
			}

			StringBuilder qn = new StringBuilder();

			if (catalog != null)
			{
				qn.append(catalog).append("_");
			}

			qn.append(schema).append("_").append(name);

			return profile.addDimension(new Dimension(profile, ObjectUtils.firstNonNull(logicalName, qn.toString()), name, schema, catalog, degenerate));
		}
	}
}
