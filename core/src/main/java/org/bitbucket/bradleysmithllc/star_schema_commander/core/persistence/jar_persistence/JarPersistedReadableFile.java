package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.jar_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableDirectory;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;

import java.io.*;

public class JarPersistedReadableFile implements PersistedReadableFile
{
	private final JarPersistedProfile persistedProfile;
	private final JarPersistedReadableDirectory persistedReadableDirectory;
	private final String name;

	public JarPersistedReadableFile(JarPersistedProfile jarPersistedProfile, JarPersistedReadableDirectory jarPersistedReadableDirectory, String name)
	{
		persistedProfile = jarPersistedProfile;
		persistedReadableDirectory = jarPersistedReadableDirectory;
		this.name = name;
	}

	@Override
	public InputStream read() throws IOException
	{
		return new BufferedInputStream(persistedProfile.openEntry(persistedReadableDirectory.path() + name));
	}

	@Override
	public Reader reader() throws IOException
	{
		return new BufferedReader(new InputStreamReader(read()));
	}

	@Override
	public boolean isDirectory()
	{
		return false;
	}

	@Override
	public PersistedReadableFile readableFile()
	{
		return this;
	}

	@Override
	public PersistedReadableDirectory readableDirectory()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public String name()
	{
		return name;
	}
}