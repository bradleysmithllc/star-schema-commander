package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class StarJoin extends Removable
{
	private final DatabaseColumn from;
	private final DatabaseColumn to;

	public StarJoin(DatabaseColumn from, DatabaseColumn to)
	{
		this.from = from;
		this.to = to;
	}

	public DatabaseColumn from()
	{
		return from;
	}

	public DatabaseColumn to()
	{
		return to;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		StarJoin starJoin = (StarJoin) o;

		if (!from.equals(starJoin.from))
		{
			return false;
		}

		return to.equals(starJoin.to);
	}

	@Override
	public int hashCode()
	{
		int result = from.hashCode();
		result = 31 * result + to.hashCode();
		return result;
	}
}