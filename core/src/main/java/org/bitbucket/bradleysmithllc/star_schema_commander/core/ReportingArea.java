package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.JsonPersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.RowData;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.sql.*;
import java.text.Collator;
import java.util.*;

public class ReportingArea extends Removable implements Comparable<ReportingArea>, PersistedCollection
{
	private final String name;
	private final String qualifiedName;

	private final Profile profile;
	private final ReportingArea parent;

	private final Map<String, Dimension> dimensionList = new TreeMap<String, Dimension>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, Dimension> logicalDimensionList = new TreeMap<String, Dimension>(Collator.getInstance(Locale.ENGLISH));

	private final Map<String, Fact> factList = new TreeMap<String, Fact>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, Fact> logicalFactList = new TreeMap<String, Fact>(Collator.getInstance(Locale.ENGLISH));

	private final Map<String, ReportingArea> reportingAreaMap = new TreeMap<String, ReportingArea>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, ReportingArea> children = new TreeMap<String, ReportingArea>(Collator.getInstance(Locale.ENGLISH));

	public ReportingArea(String name, ReportingArea parent, Profile profile)
	{
		this.name = name;
		this.parent = parent;
		this.profile = profile;

		qualifiedName = ((parent == null || parent.parent == null) ? "" : parent.qualifiedName + ".") + name;

		// if this is the default scope, add to the map
		if (parent == null)
		{
			reportingAreaMap.put(name, this);
		}
	}

	public Map<String, ReportingArea> children()
	{
		return Collections.unmodifiableMap(children);
	}

	public String name()
	{
		return name;
	}

	public String qualifiedName()
	{
		return qualifiedName;
	}

	public Map<String, ReportingArea> reportingAreas()
	{
		return Collections.unmodifiableMap(reportingAreaMap);
	}

	public void remove()
	{
		if (parent == null)
		{
			throw new IllegalStateException("Cannot remove default reporting area");
		}

		super.remove();
	}

	synchronized Dimension addDimension(Dimension dimension)
	{
		String key = (dimension.catalog() != null ? (dimension.catalog() + ".") : "") + dimension.schema() + "." + dimension.name();

		if (dimensionList.containsKey(key))
		{
			throw new IllegalArgumentException("Duplicate dimension [" + key + "]");
		}

		String logicalKey = dimension.logicalName();
		if (logicalDimensionList.containsKey(logicalKey))
		{
			throw new IllegalArgumentException("Duplicate dimension logical name [" + logicalKey + "]");
		}

		dimension.enter(ReportingArea.this);
		dimensionList.put(key, dimension);
		logicalDimensionList.put(logicalKey, dimension);
		return dimension;
	}

	public Map<String, Dimension> physicalDimensions()
	{
		dimensions();

		return Collections.unmodifiableMap(dimensionList);
	}

	public Map<String, Dimension> dimensions()
	{
		return Collections.unmodifiableMap(logicalDimensionList);
	}

	public Fact addFact(Fact fact)
	{
		String key = (fact.catalog() != null ? (fact.catalog() + ".") : "") + fact.schema() + "." + fact.name();

		if (factList.containsKey(key))
		{
			throw new IllegalArgumentException("Fact [" + key + "] already added");
		}

		String logicalKey = fact.logicalName();
		if (logicalFactList.containsKey(logicalKey))
		{
			throw new IllegalArgumentException("Fact logical name [" + logicalKey + "] already added");
		}

		fact.enter(this);
		factList.put(key, fact);
		logicalFactList.put(logicalKey, fact);

		return fact;
	}

	public Map<String, Fact> physicalFacts()
	{
		facts();

		return Collections.unmodifiableMap(factList);
	}

	public Map<String, Fact> facts()
	{
		return Collections.unmodifiableMap(logicalFactList);
	}

	public void invalidate()
	{
		reportingAreaMap.clear();
		children.clear();
		factList.clear();
		logicalFactList.clear();
		dimensionList.clear();
		logicalDimensionList.clear();
	}

	public ReportingArea parent()
	{
		return parent;
	}

	@Override
	public int compareTo(ReportingArea o)
	{
		return CompareUtils.compare(qualifiedName, o.qualifiedName);
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableDirectory root) throws IOException
	{
		if (removed())
		{
			root.delete();
			return;
		}

		// write the tag file
		Writer writer = root.openWritableFile("reportingArea.json").writer();

		try
		{
			writer.write("{}");
		}
		finally
		{
			writer.close();
		}

		Iterator<Dimension> dit = dimensionList.values().iterator();

		while (dit.hasNext())
		{
			Dimension conn = dit.next();

			PersistedWritableDirectory columns = root.openWritableDirectory("dimensions").openWritableDirectory(conn.schema() + "." + conn.name());

			conn.persist(profile, columns);

			if (conn.removed())
			{
				dit.remove();
			}
		}

		PersistedWritableDirectory factsDir = root.openWritableDirectory("facts");

		Iterator<Fact> fit = factList.values().iterator();

		while (fit.hasNext())
		{
			Fact fact = fit.next();
			PersistedWritableDirectory columns = factsDir.openWritableDirectory(fact.schema() + "." + fact.name());

			fact.persist(profile, columns);

			if (fact.removed())
			{
				fit.remove();
			}
		}

		Iterator<ReportingArea> it = children.values().iterator();

		while (it.hasNext())
		{
			ReportingArea ra = it.next();

			ra.persist(profile, root.openWritableDirectory("reportingAreas").openWritableDirectory(ra.name()));

			if (ra.removed())
			{
				it.remove();
				reportingAreaMap.remove(ra.qualifiedName());
			}
		}

		PersistedWritableDirectory queriesDir = root.openWritableDirectory("queries");

		// do queries here
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableDirectory root) throws IOException
	{
		dimensionList.clear();
		logicalDimensionList.clear();
		factList.clear();
		logicalFactList.clear();
		reportingAreaMap.clear();
		children.clear();

		// if this is the default scope, add back to the map
		if (parent == null)
		{
			reportingAreaMap.put(name, this);
		}

		PersistedReadableDirectory child = root.openReadableDirectory("reportingAreas");

		for (PersistedReadableEntry rae : child.listReadable())
		{
			if (rae.isDirectory())
			{
				ReportingArea ra = newReportingArea().name(rae.name()).create();
				ra.read(profile, rae.readableDirectory());
			}
			else
			{
				/*
				System.out.println("Garbage found in reportingAreas dir: " + rae.name());

				try
				{
					rae.delete();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				 */
			}
		}

		for (PersistedReadableEntry d : root.openReadableDirectory("dimensions").listReadable())
		{
			if (d.isDirectory())
			{
				try
				{
					PersistedReadableDirectory directory = d.readableDirectory();
					Dimension con = Profile.loadFromJson(directory.openReadableFile("dimension.json"), Dimension.class);

					con.read(profile, directory);

					addDimension(con);
				}
				catch (IOException exc)
				{
					System.out.println("Bad dimension found in profile: " + d.name());
					exc.printStackTrace(System.out);
				}
			}
			else
			{
				/*
				System.out.println("Garbage found in dimensions dir: " + d.name());
				try
				{
					d.delete();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				 */
			}
		}

		// load facts from json
		for (PersistedReadableEntry pe : root.openReadableDirectory("facts").listReadable())
		{
			if (pe.isDirectory())
			{
				try
				{
					Fact con = Profile.loadFromJson(pe.readableDirectory().openReadableFile("fact.json"), Fact.class);
					addFact(con);
					con.read(profile, pe.readableDirectory());
				}
				catch (IOException exc)
				{
					System.out.println("Bad fact found in profile: " + pe.name());
					exc.printStackTrace(System.out);
				}
			}
			else
			{
				/*
				System.out.println("Garbage found in facts dir: " + pe.name());
				try
				{
					pe.delete();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				 */
			}
		}

		for (PersistedReadableEntry pe : root.openReadableDirectory("queries").listReadable())
		{
			// read in each saved query.
		}
	}

	public PersistedQuery persist(Query query, DatabaseConnection connection) throws SQLException
	{
		List<String> columns = new ArrayList<String>();
		List<RowData> data = new ArrayList<RowData>();

		// execute the query against the connection and persist
		Connection conn = connection.open();

		try
		{
			String sql = query.query();
			ResultSet res = conn.createStatement().executeQuery(sql);

			ResultSetMetaData md = res.getMetaData();

			columns.clear();

			for (int col = 0; col < md.getColumnCount(); col++)
			{
				columns.add(md.getColumnLabel(col + 1));
			}

			try
			{
				int rowNum = 0;

				while (res.next())
				{
					RowData rd = new RowData(rowNum++);

					// put each column in the row
					for (int i = 1; i <= columns.size(); i++)
					{
						rd.add(res.getString(i));
					}

					data.add(rd);
				}
			}
			finally
			{
				res.close();
			}
		}
		catch (SQLException exc)
		{
			exc.printStackTrace();
		}
		finally
		{
			conn.close();
		}

		// persist and return a reference to it
		File dest = FileUtils.getTempDirectory();

		JsonPersistedQuery jpq = new JsonPersistedQuery(query, null, dest);
		jpq.receiveData(columns, data);
		jpq.write();

		return jpq;
	}

	void addReportingArea(ReportingArea ra)
	{
		if (reportingAreaMap.containsKey(ra.qualifiedName()))
		{
			throw new IllegalArgumentException("Reporting area [" + ra.qualifiedName() + "] already exists");
		}

		reportingAreaMap.put(ra.qualifiedName(), ra);
		children.put(ra.name(), ra);

		// propagate the name upwards
		if (parent != null)
		{
			parent.propagateReportingArea(ra);
		}
	}

	void propagateReportingArea(ReportingArea ra)
	{
		reportingAreaMap.put(ra.qualifiedName(), ra);

		// propagate the name upwards
		if (parent != null)
		{
			parent.propagateReportingArea(ra);
		}
	}

	public ReportingAreaBuilder newReportingArea()
	{
		return new ReportingAreaBuilder(profile, this);
	}

	public Dimension.DimensionBuilder newDimension()
	{
		return new Dimension.DimensionBuilder(this);
	}

	public Fact.FactBuilder newFact()
	{
		return new Fact.FactBuilder(this);
	}

	public Dimension loadDimensionFromMetaData(DatabaseConnection connection, String schema, String name) throws SQLException
	{
		return loadDimensionFromMetaData(connection, null, schema, name, null);
	}

	public Dimension loadDimensionFromMetaData(DatabaseConnection connection, String catalog, String schema, String name) throws SQLException
	{
		return loadDimensionFromMetaData(connection, catalog, schema, name, null);
	}

	public Dimension loadDimensionFromMetaData(DatabaseConnection connection, String catalog, String schema, String name, String logicalName) throws SQLException
	{
		Dimension.DimensionBuilder dimb = newDimension().schema(schema).name(name).catalog(catalog);

		if (logicalName != null)
		{
			dimb.logicalName(logicalName);
		}

		Dimension dim = dimb.create();

		Connection conn = connection.open();

		try
		{
			DatabaseMetaData md = conn.getMetaData();

			ResultSet rs = md.getColumns(catalog, schema, name, null);

			try
			{
				while (rs.next())
				{
					String colName = rs.getString(4);
					int type = rs.getInt(5);

					dim.newDatabaseColumn().name(colName).jdbcType(type).create();

					// create an attribute for each column since we don't know which is an attribute and which is a key
					dim.newDimensionAttribute().column(colName).create();
				}
			}
			finally
			{
				rs.close();
			}

			if (dim.columns().size() == 0)
			{
				String kname = (catalog == null ? "" : (catalog + ".")) + schema + "." + name;

				throw new IllegalStateException("Table [" + kname + "] is not usable.");
			}

			// keys (PK)
			/*
			ResultSet keys = md.getPrimaryKeys(null, schema, name);

			try
			{
				DimensionKey.DimensionKeyBuilder keyBuilder = null;
				String thisIndexName = null;

				while (keys.next())
				{
					String pkName = keys.getString(6);

					if (keyBuilder == null || !thisIndexName.equals(pkName))
					{
						if (keyBuilder != null)
						{
							keyBuilder.create();
						}

						keyBuilder = dim.newDimensionKey().name(pkName);
						thisIndexName = pkName;
					}

					String columnName = keys.getString(4);
					keyBuilder.column(columnName);
					// add to dimension key - make sure the primary key isn't added twice - once from pk and once from indexes.
				}

				if (keyBuilder != null)
				{
					keyBuilder.create();
				}
			}
			finally
			{
				keys.close();
			}
			 */

			// keys (Unique)
			ResultSet keys = md.getIndexInfo(null, schema, name, true, true);

			try
			{
				DimensionKey.DimensionKeyBuilder keyBuilder = null;
				String thisIndexName = "~~~^^^~~~";

				while (keys.next())
				{
					if (keys.getShort(7) == DatabaseMetaData.tableIndexStatistic)
					{
						continue;
					}

					String indexName = keys.getString(6);

					if (dim.keys().containsKey(indexName))
					{
						continue;
					}

					if (keyBuilder == null || !thisIndexName.equals(indexName))
					{
						if (keyBuilder != null)
						{
							keyBuilder.create();
						}

						keyBuilder = dim.newDimensionKey().logicalName(indexName);
						thisIndexName = indexName;
					}

					String columnName = keys.getString(9);
					keyBuilder.column(columnName);
					// add to dimension key - make sure the primary key isn't added twice - once from pk and once from indexes.
				}

				if (keyBuilder != null)
				{
					keyBuilder.create();
				}
			}
			finally
			{
				keys.close();
			}
		}
		finally
		{
			conn.close();
		}

		return dim;
	}

	public Fact loadFactFromMetaData(DatabaseConnection connection, String catalog, String schema, String name) throws SQLException
	{
		return loadFactFromMetaData(connection, catalog, schema, name, null);
	}

	public Fact loadFactFromMetaData(DatabaseConnection connection, String catalog, String schema, String name, String logical) throws SQLException
	{
		Fact.FactBuilder factBuilder = newFact().schema(schema).name(name).catalog(catalog);

		if (logical != null)
		{
			factBuilder = factBuilder.logicalName(logical);
		}

		Fact fact = factBuilder.create();

		Connection conn = connection.open();

		try
		{
			DatabaseMetaData md = conn.getMetaData();

			ResultSet rs = md.getColumns(catalog, schema, name, null);

			try
			{
				while (rs.next())
				{
					String colName = rs.getString(4);
					int type = rs.getInt(5);

					fact.newDatabaseColumn().name(colName).jdbcType(type).create();
				}
			}
			finally
			{
				rs.close();
			}

			if (fact.columns().size() == 0)
			{
				throw new IllegalStateException("Table [" + schema + "." + name + "] is not usable.");
			}
		}
		finally
		{
			conn.close();
		}

		return fact;
	}

	public void generateKeys(DatabaseConnection dbc, KeyObserver observer) throws SQLException
	{
		// this only works with at least one table specified.
		// we do for all dims and facts
		for (Dimension dim : dimensions().values())
		{
			System.out.println("Searching for keys from dimension [" + dim.logicalName() + "]");
			generateKeys(dbc, dim, null, observer);
		}

		for (Fact fact : facts().values())
		{
			System.out.println("Searching for keys in fact [" + fact.logicalName() + "]");
			generateKeys(dbc, null, fact, observer);
		}
	}

	private void generateKeys(DatabaseConnection dbc, Dimension dim, Fact fact, KeyObserver observer) throws SQLException
	{
		boolean exportMode = dim != null;

		System.out.println("Looking for " + (exportMode ? "exported" : "imported") + " keys");

		DatabaseMetaData md = dbc.open().getMetaData();

		// determine table name
		String pkSchema = dim == null ? null : dim.schema();
		String pkTable = dim == null ? null : dim.name();

		String fkSchema = fact == null ? null : fact.schema();
		String fkTable = fact == null ? null : fact.name();

		// keys (Exported)
		ResultSet keys = exportMode ? md.getExportedKeys(null, pkSchema, pkTable) : md.getImportedKeys(null, fkSchema, fkTable);

		try
		{
			Map<String, KeyRef> keyMap = new HashMap<String, KeyRef>();

			while (keys.next())
			{
				// PK will be the dimension
				String pkName = keys.getString(13);
				String PKSchema = keys.getString(2);
				String PKTable = keys.getString(3);
				String PKColumn = keys.getString(4);

				// check the pk side
				if (pkSchema != null && !PKSchema.equals(pkSchema))
				{
					continue;
				}

				if (pkTable != null && !PKTable.equals(pkTable))
				{
					continue;
				}

				// fk is on the fact side
				String fkName = keys.getString(12);
				String FKSchema = keys.getString(6);
				String FKTable = keys.getString(7);
				String FKColumn = keys.getString(8);

				// check the fk side
				if (fkSchema != null && !FKSchema.equals(fkSchema))
				{
					continue;
				}

				if (fkTable != null && !FKTable.equals(fkTable))
				{
					continue;
				}

				// record in the map(s)
				String key = pkName + "." + fkName;

				System.out.println("Evaluating key " + key);

				if (!keyMap.containsKey(key))
				{
					keyMap.put(key, new KeyRef(PKSchema, PKTable, pkName, FKSchema, FKTable, fkName));
				}

				// add the column to the key ref
				keyMap.get(key).joinColumns.put(PKColumn, FKColumn);
			}

			// now that all keys have been read - look for the dimension on the pk side and the fact on the fk side.
			// in any case where the table on both sides isn't present, skip
			for (KeyRef keyRef : keyMap.values())
			{
				String dimKey = keyRef.pkSchema + "." + keyRef.pkTable;
				Dimension pk_dim = dimensionList.get(dimKey);

				if (pk_dim == null)
				{
					System.out.println("Dimension [" + dimKey + "] not found");
					continue;
				}

				String factKey = keyRef.fkSchema + "." + keyRef.fkTable;
				Fact fk_fact = factList.get(factKey);

				if (fk_fact == null)
				{
					System.out.println("Fact [" + factKey + "] not found");
					continue;
				}

				if (observer != null)
				{
					observer.key(pk_dim, fk_fact, keyRef.pkName, keyRef.fkName);
				}

				// fact and dim exist.  get the dim key on the pk side
				DimensionKey dk = getDimKey(pk_dim, keyRef);
				getFactKey(fk_fact, dk, keyRef);
			}
		}
		finally
		{
			keys.close();
		}
	}

	private FactDimensionKey getFactKey(Fact fact, DimensionKey dk, KeyRef keyRef)
	{
		FactDimensionKey fdk = fact.physicalKeys().get(keyRef.fkName);

		if (fdk == null)
		{
			System.out.println("Generating new fact key [" + keyRef.fkName + "]");

			FactDimensionKey.FactDimensionKeyBuilder fdkb = fact.newFactDimensionKey().physicalName(keyRef.fkName);

			for (Map.Entry<String, String> join : keyRef.joinColumns.entrySet())
			{
				fdkb.join(join.getValue(), join.getKey());
			}

			fdkb.dimension(dk.dimension().logicalName()).keyName(dk.physicalName());

			// check here for role playing
			Map<String, FactDimensionKey> existingKeys = fact.keys(dk.dimension());

			if (existingKeys.size() != 0)
			{
				// if role playing the role name must be supplied
				fdkb.roleName(keyRef.fkName);
			}

			return fdkb.create();
		}
		else
		{
			System.out.println("Evaluating fact key [" + fdk.name() + "]");

			// validate columns
			if (fdk.joins().size() != keyRef.joinColumns.size())
			{
				throw new IllegalStateException("Fact [" + fact.logicalName() + "] FK Column count differs from dimension key [" + fdk.name() + "] in join criteria for foreign key [" + keyRef.fkName + "]");
			}

			for (StarJoin join : fdk.joins())
			{
				String fr = join.from().name();
				String to = join.to().name();

				if (!keyRef.joinColumns.containsKey(to))
				{
					throw new IllegalStateException("Fact [" + fact.logicalName() + "] PK Column [" + to + "] referenced in foreign key [" + fdk.name() + "] not present in join criteria for primary key [" + keyRef.pkName + "]");
				}
				else
				{
					if (!keyRef.joinColumns.get(to).equals(fr))
					{
						throw new IllegalStateException("Fact [" + fact.logicalName() + "] FK Column [" + fr + "] referenced in foreign key [" + fdk.name() + "] not present in join criteria for primary key [" + keyRef.pkName + "]");
					}
				}
			}
		}

		return fdk;
	}

	private DimensionKey getDimKey(Dimension dim, KeyRef keyRef)
	{
		DimensionKey dk = dim.physicalKeys().get(keyRef.pkName);

		if (dk == null)
		{
			System.out.println("Generating new dim key [" + keyRef.pkName + "]");

			// generate new one
			DimensionKey.DimensionKeyBuilder ndk = dim.newDimensionKey().logicalName(keyRef.pkName).physicalName(keyRef.pkName);

			// key ref keys are on the pk side
			for (String col : keyRef.joinColumns.keySet())
			{
				ndk.column(col);
			}

			return ndk.create();
		}
		else
		{
			System.out.println("Evaluating dim key [" + dk.physicalName() + "]");

			if (dk.columns().size() != keyRef.joinColumns.size())
			{
				throw new IllegalStateException("Dimension [" + dim.logicalName() + "] PK Column count differs from dimension key [" + dk.physicalName() + "] in join criteria for foreign key [" + keyRef.fkName + "]");
			}

			// check for compatibility
			for (DatabaseColumn dkcol : dk.columns())
			{
				if (!keyRef.joinColumns.containsKey(dkcol.name()))
				{
					throw new IllegalStateException("Dimension [" + dim.logicalName() + "] PK Column [" + dkcol.name() + "] referenced in dimension key [" + dk.physicalName() + "] not present in join criteria for foreign key [" + keyRef.fkName + "]");
				}
			}
		}

		return dk;
	}

	public static class ReportingAreaBuilder
	{
		private final Profile profile;
		private final ReportingArea parent;
		private String name;

		public ReportingAreaBuilder(Profile profile, ReportingArea parent)
		{
			this.parent = parent;
			this.profile = profile;
		}

		public ReportingAreaBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public ReportingArea create()
		{
			if (name == null)
			{
				throw new IllegalStateException("Name must be supplied");
			}

			if (name.indexOf('.') != -1)
			{
				throw new IllegalArgumentException("Name must must not contain dots [.]");
			}

			ReportingArea ra = new ReportingArea(name, parent, profile);

			if (parent != null)
			{
				parent.addReportingArea(ra);
			}

			return ra;
		}

	}
}

class KeyRef
{
	final String pkName;
	final String pkTable;
	final String pkSchema;

	final String fkName;
	final String fkTable;
	final String fkSchema;

	Map<String, String> joinColumns = new HashMap<String, String>();

	public KeyRef(String pkSchema, String pkTable, String pkn, String fkSchema, String fkTable, String fkn)
	{
		this.pkSchema = pkSchema;
		this.pkTable = pkTable;

		this.fkSchema = fkSchema;
		this.fkTable = fkTable;
		pkName = pkn;
		fkName = fkn;
	}
}
