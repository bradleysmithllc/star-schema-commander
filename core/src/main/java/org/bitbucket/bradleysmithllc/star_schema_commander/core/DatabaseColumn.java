package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Field;
import java.sql.Types;
import java.util.Iterator;
import java.util.Map;

public class DatabaseColumn extends Removable implements Comparable<DatabaseColumn>, PersistedObject
{
	private final String name;
	private final int jdbcType;

	DatabaseColumn(String name, int jdbcType)
	{
		this.name = name;
		this.jdbcType = jdbcType;
	}

	public static void readFromContainer(PersistedReadableDirectory root, ColumnStore dimension) throws IOException
	{
		PersistedReadableFile cols = root.openReadableFile("columns.json");

		Reader reader = cols.reader();

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			// iterate through name/value pairs
			Iterator<Map.Entry<String, JsonNode>> it = node.fields();

			while (it.hasNext())
			{
				Map.Entry<String, JsonNode> field = it.next();

				// key is column name, value is the JDBC type name
				String colName = field.getKey();
				String colTypeName = field.getValue().asText();

				// use builder to create column
				dimension.add(colName, colTypeName);
			}
		}
		finally
		{
			reader.close();
		}
	}

	public static void writeToContainer(PersistedWritableDirectory root, ColumnStore dimension) throws IOException
	{
		PersistedWritableFile cols = root.openWritableFile("columns.json");

		Writer colWriter = cols.writer();

		try
		{
			JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(colWriter));
			jsonWriter.beginObject();

			Iterator<DatabaseColumn> it = dimension.mutableColumns().values().iterator();

			while (it.hasNext())
			{
				DatabaseColumn col = it.next();

				if (col.removed())
				{
					it.remove();
				}
				else
				{
					jsonWriter.name(col.name());
					jsonWriter.value(col.jdbcTypeName());
				}
			}

			jsonWriter.endObject();
			jsonWriter.flush();

			jsonWriter.close();
		}
		finally
		{
			colWriter.close();
		}
	}

	public String name()
	{
		return name;
	}

	public int jdbcType()
	{
		return jdbcType;
	}

	public String jdbcTypeName()
	{
		switch (jdbcType)
		{
			case Types.BIT:
				return "BIT";
			case Types.TINYINT:
				return "TINYINT";
			case Types.SMALLINT:
				return "SMALLINT";
			case Types.INTEGER:
				return "INTEGER";
			case Types.BIGINT:
				return "BIGINT";
			case Types.FLOAT:
				return "FLOAT";
			case Types.REAL:
				return "REAL";
			case Types.DOUBLE:
				return "DOUBLE";
			case Types.NUMERIC:
				return "NUMERIC";
			case Types.DECIMAL:
				return "DECIMAL";
			case Types.CHAR:
				return "CHAR";
			case Types.VARCHAR:
				return "VARCHAR";
			case Types.LONGVARCHAR:
				return "LONGVARCHAR";
			case Types.DATE:
				return "DATE";
			case Types.TIME:
				return "TIME";
			case Types.TIMESTAMP:
				return "TIMESTAMP";
			case Types.BINARY:
				return "BINARY";
			case Types.VARBINARY:
				return "VARBINARY";
			case Types.LONGVARBINARY:
				return "LONGVARBINARY";
			case Types.NULL:
				return "NULL";
			case Types.OTHER:
				return "OTHER";
			case Types.JAVA_OBJECT:
				return "JAVA_OBJECT";
			case Types.DISTINCT:
				return "DISTINCT";
			case Types.STRUCT:
				return "STRUCT";
			case Types.ARRAY:
				return "ARRAY";
			case Types.BLOB:
				return "BLOB";
			case Types.CLOB:
				return "CLOB";
			case Types.REF:
				return "REF";
			case Types.DATALINK:
				return "DATALINK";
			case Types.BOOLEAN:
				return "BOOLEAN";
			case Types.ROWID:
				return "ROWID";
			case Types.NCHAR:
				return "NCHAR";
			case Types.NVARCHAR:
				return "NVARCHAR";
			case Types.LONGNVARCHAR:
				return "LONGNVARCHAR";
			case Types.NCLOB:
				return "NCLOB";
			case Types.SQLXML:
				return "SQLXML";
			default:
				throw new UnsupportedOperationException("Bad JDBC Type {" + jdbcType + "}");
		}
	}

	@Override
	public int compareTo(DatabaseColumn o)
	{
		return CompareUtils.compare(name, o.name);
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		if (removed())
		{
			file.delete();
		}
		else
		{
			BufferedWriter writer = new BufferedWriter(file.writer());

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("name").value(name);
				jsonWriter.name("jdbc-type").value(jdbcType);

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{

	}

	/*Used to generate the list of all names in the switch statement*/
	/*
	public static void main(String[] argv)
	{
		Field[] fields = Types.class.getDeclaredFields();

		for (Field field : fields)
		{
			System.out.println("case Types." + field.getName() + ": return \"" + field.getName() + "\";");
		}
	}
	 */

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		DatabaseColumn that = (DatabaseColumn) o;

		if (jdbcType != that.jdbcType)
		{
			return false;
		}

		return name.equals(that.name);
	}

	@Override
	public int hashCode()
	{
		int result = name.hashCode();
		result = 31 * result + jdbcType;
		return result;
	}

	public static class DatabaseColumnBuilder
	{
		private final ColumnStore profile;

		private String name;
		private int jdbcType = Integer.MIN_VALUE;

		public DatabaseColumnBuilder(ColumnStore profile)
		{
			this.profile = profile;
		}

		public DatabaseColumnBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public DatabaseColumnBuilder jdbcType(int type)
		{
			jdbcType = type;
			return this;
		}

		public DatabaseColumnBuilder jdbcTypeName(String name)
		{
			jdbcType(decodeJdbcTypeName(name));
			return this;
		}

		private int decodeJdbcTypeName(String name)
		{
			try
			{
				Field field = Types.class.getDeclaredField(name);
				return field.getInt(null);
			}
			catch (NoSuchFieldException e)
			{
				throw new IllegalArgumentException("Not a JDBC type name: " + name, e);
			}
			catch (Exception e)
			{
				throw new IllegalArgumentException("Bad JDBC type name: " + name, e);
			}
		}

		public DatabaseColumn create()
		{
			if (name == null)
			{
				throw new IllegalArgumentException("Name may not be null");
			}

			if (jdbcType == Integer.MIN_VALUE)
			{
				throw new IllegalArgumentException("jdbcType may not be null");
			}

			DatabaseColumn databaseColumn = new DatabaseColumn(name, jdbcType);

			// validate the type parameter
			databaseColumn.jdbcTypeName();

			return profile.addDatabaseColumn(databaseColumn);
		}
	}
}
