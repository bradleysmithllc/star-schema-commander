package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class OracleConnectionBuilder extends BaseConnectionBuilder<OracleConnectionBuilder>
{
	private String serviceName;

	public OracleConnectionBuilder(Profile profile)
	{
		super(profile);

		port(1521);
	}

	public OracleConnectionBuilder name(String n)
	{
		name = n;
		return this;
	}

	public OracleConnectionBuilder serviceName(String server)
	{
		serviceName = server;
		return this;
	}

	@Override
	public OracleConnectionBuilder serverName(String server)
	{
		return super.serverName(server);
	}

	@Override
	protected propertyRequired requiresServerName()
	{
		return propertyRequired.required;
	}

	@Override
	public OracleConnectionBuilder port(int p)
	{
		return super.port(p);
	}

	@Override
	protected propertyRequired requiresPort()
	{
		return propertyRequired.optional;
	}

	@Override
	public OracleConnectionBuilder username(String user)
	{
		return super.username(user);
	}

	@Override
	protected propertyRequired requiresUserName()
	{
		return propertyRequired.required;
	}

	@Override
	public OracleConnectionBuilder password(String pass)
	{
		return super.password(pass);
	}

	@Override
	protected propertyRequired requiresPassword()
	{
		return propertyRequired.required;
	}

	protected DatabaseConnection createSub()
	{
		DatabaseConnection dbo = new DatabaseConnection(name, properties("jdbc:oracle:thin:@//" + serverName + (port != 1521 ? (":" + port) : "") + (serviceName != null ? ("/" + serviceName) : "")),
			"oracle.jdbc.OracleDriver",
			username,
			username,
			password
		);
		return profile.addDatabaseConnection(dbo);
	}

	@Override
	protected String getVendorName()
	{
		return "Oracle";
	}
}