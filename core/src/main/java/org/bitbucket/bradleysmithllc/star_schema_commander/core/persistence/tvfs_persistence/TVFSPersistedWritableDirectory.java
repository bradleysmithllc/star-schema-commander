package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.tvfs_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableDirectory;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;

import java.io.IOException;

public class TVFSPersistedWritableDirectory implements PersistedWritableDirectory
{
	private final TVFSPersistedProfile jarPersistedProfile;
	private final String path;

	public TVFSPersistedWritableDirectory(TVFSPersistedProfile jpp)
	{
		this(jpp, null, null);
	}

	public TVFSPersistedWritableDirectory(TVFSPersistedProfile jpp, String p, String name)
	{
		path = p;
		jarPersistedProfile = jpp;

		// always allocate the directory on construction
		jarPersistedProfile.checkPath(name, key());
	}

	@Override
	public PersistedWritableFile openWritableFile(String name) throws IOException
	{
		return new TVFSPersistedWritableFile(this, jarPersistedProfile, name);
	}

	@Override
	public PersistedWritableDirectory openWritableDirectory(final String name) throws IOException
	{
		return new TVFSPersistedWritableDirectory(jarPersistedProfile, key() + name, name);
	}

	@Override
	public void delete() throws IOException
	{
		jarPersistedProfile.deallocateDirectory(key());
	}

	@Override
	public boolean isDirectory()
	{
		return true;
	}

	@Override
	public PersistedWritableFile writableFile()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PersistedWritableDirectory writableDirectory()
	{
		return this;
	}

	@Override
	public String name()
	{
		return path;
	}

	public String key()
	{
		return path == null ? "/" : (path + "/");
	}
}
