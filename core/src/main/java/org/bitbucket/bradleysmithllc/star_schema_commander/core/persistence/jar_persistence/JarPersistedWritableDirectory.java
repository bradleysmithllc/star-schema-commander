package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.jar_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableDirectory;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;

import java.io.IOException;

public class JarPersistedWritableDirectory implements PersistedWritableDirectory
{
	private final JarPersistedProfile persistedProfile;
	private final JarPersistedWritableDirectory persistedWritableDirectory;
	private final String name;

	public JarPersistedWritableDirectory(JarPersistedProfile jpp)
	{
		this(jpp, null, null);
	}

	public JarPersistedWritableDirectory(JarPersistedProfile jpp, JarPersistedWritableDirectory parent, String p)
	{
		name = p;
		persistedProfile = jpp;
		persistedWritableDirectory = parent;

		// always allocate the directory on construction
		persistedProfile.checkPath(path());
	}

	public String path()
	{
		if (persistedWritableDirectory == null)
		{
			return "";
		}
		else if (persistedWritableDirectory.path().equals(""))
		{
			return name;
		}
		else
		{
			return persistedWritableDirectory.path() + "/" + name;
		}
	}

	@Override
	public PersistedWritableFile openWritableFile(String name) throws IOException
	{
		return new JarPersistedWritableFile(this, persistedProfile, name);
	}

	@Override
	public PersistedWritableDirectory openWritableDirectory(final String name) throws IOException
	{
		return new JarPersistedWritableDirectory(persistedProfile, this, name);
	}

	@Override
	public void delete() throws IOException
	{
		persistedProfile.deallocateDirectory(path());
	}

	@Override
	public boolean isDirectory()
	{
		return true;
	}

	@Override
	public PersistedWritableFile writableFile()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PersistedWritableDirectory writableDirectory()
	{
		return this;
	}

	@Override
	public String name()
	{
		return name;
	}
}
