package org.bitbucket.bradleysmithllc.star_schema_commander.core.query;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

public class Selector<T extends Comparable<T>> implements Comparable<Selector<T>>
{
	private final T t;
	private final Query.aggregation _aggregation;
	private final String alias;

	public Selector(T t, Query.aggregation aggregation, String alias)
	{
		this.t = t;
		_aggregation = aggregation;
		this.alias = alias;
	}

	public T it()
	{
		return t;
	}

	public Query.aggregation aggregation()
	{
		return _aggregation;
	}

	public String alias()
	{
		return alias;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Selector<?> selector = (Selector<?>) o;

		if (!t.equals(selector.t))
		{
			return false;
		}
		if (_aggregation != selector._aggregation)
		{
			return false;
		}
		return !(alias != null ? !alias.equals(selector.alias) : selector.alias != null);

	}

	@Override
	public int hashCode()
	{
		int result = t.hashCode();
		result = 31 * result + _aggregation.hashCode();
		result = 31 * result + (alias != null ? alias.hashCode() : 0);
		return result;
	}

	@Override
	public int compareTo(Selector<T> o)
	{
		return CompareUtils.compare(t, o.t, _aggregation, o._aggregation, alias, o.alias);
	}
}
