package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.jar_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.FileChangeWatcher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.ReadVisitor;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.WriteVisitor;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class JarPersistedProfile implements PersistedProfile, FileChangeWatcher.FileChangeListener
{
	private final File jarFile;
	private final List<ProfileChangeListener> profileChangeListeners = new ArrayList<ProfileChangeListener>();
	private final FileChangeWatcher jarFileWatcher;

	private final File assembly;
	private final Map<String, ZipEntry> zfentries = new HashMap<String, ZipEntry>();
	private final Map<String, List<String>> zfhierarchy = new HashMap<String, List<String>>();
	Map<String, String> zfdirectories = new HashMap<String, String>();
	Map<String, List<String>> contents = new HashMap<String, List<String>>();
	Map<String, Pair<String, File>> fileData = new HashMap<String, Pair<String, File>>();
	private ZipFile zFile;

	public JarPersistedProfile(File jarFile) throws IOException
	{
		this.jarFile = jarFile;
		jarFileWatcher = new FileChangeWatcher(jarFile);

		assembly = new File(FileUtils.getTempDirectory(), jarFile.getName() + "_" + System.currentTimeMillis());
	}

	@Override
	public File filesystemLocation()
	{
		return jarFile;
	}

	@Override
	public void addProfileChangeListener(ProfileChangeListener listener)
	{
		profileChangeListeners.add(listener);

		if (profileChangeListeners.size() == 1)
		{
			jarFileWatcher.watch(this);
		}
	}

	@Override
	public void removeProfileChangeListener(ProfileChangeListener listener) throws IOException
	{
		if (!profileChangeListeners.remove(listener))
		{
			throw new IllegalArgumentException("Listener not registered");
		}

		profileChangeListeners.remove(listener);

		release();
	}

	private void release()
	{
		if (profileChangeListeners.size() != 0)
		{
			releaseProfileChangeListeners();
		}
	}

	@Override
	public int listenerCount()
	{
		return profileChangeListeners.size();
	}

	@Override
	public void releaseProfileChangeListeners()
	{
		profileChangeListeners.clear();

		release();
	}

	@Override
	public void read(ReadVisitor visitor) throws IOException
	{
		StopWatch stw = new StopWatch();
		stw.start();

		zfentries.clear();
		zfhierarchy.clear();
		zfdirectories.clear();

		zFile = new ZipFile(jarFile);

		try
		{
			// catalog the zip file
			Enumeration<? extends ZipEntry> entries = zFile.entries();

			while (entries.hasMoreElements())
			{
				ZipEntry entry = entries.nextElement();

				String name = entry.getName();

				if (name.endsWith("/"))
				{
					name = name.substring(0, name.length() - 1);
					zfdirectories.put(name, name);
				}

				catalogPath(name);
				zfentries.put(name, entry);
			}

			visitor.visitReader(this, new JarPersistedReadableDirectory(this));
		}
		finally
		{
			zfentries.clear();
			zfhierarchy.clear();
			zFile.close();
			zFile = null;
		}

		stw.stop();
		System.out.println("Read profile in " + stw.getTime() + " ms");
	}

	private void catalogPath(String name)
	{
		// remove leading '/'
		//String nname = name.substring(1);

		String[] names = name.split("/");

		String path = "";

		// proceed from left to right, cataloging
		for (int i = 0; i < names.length; i++)
		{
			List<String> list = zfhierarchy.get(path);

			if (list == null)
			{
				list = new ArrayList<String>();
				zfhierarchy.put(path, list);
			}

			if (!list.contains(names[i]))
			{
				list.add(names[i]);
			}

			path = path.equals("") ? (names[i] + "/") : (path + names[i] + "/");
		}
	}

	@Override
	public void write(WriteVisitor visitor) throws IOException
	{
		StopWatch stw = new StopWatch();
		stw.start();

		if (assembly.exists())
		{
			FileUtils.cleanDirectory(assembly);
		}
		else
		{
			FileUtils.forceMkdir(assembly);
		}

		// clear out persistence records
		contents.clear();
		fileData.clear();

		JarPersistedWritableDirectory writableDirectory = new JarPersistedWritableDirectory(this);

		try
		{
			StopWatch stw2 = new StopWatch();
			stw2.start();
			visitor.visitWriter(this, writableDirectory);
			stw2.stop();

			System.out.println("Visited profile in " + stw2.getTime() + " ms");
		}
		finally
		{
			assemble();
		}

		stw.stop();
		System.out.println("Persisted profile in " + stw.getTime() + " ms");
	}

	private void assemble() throws IOException
	{
		File assemblyTemp = new File(assembly, jarFile.getName() + "$tmp");

		StopWatch stw = new StopWatch();

		stw.start();
		ZipOutputStream zout = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(assemblyTemp), 128 * 1024));
		zout.setMethod(ZipOutputStream.DEFLATED);

		try
		{
			for (Map.Entry<String, List<String>> entry : contents.entrySet())
			{
				// add the directory
				String key = entry.getKey();

				String dirName = key + "/";

				if (!key.equals(""))
				{
					ZipEntry ze = new ZipEntry(dirName);
					zout.putNextEntry(ze);
					zout.closeEntry();
				}
				else
				{
					dirName = "";
				}

				// add each file
				for (String entryName : entry.getValue())
				{
					String pathKey = dirName + entryName;
					Pair<String, File> fd = fileData.get(pathKey);

					String fileName = dirName + fd.getLeft();
					ZipEntry ze = new ZipEntry(fileName);

					zout.putNextEntry(ze);

					try
					{
						FileReader fileReader = new FileReader(fd.getRight());

						try
						{
							IOUtils.copy(fileReader, zout);
						}
						finally
						{
							fileReader.close();
						}
					}
					finally
					{
						zout.closeEntry();
					}
				}
			}
		}
		finally
		{
			zout.close();
		}

		stw.stop();
		System.out.println("Took " + stw.getTime() + " ms to write out profile");
		stw.reset();
		stw.start();

		// create a remote temp file
		File jarTemp = new File(jarFile.getParentFile(), assemblyTemp.getName());
		FileUtils.copyFile(assemblyTemp, jarTemp);

		stw.stop();
		System.out.println("Took " + stw.getTime() + " ms to copy to zip temp");
		stw.reset();
		stw.start();

		// replace target with temp
		if (jarFile.exists())
		{
			FileUtils.forceDelete(jarFile);
		}

		FileUtils.moveFile(jarTemp, jarFile);

		stw.stop();
		System.out.println("Took " + stw.getTime() + " ms to replace target profile");
		stw.reset();
		stw.start();

		// destroy working
		FileUtils.forceDelete(assembly);
		stw.stop();

		System.out.println("Took " + stw.getTime() + " clean up temp");
	}

	void checkPath(String key)
	{
		if (!contents.containsKey(key))
		{
			contents.put(key, new ArrayList<String>());
		}
	}

	File allocateFile(String key, String name)
	{
		String path = key.equals("") ? name : (key + "/" + name);

		List<String> strings = contents.get(key);

		if (strings.contains(name))
		{
			return fileData.get(path).getRight();
		}
		else
		{
			File file = new File(assembly, path.replace("/", "$"));
			fileData.put(path, new ImmutablePair<String, File>(name, file));
			strings.add(name);
			return file;
		}
	}

	void deallocateFile(String key, String name)
	{
		contents.get(key).remove(name);
	}

	void deallocateDirectory(String key)
	{
		contents.remove(key);
	}

	@Override
	public void pathInitialized(File path)
	{
		for (ProfileChangeListener list : profileChangeListeners)
		{
			list.profileInitialized();
		}
	}

	@Override
	public void pathCreated(File path)
	{
		for (ProfileChangeListener list : profileChangeListeners)
		{
			list.profileChanged();
		}
	}

	@Override
	public void pathDeleted(File path)
	{
		// ignore this - it will only break
	}

	@Override
	public void pathModified(File path)
	{
		pathCreated(path);
	}

	public InputStream openEntry(String s) throws IOException
	{
		return zFile.getInputStream(zfentries.get(s));
	}

	public List<String> listNames(String path)
	{
		return zfhierarchy.get(path);
	}

	public boolean isDirectoryPath(String s)
	{
		return zfdirectories.containsKey(s);
	}
}
