package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedObject;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;
import org.jasypt.util.text.BasicTextEncryptor;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection extends Removable implements PersistedObject
{
	private String name;
	private String userName;
	private String password;
	private String jdbcUrl;
	private String driverClass;
	private String defaultSchema;

	DatabaseConnection()
	{
	}

	DatabaseConnection(String name, String jdbcUrl, String driverClass, String defaultSchema, String userName, String password)
	{
		this.name = name;
		this.jdbcUrl = jdbcUrl;
		this.driverClass = driverClass;
		this.defaultSchema = defaultSchema;
		this.userName = userName;
		this.password = password;
	}

	public static void validate(File connections)
	{
		connections.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File pathname)
			{
				if (!pathname.isFile())
				{
					throw new IllegalStateException("Directory not part of Star(schema) Commander profile: [" + pathname.getName() + "]");
				}
				else if (!pathname.getName().endsWith(".json"))
				{
					throw new IllegalStateException("File not part of Star(schema) Commander profile: [" + pathname.getName() + "]");
				}

				return false;
			}
		});
	}

	public String name()
	{
		return name;
	}

	public String jdbcUrl()
	{
		return jdbcUrl;
	}

	public String driverClass()
	{
		return driverClass;
	}

	public String defaultSchema()
	{
		return defaultSchema;
	}

	public String userName()
	{
		return userName;
	}

	public String password()
	{
		return password;
	}

	private BasicTextEncryptor getBasicTextEncryptor()
	{
		BasicTextEncryptor passwordEncryptor = new BasicTextEncryptor();
		passwordEncryptor.setPassword(getClass().getCanonicalName());
		return passwordEncryptor;
	}

	public Connection open() throws SQLException
	{
		try
		{
			Class.forName(driverClass);
		}
		catch (ClassNotFoundException e)
		{
			throw new IllegalArgumentException("Error loading driver class", e);
		}

		return DriverManager.getConnection(jdbcUrl, userName, password);
	}

	public void decryptPassword()
	{
		if (password != null)
		{
			BasicTextEncryptor passwordEncryptor = getBasicTextEncryptor();
			password = passwordEncryptor.decrypt(password);
		}
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		if (removed())
		{
			file.delete();
		}
		else
		{
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(file.write()));

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("name").value(name);
				jsonWriter.name("user-name").value(userName);

				String textPassword = password;

				if (textPassword != null)
				{
					BasicTextEncryptor passwordEncryptor = getBasicTextEncryptor();
					textPassword = passwordEncryptor.encrypt(password);
				}

				jsonWriter.name("password").value(textPassword);

				jsonWriter.name("jdbc-url").value(jdbcUrl);
				jsonWriter.name("driver-class").value(driverClass);
				jsonWriter.name("default-schema").value(defaultSchema);

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile pf) throws IOException
	{
		JsonNode node = Profile.loadJson(pf);

		name = node.get("name").asText();

		JsonNode usernd = node.get("user-name");

		if (!usernd.isNull())
		{
			userName = usernd.asText();
		}

		JsonNode passwordnd = node.get("password");

		if (!passwordnd.isNull())
		{
			password = passwordnd.asText();
		}

		jdbcUrl = node.get("jdbc-url").asText();
		driverClass = node.get("driver-class").asText();
		defaultSchema = node.get("default-schema").asText();

		decryptPassword();
	}
}
