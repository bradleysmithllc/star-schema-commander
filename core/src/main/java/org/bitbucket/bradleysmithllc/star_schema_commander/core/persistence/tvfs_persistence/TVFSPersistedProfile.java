package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.tvfs_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileOutputStream;
import net.java.truevfs.access.TVFS;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.FileChangeWatcher;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.ReadVisitor;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.WriteVisitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TVFSPersistedProfile implements PersistedProfile, FileChangeWatcher.FileChangeListener
{
	private final TFile jarFile;
	private final List<ProfileChangeListener> profileChangeListeners = new ArrayList<ProfileChangeListener>();
	private final FileChangeWatcher jarFileWatcher;

	private final File assembly;

	Map<String, Pair<String, List<String>>> contents = new HashMap<String, Pair<String, List<String>>>();
	Map<String, Pair<String, File>> fileData = new HashMap<String, Pair<String, File>>();

	public TVFSPersistedProfile(File fjarFile) throws IOException
	{
		this.jarFile = new TFile(fjarFile.getAbsolutePath());
		jarFileWatcher = new FileChangeWatcher(jarFile);

		assembly = new File(FileUtils.getTempDirectory(), jarFile.getName() + "_" + System.currentTimeMillis());

		FileUtils.forceMkdir(assembly);
	}

	@Override
	public File filesystemLocation()
	{
		return jarFile;
	}

	@Override
	public void addProfileChangeListener(ProfileChangeListener listener)
	{
		profileChangeListeners.add(listener);

		if (profileChangeListeners.size() == 1)
		{
			jarFileWatcher.watch(this);
		}
	}

	@Override
	public void removeProfileChangeListener(ProfileChangeListener listener) throws IOException
	{
		int size = profileChangeListeners.size();

		if (!profileChangeListeners.remove(listener))
		{
			throw new IllegalArgumentException("Listener not registered");
		}

		release(size);
	}

	private void release(int c)
	{
		if (profileChangeListeners.size() != 0)
		{
			releaseProfileChangeListeners();
		}
	}

	@Override
	public int listenerCount()
	{
		return profileChangeListeners.size();
	}

	@Override
	public void releaseProfileChangeListeners()
	{
		int size = profileChangeListeners.size();

		profileChangeListeners.clear();

		release(size);
	}

	@Override
	public void read(ReadVisitor visitor) throws IOException
	{
		TVFSPersistedReadableDirectory writableDirectory = new TVFSPersistedReadableDirectory(this);

		try
		{
			visitor.visitReader(this, writableDirectory);
		}
		finally
		{
			// close resources
			TVFS.umount(jarFile);
		}
	}

	@Override
	public void write(WriteVisitor visitor) throws IOException
	{
		TVFSPersistedWritableDirectory writableDirectory = new TVFSPersistedWritableDirectory(this);

		try
		{
			visitor.visitWriter(this, writableDirectory);
		}
		finally
		{
			assemble();
		}
	}

	private void assemble() throws IOException
	{
		long lastModified = jarFile.lastModified();

		if (jarFile.exists())
		{
			jarFile.rm_r();
		}

		for (Map.Entry<String, Pair<String, List<String>>> entry : contents.entrySet())
		{
			// add the directory
			String key = entry.getKey();

			TFile tfDir = recursiveTFile(jarFile, key);

			// add each file
			for (String entryName : entry.getValue().getRight())
			{
				Pair<String, File> fd = fileData.get(key + entryName);

				TFile tf = new TFile(tfDir, fd.getLeft());
				//ze = new ZipEntry(pathName);

				//zout.putNextEntry(ze);

				OutputStream out = new TFileOutputStream(tf);

				try
				{
					FileInputStream fileReader = new FileInputStream(fd.getRight());

					try
					{
						IOUtils.copy(fileReader, out);
					}
					finally
					{
						fileReader.close();
					}
				}
				finally
				{
					out.close();
				}
			}
		}

		TVFS.umount(jarFile);

		// double-check modification date is at least one second from the last date
		if (lastModified == jarFile.lastModified())
		{
			long currentTime = System.currentTimeMillis();
			long targetTime = System.currentTimeMillis() + (1000L - currentTime % 1000L) + 100L;

			while (System.currentTimeMillis() < targetTime)
			{
				try
				{
					Thread.sleep(100L);
					jarFile.setLastModified(System.currentTimeMillis());
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private TFile recursiveTFile(TFile jarTFile, String key)
	{
		// ignore last '/'
		key = key.substring(0, key.length() - 1);

		// split on '/'
		String[] path = key.split("/");

		for (String pathSeg : path)
		{
			jarTFile = new TFile(jarTFile, pathSeg);
		}

		return jarTFile;
	}

	void checkPath(String name, String key)
	{
		if (!contents.containsKey(key))
		{
			contents.put(key, new ImmutablePair<String, List<String>>(name, new ArrayList<String>()));
		}
	}

	File allocateFile(String key, String name)
	{
		String path = key + name;

		Pair<String, List<String>> strings = contents.get(key);
		if (strings.getRight().contains(name))
		{
			return fileData.get(path).getRight();
		}
		else
		{
			File file = new File(assembly, path.replace("/", "$"));
			fileData.put(path, new ImmutablePair<String, File>(name, file));
			strings.getRight().add(name);
			return file;
		}
	}

	void deallocateFile(String key, String name)
	{
		List<String> right = contents.get(key).getRight();
		right.remove(name);
	}

	void deallocateDirectory(String key)
	{
		contents.remove(key);
	}

	@Override
	public void pathInitialized(File path)
	{
		for (ProfileChangeListener list : profileChangeListeners)
		{
			list.profileInitialized();
		}
	}

	@Override
	public void pathCreated(File path)
	{
		for (ProfileChangeListener list : profileChangeListeners)
		{
			list.profileChanged();
		}
	}

	@Override
	public void pathDeleted(File path)
	{
		// ignore this - hope it gets recreated soon
	}

	@Override
	public void pathModified(File path)
	{
		pathCreated(path);
	}

	TFile tfile()
	{
		return jarFile;
	}
}