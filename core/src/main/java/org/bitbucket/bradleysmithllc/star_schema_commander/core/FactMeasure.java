package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedObject;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.*;

public class FactMeasure extends Removable implements Comparable<FactMeasure>, PersistedObject
{
	private final DatabaseColumn column;
	private final Fact fact;
	private final String logicalName;

	public FactMeasure(DatabaseColumn column, String logicalName, Fact fact)
	{
		this.column = column;
		this.fact = fact;
		this.logicalName = logicalName;
	}

	public static void loadFromJson(File pathname, Fact fact) throws IOException
	{
	}

	public static void read(PersistedReadableFile file, Fact fact) throws IOException
	{
		BufferedReader reader = new BufferedReader(file.reader());

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			String ln = node.get("logical-name").asText();
			String cn = node.get("column-name").asText();

			fact.newFactMeasure().logicalName(ln).column(cn).create();
		}
		finally
		{
			reader.close();
		}
	}

	public String logicalName()
	{
		return logicalName;
	}

	public Fact fact()
	{
		return fact;
	}

	public DatabaseColumn column()
	{
		return column;
	}

	@Override
	public int compareTo(FactMeasure o)
	{
		return CompareUtils.compare(logicalName, o.logicalName);
	}

	public String qualifiedName()
	{
		return fact.qualifiedName() + "." + logicalName();
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		if (removed())
		{
			file.delete();
		}
		else
		{
			// write the dimension attribute object
			Writer writer = file.writer();

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("logical-name").value(logicalName);
				jsonWriter.name("column-name").value(column.name());

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		FactMeasure that = (FactMeasure) o;

		if (!column.equals(that.column))
		{
			return false;
		}

		if (!fact.qualifiedName().equals(that.fact.qualifiedName()))
		{
			return false;
		}

		return logicalName.equals(that.logicalName);
	}

	@Override
	public int hashCode()
	{
		int result = column.hashCode();
		result = 31 * result + fact.logicalName().hashCode();
		result = 31 * result + logicalName.hashCode();
		return result;
	}

	public static class FactMeasureBuilder
	{
		private final Fact fact;
		private String column;
		private String logicalName;

		public FactMeasureBuilder(Fact fact)
		{
			this.fact = fact;
		}

		public FactMeasureBuilder column(String name)
		{
			column = name;
			return this;
		}

		public FactMeasureBuilder logicalName(String name)
		{
			logicalName = name;
			return this;
		}

		public FactMeasure create()
		{
			if (column == null)
			{
				throw new IllegalStateException("Missing column name");
			}

			if (logicalName == null)
			{
				logicalName = column;
			}

			DatabaseColumn column1 = fact.columns().get(column);
			if (column1 == null)
			{
				throw new IllegalStateException("Column [" + column + "] not found");
			}

			FactMeasure fm = new FactMeasure(column1, logicalName, fact);
			fact.addMeasure(fm);

			return fm;
		}
	}
}
