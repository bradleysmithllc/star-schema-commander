package org.bitbucket.bradleysmithllc.star_schema_commander.core.query;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedObject;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;

import java.io.*;
import java.util.*;

public class Query implements PersistedObject
{
	private final Fact _for;
	private final Set<Selector<FactMeasure>> measures;
	private final Set<FactDimensionKey> keys;
	private final Map<FactDimensionKey, Set<Selector<DimensionAttribute>>> attributes;
	private final int limit;
	private final UUID uuid = UUID.randomUUID();
	private Quoter quoter = new Quoter();
	private Padder padder = new Padder();
	private int aggregateCount = Integer.MIN_VALUE;

	public Query(Fact aFor, Set<Selector<FactMeasure>> measures, Set<FactDimensionKey> keys, Map<FactDimensionKey, Set<Selector<DimensionAttribute>>> attributes, int limit)
	{
		_for = aFor;
		this.limit = limit;
		this.measures = measures;
		this.keys = keys;
		this.attributes = attributes;
	}

	public static Query read(Profile profile, PersistedReadableFile qrf) throws IOException
	{
		// read this in using the builder
		Reader reader = qrf.reader();

		try
		{
			return read(profile, reader);
		}
		finally
		{
			reader.close();
		}
	}

	public static Query read(Profile profile, Reader reader) throws IOException
	{
		JsonNode json = JsonLoader.fromReader(reader);

		// look up the ra
		String raName = json.get("reporting-area").asText();
		ReportingArea ra = profile.defaultReportingArea().reportingAreas().get(raName);

		if (ra == null)
		{
			throw new IllegalArgumentException("Reporting Area [" + raName + "] no longer present");
		}

		// locate the fact
		String factName = json.get("fact-logical-name").asText();
		Fact fact = ra.facts().get(factName);

		if (fact == null)
		{
			throw new IllegalArgumentException("Fact [" + factName + "] no longer present");
		}

		QueryBuilder qb = new QueryBuilder(fact);

		int limit = json.get("limit").asInt();

		if (limit != -1)
		{
			qb.limit(limit);
		}

		// read in measures
		JsonNode measures = json.get("measures");

		Iterator<JsonNode> mit = measures.elements();

		while (mit.hasNext())
		{
			JsonNode entry = mit.next();

			String name = entry.get("measure").asText();

			String alias = null;

			JsonNode alnode = entry.get("alias");

			if (!alnode.isNull())
			{
				alias = alnode.asText();
			}

			aggregation agg = aggregation.valueOf(entry.get("aggregation").asText());

			FactMeasure fm = fact.measures().get(name);

			qb.include(fm, agg, alias);
		}

		JsonNode keys = json.get("keys");

		Iterator<Map.Entry<String, JsonNode>> kit = keys.fields();

		while (kit.hasNext())
		{
			Map.Entry<String, JsonNode> keyNode = kit.next();

			FactDimensionKey fk = fact.keys().get(keyNode.getKey());
			Map<String, DimensionAttribute> attributes = fk.dimensionKey().dimension().attributes();

			qb.join(fk);

			Iterator<JsonNode> attrit = keyNode.getValue().elements();

			while (attrit.hasNext())
			{
				JsonNode attrnode = attrit.next();

				String attrname = attrnode.get("attribute").asText();
				String alias = null;

				JsonNode alnode = attrnode.get("alias");

				if (!alnode.isNull())
				{
					alias = alnode.asText();
				}

				aggregation agg = aggregation.valueOf(attrnode.get("aggregation").asText());

				qb.include(attributes.get(attrname), agg, alias);
			}
		}

		return qb.create();
	}

	public UUID getUuid()
	{
		return uuid;
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		Writer writer = file.writer();

		try
		{
			persist(writer);
		}
		finally
		{
			writer.close();
		}
	}

	public void persist(Writer writer1) throws IOException
	{
		// the complementary read to this write method is the static read function, not the instance read.
		JsonWriter writer = new JsonWriter(writer1);
		writer.beginObject();

		writer.name("reporting-area").value(_for.memberOf().qualifiedName());
		writer.name("fact-logical-name").value(_for.logicalName());
		writer.name("limit").value(limit);

		writer.name("measures");

		writer.beginArray();
		for (Selector<FactMeasure> measure : measures)
		{
			writer.beginObject();
			writer.name("measure").value(measure.it().logicalName());
			writer.name("alias").value(measure.alias());
			writer.name("aggregation").value(measure.aggregation().name());
			writer.endObject();
		}
		writer.endArray();

		writer.name("keys");

		writer.beginObject();
		for (Map.Entry<FactDimensionKey, Set<Selector<DimensionAttribute>>> measure : attributes.entrySet())
		{
			writer.name(measure.getKey().name());

			writer.beginArray();
			for (Selector<DimensionAttribute> attr : measure.getValue())
			{
				writer.beginObject();
				writer.name("attribute").value(attr.it().logicalName());
				writer.name("alias").value(attr.alias());
				writer.name("aggregation").value(attr.aggregation().name());
				writer.endObject();
			}
			writer.endArray();

		}
		writer.endObject();

		writer.endObject();
		writer.flush();
	}

	/**
	 * @param profile
	 * @param file
	 * @throws IOException
	 * @deprecated See static Query read(PersistedReadableFile file)
	 */
	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{
		throw new UnsupportedOperationException();
	}

	public String query()
	{
		StringWriter stw = new StringWriter();

		VelocityEngine ve = new VelocityEngine();
		ve.init();

		VelocityContext vc = new VelocityContext();

		vc.put("columnCount", columnCount());

		vc.put("limit", limit);
		vc.put("newline", "\n");
		vc.put("padder", padder);
		vc.put("quoter", quoter);
		vc.put("fact", _for);
		vc.put("columns", columns());
		vc.put("measures", measures);
		vc.put("keys", keys);
		vc.put("attributes", attributes);
		vc.put("groupBy", groupBy());

		try
		{
			InputStream resourceAsStream = getClass().getResourceAsStream("queryTemplate.vm");
			ve.evaluate(vc, stw, "log", IOUtils.toString(resourceAsStream));
		}
		catch (IOException e)
		{
			throw new RuntimeException("");
		}

		return stw.toString();
	}

	private List<Expression> columns()
	{
		return columns(null);
	}

	private List<Expression> columns(aggregation agg)
	{
		List<Expression> expressions = new ArrayList<Expression>();

		StringBuilder stb = new StringBuilder();

		for (Selector<FactMeasure> measure : measures)
		{
			if (agg == null || measure.aggregation() == agg)
			{
				stb.setLength(0);
				stb.append(measure.aggregation().aggPreText())
					.append("FACT.")
					.append(quoter.quote(measure.it().column().name()))
					.append(measure.aggregation().aggPostText());
				String expression = stb.toString();

				String str = ObjectUtils.firstNonNull(measure.alias(), measure.it().logicalName());
				expressions.add(new Expression(expression, quoter.quote(str)));
			}
		}

		for (Map.Entry<FactDimensionKey, Set<Selector<DimensionAttribute>>> attributeList : attributes.entrySet())
		{
			FactDimensionKey key = attributeList.getKey();

			for (Selector<DimensionAttribute> attribute : attributeList.getValue())
			{
				if (agg == null || agg == attribute.aggregation())
				{
					stb.setLength(0);
					stb.append(attribute.aggregation().aggPreText());

					if (attribute.it().in().degenerate())
					{
						stb.append("FACT");
					}
					else
					{
						stb.append(quoter.quote(key.name()));
					}

					stb.append(".");
					stb.append(quoter.quote(attribute.it().column().name()));

					stb.append(attribute.aggregation().aggPostText());
					String expression = stb.toString();
					String alias = "";

					// In the case of role playing, assign a unique alias by default
					if (key.rolePlaying())
					{
						alias = ObjectUtils.firstNonNull(attribute.alias(), key.name() + "_" + attribute.it().logicalName());
					}
					else
					{
						alias = ObjectUtils.firstNonNull(attribute.alias(), attribute.it().logicalName());
					}

					expressions.add(new Expression(expression, quoter.quote(alias)));
				}
			}
		}

		return expressions;
	}

	private List<Expression> groupBy()
	{
		if (aggregateCount == Integer.MIN_VALUE)
		{
			aggregateCount = 0;

			for (Selector<FactMeasure> val : measures)
			{
				if (val.aggregation() != aggregation.none)
				{
					aggregateCount++;
				}
			}

			for (Set<Selector<DimensionAttribute>> list : attributes.values())
			{
				for (Selector<DimensionAttribute> val : list)
				{
					if (val.aggregation() != aggregation.none)
					{
						aggregateCount++;
					}
				}
			}
		}

		if (aggregateCount == 0)
		{
			return Collections.emptyList();
		}

		return columns(aggregation.none);
	}

	private int columnCount()
	{
		return measures.size() + attributeCount();
	}

	private int attributeCount()
	{
		int totalCount = 0;

		for (Map.Entry<FactDimensionKey, Set<Selector<DimensionAttribute>>> fdk : attributes.entrySet())
		{
			totalCount += fdk.getValue().size();
		}

		return totalCount;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Query query = (Query) o;

		if (limit != query.limit)
		{
			return false;
		}
		if (!_for.qualifiedName().equals(query._for.qualifiedName()))
		{
			return false;
		}
		if (!measures.equals(query.measures))
		{
			return false;
		}

		return attributes.equals(query.attributes);
	}

	@Override
	public int hashCode()
	{
		int result = _for.qualifiedName().hashCode();
		result = 31 * result + measures.hashCode();
		result = 31 * result + attributes.hashCode();
		result = 31 * result + limit;
		return result;
	}

	public enum aggregation
	{
		none,
		sum,
		sum_distinct,
		max,
		min,
		average,
		average_distinct,
		count,
		count_distinct;

		public String aggPreText()
		{
			switch (this)
			{
				case none:
					return "";
				case average:
					return "AVG(";
				case average_distinct:
					return "AVG(DISTINCT ";
				case sum_distinct:
					return "SUM(DISTINCT ";
				case count_distinct:
					return "COUNT(DISTINCT ";
				default:
					return name().toUpperCase() + "(";
			}
		}

		public String aggPostText()
		{
			switch (this)
			{
				case none:
					return "";
				default:
					return ")";
			}
		}
	}

	public static final class SelectorBuilder<T extends Comparable<T>>
	{
		private final T t;
		private aggregation _aggregation = aggregation.none;
		private String alias;

		public SelectorBuilder(T t)
		{
			this.t = t;
		}

		public SelectorBuilder<T> aggregate(aggregation agg)
		{
			_aggregation = agg;
			return this;
		}

		public SelectorBuilder<T> sum()
		{
			_aggregation = aggregation.sum;
			return this;
		}

		public SelectorBuilder<T> min()
		{
			_aggregation = aggregation.min;
			return this;
		}

		public SelectorBuilder<T> max()
		{
			_aggregation = aggregation.max;
			return this;
		}

		public SelectorBuilder<T> average()
		{
			_aggregation = aggregation.average;
			return this;
		}

		public SelectorBuilder<T> count()
		{
			_aggregation = aggregation.count;
			return this;
		}

		public SelectorBuilder<T> count_distinct()
		{
			_aggregation = aggregation.count_distinct;
			return this;
		}

		public SelectorBuilder<T> alias(String a)
		{
			alias = a;
			return this;
		}

		public Selector<T> create()
		{
			return new Selector<T>(t, _aggregation, alias);
		}
	}
}
