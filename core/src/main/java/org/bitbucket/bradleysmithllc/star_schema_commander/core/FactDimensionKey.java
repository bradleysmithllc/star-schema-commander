package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedObject;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.*;
import java.util.*;

public class FactDimensionKey extends Removable implements Comparable<FactDimensionKey>, PersistedObject
{
	private final String roleName;
	private final String physicalName;

	private final Fact fact;
	private final DimensionKey dimensionKey;

	private final List<StarJoin> joins = new ArrayList<StarJoin>();

	public FactDimensionKey(
		String roleName,
		Fact fact,
		DimensionKey dimensionKey,
		List<StarJoin> j,
		String physicalName)
	{
		this.physicalName = physicalName;
		this.roleName = roleName;
		this.fact = fact;
		this.dimensionKey = dimensionKey;
		joins.addAll(j);
	}

	public static void read(PersistedReadableFile file, Fact fact) throws IOException
	{
		FactDimensionKeyBuilder builder = fact.newFactDimensionKey();

		Reader reader = new BufferedReader(file.reader());

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			JsonNode roleNode = node.get("role-name");
			if (roleNode != null && !roleNode.isNull())
			{
				builder.roleName(roleNode.asText());
			}

			JsonNode dimNode = node.get("dimension-name");
			builder.dimension(dimNode.asText());

			JsonNode keyNode = node.get("dimension-key");
			builder.keyName(keyNode.asText());

			keyNode = node.get("physical-name");
			builder.physicalName(keyNode.asText());

			ArrayNode joins = (ArrayNode) node.get("star-joins");

			Iterator<JsonNode> jit = joins.iterator();

			while (jit.hasNext())
			{
				JsonNode j = jit.next();
				String from = j.get("fact-column-name").asText();
				String to = j.get("dimension-column-name").asText();

				builder.join(from, to);
			}

			builder.create();
		}
		finally
		{
			reader.close();
		}
	}

	@Override
	public void remove()
	{
		super.remove();

		// if this is pointing to a degenerate dimension, remove that as well
		if (dimensionKey.dimension().degenerate())
		{
			dimensionKey.dimension().remove();
		}
	}

	public String physicalName()
	{
		return physicalName;
	}

	public boolean rolePlaying()
	{
		// defined as more than one key to the same dimension
		Map<String, FactDimensionKey> keyMap = fact.keys(dimensionKey.dimension());

		return keyMap.size() > 1 ? true : !keyMap.containsValue(this);
	}

	public String roleName()
	{
		return roleName;
	}

	public Fact fact()
	{
		return fact;
	}

	public DimensionKey dimensionKey()
	{
		return dimensionKey;
	}

	public List<StarJoin> joins()
	{
		return Collections.unmodifiableList(joins);
	}

	public String name()
	{
		// the name is the dimension name with the role name appended
		return dimensionKey.dimension().logicalName() + (roleName != null ? ("_" + roleName) : "");
	}

	@Override
	public int compareTo(FactDimensionKey o)
	{
		return CompareUtils.compare(name(), o.name());
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		if (removed())
		{
			file.delete();
		}
		else
		{
			// write the dimension attribute object
			Writer writer = file.writer();

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("role-name").value(roleName);

				// dimension info
				jsonWriter.name("dimension-name").value(dimensionKey.dimension().logicalName());
				jsonWriter.name("dimension-key").value(dimensionKey.logicalName());
				jsonWriter.name("physical-name").value(physicalName());

				jsonWriter.name("star-joins");
				jsonWriter.beginArray();
				// joins
				for (StarJoin join : joins)
				{
					jsonWriter.beginObject();
					jsonWriter.name("fact-column-name").value(join.from().name());
					jsonWriter.name("dimension-column-name").value(join.to().name());
					jsonWriter.endObject();
				}
				jsonWriter.endArray();

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{

	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		FactDimensionKey that = (FactDimensionKey) o;

		if (roleName != null ? !roleName.equals(that.roleName) : that.roleName != null)
		{
			return false;
		}

		if (!physicalName.equals(that.physicalName))
		{
			return false;
		}

		if (!fact.qualifiedName().equals(that.fact.qualifiedName()))
		{
			return false;
		}

		if (!dimensionKey.equals(that.dimensionKey))
		{
			return false;
		}

		return joins.equals(that.joins);
	}

	@Override
	public int hashCode()
	{
		int result = roleName != null ? roleName.hashCode() : 0;
		result = 31 * result + physicalName.hashCode();
		result = 31 * result + fact.qualifiedName().hashCode();
		result = 31 * result + dimensionKey.hashCode();
		result = 31 * result + joins.hashCode();
		return result;
	}

	public static class FactDimensionKeyBuilder
	{
		private final Fact fact;
		private String roleName;
		private String dimension;
		private String keyName;
		private String physicalName;
		private List<Join> sjoins = new ArrayList<Join>();

		public FactDimensionKeyBuilder(Fact fact)
		{
			this.fact = fact;
		}

		public FactDimensionKeyBuilder roleName(String str)
		{
			roleName = str;
			return this;
		}

		public FactDimensionKeyBuilder dimension(String str)
		{
			dimension = str;
			return this;
		}

		public FactDimensionKeyBuilder keyName(String str)
		{
			keyName = str;
			return this;
		}

		public FactDimensionKeyBuilder physicalName(String str)
		{
			physicalName = str;
			return this;
		}

		public FactDimensionKeyBuilder join(String from, String to)
		{
			sjoins.add(new Join(from, to));
			return this;
		}

		public FactDimensionKey create()
		{
			if (dimension == null)
			{
				throw new IllegalStateException("Dimension name required");
			}

			if (keyName == null)
			{
				throw new IllegalStateException("Key name required");
			}

			if (sjoins.size() == 0)
			{
				throw new IllegalStateException("Joins required");
			}

			if (physicalName == null)
			{
				// use the role and key names
				physicalName = keyName + "_" + dimension + (roleName != null ? ("_" + roleName) : "");
			}

			ReportingArea ra = fact.memberOf();

			Dimension dim = ra.dimensions().get(dimension);

			if (dim == null)
			{
				throw new IllegalStateException("Dimension [" + dimension + "] not found");
			}

			DimensionKey dk = dim.keys().get(keyName);

			if (dk == null)
			{
				throw new IllegalStateException("Dimension Key [" + keyName + "] not found");
			}

			List<DatabaseColumn> databaseColumns = dk.columns();
			if (sjoins.size() != databaseColumns.size())
			{
				throw new IllegalStateException("Incomplete column specification");
			}

			List<StarJoin> joins = new ArrayList<StarJoin>();

			for (Join join : sjoins)
			{
				DatabaseColumn dim_dc = getColumn(join.to, databaseColumns, "dimension");
				DatabaseColumn fact_dc = getColumn(join.from, fact.columns().values(), "fact");

				joins.add(new StarJoin(fact_dc, dim_dc));
			}

			FactDimensionKey fdk = new FactDimensionKey(roleName, fact, dk, joins, physicalName);
			fact.addKey(fdk);
			return fdk;
		}

		private DatabaseColumn getColumn(String from, Collection<DatabaseColumn> values, String context)
		{
			for (DatabaseColumn dc : values)
			{
				if (dc.name().equals(from))
				{
					return dc;
				}
			}

			throw new IllegalArgumentException("Column [" + from + "] not found in " + context);
		}

		private static final class Join
		{
			String from;
			String to;

			public Join(String from, String to)
			{
				this.from = from;
				this.to = to;
			}
		}
	}
}
