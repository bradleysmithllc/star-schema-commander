package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.QueryBuilder;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.Collator;
import java.util.*;

public class Fact extends Removable implements ColumnStore, Comparable<Fact>, PersistedCollection
{
	private final String logicalName;
	private final String catalog;
	private final String schema;
	private final String name;

	private final Map<String, DatabaseColumn> columnMap = new TreeMap<String, DatabaseColumn>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, FactMeasure> measureMap = new TreeMap<String, FactMeasure>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, FactDimensionKey> keyMap = new TreeMap<String, FactDimensionKey>(Collator.getInstance(Locale.ENGLISH));
	private final Map<String, FactDimensionKey> physicalKeyMap = new TreeMap<String, FactDimensionKey>(Collator.getInstance(Locale.ENGLISH));

	private transient ReportingArea reportingArea;

	public Fact(String logicalName, String catalog, String schema, String name)
	{
		this.logicalName = logicalName;
		this.schema = schema;
		this.name = name;
		this.catalog = catalog;
	}

	public String logicalName()
	{
		return logicalName;
	}

	public String schema()
	{
		return schema;
	}

	public String catalog()
	{
		return catalog;
	}

	public String name()
	{
		return name;
	}

	void enter(ReportingArea area)
	{
		reportingArea = area;
	}

	public ReportingArea memberOf()
	{
		return reportingArea;
	}

	public Map<String, DatabaseColumn> columns()
	{
		return Collections.unmodifiableMap(columnMap);
	}

	@Override
	public Map<String, DatabaseColumn> mutableColumns()
	{
		return columnMap;
	}

	@Override
	public void add(String name, String typeName)
	{
		newDatabaseColumn().name(name).jdbcTypeName(typeName).create();
	}

	public Map<String, FactMeasure> measures()
	{
		return Collections.unmodifiableMap(measureMap);
	}

	public Map<String, FactDimensionKey> keys()
	{
		return Collections.unmodifiableMap(keyMap);
	}

	public Map<String, FactDimensionKey> physicalKeys()
	{
		return Collections.unmodifiableMap(physicalKeyMap);
	}

	public Map<String, FactDimensionKey> keys(Dimension d)
	{
		Map<String, FactDimensionKey> keys = new HashMap<String, FactDimensionKey>();

		for (Map.Entry<String, FactDimensionKey> fdk : keyMap.entrySet())
		{
			if (fdk.getValue().dimensionKey().dimension().equals(d))
			{
				keys.put(fdk.getKey(), fdk.getValue());
			}
		}

		return Collections.unmodifiableMap(keys);
	}

	/**
	 * Retrieves a list of all keys this column participates in.
	 */
	public Map<String, FactDimensionKey> keys(DatabaseColumn col)
	{
		Map<String, FactDimensionKey> keys = new HashMap<String, FactDimensionKey>();

		for (Map.Entry<String, FactDimensionKey> fdk : keyMap.entrySet())
		{
			for (StarJoin join : fdk.getValue().joins())
			{
				DatabaseColumn f = join.from();

				if (f == col)
				{
					keys.put(fdk.getKey(), fdk.getValue());
				}
			}
		}

		return keys;
	}

	public DegenerateDimensionBuilder newDegenerateDimension()
	{
		// illegal if one already exists
		if (hasDegenerateDimension())
		{
			throw new IllegalStateException("Degenerate Dimension already exists");
		}

		return new DegenerateDimensionBuilder(this);
	}

	public DatabaseColumn.DatabaseColumnBuilder newDatabaseColumn()
	{
		return new DatabaseColumn.DatabaseColumnBuilder(this);
	}

	public FactMeasure.FactMeasureBuilder newFactMeasure()
	{
		return new FactMeasure.FactMeasureBuilder(this);
	}

	public FactDimensionKey.FactDimensionKeyBuilder newFactDimensionKey()
	{
		return new FactDimensionKey.FactDimensionKeyBuilder(this);
	}

	void addMeasure(FactMeasure fm)
	{
		if (measureMap.containsKey(fm.logicalName()))
		{
			throw new IllegalStateException("Column already added [" + fm.logicalName() + "]");
		}

		measureMap.put(fm.logicalName(), fm);
	}

	void addKey(FactDimensionKey fdk)
	{
		if (keyMap.containsKey(fdk.name()))
		{
			throw new IllegalStateException("Key with logical name [" + fdk.name() + "] already added [" + fdk.name() + "]");
		}

		if (physicalKeyMap.containsKey(fdk.physicalName()))
		{
			throw new IllegalStateException("Key with physical name already added [" + fdk.physicalName() + "]");
		}

		physicalKeyMap.put(fdk.physicalName(), fdk);
		keyMap.put(fdk.name(), fdk);
	}

	@Override
	public DatabaseColumn addDatabaseColumn(DatabaseColumn databaseColumn)
	{
		if (columnMap.containsKey(databaseColumn.name()))
		{
			throw new IllegalStateException("Column [" + databaseColumn.name() + "] already added");
		}

		columnMap.put(databaseColumn.name(), databaseColumn);
		return databaseColumn;
	}

	@Override
	public String toString()
	{
		return "Fact{" +
			"logicalName='" + logicalName + '\'' +
			", schema='" + schema + '\'' +
			", name='" + name + '\'' +
			'}';
	}

	@Override
	public int compareTo(Fact o)
	{
		return CompareUtils.compare(logicalName, o.logicalName);
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableDirectory root) throws IOException
	{
		if (removed())
		{
			root.delete();
		}
		else
		{
			PersistedWritableFile target = root.openWritableFile("fact.json");

			// write the dimension attribute object
			Writer writer = target.writer();

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("name").value(name);
				jsonWriter.name("logical-name").value(logicalName);
				jsonWriter.name("schema").value(schema);
				jsonWriter.name("catalog").value(catalog);

				jsonWriter.name("column-map").beginObject().endObject();
				jsonWriter.name("measure-map").beginObject().endObject();
				jsonWriter.name("key-map").beginObject().endObject();
				jsonWriter.name("physical-key-map").beginObject().endObject();

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}

			// now write each column
			DatabaseColumn.writeToContainer(root, this);

			// now write each measure
			PersistedWritableFile pwf = root.openWritableFile("measures.json");

			Writer pwriter = pwf.writer();

			try
			{
				JsonWriter jwriter = new JsonWriter(pwriter);
				jwriter.beginObject();

				Iterator<FactMeasure> mit = measureMap.values().iterator();

				while (mit.hasNext())
				{
					FactMeasure col = mit.next();

					if (col.removed())
					{
						mit.remove();
					}
					else
					{
						jwriter.name(col.logicalName()).value(col.column().name());
					}
				}

				jwriter.endObject();
				jwriter.flush();
				jwriter.close();
			}
			finally
			{
				pwriter.close();
			}

			// now write each column
			PersistedWritableDirectory cols = root.openWritableDirectory("keys");

			Iterator<FactDimensionKey> kit = keyMap.values().iterator();

			while (kit.hasNext())
			{
				FactDimensionKey col = kit.next();
				col.persist(profile, cols.openWritableFile(col.name() + ".json"));

				if (col.removed())
				{
					kit.remove();
				}
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableDirectory root) throws IOException
	{
		// load columns from json
		DatabaseColumn.readFromContainer(root, this);

		// load measures from json
		PersistedReadableFile rf = root.openReadableFile("measures.json");

		Reader reader = rf.reader();

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			Iterator<Map.Entry<String, JsonNode>> fn = node.fields();

			while (fn.hasNext())
			{
				Map.Entry<String, JsonNode> f = fn.next();

				newFactMeasure().logicalName(f.getKey()).column(f.getValue().asText()).create();
			}
		}
		finally
		{
			reader.close();
		}

		// load keys from json
		for (PersistedReadableEntry pe : root.openReadableDirectory("keys").listReadable())
		{
			try
			{
				FactDimensionKey.read(pe.readableFile(), Fact.this);
			}
			catch (IOException exc)
			{
				System.out.println("Bad key found in profile: " + pe.name());
				exc.printStackTrace(System.out);
			}
		}
	}

	public QueryBuilder newQuery()
	{
		return new QueryBuilder(this);
	}

	public boolean hasDegenerateDimension()
	{
		return degenerateDimension() != null;
	}

	public Dimension degenerateDimension()
	{
		for (Map.Entry<String, FactDimensionKey> fdk : keys().entrySet())
		{
			Dimension dimension = fdk.getValue().dimensionKey().dimension();
			if (dimension.degenerate())
			{
				return dimension;
			}
		}

		return null;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Fact fact = (Fact) o;

		if (!logicalName.equals(fact.logicalName))
		{
			return false;
		}

		if (catalog != null ? !catalog.equals(fact.catalog) : fact.catalog != null)
		{
			return false;
		}

		if (!schema.equals(fact.schema))
		{
			return false;
		}

		if (!name.equals(fact.name))
		{
			return false;
		}

		if (!columnMap.equals(fact.columnMap))
		{
			return false;
		}

		if (!measureMap.equals(fact.measureMap))
		{
			return false;
		}

		if (!keyMap.equals(fact.keyMap))
		{
			return false;
		}

		if (!physicalKeyMap.equals(fact.physicalKeyMap))
		{
			return false;
		}

		return reportingArea.equals(fact.reportingArea);
	}

	@Override
	public int hashCode()
	{
		int result = logicalName.hashCode();
		result = 31 * result + (catalog != null ? catalog.hashCode() : 0);
		result = 31 * result + schema.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + columnMap.hashCode();
		result = 31 * result + measureMap.hashCode();
		result = 31 * result + keyMap.hashCode();
		result = 31 * result + physicalKeyMap.hashCode();
		result = 31 * result + reportingArea.hashCode();
		return result;
	}

	public String qualifiedName()
	{
		return reportingArea.qualifiedName() + "." + logicalName();
	}

	public String sqlQualifiedName()
	{
		return (catalog == null ? "" : (catalog + ".")) + schema + "." + name;
	}

	public static class FactBuilder
	{
		private final ReportingArea reportingArea;
		private String logicalName;
		private String name;
		private String schema;
		private String catalog;

		public FactBuilder(ReportingArea ra)
		{
			reportingArea = ra;
		}

		public FactBuilder logicalName(String na)
		{
			logicalName = na;
			return this;
		}

		public FactBuilder catalog(String na)
		{
			catalog = na;
			return this;
		}

		public FactBuilder name(String na)
		{
			name = na;
			return this;
		}

		public FactBuilder schema(String sch)
		{
			schema = sch;
			return this;
		}

		public Fact create()
		{
			if (name == null)
			{
				throw new IllegalStateException("Name is required");
			}

			if (schema == null)
			{
				throw new IllegalStateException("Schema is required");
			}

			StringBuilder stb = new StringBuilder();

			if (catalog != null)
			{
				stb.append(catalog).append("_");
			}

			stb.append(schema).append("_").append(name);

			Fact f = reportingArea.addFact(new Fact(ObjectUtils.firstNonNull(logicalName, stb.toString()), catalog, schema, name));
			f.enter(reportingArea);
			return f;
		}
	}

	public class DegenerateDimensionBuilder
	{
		private final Fact fact;

		private String logicalName;
		private Map<String, String> columns = new TreeMap<String, String>();

		public DegenerateDimensionBuilder(Fact fact)
		{
			this.fact = fact;
		}

		public DegenerateDimensionBuilder logicalName(String name)
		{
			logicalName = name;
			return this;
		}

		public DegenerateDimensionBuilder column(String name)
		{
			return column(name, name);
		}

		public DegenerateDimensionBuilder column(String name, String logical)
		{
			columns.put(name, logical);
			return this;
		}

		public Dimension create()
		{
			synchronized (Fact.this)
			{
				// illegal if one already exists
				if (fact.hasDegenerateDimension())
				{
					throw new IllegalStateException("Degenerate Dimension already exists");
				}
			}

			if (logicalName == null)
			{
				logicalName = "DD_" + fact.logicalName();
			}

			if (columns.size() == 0)
			{
				throw new IllegalArgumentException("Must supply a column name.");
			}

			// create the dimension
			Dimension dim = fact.reportingArea.newDimension().schema(fact.schema()).name(fact.name()).logicalName(logicalName).degenerate().create();

			// create a dimension key to tie to the fact
			DimensionKey.DimensionKeyBuilder dk = dim.newDimensionKey().physicalName(logicalName).logicalName(logicalName);
			FactDimensionKey.FactDimensionKeyBuilder fdk = fact.newFactDimensionKey().keyName(logicalName).dimension(dim.logicalName()).roleName("DegenerateDimension").physicalName(logicalName);

			for (Map.Entry<String, String> col : columns.entrySet())
			{
				DatabaseColumn fcol = fact.columns().get(col.getKey());

				if (fcol == null)
				{
					throw new IllegalArgumentException("Column not found in fact [" + col + "]");
				}

				// create same column in the dimension and an attribute
				DatabaseColumn dcol = dim.newDatabaseColumn().name(fcol.name()).jdbcType(fcol.jdbcType()).create();
				dim.newDimensionAttribute().column(fcol.name()).logicalName(col.getValue()).create();

				dk.column(fcol.name());

				fdk.join(col.getKey(), col.getKey());
			}

			dk.create();
			fdk.create();

			return dim;
		}
	}
}
