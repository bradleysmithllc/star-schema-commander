package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.tvfs_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import net.java.truevfs.access.TFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableDirectory;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableEntry;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TVFSPersistedReadableDirectory implements PersistedReadableDirectory
{
	private final TVFSPersistedProfile jarPersistedProfile;
	private final TVFSPersistedReadableDirectory parentDirectory;

	private final String path;
	private final String name;
	private final TFile file;

	public TVFSPersistedReadableDirectory(TVFSPersistedProfile jpp)
	{
		this(jpp, null, null);
	}

	public TVFSPersistedReadableDirectory(TVFSPersistedProfile jpp, TVFSPersistedReadableDirectory pd, String name)
	{
		jarPersistedProfile = jpp;
		parentDirectory = pd;

		path = parentDirectory != null ? (parentDirectory.key() + name) : name;

		this.name = name;
		file = name == null ? jpp.tfile() : new TFile(pd.tfile(), name);
	}

	TFile tfile()
	{
		return file;
	}

	@Override
	public PersistedReadableDirectory openReadableDirectory(final String name) throws IOException
	{
		return new TVFSPersistedReadableDirectory(jarPersistedProfile, this, name);
	}

	@Override
	public boolean isDirectory()
	{
		return true;
	}

	@Override
	public PersistedReadableFile readableFile()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public PersistedReadableDirectory readableDirectory()
	{
		return this;
	}

	@Override
	public List<PersistedReadableEntry> listReadable()
	{
		List<PersistedReadableEntry> entries = new ArrayList<PersistedReadableEntry>();

		TFile[] list = tfile().listFiles();

		if (list != null)
		{
			for (TFile childName : list)
			{
				String name = childName.getName();

				// strip off the path
				int indexOf = name.lastIndexOf(TFile.separatorChar);

				if (indexOf != -1)
				{
					name = name.substring(indexOf + 1);
				}

				if (childName.isDirectory())
				{
					entries.add(new TVFSPersistedReadableDirectory(jarPersistedProfile, this, name));
				}
				else if (childName.isFile())
				{
					entries.add(new TVFSPersistedReadableFile(this, jarPersistedProfile, name));
				}
			}
		}

		return entries;
	}

	@Override
	public PersistedReadableFile openReadableFile(String name) throws IOException
	{
		return new TVFSPersistedReadableFile(this, jarPersistedProfile, name);
	}

	@Override
	public String name()
	{
		return name;
	}

	public String key()
	{
		return path == null ? "/" : (path + "/");
	}
}
