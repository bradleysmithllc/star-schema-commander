package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class FileChangeWatcher
{
	private final File pathToWatch;
	private final Semaphore startLatch = new Semaphore(1);
	private final Semaphore stopLatch = new Semaphore(1);
	private final Semaphore shutdownTrigger = new Semaphore(1);

	private FileBroadcaster fileBroadcaster = null;

	public FileChangeWatcher(File pathToWatch)
	{
		this.pathToWatch = pathToWatch;
	}

	public boolean watching()
	{
		return startLatch.availablePermits() == 0;
	}

	public synchronized void stopWatching()
	{
		if (!watching())
		{
			// not running
			throw new IllegalStateException("Watcher not active");
		}

		try
		{
			fileBroadcaster.stop();
			stopLatch.drainPermits();
			shutdownTrigger.release();
			stopLatch.acquireUninterruptibly();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	public synchronized void watch(FileChangeListener listener)
	{
		fileBroadcaster = new FileBroadcaster(listener, pathToWatch);

		try
		{
			fileBroadcaster.start();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}

		startLatch.drainPermits();
		shutdownTrigger.drainPermits();

		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				long modified = Long.MIN_VALUE;
				long size = Long.MIN_VALUE;
				boolean started = false;

				file_state lastObservedState = file_state.uninitialized;

				while (true)
				{
					// scan filesystem
					switch (lastObservedState)
					{
						case uninitialized:
							// record what is seen
							if (!pathToWatch.exists())
							{
								lastObservedState = file_state.does_not_exist;
							}
							else
							{
								modified = pathToWatch.lastModified();
								size = pathToWatch.length();

								lastObservedState = file_state.exists;
							}

							fileBroadcaster.initialized();
							break;
						case exists:
							if (!pathToWatch.exists())
							{
								lastObservedState = file_state.does_not_exist;
								modified = Long.MIN_VALUE;
								size = Long.MIN_VALUE;
								fileBroadcaster.deleted();
							}
							else
							{
								long new_modified = pathToWatch.lastModified();
								long new_size = pathToWatch.length();

								if (new_modified != modified || new_size != size)
								{
									// pause here to make sure that the file is done changing
									try
									{
										while (true)
										{
											modified = new_modified;
											size = new_size;

											if (shutdownTrigger.tryAcquire(400L, TimeUnit.MILLISECONDS))
											{
												// this means stop
												break;
											}

											new_modified = pathToWatch.lastModified();
											new_size = pathToWatch.length();

											if (new_modified == modified && new_size == size)
											{
												break;
											}
										}
									}
									catch (InterruptedException e)
									{
										e.printStackTrace();
									}

									fileBroadcaster.modified();
								}
							}
							break;
						case does_not_exist:
							if (!pathToWatch.exists())
							{
								// not interesting
							}
							else
							{
								lastObservedState = file_state.exists;
								modified = pathToWatch.lastModified();
								size = pathToWatch.length();
								fileBroadcaster.created();
							}
							break;
					}

					if (!started)
					{
						startLatch.release();
						started = true;
					}

					try
					{
						if (shutdownTrigger.tryAcquire(750L, TimeUnit.MILLISECONDS))
						{
							// this means stop
							break;
						}
					}
					catch (InterruptedException e)
					{
						// this means we have NOT been triggered to stop
					}
				}

				stopLatch.release();
			}
		});
		thread.setName("Watcher[" + pathToWatch.getAbsolutePath() + "]");
		thread.start();

		try
		{
			startLatch.tryAcquire(10000L, TimeUnit.MILLISECONDS);
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException("Did not get a response from the start semaphore");
		}
	}

	enum file_state
	{
		uninitialized,
		exists,
		does_not_exist
	}

	public interface FileChangeListener
	{
		void pathInitialized(File path);

		void pathCreated(File path);

		void pathDeleted(File path);

		void pathModified(File path);
	}
}

class FileBroadcaster implements Runnable
{
	private final FileChangeWatcher.FileChangeListener fileChangeListener;
	private final File pathToWatch;

	private final CountDownLatch startLatch = new CountDownLatch(1);
	private final CountDownLatch stopLatch = new CountDownLatch(1);
	private final CountDownLatch stopWaitLatch = new CountDownLatch(1);
	private final Deque<message> queue = new LinkedList<message>();

	FileBroadcaster(FileChangeWatcher.FileChangeListener fileChangeListener, File pathToWatch)
	{
		this.fileChangeListener = fileChangeListener;
		this.pathToWatch = pathToWatch;
	}

	public void start() throws InterruptedException
	{
		new Thread(this).start();
		startLatch.await(10000L, TimeUnit.MILLISECONDS);
	}

	public void stop() throws InterruptedException
	{
		stopLatch.countDown();
		stopWaitLatch.await(10000L, TimeUnit.MILLISECONDS);
	}

	@Override
	public void run()
	{
		startLatch.countDown();

		try
		{
			while (!stopLatch.await(100L, TimeUnit.MILLISECONDS))
			{
				message msg = queue.peekLast();

				if (msg != null)
				{
					msg = queue.removeLast();

					switch (msg)
					{
						case initialized:
							broadcastFileInitialized();
							break;
						case created:
							broadcastFileCreated();
							break;
						case modified:
							broadcastFileModified();
							break;
						case deleted:
							broadcastFileDeleted();
							break;
					}
				}
			}

			stopWaitLatch.countDown();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	private void broadcastFileInitialized()
	{
		if (fileChangeListener != null)
		{
			fileChangeListener.pathInitialized(pathToWatch);
		}
	}

	private void broadcastFileCreated()
	{
		if (fileChangeListener != null)
		{
			fileChangeListener.pathCreated(pathToWatch);
		}
	}

	private void broadcastFileModified()
	{
		if (fileChangeListener != null)
		{
			fileChangeListener.pathModified(pathToWatch);
		}
	}

	private void broadcastFileDeleted()
	{
		if (fileChangeListener != null)
		{
			fileChangeListener.pathDeleted(pathToWatch);
		}
	}

	public void initialized()
	{
		queue.addFirst(message.initialized);
	}

	public void deleted()
	{
		queue.addFirst(message.deleted);
	}

	public void modified()
	{
		queue.addFirst(message.modified);
	}

	public void created()
	{
		queue.addFirst(message.created);
	}

	enum message
	{
		initialized,
		created,
		modified,
		deleted
	}
}
