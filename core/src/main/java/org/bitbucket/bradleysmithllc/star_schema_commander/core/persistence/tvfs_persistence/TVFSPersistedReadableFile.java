package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.tvfs_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileInputStream;
import net.java.truevfs.access.TFileReader;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableDirectory;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

public class TVFSPersistedReadableFile implements PersistedReadableFile
{
	private final String name;
	private final TVFSPersistedReadableDirectory persistedReadableDirectory;
	private final TVFSPersistedProfile persistedProfile;
	private final TFile filePath;

	public TVFSPersistedReadableFile(TVFSPersistedReadableDirectory jarPersistedReadableDirectory, TVFSPersistedProfile jarPersistedProfile, String name)
	{
		persistedProfile = jarPersistedProfile;
		persistedReadableDirectory = jarPersistedReadableDirectory;
		this.name = name;

		filePath = new TFile(jarPersistedReadableDirectory.tfile(), name);
	}

	@Override
	public InputStream read() throws IOException
	{
		return new TFileInputStream(filePath);
	}

	@Override
	public boolean isDirectory()
	{
		return false;
	}

	@Override
	public PersistedReadableFile readableFile()
	{
		return this;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public Reader reader() throws IOException
	{
		return new TFileReader(filePath);
	}

	@Override
	public PersistedReadableDirectory readableDirectory()
	{
		throw new UnsupportedOperationException();
	}
}
