package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

public class QualifiedTable implements Comparable<QualifiedTable>
{
	private final String catalog;
	private final String schema;
	private final String name;
	private final String qualifiedName;

	public QualifiedTable(String catalog, String schema, String name)
	{
		this.schema = schema;
		this.name = name;
		this.catalog = catalog;

		qualifiedName = (catalog == null ? "" : (catalog + ".")) + schema + "." + name;
	}

	public String name()
	{
		return name;
	}

	public String schema()
	{
		return schema;
	}

	public String catalog()
	{
		return catalog;
	}

	public String qualifiedName()
	{
		return qualifiedName;
	}

	@Override
	public int compareTo(QualifiedTable o)
	{
		return CompareUtils.compare(qualifiedName, o.qualifiedName);
	}

	public static final class QualifiedTableBuilder
	{
		private String name;
		private String schema;
		private String catalog;

		public QualifiedTableBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public QualifiedTableBuilder catalog(String name)
		{
			this.catalog = name;
			return this;
		}

		public QualifiedTableBuilder schema(String sc)
		{
			schema = sc;
			return this;
		}

		public QualifiedTable create()
		{
			return new QualifiedTable(catalog, schema, name);
		}
	}
}
