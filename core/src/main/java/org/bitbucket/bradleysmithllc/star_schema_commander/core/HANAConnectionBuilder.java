package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class HANAConnectionBuilder extends BaseConnectionBuilder<HANAConnectionBuilder>
{
	private int instanceNumber;

	public HANAConnectionBuilder(Profile profile)
	{
		super(profile);
		port(30015);
	}

	public HANAConnectionBuilder instanceNumber(int num)
	{
		return port(30015 + (num * 100));
	}

	@Override
	protected propertyRequired requiresUserName()
	{
		return propertyRequired.required;
	}

	@Override
	protected propertyRequired requiresPassword()
	{
		return propertyRequired.required;
	}

	@Override
	protected propertyRequired requiresServerName()
	{
		return propertyRequired.required;
	}

	@Override
	public HANAConnectionBuilder username(String user)
	{
		return super.username(user);
	}

	@Override
	public HANAConnectionBuilder password(String pass)
	{
		return super.password(pass);
	}

	@Override
	public HANAConnectionBuilder serverName(String server)
	{
		return super.serverName(server);
	}

	protected DatabaseConnection createSub()
	{
		DatabaseConnection dbo = new DatabaseConnection(
			name, properties("jdbc:sap://" + serverName + ":" + port), "com.sap.db.jdbc.Driver", username, username, password);

		return profile.addDatabaseConnection(dbo);
	}

	@Override
	protected String getVendorName()
	{
		return "SAP HANA";
	}
}
