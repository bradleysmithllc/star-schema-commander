package org.bitbucket.bradleysmithllc.star_schema_commander.core.query;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.DimensionAttribute;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.Fact;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactDimensionKey;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.FactMeasure;

import java.util.*;

public class QueryBuilder
{
	private final Fact _for;

	private final Map<String, String> aliases = new HashMap<String, String>();

	private final Set<Selector<FactMeasure>> measures = new TreeSet<Selector<FactMeasure>>();
	private final Set<FactDimensionKey> keys = new TreeSet<FactDimensionKey>();
	private final Map<FactDimensionKey, Set<Selector<DimensionAttribute>>> attributes = new TreeMap<FactDimensionKey, Set<Selector<DimensionAttribute>>>();
	private boolean reallyNoMeasures = false;

	private FactDimensionKey lastKey;

	private int limit = -1;

	public QueryBuilder(Fact fact)
	{
		_for = fact;
	}

	public QueryBuilder include(FactMeasure measure)
	{
		return include(measure, Query.aggregation.none);
	}

	public QueryBuilder include(FactMeasure measure, Query.aggregation agg)
	{
		return include(measure, agg, null);
	}

	public QueryBuilder include(FactMeasure measure, Query.aggregation agg, String alias)
	{
		if (!_for.measures().containsValue(measure))
		{
			throw new IllegalArgumentException("Measure [" + measure.logicalName() + "] does not belong to this fact");
		}

		if (alias != null)
		{
			if (aliases.containsKey(alias))
			{
				throw new IllegalArgumentException("Alias [" + alias + "] was already used.");
			}
			else
			{
				aliases.put(alias, alias);
			}
		}

		Selector<FactMeasure> measureSelector = new Query.SelectorBuilder<FactMeasure>(measure).aggregate(agg).alias(alias).create();

		if (measures.contains(measureSelector))
		{
			throw new IllegalArgumentException("Measure [" + measure.logicalName() + "] has already been added at aggregation level " + agg.name());
		}

		measures.add(measureSelector);

		return this;
	}

	public QueryBuilder join(FactDimensionKey key)
	{
		if (key == null)
		{
			throw new IllegalArgumentException("Null Dimension [" + null + "] has no business here");
		}

		if (!_for.keys().containsValue(key))
		{
			throw new IllegalArgumentException("Dimension key [" + key.name() + "] does not belong to this fact");
		}

		if (keys.contains(key))
		{
			throw new IllegalArgumentException("Key [" + key.name() + "] has already been added");
		}

		lastKey = key;
		attributes.put(key, new TreeSet<Selector<DimensionAttribute>>());

		keys.add(key);

		return this;
	}

	public QueryBuilder limit(int count)
	{
		if (count <= 0)
		{
			throw new IllegalArgumentException("Limit may not be less than or equal to 0");
		}

		limit = count;

		return this;
	}

	public QueryBuilder reallyNoMeasures()
	{
		reallyNoMeasures = true;
		return this;
	}

	public QueryBuilder include(DimensionAttribute attribute)
	{
		return include(attribute, Query.aggregation.none);
	}

	public QueryBuilder include(DimensionAttribute attribute, Query.aggregation aggr)
	{
		return include(attribute, aggr, null);
	}

	public QueryBuilder include(DimensionAttribute attribute, Query.aggregation aggr, String alias)
	{
		if (lastKey == null)
		{
			throw new IllegalStateException("No current key");
		}

		if (attribute == null)
		{
			throw new IllegalArgumentException("Null attribute");
		}

		if (alias != null)
		{
			if (aliases.containsKey(alias))
			{
				throw new IllegalArgumentException("Alias [" + alias + "] was already used.");
			}
			else
			{
				aliases.put(alias, alias);
			}
		}

		attributes.get(lastKey).add(new Query.SelectorBuilder<DimensionAttribute>(attribute).aggregate(aggr).alias(alias).create());
		return this;
	}

	public Query create()
	{
		if (measures.size() == 0 && !reallyNoMeasures)
		{
			for (FactMeasure measure : _for.measures().values())
			{
				measures.add(new Query.SelectorBuilder<FactMeasure>(measure).create());
			}
		}

		return new Query(_for, measures, keys, attributes, limit);
	}
}
