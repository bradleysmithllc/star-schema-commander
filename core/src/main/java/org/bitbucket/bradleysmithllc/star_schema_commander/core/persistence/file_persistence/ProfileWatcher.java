package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.file_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class ProfileWatcher implements Runnable
{
	private final PersistedProfile profile;
	private final PersistedProfile.ProfileChangeListener updateListener;
	private Semaphore semaphore;

	private WatchService watcherService;

	public ProfileWatcher(PersistedProfile profile, PersistedProfile.ProfileChangeListener listener, Semaphore sem)
	{
		this.profile = profile;
		this.updateListener = listener;
		semaphore = sem;
	}

	public void run()
	{
		try
		{
			watcherService = FileSystems.getDefault().newWatchService();

			Path dir = profile.filesystemLocation().toPath();

			WatchKey key = dir.register(
				watcherService,
				StandardWatchEventKinds.ENTRY_CREATE,
				StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.ENTRY_MODIFY
			);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Could not create watcher", e);
		}

		int pollsSinceUpdates = 0;

		while (true)
		{
			try
			{
				// poll once before releasing the semaphore
				WatchKey takeKey = watcherService.poll(1L, TimeUnit.SECONDS);

				if (semaphore != null)
				{
					try
					{
						semaphore.release(1);
					}
					finally
					{
						semaphore = null;
					}
				}

				// don't do this right away - wait a few seconds until filesystem updates are complete
				if (pollsSinceUpdates != 0)
				{
					pollsSinceUpdates++;
				}

				if (takeKey == null)
				{
					if (pollsSinceUpdates >= 5)
					{
						try
						{
							// reset profile first
							try
							{
								updateListener.profileChanged();
							}
							catch (Throwable the)
							{
								throw new RuntimeException(the);
							}
						}
						finally
						{
							pollsSinceUpdates = 0;
						}
					}
					continue;
				}
				else
				{
					if (pollsSinceUpdates == 0)
					{
						pollsSinceUpdates = 1;
					}
				}

				// not sure why, but if the events are not enumerated they just keep repeating - even with reset()
				for (WatchEvent<?> env : takeKey.pollEvents())
				{
				}

				takeKey.reset();
			}
			catch (InterruptedException e)
			{
				return;
			}
			catch (ClosedWatchServiceException exc)
			{
				// trigger to stop monitoring.  Release thread to prevent slowing down the system
				return;
			}
		}
	}

	void interrupt() throws IOException
	{
		if (watcherService != null)
		{
			watcherService.close();
		}
		else
		{
			throw new IllegalStateException("Watcher service is not active.");
		}
	}
}
