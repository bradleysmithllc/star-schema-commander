package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedObject;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.util.CompareUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class DimensionKey extends Removable implements Comparable<DimensionKey>, PersistedObject
{
	private final String logicalName;
	private final String physicalName;

	private final Dimension dimension;
	private final List<DatabaseColumn> columns;

	public DimensionKey(Dimension dimension, String logicalName, String physicalName, List<DatabaseColumn> columns)
	{
		this.logicalName = logicalName;
		this.physicalName = physicalName;
		this.columns = columns;
		this.dimension = dimension;
	}

	public static void read(PersistedReadableFile file, Dimension dimension) throws IOException
	{
		BufferedReader reader = new BufferedReader(file.reader());

		try
		{
			JsonNode node = JsonLoader.fromReader(reader);

			String logicaName = node.get("logical-name").asText();
			String physicalName = node.get("physical-name").asText();

			ArrayNode columnName = (ArrayNode) node.get("column-names");

			DimensionKeyBuilder attributeBuilder = dimension.newDimensionKey().logicalName(logicaName).physicalName(physicalName);

			Iterator<JsonNode> it = columnName.iterator();

			while (it.hasNext())
			{
				JsonNode anode = it.next();
				attributeBuilder.column(anode.asText());
			}

			attributeBuilder.create();
		}
		finally
		{
			reader.close();
		}
	}

	@Override
	public void remove()
	{
		super.remove();

		// find any fact keys and remove
		for (Fact fact : dimension().facts())
		{
			for (Map.Entry<String, FactDimensionKey> fdk : fact.keys(dimension).entrySet())
			{
				FactDimensionKey dimensionKey = fdk.getValue();
				if (!dimensionKey.removed())
				{
					dimensionKey.remove();
				}
			}
		}
	}

	public String logicalName()
	{
		return logicalName;
	}

	public String physicalName()
	{
		return physicalName;
	}

	public List<DatabaseColumn> columns()
	{
		return Collections.unmodifiableList(columns);
	}

	public Dimension dimension()
	{
		return dimension;
	}

	@Override
	public int compareTo(DimensionKey o)
	{
		return CompareUtils.compare(logicalName, o.logicalName, physicalName, o.physicalName);
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
		if (removed())
		{
			file.delete();
		}
		else
		{
			// write the dimension attribute object
			BufferedWriter writer = new BufferedWriter(file.writer());

			try
			{
				JsonWriter jsonWriter = new JsonWriter(new BufferedWriter(writer));
				jsonWriter.beginObject();

				jsonWriter.name("logical-name").value(logicalName());
				jsonWriter.name("physical-name").value(physicalName());

				jsonWriter.name("column-names").beginArray();

				for (DatabaseColumn i : columns)
				{
					jsonWriter.value(i.name());
				}

				jsonWriter.endArray();

				jsonWriter.endObject();
				jsonWriter.flush();
			}
			finally
			{
				writer.close();
			}
		}
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{

	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		DimensionKey that = (DimensionKey) o;

		if (!logicalName.equals(that.logicalName))
		{
			return false;
		}

		if (!physicalName.equals(that.physicalName))
		{
			return false;
		}

		if (!dimension.qualifiedName().equals(that.dimension.qualifiedName()))
		{
			return false;
		}

		return columns.equals(that.columns);
	}

	@Override
	public int hashCode()
	{
		int result = logicalName.hashCode();
		result = 31 * result + physicalName.hashCode();
		result = 31 * result + dimension.qualifiedName().hashCode();
		result = 31 * result + columns.hashCode();
		return result;
	}

	public static class DimensionKeyBuilder
	{
		private final Dimension dimension;
		private final List<String> columns = new ArrayList<String>();
		private String logicalName;
		private String physicalName;

		public DimensionKeyBuilder(Dimension dimension)
		{
			this.dimension = dimension;
		}

		public DimensionKeyBuilder logicalName(String name)
		{
			this.logicalName = name;
			return this;
		}

		public DimensionKeyBuilder physicalName(String name)
		{
			this.physicalName = name;
			return this;
		}

		public DimensionKeyBuilder column(String name)
		{
			if (columns.contains(name))
			{
				throw new IllegalArgumentException("Name already added [" + name + "]");
			}

			columns.add(name);
			return this;
		}

		public DimensionKey create()
		{
			if (logicalName == null)
			{
				throw new IllegalStateException("Logical name must be supplied");
			}

			if (physicalName == null)
			{
				physicalName = logicalName;
			}

			if (columns.size() == 0)
			{
				throw new IllegalStateException("Columns must be supplied");
			}

			List<DatabaseColumn> cols = new ArrayList<DatabaseColumn>();

			Map<String, DatabaseColumn> columns1 = dimension.columns();

			for (String colName : columns)
			{
				DatabaseColumn e = columns1.get(colName);

				if (e == null)
				{
					throw new IllegalStateException("Column [" + colName + "] not present in dimension");
				}

				cols.add(e);
			}

			DimensionKey dk = new DimensionKey(dimension, logicalName, physicalName, cols);
			dimension.addDimensionKey(dk);

			return dk;
		}
	}
}
