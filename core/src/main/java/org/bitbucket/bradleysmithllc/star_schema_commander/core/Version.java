package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class Version
{
	public static final String _CURRENT_VERSION = "ssc.1.D";

	private final String version;
	private final long writeTimestamp = System.currentTimeMillis();
	private final String rid = rid();

	Version(String version)
	{
		this.version = version;
	}

	private static String rid()
	{
		StringBuilder stb = new StringBuilder();

		long ts = System.currentTimeMillis();

		// allocate a buffer with the length of the last three digits of the timestamp
		long lsd = ts % 10L;

		for (long i = 0; i < lsd; i++)
		{
			stb.append(i);
		}

		lsd = (ts / 10L) % 10L;

		for (long i = 0; i < lsd; i++)
		{
			stb.append(i);
		}

		lsd = (ts / 100L) % 10L;

		for (long i = 0; i < lsd; i++)
		{
			stb.append(i);
		}

		// lame fix for the file monitor.
		// append a random number up to the size of a long
		stb.append("_");
		stb.append((long) (Math.random() * Long.MAX_VALUE));
		stb.append("_");

		long chars = ((long) (Math.random() * Long.MAX_VALUE)) % 25L;

		for (int i = 0; i < chars; i++)
		{
			stb.append('+');
		}

		return stb.toString();
	}

	public String version()
	{
		return version;
	}
}
