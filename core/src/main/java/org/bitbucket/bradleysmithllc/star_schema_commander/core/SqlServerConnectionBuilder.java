package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class SqlServerConnectionBuilder extends BaseConnectionBuilder<SqlServerConnectionBuilder>
{
	public SqlServerConnectionBuilder(Profile profile)
	{
		super(profile);

		port(1433);
	}

	@Override
	public SqlServerConnectionBuilder serverName(String server)
	{
		return super.serverName(server);
	}

	@Override
	protected propertyRequired requiresServerName()
	{
		return propertyRequired.required;
	}

	@Override
	public SqlServerConnectionBuilder port(int p)
	{
		return super.port(p);
	}

	@Override
	protected propertyRequired requiresPort()
	{
		return propertyRequired.optional;
	}

	@Override
	public SqlServerConnectionBuilder username(String user)
	{
		return super.username(user);
	}

	@Override
	protected propertyRequired requiresUserName()
	{
		return propertyRequired.optional;
	}

	@Override
	public SqlServerConnectionBuilder password(String pass)
	{
		return super.password(pass);
	}

	@Override
	protected propertyRequired requiresPassword()
	{
		return propertyRequired.optional;
	}

	public SqlServerConnectionBuilder database(String d)
	{
		property("databaseName", d);
		return this;
	}

	public SqlServerConnectionBuilder instanceName(String ins)
	{
		if (ins == null)
		{
			clearProperty("instanceName");
		}
		else
		{
			property("instanceName", ins);
		}
		return this;
	}

	public SqlServerConnectionBuilder authenticationScheme(authenticationScheme scheme)
	{
		property("authenticationScheme", scheme.name());
		return this;
	}

	/**
	 * If this is enabled the username and password are not needed
	 *
	 * @return
	 */
	public SqlServerConnectionBuilder integratedSecurity()
	{
		property("integratedSecurity", "true");
		return this;
	}

	protected DatabaseConnection createSub()
	{
		DatabaseConnection dbo = new DatabaseConnection(name, properties("jdbc:sqlserver://" + serverName + (port != 1433 ? (":" + port) : "")), "com.microsoft.sqlserver.jdbc.SQLServerDriver", "dbo", username, password);
		return profile.addDatabaseConnection(dbo);
	}

	@Override
	protected String getVendorName()
	{
		return "SqlServer";
	}

	public enum authenticationScheme
	{
		NativeAuthentication,
		JavaKerberos
	}
}
