package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.net.ssl.SSLSocketFactory;

public class PostgreSQLConnectionBuilder extends BaseConnectionBuilder<PostgreSQLConnectionBuilder>
{
	private String database;

	public PostgreSQLConnectionBuilder(Profile profile)
	{
		super(profile);
		port(5432);
	}

	public PostgreSQLConnectionBuilder database(String db)
	{
		database = db;
		return this;
	}

	@Override
	public PostgreSQLConnectionBuilder serverName(String server)
	{
		return super.serverName(server);
	}

	@Override
	protected propertyRequired requiresServerName()
	{
		return propertyRequired.optional;
	}

	@Override
	public PostgreSQLConnectionBuilder port(int p)
	{
		return super.port(p);
	}

	@Override
	protected propertyRequired requiresPort()
	{
		return propertyRequired.optional;
	}

	@Override
	public PostgreSQLConnectionBuilder username(String user)
	{
		return super.username(user);
	}

	@Override
	protected propertyRequired requiresUserName()
	{
		return propertyRequired.optional;
	}

	@Override
	public PostgreSQLConnectionBuilder password(String pass)
	{
		return super.password(pass);
	}

	@Override
	protected propertyRequired requiresPassword()
	{
		return propertyRequired.optional;
	}

	/**
	 * Properties exposed as options
	 *
	 * @return
	 */
	public PostgreSQLConnectionBuilder useSSL()
	{
		property("ssl", "true");
		return this;
	}

	public PostgreSQLConnectionBuilder sslfactory(Class cl)
	{
		if (!SSLSocketFactory.class.isAssignableFrom(cl))
		{
			throw new IllegalArgumentException("Class <" + cl.getName() + "> does not extend " + SSLSocketFactory.class.getName());
		}

		property("sslfactory", cl.getName());
		return this;
	}

	public PostgreSQLConnectionBuilder sslfactoryarg(String clName)
	{
		property("sslfactoryarg", clName);
		return this;
	}

	public PostgreSQLConnectionBuilder compatible(String clName)
	{
		property("compatible", clName);
		return this;
	}

	public PostgreSQLConnectionBuilder charSet(String clName)
	{
		property("charSet", clName);
		return this;
	}

	public PostgreSQLConnectionBuilder allowEncodingChanges(boolean allow)
	{
		property("allowEncodingChanges", Boolean.toString(allow));
		return this;
	}

	public PostgreSQLConnectionBuilder prepareThreshold(int thresh)
	{
		property("prepareThreshold", String.valueOf(thresh));
		return this;
	}

	public PostgreSQLConnectionBuilder protocolV2()
	{
		property("protocolVersion", "2");
		return this;
	}

	public PostgreSQLConnectionBuilder protocolV3()
	{
		property("protocolVersion", "3");
		return this;
	}

	public PostgreSQLConnectionBuilder debugLoglevel()
	{
		property("loglevel", "2");
		return this;
	}

	public PostgreSQLConnectionBuilder infoLoglevel()
	{
		property("loglevel", "1");
		return this;
	}

	protected DatabaseConnection createSub()
	{
		StringBuilder stb = new StringBuilder("jdbc:postgresql:");

		if (database == null)
		{
			throw new IllegalStateException("PostgreSQL connection requires a database name");
		}

		boolean altered = false;
		if (serverName != null)
		{
			stb.append("//");
			stb.append(serverName);
			altered = true;
		}

		if (port != 5432)
		{
			if (!altered)
			{
				stb.append("//localhost");
			}

			stb.append(":");
			stb.append(port);

			altered = true;
		}

		if (database != null)
		{
			if (altered)
			{
				stb.append("/");
			}

			stb.append(database);
		}

		DatabaseConnection dbo = new DatabaseConnection(name, properties(stb.toString()),
			"org.postgresql.Driver",
			username,
			username,
			password
		);

		return profile.addDatabaseConnection(dbo);
	}

	@Override
	protected String getVendorName()
	{
		return "PostgreSQL";
	}
}
