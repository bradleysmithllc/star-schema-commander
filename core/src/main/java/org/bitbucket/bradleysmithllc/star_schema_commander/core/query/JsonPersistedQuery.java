package org.bitbucket.bradleysmithllc.star_schema_commander.core.query;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedReadableFile;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedWritableFile;

import java.io.*;
import java.util.List;

public class JsonPersistedQuery implements PersistedQuery
{
	private final String name;
	private final File path;

	private Query query;
	private QueryDataContainer queryDataContainer;

	public JsonPersistedQuery(Query query, String name, File dataDir)
	{
		this.name = name;
		path = dataDir;
		this.query = query;
	}

	public void receiveData(List<String> columns, List<RowData> data)
	{
		queryDataContainer = new QueryDataContainer(columns, data);
	}

	@Override
	public Query query()
	{
		return query;
	}

	@Override
	public String name()
	{
		return name;
	}

	@Override
	public List<String> columns()
	{
		if (queryDataContainer == null)
		{
			loadQueryContainer();
		}

		return queryDataContainer.getColumns();
	}

	@Override
	public List<RowData> data()
	{
		if (queryDataContainer == null)
		{
			loadQueryContainer();
		}

		return queryDataContainer.getRowData();
	}

	private void loadQueryContainer()
	{
		Gson gson = new GsonBuilder().create();

		try
		{
			FileReader json = new FileReader(path);

			try
			{
				queryDataContainer = gson.fromJson(new BufferedReader(json), QueryDataContainer.class);
			}
			finally
			{
				json.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void persist(PersistedProfile profile, PersistedWritableFile file) throws IOException
	{
	}

	@Override
	public void read(PersistedProfile profile, PersistedReadableFile file) throws IOException
	{
	}

	public void write()
	{
		try
		{
			// write out the query
			File queryF = new File(path, ObjectUtils.firstNonNull(name, query.getUuid()) + ".json");

			// persist meta information
			FileWriter fileWriter = new FileWriter(queryF);

			try
			{
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				query.persist(bufferedWriter);
				bufferedWriter.flush();
			}
			finally
			{
				fileWriter.close();
			}

			if (queryDataContainer != null)
			{
				// persist data
				File dataF = new File(path, ObjectUtils.firstNonNull(name, query.getUuid()) + "_data.json");

				// persist meta information
				fileWriter = new FileWriter(dataF);

				try
				{
					Gson gson = new GsonBuilder().create();
					BufferedWriter writer = new BufferedWriter(fileWriter);
					gson.toJson(queryDataContainer, writer);
					writer.flush();
				}
				finally
				{
					fileWriter.close();
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
