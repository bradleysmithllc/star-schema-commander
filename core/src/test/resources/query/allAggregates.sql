SELECT
	SUM(FACT.col1) AS col1,
	AVG(FACT.col3) AS col3,
	AVG(DISTINCT FACT.col4) AS col4,
	MIN(FACT.col5) AS col5,
	MAX(FACT.col6) AS col6,
	COUNT(FACT.col7) AS col7,
	COUNT(DISTINCT FACT.col8) AS col8,
	SUM(DISTINCT FACT.col9) AS col9,
	d_n.d AS d_n_d,
	d_n.d2 AS d_n_d2,
	d_n.d3 AS d_n_d3,
	d_n.d4 AS d_n_d4,
	"d_n_D 2".d AS "d_n_D 2_d",
	FACT.ddcol AS ddcol
FROM
	d.f AS FACT
JOIN
	DB.d.n AS d_n
		ON
			FACT.col1 = d_n.d
JOIN
	DB.d.n AS "d_n_D 2"
		ON
			FACT.col2 = "d_n_D 2".d2
			AND
			FACT.col3 = "d_n_D 2".d3
GROUP BY
	d_n.d,
	d_n.d2,
	d_n.d3,
	d_n.d4,
	"d_n_D 2".d,
	FACT.ddcol
