SELECT
	FACT.col1 AS col1,
	SUM(FACT.col2) AS "Column 2",
	d_n.d AS d_n_d,
	d_n.d2 AS d_n_d2,
	d_n.d3 AS d_n_d3,
	d_n.d4 AS d_n_d4,
	FACT.ddcol AS ddcol
FROM
	d.f AS FACT
JOIN
	DB.d.n AS d_n
		ON
			FACT.col1 = d_n.d
GROUP BY
	FACT.col1,
	d_n.d,
	d_n.d2,
	d_n.d3,
	d_n.d4,
	FACT.ddcol
