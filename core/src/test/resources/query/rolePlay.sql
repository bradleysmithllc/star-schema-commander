SELECT
	FACT.col2 AS "Column 2",
	d_n.d AS d_n_d,
	"d_n_D 2".d AS "d_n_D 2_d"
FROM
	d.f AS FACT
JOIN
	DB.d.n AS d_n
		ON
			FACT.col1 = d_n.d
JOIN
	DB.d.n AS "d_n_D 2"
		ON
			FACT.col2 = "d_n_D 2".d2
			AND
			FACT.col3 = "d_n_D 2".d3
