SELECT
	SUM(FACT.col1) AS alias1,
	AVG(FACT.col1) AS alias2,
	d_n.d AS d_n_d,
	d_n.d2 AS d_n_d2,
	d_n.d3 AS d_n_d3,
	d_n.d4 AS d_n_d4,
	FACT.ddcol AS ddcol
FROM
	d.f AS FACT
JOIN
	DB.d.n AS d_n
		ON
			FACT.col1 = d_n.d
GROUP BY
	d_n.d,
	d_n.d2,
	d_n.d3,
	d_n.d4,
	FACT.ddcol
