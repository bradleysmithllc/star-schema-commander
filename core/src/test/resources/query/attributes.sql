SELECT
	FACT.col1 AS col1,
	FACT.col10 AS col10,
	FACT.col3 AS col3,
	FACT.col4 AS col4,
	FACT.col5 AS col5,
	FACT.col6 AS col6,
	FACT.col7 AS col7,
	FACT.col8 AS col8,
	FACT.col9 AS col9,
	FACT.col2 AS "Column 2",
	d_n.d AS d_n_d,
	DB_dr_n_ASA.d AS d
FROM
	d.f AS FACT
JOIN
	DB.d.n AS d_n
		ON
			FACT.col1 = d_n.d
JOIN
	DB.dr.n AS DB_dr_n_ASA
		ON
			FACT.col2 = DB_dr_n_ASA.d
