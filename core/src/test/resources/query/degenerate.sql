SELECT
	FACT.col1 AS col1,
	FACT.col10 AS col10,
	FACT.col3 AS col3,
	FACT.col4 AS col4,
	FACT.col5 AS col5,
	FACT.col6 AS col6,
	FACT.col7 AS col7,
	FACT.col8 AS col8,
	FACT.col9 AS col9,
	FACT.col2 AS "Column 2",
	FACT.ddcol AS ddcol
FROM
	d.f AS FACT
