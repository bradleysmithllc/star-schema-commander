package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;

public class ColumnBuilderTest extends ProfileFactory
{
	@Test
	public void missingNameDimension() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Name may not be null");
		createProfile().defaultReportingArea().newDimension().schema("s").name("n").create().newDatabaseColumn().create();
	}

	@Test
	public void badJdbcTypeName() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Not a JDBC type name: VIOL");

		createProfile().defaultReportingArea().newDimension().schema("s").name("n").create().newDatabaseColumn().jdbcTypeName("VIOL").create();
	}

	@Test
	public void missingNameFact() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Name may not be null");
		createProfile().defaultReportingArea().newFact().schema("s").name("n").create().newDatabaseColumn().create();
	}

	@Test
	public void missingTypeDimension() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Type may not be null");
		createProfile().defaultReportingArea().newDimension().schema("s").name("n").create().newDatabaseColumn().name("n").create();
	}

	@Test
	public void missingTypeFact() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Type may not be null");
		createProfile().defaultReportingArea().newFact().schema("s").name("n").create().newDatabaseColumn().name("n").create();
	}

	@Test
	public void badTypeDimension() throws IOException
	{
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("Bad JDBC Type {2147483647}");
		createProfile().defaultReportingArea().newDimension().schema("s").name("n").create().newDatabaseColumn().name("n").jdbcType(Integer.MAX_VALUE).create();
	}

	@Test
	public void badTypeFact() throws IOException
	{
		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("Bad JDBC Type {2147483647}");
		createProfile().defaultReportingArea().newFact().schema("s").name("n").create().newDatabaseColumn().name("n").jdbcType(Integer.MAX_VALUE).create();
	}

	@Test
	public void test1() throws IOException
	{
		Profile profile = createProfile();
		Dimension dim = profile.defaultReportingArea().newDimension().schema("s").name("n").create();
		dim.newDatabaseColumn().name("BA_UNIFORM_RESOURCE_LOCATOR").jdbcType(Types.VARCHAR).create();
		profile.persist();

		Profile p = new Profile(profile.getProfileDirectory());
	}

	@Test
	public void createDimension() throws IOException
	{
		Profile p = createProfile();

		Dimension dimension = p.defaultReportingArea().newDimension().schema("s").name("n").create();
		dimension.newDatabaseColumn().name("n").jdbcType(Types.ARRAY).create();
		dimension.newDatabaseColumn().name("n2").jdbcTypeName("ARRAY").create();
		p.persist();

		Assert.assertNotNull(dimension.columns().get("n"));

		p = new Profile(p.getProfileDirectory());
		Dimension dimension1 = p.defaultReportingArea().dimensions().get("s_n");
		Assert.assertNotNull(dimension1);

		DatabaseColumn databaseColumn = dimension1.columns().get("n");
		Assert.assertNotNull(databaseColumn);
		Assert.assertEquals("ARRAY", databaseColumn.jdbcTypeName());

		databaseColumn = dimension1.columns().get("n2");
		Assert.assertNotNull(databaseColumn);
		Assert.assertEquals("ARRAY", databaseColumn.jdbcTypeName());

		// remove
		p.defaultReportingArea().dimensions().get("s_n").columns().get("n").remove();
		Assert.assertNotNull(p.defaultReportingArea().dimensions().get("s_n").columns().get("n"));
		p.persist();
		Assert.assertNull(p.defaultReportingArea().dimensions().get("s_n").columns().get("n"));

		p = new Profile(p.getProfileDirectory());
		Assert.assertNull(p.defaultReportingArea().dimensions().get("s_n").columns().get("n"));
	}

	@Test
	public void createFact() throws IOException
	{
		Profile p = createProfile();

		Fact fact = p.defaultReportingArea().newFact().schema("s").name("n").create();
		fact.newDatabaseColumn().name("n").jdbcType(Types.ARRAY).create();
		p.persist();

		Assert.assertNotNull(fact.columns().get("n"));

		p = new Profile(p.getProfileDirectory());
		Fact fact1 = p.defaultReportingArea().facts().get("s_n");
		Assert.assertNotNull(fact1);
		Assert.assertNotNull(fact1.columns().get("n"));

		// remove
		p.defaultReportingArea().facts().get("s_n").columns().get("n").remove();
		Assert.assertNotNull(p.defaultReportingArea().facts().get("s_n").columns().get("n"));
		p.persist();
		Assert.assertNull(p.defaultReportingArea().facts().get("s_n").columns().get("n"));

		p = new Profile(p.getProfileDirectory());
		Assert.assertNull(p.defaultReportingArea().facts().get("s_n").columns().get("n"));
	}
}
