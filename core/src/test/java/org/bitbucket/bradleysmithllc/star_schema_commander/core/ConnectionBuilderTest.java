package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileReader;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

public class ConnectionBuilderTest extends ProfileFactory
{
	private static AtomicLong index = new AtomicLong(System.currentTimeMillis());

	@Test
	public void h2ConnectionMissingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newH2Connection().create();
	}

	@Test
	public void h2ConnectionCreate() throws IOException
	{
		createProfile().newH2Connection().name("a").create();
	}

	@Test
	public void h2ConnectionPathMustBeAFile() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Must use a regular file as a data source.");

		createProfile().newH2Connection().name("a").path(temporaryFolder.newFolder()).create();
	}

	@Test
	public void nullUser() throws IOException
	{
		File path1 = temporaryFolder.newFile();

		Profile profile = createProfile();
		profile.newH2Connection().name("a").path(path1).create();
		profile.persist();

		profile = new Profile(profile.getProfileDirectory());
		Assert.assertNull(profile.connections().get("a").userName());
	}

	@Test
	public void notNullUser() throws IOException
	{
		File path1 = temporaryFolder.newFile();

		Profile profile = createProfile();
		profile.newH2Connection().name("b").path(path1).username("user").create();
		profile.persist();

		profile = new Profile(profile.getProfileDirectory());
		Assert.assertEquals("user", profile.connections().get("b").userName());
	}

	@Test
	public void nullPassword() throws IOException
	{
		File path1 = temporaryFolder.newFile();

		Profile profile = createProfile();
		profile.newH2Connection().name("a").path(path1).create();
		profile.persist();

		profile = new Profile(profile.getProfileDirectory());
		Assert.assertNull(profile.connections().get("a").password());
	}

	@Test
	public void notNullPassword() throws IOException
	{
		File path1 = temporaryFolder.newFile();

		Profile profile = createProfile();
		profile.newH2Connection().name("b").path(path1).password("user").create();
		profile.persist();

		profile = new Profile(profile.getProfileDirectory());
		Assert.assertEquals("user", profile.connections().get("b").password());
	}

	@Test
	public void h2ConnectionPath() throws IOException
	{
		File path1 = temporaryFolder.newFile();
		Assert.assertEquals("jdbc:h2:" + path1.getAbsolutePath(), createProfile().newH2Connection().name("a").path(path1).create().jdbcUrl());
	}

	@Test
	public void h2ConnectionProperties() throws IOException
	{
		File path1 = temporaryFolder.newFile();
		Assert.assertEquals("jdbc:h2:" + path1.getAbsolutePath() + ";a=b", createProfile().newH2Connection().name("a").property("a", "b").path(path1).create().jdbcUrl());
	}

	@Test
	public void sqlConnectionUserTrusted() throws IOException
	{
		createProfile().newSqlServerJTDSConnection().name("name").serverName("server").domain("d").create();
	}

	@Test
	public void sqlConnectionUserNotTrusted() throws IOException
	{
		createProfile().newSqlServerJTDSConnection().name("name").serverName("server").username("user").password("pass").create();
	}

	@Test
	public void sqlConnectionMissingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newSqlServerJTDSConnection().create();
	}

	@Test
	public void sqlConnectionBlankName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newSqlServerJTDSConnection().name("").create();
	}

	@Test
	public void sqlConnectionSpacesName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newSqlServerJTDSConnection().name("     ").create();
	}

	@Test
	public void sqlConnectionMissingServerName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("SqlServer Connection requires a server name");

		createProfile().newSqlServerJTDSConnection().name("m").create();
	}

	@Test
	public void sqlConnectionServerName() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDefaultPort() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").port(1433).create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionPort() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").port(1434).create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server:1434", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDomain() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").domain("domain").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server;domain=domain;useNTLMv2=true", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionInstance() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").instanceName("InStA").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server;instance=InStA", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDomainInstance() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").domain("domain").instanceName("ins").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server;domain=domain;instance=ins;useNTLMv2=true", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionInstancePort() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").instanceName("InStA").port(142).create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server:142;instance=InStA", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDatabase() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").database("db").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server/db", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDatabasePort() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").database("db").port(145).create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server:145/db", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDatabasePortInstance() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").database("db").port(145).instanceName("ins").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server:145/db;instance=ins", co.jdbcUrl());
	}

	@Test
	public void sqlConnectionDatabaseInstance() throws IOException
	{
		DatabaseConnection co = createProfile().newSqlServerJTDSConnection().name("m").serverName("server").database("db").instanceName("ins").create();
		Assert.assertEquals("jdbc:jtds:sqlserver://server/db;instance=ins", co.jdbcUrl());
	}

	@Test
	public void persistConnections() throws IOException
	{
		Profile profile = createProfile();
		profile.newSqlServerJTDSConnection().name("name").serverName("server").database("database").instanceName("instance").username("user").password("password").create();
		profile.persist();

		// verify the file exists
		File name = connection("name");
		Assert.assertTrue(name.exists());

		// verify the state
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

		StringWriter stw = new StringWriter();
		TFileReader treader = new TFileReader(name);

		try
		{
			IOUtils.copy(treader, stw);
		}
		finally
		{
			treader.close();
		}

		String json = stw.toString();

		DatabaseConnection conn = gson.fromJson(json, DatabaseConnection.class);
		conn.decryptPassword();

		Assert.assertEquals("name", conn.name());
		Assert.assertEquals("jdbc:jtds:sqlserver://server/database;instance=instance", conn.jdbcUrl());
		Assert.assertEquals("net.sourceforge.jtds.jdbc.Driver", conn.driverClass());
		Assert.assertEquals("dbo", conn.defaultSchema());
		Assert.assertEquals("user", conn.userName());
		Assert.assertEquals("password", conn.password());
	}

	@Test
	public void removeConnection() throws IOException
	{
		Profile profile = createProfile();
		DatabaseConnection conn = profile.newSqlServerJTDSConnection().name("name").serverName("server").database("database").instanceName("instance").create();
		profile.persist();

		// verify the file exists
		Profile p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNotNull(p2.connections().get("name"));

		// remove
		conn.remove();

		// still exists
		p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNotNull(p2.connections().get("name"));
		Assert.assertNotNull(profile.connections().get("name"));

		profile.persist();

		// now gone
		Assert.assertNull(profile.connections().get("name"));
		p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNull(p2.connections().get("name"));
	}

	@Test
	public void duplicateConnection() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Duplicate connection name [name]");

		Profile profile = createProfile();
		DatabaseConnection conn = profile.newSqlServerJTDSConnection().name("name").serverName("server").database("database").instanceName("instance").create();
		profile.newSqlServerJTDSConnection().name("name").serverName("ser").database("db").create();
	}

	private File connection(String name)
	{
		return new TFile(connections(), name + ".json");
	}

	private File connections()
	{
		return new TFile(path, "connections");
	}

	//@Test
	public void testActual() throws IOException, SQLException
	{
		Connection co = createProfile().newSqlServerConnection().name("m").serverName("bsmithvm").username("ciadmin").password("ciadmin").database("CustomerMaster").create().open();
		DatabaseMetaData md = co.getMetaData();

		ResultSet tables = md.getTables(null, null, null, null);

		while (tables.next())
		{
			System.out.println(tables.getString(1) + "." + tables.getString(2) + "." + tables.getString(3));
		}

		tables.close();

		System.out.println("Exported from CustomerPartyInfo");
		tables = md.getExportedKeys(null, null, "CustomerPartyInfo");

		while (tables.next())
		{
			String pkName = tables.getString(13);
			String PKCatalog = tables.getString(1);
			String PKSchema = tables.getString(2);
			String PKTable = tables.getString(3);
			String PKColumn = tables.getString(4);

			String fkName = tables.getString(12);
			String FKCatalog = tables.getString(5);
			String FKSchema = tables.getString(6);
			String FKTable = tables.getString(7);
			String FKColumn = tables.getString(8);

			System.out.println(PKCatalog + "." + PKSchema + "." + PKTable + "." + PKColumn + "{" + pkName + "} << " + FKCatalog + "." + FKSchema + "." + FKTable + "." + FKColumn + "{" + fkName + "}");
		}
		tables.close();

		System.out.println("\n\n\nImported from CustomerPartyInfo");
		tables = md.getImportedKeys(null, null, "CustomerPartyInfo");

		while (tables.next())
		{
			String pkName = tables.getString(13);
			String PKCatalog = tables.getString(1);
			String PKSchema = tables.getString(2);
			String PKTable = tables.getString(3);
			String PKColumn = tables.getString(4);

			String fkName = tables.getString(12);
			String FKCatalog = tables.getString(5);
			String FKSchema = tables.getString(6);
			String FKTable = tables.getString(7);
			String FKColumn = tables.getString(8);

			System.out.println(PKCatalog + "." + PKSchema + "." + PKTable + "." + PKColumn + "{" + pkName + "} << " + FKCatalog + "." + FKSchema + "." + FKTable + "." + FKColumn + "{" + fkName + "}");
		}
		tables.close();

		System.out.println("Exported from CustomerInfo");
		tables = md.getExportedKeys(null, null, "CustomerInfo");

		while (tables.next())
		{
			String pkName = tables.getString(13);
			String PKCatalog = tables.getString(1);
			String PKSchema = tables.getString(2);
			String PKTable = tables.getString(3);
			String PKColumn = tables.getString(4);

			String fkName = tables.getString(12);
			String FKCatalog = tables.getString(5);
			String FKSchema = tables.getString(6);
			String FKTable = tables.getString(7);
			String FKColumn = tables.getString(8);

			System.out.println(PKCatalog + "." + PKSchema + "." + PKTable + "." + PKColumn + "{" + pkName + "} << " + FKCatalog + "." + FKSchema + "." + FKTable + "." + FKColumn + "{" + fkName + "}");
		}
		tables.close();

		System.out.println("\n\n\nImported from CustomerInfo");
		tables = md.getImportedKeys(null, null, "CustomerInfo");

		while (tables.next())
		{
			String pkName = tables.getString(13);
			String PKCatalog = tables.getString(1);
			String PKSchema = tables.getString(2);
			String PKTable = tables.getString(3);
			String PKColumn = tables.getString(4);

			String fkName = tables.getString(12);
			String FKCatalog = tables.getString(5);
			String FKSchema = tables.getString(6);
			String FKTable = tables.getString(7);
			String FKColumn = tables.getString(8);

			System.out.println(PKCatalog + "." + PKSchema + "." + PKTable + "." + PKColumn + "{" + pkName + "} << " + FKCatalog + "." + FKSchema + "." + FKTable + "." + FKColumn + "{" + fkName + "}");
		}
		tables.close();

		System.out.println("\n\n\nExported from Party");
		tables = md.getExportedKeys(null, null, "Party");

		while (tables.next())
		{
			String PKCatalog = tables.getString(1);
			String PKSchema = tables.getString(2);
			String PKTable = tables.getString(3);
			String PKColumn = tables.getString(4);

			String FKCatalog = tables.getString(5);
			String FKSchema = tables.getString(6);
			String FKTable = tables.getString(7);
			String FKColumn = tables.getString(8);

			System.out.println(PKCatalog + "." + PKSchema + "." + PKTable + "." + PKColumn + " << " + FKCatalog + "." + FKSchema + "." + FKTable + "." + FKColumn);
		}
		tables.close();

		System.out.println("\n\n\nImported from Party");
		tables = md.getImportedKeys(null, null, "Party");

		while (tables.next())
		{
			String PKCatalog = tables.getString(1);
			String PKSchema = tables.getString(2);
			String PKTable = tables.getString(3);
			String PKColumn = tables.getString(4);

			String FKCatalog = tables.getString(5);
			String FKSchema = tables.getString(6);
			String FKTable = tables.getString(7);
			String FKColumn = tables.getString(8);

			System.out.println(PKCatalog + "." + PKSchema + "." + PKTable + "." + PKColumn + " << " + FKCatalog + "." + FKSchema + "." + FKTable + "." + FKColumn);
		}
		tables.close();
	}

	//@Test
	public void testAD() throws IOException, SQLException
	{
		Connection co = createProfile().newSqlServerConnection().name("m").serverName("bsmithvm").integratedSecurity().authenticationScheme(SqlServerConnectionBuilder.authenticationScheme.JavaKerberos).username("adadmin8").password("!@#123ASDasd").instanceName("SQL2K8AD").create().open();
		DatabaseMetaData md = co.getMetaData();

		ResultSet tables = md.getTables(null, null, null, null);

		while (tables.next())
		{
			System.out.println(tables.getString(1) + "." + tables.getString(2) + "." + tables.getString(3));
		}
	}

	@Test
	public void oracleConnectionMissingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newOracleConnection().create();
	}

	@Test
	public void oracleConnectionMissingServer() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Oracle Connection requires a server name");

		createProfile().newOracleConnection().name("n").create();
	}

	@Test
	public void oracleConnectionMissingUser() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Oracle Connection requires a user name");

		createProfile().newOracleConnection().name("n").serverName("se").create();
	}

	@Test
	public void oracleConnectionMissingPassword() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Oracle Connection requires a password");

		createProfile().newOracleConnection().name("n").serverName("se").username("u").create();
	}

	@Test
	public void oracleConnection() throws IOException
	{
		Assert.assertEquals("jdbc:oracle:thin:@//server", createProfile().newOracleConnection().name("n").serverName("server").username("u").password("p").create().jdbcUrl());
	}

	@Test
	public void oracleConnectionPort() throws IOException
	{
		Assert.assertEquals("jdbc:oracle:thin:@//serve:10", createProfile().newOracleConnection().name("n").serverName("serve").username("u").password("p").port(10).create().jdbcUrl());
	}

	@Test
	public void oracleConnectionService() throws IOException
	{
		Assert.assertEquals("jdbc:oracle:thin:@//serv/service", createProfile().newOracleConnection().name("n").serverName("serv").serviceName("service").username("u").password("p").create().jdbcUrl());
	}

	@Test
	public void oracleConnectionServicePort() throws IOException
	{
		Assert.assertEquals("jdbc:oracle:thin:@//serv:11/service", createProfile().newOracleConnection().name("n").serverName("serv").serviceName("service").port(11).username("u").password("p").create().jdbcUrl());
	}

	@Test
	public void postgreSQLConnectionMissingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newPostgreSQLConnection().create();
	}

	@Test
	public void postgreSQLConnectionMissingDatabase() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("PostgreSQL connection requires a database name");

		createProfile().newPostgreSQLConnection().name("ps").create();
	}

	@Test
	public void postgreSQLConnectionDatabase() throws IOException
	{
		Assert.assertEquals("jdbc:postgresql:db", createProfile().newPostgreSQLConnection().name("ps").database("db").create().jdbcUrl());
	}

	@Test
	public void postgreSQLConnectionDatabaseServer() throws IOException
	{
		Assert.assertEquals("jdbc:postgresql://server/database", createProfile().newPostgreSQLConnection().name("ps").serverName("server").database("database").create().jdbcUrl());
	}

	@Test
	public void postgreSQLConnectionDatabasePort() throws IOException
	{
		Assert.assertEquals("jdbc:postgresql://localhost:65/database", createProfile().newPostgreSQLConnection().name("ps").port(65).database("database").create().jdbcUrl());
	}

	@Test
	public void postgreSQLConnectionDatabaseServerPort() throws IOException
	{
		Assert.assertEquals("jdbc:postgresql://server:65/database", createProfile().newPostgreSQLConnection().name("ps").serverName("server").port(65).database("database").create().jdbcUrl());
	}

	//@Test
	public void one() throws SQLException
	{
		/*
		Profile p = new Profile(Profile.defaultProfileDir());
		DatabaseConnection conn = p.connections().get("h2_local");

		Connection co = conn.open();

		co.createStatement().execute("ALTER TABLE PUBLIC.FACT\n" +
			"ADD FOREIGN KEY (DIM_ID2)\n" +
			"REFERENCES PUBLIC.DIM(ID);");

		co.close();
		 */
	}

	@Test
	public void hanaConnectionMissingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Connection requires a name");

		createProfile().newHANAConnection().create();
	}

	@Test
	public void hanaConnectionMissingServer() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("SAP HANA Connection requires a server name");

		createProfile().newHANAConnection().name("s").create();
	}

	@Test
	public void hanaConnectionMissingUser() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("SAP HANA Connection requires a user name");

		createProfile().newHANAConnection().name("s").serverName("s").create();
	}

	@Test
	public void hanaConnectionMissingPassword() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("SAP HANA Connection requires a password");

		createProfile().newHANAConnection().name("s").serverName("s").username("u").create();
	}

	@Test
	public void hanaConnectionCreate() throws IOException
	{
		createProfile().newHANAConnection().name("a").serverName("local").username("usr").password("p").create();
	}

	@Test
	public void hanaConnectionDefaultPort() throws IOException
	{
		Assert.assertEquals("jdbc:sap://srvrnm:30015", createProfile().newHANAConnection().name("srvr").serverName("srvrnm").username("usr").password("psw").create().jdbcUrl());
	}

	@Test
	public void hanaConnectionPortOverride() throws IOException
	{
		Assert.assertEquals("jdbc:sap://srvrnm:32115", createProfile().newHANAConnection().name("srvr").serverName("srvrnm").username("usr").password("psw").instanceNumber(21).create().jdbcUrl());
	}
}
