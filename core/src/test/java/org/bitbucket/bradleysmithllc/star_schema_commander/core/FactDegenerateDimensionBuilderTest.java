package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;
import java.util.List;

public class FactDegenerateDimensionBuilderTest extends ProfileFactory
{
	@Test
	public void columnsRequired() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Must supply a column name.");

		Profile profile = createProfile();
		profile.defaultReportingArea().newFact().schema("s").name("phy").create().newDegenerateDimension().create();
	}

	@Test
	public void inheritSchema() throws IOException
	{
		Profile profile = createProfile();
		Fact fact = getFact(profile, null);
		Assert.assertEquals("s", fact.newDegenerateDimension().column("ddcol").create().schema());
	}

	@Test
	public void logicalAttributeNames() throws IOException
	{
		Profile profile = createProfile();
		Fact fact = getFact(profile, null);

		Dimension dd = fact.newDegenerateDimension().column("ddcol").column("ddcol1", "LogicalCol1").create();

		Assert.assertEquals("ddcol", dd.attributes().get("ddcol").logicalName());
		Assert.assertEquals("ddcol", dd.attributes().get("ddcol").column().name());
		Assert.assertEquals("LogicalCol1", dd.attributes().get("LogicalCol1").logicalName());
		Assert.assertEquals("ddcol1", dd.attributes().get("LogicalCol1").column().name());
	}

	private Fact getFact(Profile profile, String logicalName)
	{
		Fact.FactBuilder fb = profile.defaultReportingArea().newFact().schema("s").name("phy");

		if (logicalName != null)
		{
			fb = fb.logicalName("fact");
		}

		Fact fact = fb.create();

		fact.newDatabaseColumn().name("ddcol").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("ddcol1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("ddcol2").jdbcType(Types.INTEGER).create();

		return fact;
	}

	@Test
	public void inheritName() throws IOException
	{
		Profile profile = createProfile();
		Assert.assertEquals("phy", getFact(profile, null).newDegenerateDimension().column("ddcol").create().name());
	}

	@Test
	public void logicalName() throws IOException
	{
		Profile profile = createProfile();
		Assert.assertEquals("DDF", getFact(profile, null).newDegenerateDimension().column("ddcol").logicalName("DDF").create().logicalName());
	}

	@Test
	public void defaultLogicalName() throws IOException
	{
		Profile profile = createProfile();
		Assert.assertEquals("DD_fact", getFact(profile, "fact").newDegenerateDimension().column("ddcol").create().logicalName());
	}

	@Test
	public void defaultLogicalPhysicalName() throws IOException
	{
		Profile profile = createProfile();
		Assert.assertEquals("DD_s_phy", getFact(profile, null).newDegenerateDimension().column("ddcol").create().logicalName());
	}

	@Test
	public void attributes() throws IOException
	{
		Profile profile = createProfile();
		Dimension dim = getFact(profile, null).newDegenerateDimension().column("ddcol").logicalName("DDF").create();

		DimensionAttribute attr = dim.attributes().get("ddcol");
		Assert.assertNotNull(attr);
	}

	@Test
	public void dk() throws IOException
	{
		Profile profile = createProfile();
		Dimension dim = getFact(profile, null).newDegenerateDimension().column("ddcol").column("ddcol2").logicalName("DDF").create();

		DimensionKey ddf = dim.keys().get("DDF");
		Assert.assertNotNull(ddf);

		// check columns
		Assert.assertEquals(2, ddf.columns().size());
		Assert.assertEquals("ddcol", ddf.columns().get(0).name());
		Assert.assertEquals("ddcol2", ddf.columns().get(1).name());
	}

	@Test
	public void oneDimension() throws IOException
	{
		Profile profile = createProfile();
		Fact fact = getFact(profile, null);

		Assert.assertFalse(fact.hasDegenerateDimension());
		Assert.assertNull(fact.degenerateDimension());

		Dimension dim = fact.newDegenerateDimension().column("ddcol").column("ddcol2").logicalName("DDF").create();

		Assert.assertTrue(fact.hasDegenerateDimension());
		Assert.assertNotNull(fact.degenerateDimension());
		Assert.assertSame(dim, fact.degenerateDimension());
	}

	@Test
	public void thereCanBeOnlyOne() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Degenerate Dimension already exists");

		Profile profile = createProfile();
		Fact fact = getFact(profile, null);

		fact.newDegenerateDimension().column("ddcol").column("ddcol2").logicalName("DDF").create();
		fact.newDegenerateDimension();
	}

	@Test
	public void dontTryToCheat() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Degenerate Dimension already exists");

		Profile profile = createProfile();
		Fact fact = getFact(profile, null);

		Fact.DegenerateDimensionBuilder ddb1 = fact.newDegenerateDimension().column("ddcol").column("ddcol2").logicalName("DDF1");
		Fact.DegenerateDimensionBuilder ddb2 = fact.newDegenerateDimension().column("ddcol").column("ddcol2").logicalName("DDF2");

		ddb1.create();
		ddb2.create();
	}

	@Test
	public void fdk() throws IOException
	{
		Profile profile = createProfile();
		Fact fact = getFact(profile, null);

		Dimension dim = fact.newDegenerateDimension().column("ddcol").column("ddcol2").logicalName("DDF").create();

		FactDimensionKey ddf = fact.keys().get("DDF_DegenerateDimension");
		Assert.assertNotNull(ddf);

		Assert.assertSame(dim.keys().get("DDF"), ddf.dimensionKey());

		Assert.assertEquals("DegenerateDimension", ddf.roleName());

		Assert.assertEquals("DDF", ddf.physicalName());
		Assert.assertEquals("DDF_DegenerateDimension", ddf.name());

		// check joins
		List<StarJoin> starJoins = ddf.joins();

		Assert.assertEquals(2, starJoins.size());
		Assert.assertEquals("ddcol", starJoins.get(0).from().name());
		Assert.assertEquals("ddcol", starJoins.get(0).to().name());

		Assert.assertEquals("ddcol2", starJoins.get(1).from().name());
		Assert.assertEquals("ddcol2", starJoins.get(1).to().name());

		//Assert.assertEquals("ddcol2", ddf.columns().get(1).name());
	}

	@Test
	public void columns() throws IOException
	{
		Profile profile = createProfile();
		Dimension dim = getFact(profile, null).newDegenerateDimension().column("ddcol").logicalName("DDF").create();

		DatabaseColumn attr = dim.columns().get("ddcol");
		Assert.assertNotNull(attr);
	}
}
