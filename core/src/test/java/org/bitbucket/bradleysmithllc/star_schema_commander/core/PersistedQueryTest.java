package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.PersistedQuery;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.RowData;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

public class PersistedQueryTest extends ProfileFactory
{
	@Test
	public void testEmpty() throws IOException, SQLException
	{
		Profile p = createProfile();

		Fact fact = p.defaultReportingArea().newFact().schema("PUBLIC").name("fact").create();

		fact.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();

		fact.newFactMeasure().column("col").logicalName("Column 1").create();
		fact.newFactMeasure().column("col2").logicalName("Column 2").create();
		fact.newFactMeasure().column("col3").logicalName("Column 3").create();

		DatabaseConnection conn = getDatabaseConnection(p, "plainFact");

		PersistedQuery pq = p.defaultReportingArea().persist(fact.newQuery().create(), conn);

		Assert.assertEquals(3, pq.columns().size());
		Assert.assertEquals("Column 1", pq.columns().get(0));
		Assert.assertEquals("Column 2", pq.columns().get(1));
		Assert.assertEquals("Column 3", pq.columns().get(2));

		Assert.assertEquals(4, pq.data().size());

		asrt(pq.data().get(0), "1", "2", "3");
		asrt(pq.data().get(1), "2", "3", "4");
		asrt(pq.data().get(2), "3", "4", "5");
	}

	private DatabaseConnection getDatabaseConnection(Profile p, String name) throws IOException, SQLException
	{
		DatabaseConnection conn = p.newH2Connection().name(name).path(temporaryFolder.newFile()).create();

		Connection connection = conn.open();

		try
		{
			Statement statement = connection.createStatement();

			try
			{
				statement.execute(IOUtils.toString(getClass().getResource("/db/" + name + ".db.sql")));
			}
			finally
			{
				statement.close();
			}
		}
		finally
		{
			connection.close();
		}

		return conn;
	}

	private void asrt(RowData data, String... expecteds)
	{
		Assert.assertEquals(expecteds[0], data.get(0));
		Assert.assertEquals(expecteds[1], data.get(1));
		Assert.assertEquals(expecteds[2], data.get(2));
	}
}