package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class ReportingAreaBuilderTest extends ProfileFactory
{
	@Test
	public void missingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Name must be supplied");
		createProfile().defaultReportingArea().newReportingArea().create();
	}

	@Test
	public void qualifiedName() throws IOException
	{
		Profile profile = createProfile();
		ReportingArea ra = profile.defaultReportingArea().newReportingArea().name("a").create();

		Assert.assertEquals("a", ra.qualifiedName());
		Assert.assertEquals("a", ra.name());

		ReportingArea ra2 = ra.newReportingArea().name("b").create();
		Assert.assertEquals("a.b", ra2.qualifiedName());
		Assert.assertEquals("b", ra2.name());

		Assert.assertNotNull(profile.defaultReportingArea().reportingAreas().get("a"));
		Assert.assertNotNull(profile.defaultReportingArea().reportingAreas().get("a.b"));
		Assert.assertNotNull(ra.reportingAreas().get("a.b"));
	}

	@Test
	public void badName() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Name must must not contain dots [.]");
		createProfile().defaultReportingArea().newReportingArea().name("a.").create();
	}

	@Test
	public void duplicateName() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Reporting area [a] already exists");
		ReportingArea reportingArea = createProfile().defaultReportingArea();
		reportingArea.newReportingArea().name("a").create();
		reportingArea.newReportingArea().name("a").create();
	}

	@Test
	public void persistence() throws Exception
	{
		Profile profile1 = createProfile();
		Profile profile2 = new Profile(profile1.getProfileDirectory());

		final Semaphore sem = new Semaphore(1);
		sem.drainPermits();

		final Semaphore ready_sem = new Semaphore(1);
		ready_sem.drainPermits();

		profile2.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				sem.release();
			}

			@Override
			public void profileInitialized()
			{
				ready_sem.release();
			}
		});

		ready_sem.acquireUninterruptibly();

		ReportingArea reportingArea = profile1.defaultReportingArea();
		reportingArea.newReportingArea().name("a").create();
		profile1.persist();

		sem.acquireUninterruptibly();

		Assert.assertNotNull(profile2.defaultReportingArea().reportingAreas().get("a"));
		Assert.assertEquals(1, profile2.defaultReportingArea().children().size());

		profile1.defaultReportingArea().reportingAreas().get("a").remove();
		profile1.persist();

		sem.acquireUninterruptibly();

		Assert.assertNull(profile2.defaultReportingArea().reportingAreas().get("a"));
		Assert.assertEquals(0, profile2.defaultReportingArea().children().size());
		Assert.assertEquals(1, profile2.defaultReportingArea().reportingAreas().size());
	}

	@Test
	public void qualifiedNames() throws IOException
	{
		Profile profile = createProfile();

		// a -> b -> c, d
		ReportingArea defaultReportingArea = profile.defaultReportingArea();
		ReportingArea ra_a = defaultReportingArea.newReportingArea().name("a").create();
		ReportingArea ra_a_b = ra_a.newReportingArea().name("b").create();
		ReportingArea ra_a_b_c = ra_a_b.newReportingArea().name("c").create();
		ReportingArea ra_a_b_d = ra_a_b.newReportingArea().name("d").create();

		// e -> f -> g, h
		ReportingArea ra_e = defaultReportingArea.newReportingArea().name("e").create();
		ReportingArea ra_e_f = ra_e.newReportingArea().name("f").create();
		ReportingArea ra_e_f_g = ra_e_f.newReportingArea().name("g").create();
		ReportingArea ra_e_f_h = ra_e_f.newReportingArea().name("h").create();

		// assert all packages at the default level.  List includes the default
		Map<String, ReportingArea> reportingAreaMap = defaultReportingArea.reportingAreas();
		Assert.assertEquals(9, reportingAreaMap.size());

		Assert.assertNotNull(reportingAreaMap.get("[default]"));
		Assert.assertNotNull(reportingAreaMap.get("a"));
		Assert.assertNotNull(reportingAreaMap.get("a.b"));
		Assert.assertNotNull(reportingAreaMap.get("a.b.c"));
		Assert.assertNotNull(reportingAreaMap.get("a.b.d"));
		Assert.assertNotNull(reportingAreaMap.get("e"));
		Assert.assertNotNull(reportingAreaMap.get("e.f"));
		Assert.assertNotNull(reportingAreaMap.get("e.f.g"));
		Assert.assertNotNull(reportingAreaMap.get("e.f.h"));

		// assert on lower-scoped packages
		Assert.assertEquals(2, ra_a_b.reportingAreas().size());
		Assert.assertEquals(2, ra_e_f.reportingAreas().size());

		Assert.assertNotNull(ra_a_b.reportingAreas().get("a.b.c"));
		Assert.assertNotNull(ra_a_b.reportingAreas().get("a.b.d"));

		Assert.assertNotNull(ra_e_f.reportingAreas().get("e.f.g"));
		Assert.assertNotNull(ra_e_f.reportingAreas().get("e.f.h"));

		// verify one sub tree for children
		Assert.assertEquals(2, defaultReportingArea.children().size());
		Assert.assertEquals(1, ra_a.children().size());
		Assert.assertEquals(2, ra_a_b.children().size());
		Assert.assertEquals(0, ra_a_b_c.children().size());
		Assert.assertEquals(0, ra_a_b_d.children().size());

		// verify reporting areas are persisted
		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());

		ReportingArea dra = p2.defaultReportingArea();

		Assert.assertNotNull(dra.reportingAreas().get("a"));
	}
}
