package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.PersistedProfile;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class ProfileListenerTest extends ProfileFactory
{
	@Test
	public void removeListenerNotAdded() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Listener not registered");

		createProfile().removeProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
			}

			@Override
			public void profileInitialized()
			{
			}
		});
	}

	@Test
	public void addListeners() throws IOException, InterruptedException
	{
		final Semaphore counter = new Semaphore(5);
		final Semaphore ready_counter = new Semaphore(5);

		ready_counter.drainPermits();
		counter.drainPermits();
		final List<String> counterMap = new ArrayList<String>();

		Profile profile = createProfile();

		profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				put(counterMap, "1");
				System.out.println("1");
				counter.release(1);
			}

			@Override
			public void profileInitialized()
			{
				System.out.println("Init");
				ready_counter.release();
			}
		});

		profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				put(counterMap, "2");
				System.out.println("2");
				counter.release(1);
			}

			@Override
			public void profileInitialized()
			{
			}
		});

		profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				put(counterMap, "3");
				System.out.println("3");
				counter.release(1);
			}

			@Override
			public void profileInitialized()
			{
			}
		});

		profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				put(counterMap, "4");
				System.out.println("4");
				counter.release(1);
			}

			@Override
			public void profileInitialized()
			{
			}
		});

		profile.addProfileChangeListener(new PersistedProfile.ProfileChangeListener()
		{
			@Override
			public void profileChanged()
			{
				put(counterMap, "5");
				System.out.println("5");
				counter.release(1);
			}

			@Override
			public void profileInitialized()
			{
			}
		});

		ready_counter.acquireUninterruptibly();

		System.out.println("Change");
		profile.newH2Connection().name("n").create();
		profile.persist();

		counter.acquireUninterruptibly(5);

		profile.releaseProfileChangeListeners();

		Assert.assertEquals(0, counter.availablePermits());
	}

	@Test
	public void changeMinimumTS() throws IOException
	{
		Profile profile = createProfile();

		File profileJson = new File(profile.getProfileDirectory(), "profile.json");

		for (int i = 0; i < 10; i++)
		{
			long tspre = profileJson.lastModified();
			long lenpre = profileJson.length();

			String pre = FileUtils.readFileToString(profileJson);
			profile.persist();
			String post = FileUtils.readFileToString(profileJson);

			long tspost = profileJson.lastModified();
			long lenpost = profileJson.length();

			if (tspre == tspost && lenpre == lenpost)
			{
				Assert.fail("Trigger unchanged[" + i + "] {" + pre + "} >> {" + post + "}");
			}
		}
	}

	private synchronized void put(List<String> counterMap, String s)
	{
		if (counterMap.contains(s))
		{
			throw new IllegalStateException(s);
		}
		else
		{
			counterMap.add(s);
		}
	}
}
