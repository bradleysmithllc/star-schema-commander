package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;

public class FactsDimsAndKeysTest extends ProfileFactory
{
	@Test
	public void removeDimRemovesKeys() throws IOException
	{
		Profile p = createProfile();

		Fact f = p.defaultReportingArea().newFact().schema("s").name("f").create();
		f.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();

		Dimension dim = p.defaultReportingArea().newDimension().schema("s").name("d").create();
		dim.newDatabaseColumn().name("fk_col").jdbcType(Types.INTEGER).create();
		dim.newDimensionKey().column("fk_col").physicalName("PHY").logicalName("log").create();

		f.newFactDimensionKey().keyName("log").join("col", "fk_col").dimension("s_d").physicalName("physname").create();

		Assert.assertNotNull(f.keys().get("s_d"));

		dim.keys().get("log").remove();

		p.persist();
		p.invalidate();

		Assert.assertNull(p.defaultReportingArea().facts().get("s_f").keys().get("s_d"));
		Assert.assertNull(p.defaultReportingArea().dimensions().get("s_d").keys().get("log"));
	}

	@Test
	public void removeDegenKeyRemovesDim() throws IOException
	{
		Profile p = createProfile();

		Fact f = p.defaultReportingArea().newFact().schema("s").name("f").create();
		f.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();

		Assert.assertNull(p.defaultReportingArea().dimensions().get("DD_s_f"));
		f.newDegenerateDimension().column("col").create();

		Assert.assertNotNull(p.defaultReportingArea().dimensions().get("DD_s_f"));

		f.keys().get("DD_s_f_DegenerateDimension").remove();

		Assert.assertNotNull(p.defaultReportingArea().dimensions().get("DD_s_f"));

		p.persist();
		p.invalidate();

		Assert.assertNull(p.defaultReportingArea().facts().get("s_f").keys().get("DegenerateDimension"));
		Assert.assertNull(p.defaultReportingArea().dimensions().get("DD_s_f"));
	}
}
