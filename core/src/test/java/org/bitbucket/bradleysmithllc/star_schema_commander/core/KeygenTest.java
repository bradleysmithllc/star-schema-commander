package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RunWith(Parameterized.class)
public class KeygenTest extends ProfileFactory
{
	private final TestSpec testSpec;

	public KeygenTest(TestSpec spec)
	{
		testSpec = spec;
	}

	@Parameterized.Parameters(name = "id: {0}")
	public static Collection<Object[]> data() throws Exception
	{
		URL url = getSource("TestSpecs.json");

		JsonNode node = JsonLoader.fromURL(url);

		Gson gb = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

		TestSpecs specs = gb.fromJson(node.toString(), TestSpecs.class);

		List<Object[]> data = new ArrayList<Object[]>();

		for (Map.Entry<String, TestSpec> spec : specs.testSpecs.entrySet())
		{
			spec.getValue().id = spec.getKey();
			spec.getValue().db1ddl = loadDDL(spec.getKey() + ".ddl1");
			spec.getValue().db2ddl = loadDDL(spec.getKey() + ".ddl2");
			data.add(new TestSpec[]{spec.getValue()});
		}

		return data;
	}

	private static URL getSource(String s)
	{
		return QueryTest.class.getResource("/keygen/" + s);
	}

	private static String loadDDL(String s) throws IOException
	{
		URL url = getSource(s);

		if (url != null)
		{
			return IOUtils.toString(url);
		}

		return null;
	}

	@Test
	public void testSpec() throws Exception
	{
		if (testSpec.expectedExceptionClass != null)
		{
			expectedException.expect((Class) Class.forName(testSpec.expectedExceptionClass));
		}

		if (testSpec.expectedExceptionMessage != null)
		{
			expectedException.expectMessage(testSpec.expectedExceptionMessage);
		}

		// create a profile
		Profile prof = createProfile();

		// create two different H2 connections
		DatabaseConnection db1conn = null;
		DatabaseConnection db2conn = null;

		// execute the ddl for each
		if (testSpec.db1ddl != null)
		{
			db1conn = prof.newH2Connection().name("db1").create();
			buildDB(db1conn, testSpec.db1ddl);
		}

		if (testSpec.db2ddl != null)
		{
			db2conn = prof.newH2Connection().name("db2").create();
			buildDB(db2conn, testSpec.db2ddl);
		}

		if (db1conn == null)
		{
			throw new IllegalStateException("DB1 connection is null.  Cannot continue.");
		}

		// import all dims and facts from db1
		if (testSpec.importDims != null)
		{
			for (String dim : testSpec.importDims)
			{
				prof.defaultReportingArea().loadDimensionFromMetaData(db1conn, null, db1conn.defaultSchema(), dim);
			}
		}

		if (testSpec.importFacts != null)
		{
			for (String fact : testSpec.importFacts)
			{
				prof.defaultReportingArea().loadFactFromMetaData(db1conn, null, db1conn.defaultSchema(), fact);
			}
		}

		// import keys from db1
		prof.defaultReportingArea().generateKeys(db1conn, null);

		// and again from db2
		if (db2conn != null)
		{
			prof.defaultReportingArea().generateKeys(db2conn, null);
		}

		if (testSpec.dimKeys != null)
		{
			for (Map.Entry<String, KeySpec> dimKey : testSpec.dimKeys.entrySet())
			{
				Dimension dim = prof.defaultReportingArea().dimensions().get(dimKey.getKey());
				Assert.assertNotNull(dim);

				// assert number of keys
				Assert.assertEquals(dimKey.getValue().keys.size(), dim.keys().size());

				for (Map.Entry<String, Object> key : dimKey.getValue().keys.entrySet())
				{
					DimensionKey dk = dim.keys().get(key.getKey());

					Assert.assertNotNull(dk);
				}
			}
		}

		if (testSpec.factKeys != null)
		{
			for (Map.Entry<String, KeySpec> dimKey : testSpec.factKeys.entrySet())
			{
				Fact fact = prof.defaultReportingArea().facts().get(dimKey.getKey());
				Assert.assertNotNull(fact);

				// assert number of keys
				Assert.assertEquals(dimKey.getValue().keys.size(), fact.keys().size());

				for (Map.Entry<String, Object> key : dimKey.getValue().keys.entrySet())
				{
					FactDimensionKey dk = fact.physicalKeys().get(key.getKey());

					Assert.assertNotNull(dk);
				}
			}
		}
	}

	private void buildDB(DatabaseConnection connection, String db1ddl) throws SQLException
	{
		if (db1ddl == null)
		{
			return;
		}

		Connection con = connection.open();

		Statement st = con.createStatement();

		try
		{
			st.execute(db1ddl);
		}
		finally
		{
			st.close();
		}
	}
}

class TestSpecs
{
	Map<String, TestSpec> testSpecs;
}

class TestSpec
{
	String id;

	String db1ddl;
	String db2ddl;

	List<String> importDims;
	List<String> importFacts;

	String expectedExceptionClass;
	String expectedExceptionMessage;

	Map<String, KeySpec> factKeys;
	Map<String, KeySpec> dimKeys;

	@Override
	public String toString()
	{
		return "{" +
			"id='" + id + '\'' + '}';
	}
}

class KeySpec
{
	Map<String, Object> keys;

	@Override
	public String toString()
	{
		return "KeySpec{" +
			"keys=" + keys +
			'}';
	}
}
