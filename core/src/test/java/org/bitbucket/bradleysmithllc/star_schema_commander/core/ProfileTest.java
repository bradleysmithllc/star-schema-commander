package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.sql.Types;
import java.util.Iterator;
import java.util.Map;

public class ProfileTest extends ProfileFactory
{
	static final int[] TYPES = new int[]{
		Types.INTEGER
	};

	@Test
	public void createFolderExists() throws IOException
	{
		File file = temporaryFolder.newFolder();

		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Path already exists [" + file + "]");

		Profile.create(file);
	}

	@Ignore(value = "I don't care about this anymore")
	@Test
	public void createFolderDoesNotExist() throws IOException
	{
		File file = temporaryFolder.newFile("test.zip");
		FileUtils.forceDelete(file);

		Profile.create(file);

		// verify the folder is created, and that there is a connections folder
		Assert.assertTrue(file.exists());
		Assert.assertTrue(new File(file, "connections").exists());
	}

	@Ignore(value = "Not valid anymore")
	@Test
	public void profileFolderExists() throws IOException
	{
		File file = temporaryFolder.newFile("profile.zip");
		FileUtils.writeStringToFile(new File(file, "profile.json"), "{version: 'what'}");

		new Profile(file);
	}

	@Test
	public void profileFolderDoesNotExist() throws IOException
	{
		File file = temporaryFolder.newFile("profile.zip");
		FileUtils.forceDelete(file);

		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Profile path does not exist [" + file + "]");

		new Profile(file);
	}

	@Ignore(value = "Not valid anymore")
	@Test
	public void validateBadProfileDir() throws IOException
	{
		File file = temporaryFolder.newFolder();
		FileUtils.writeStringToFile(new File(file, "profile.json"), "{version: 'what'}");
		FileUtils.forceMkdir(new File(file, "con"));

		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Directory not part of Star(schema) Commander profile: [con]");
	}

	@Ignore(value = "Not valid anymore")
	@Test
	public void validateBadProfileSubDir() throws IOException
	{
		File file = temporaryFolder.newFolder();
		FileUtils.writeStringToFile(new File(file, "profile.json"), "{version: 'what'}");

		FileUtils.forceMkdir(new File(new File(file, "connections"), "blah"));

		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Directory not part of Star(schema) Commander profile: [blah]");
	}

	@Ignore(value = "Not valid anymore")
	@Test
	public void validateEmpty() throws IOException
	{
		File file = temporaryFolder.newFolder();
		FileUtils.writeStringToFile(new File(file, "profile.json"), "{version: 'what'}");
	}

	@Test
	public void validateInitialized() throws IOException
	{
		File file = temporaryFolder.newFolder();
		file.delete();

		Profile.create(file);
	}

	@Test
	public void connectionsInAlphaOrder() throws IOException
	{
		Profile p = createProfile();
		p.newH2Connection().name("h").create();

		Assert.assertEquals("h", p.connections().values().iterator().next().name());

		p.newH2Connection().name("a").create();

		Iterator<DatabaseConnection> iterator = p.connections().values().iterator();
		Assert.assertEquals("a", iterator.next().name());
		Assert.assertEquals("h", iterator.next().name());

		Iterator<String> kiterator = p.connections().keySet().iterator();
		Assert.assertEquals("a", kiterator.next());
		Assert.assertEquals("h", kiterator.next());

		p.newH2Connection().name("z").create();

		kiterator = p.connections().keySet().iterator();
		Assert.assertEquals("a", kiterator.next());
		Assert.assertEquals("h", kiterator.next());
		Assert.assertEquals("z", kiterator.next());
	}

	@Test
	public void connections() throws IOException, InterruptedException
	{
		Profile p = createProfile();

		p.newSqlServerJTDSConnection()
			.name("craig")
			.serverName("aardvark")
			.database("uoknor")
			.instanceName("bdsmith")
			.username("sqlUserName")
			.password("password")
			.create();

		Profile p2 = new Profile(p.getProfileDirectory());
		Assert.assertEquals(0, p2.connections().size());

		p.persist();
		p2.invalidate();

		Map<String, DatabaseConnection> con = p2.connections();
		Assert.assertEquals(1, con.size());

		DatabaseConnection co = con.get("craig");

		Assert.assertEquals("craig", co.name());
		Assert.assertEquals("dbo", co.defaultSchema());
		Assert.assertEquals("net.sourceforge.jtds.jdbc.Driver", co.driverClass());
		Assert.assertEquals("jdbc:jtds:sqlserver://aardvark/uoknor;instance=bdsmith", co.jdbcUrl());
		Assert.assertEquals("sqlUserName", co.userName());
		Assert.assertEquals("password", co.password());
	}

	@Test
	public void persistLargeProfile() throws IOException, InterruptedException
	{
		Profile p = createProfile();

		// create 100 connections
		for (int i = 0; i < 100; i++)
		{
			p.newSqlServerJTDSConnection()
				.name("craig_" + i)
				.serverName("aardvark")
				.database("uoknor")
				.instanceName("bdsmith")
				.username("sqlUserName")
				.password("password")
				.create();
		}

		// create 50 reporting areas
		for (int i = 0; i < 50; i++)
		{
			ReportingArea pchild = p.defaultReportingArea().newReportingArea().name("RA_" + i).create();

			createDimsAndFacts(pchild.newReportingArea().name("Child_1").create());
			createDimsAndFacts(pchild.newReportingArea().name("Child_2").create());
			ReportingArea child_3 = pchild.newReportingArea().name("Child_3").create();
			createDimsAndFacts(child_3);

			createDimsAndFacts(child_3.newReportingArea().name("SubChild1").create());
		}

		StopWatch stw = new StopWatch();

		stw.start();
		p.persist();
		stw.stop();

		Assert.assertTrue(stw.getTime() < 100000L);
	}

	private void createDimsAndFacts(ReportingArea child_1)
	{
		// create dim with 50 columns
		for (int dimNo = 0; dimNo < 5; dimNo++)
		{
			Dimension dim = child_1.newDimension().name("dim_" + dimNo).schema("dbo").create();

			// create columns
			for (int i = 0; i < 50; i++)
			{
				dim.newDatabaseColumn().name("col_" + i).jdbcType(TYPES[i % TYPES.length]).create();
			}

			// create attributes for all columns
			for (Map.Entry<String, DatabaseColumn> entry : dim.columns().entrySet())
			{
				dim.newDimensionAttribute().column(entry.getValue().name()).create();
			}

			int counter = 0;

			// create a few keys
			for (Map.Entry<String, DatabaseColumn> entry : dim.columns().entrySet())
			{
				String name = "KEY_" + counter++;
				dim.newDimensionKey().column(entry.getValue().name()).physicalName(name).logicalName(name).create();
			}
		}

		// create 5 facts
		for (int fact = 0; fact < 5; fact++)
		{
			Fact f = child_1.newFact().schema("dbo").name("F_" + fact).create();

			// columns for the facts
			for (int col = 0; col < 50; col++)
			{
				f.newDatabaseColumn().name("col_" + col).jdbcType(TYPES[col % TYPES.length]).create();
				f.newFactMeasure().logicalName("col_" + col).column("col_" + col).create();
			}
		}
	}
}
