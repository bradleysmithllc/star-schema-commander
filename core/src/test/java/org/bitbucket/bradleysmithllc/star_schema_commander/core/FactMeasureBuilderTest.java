package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;

public class FactMeasureBuilderTest extends ProfileFactory
{
	@Test
	public void missingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Missing column name");
		createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create().newFactMeasure().create();
	}

	@Test
	public void column() throws IOException
	{
		Fact fact = createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();

		FactMeasure fm = fact.newFactMeasure().column("col").create();
		Assert.assertEquals("[default].dbo_n.col", fm.qualifiedName());
		Assert.assertEquals("col", fm.logicalName());
	}

	@Test
	public void logicalName() throws IOException
	{
		Fact fact = createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();

		FactMeasure fm = fact.newFactMeasure().column("col").logicalName("log").create();
		Assert.assertEquals("[default].dbo_n.log", fm.qualifiedName());
		Assert.assertEquals("log", fm.logicalName());
	}

	@Test
	public void removePersist() throws IOException
	{
		Profile profile = createProfile();
		Fact fact = profile.defaultReportingArea().newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();

		FactMeasure fm = fact.newFactMeasure().column("col").logicalName("log").create();
		Assert.assertEquals("log", fm.logicalName());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());
		fact = p2.defaultReportingArea().facts().get("dbo_n");
		Assert.assertNotNull(fact);

		fm = fact.measures().get("log");
		Assert.assertNotNull(fm);
		fm.remove();
		Assert.assertNotNull(fact.measures().get("log"));

		p2.persist();
		Assert.assertNull(p2.defaultReportingArea().facts().get("dbo_n").measures().get("log"));

		p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNull(p2.defaultReportingArea().facts().get("dbo_n").measures().get("log"));
	}

	@Test
	public void persist() throws IOException
	{
		Profile profile = createProfile();
		Fact f = profile.defaultReportingArea().newFact().schema("s").name("n").create();

		f.newDatabaseColumn().name("name").jdbcType(Types.INTEGER).create();

		f.newFactMeasure().logicalName("logical").column("name").create();
		profile.persist();

		profile = new Profile(profile.getProfileDirectory());

		f = profile.defaultReportingArea().facts().get("s_n");
		FactMeasure fm = f.measures().get("logical");

		Assert.assertNotNull(fm);

		Assert.assertNotNull(fm.column());
	}
}
