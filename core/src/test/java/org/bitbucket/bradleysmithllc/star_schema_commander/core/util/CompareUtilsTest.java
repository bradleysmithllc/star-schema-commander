package org.bitbucket.bradleysmithllc.star_schema_commander.core.util;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CompareUtilsTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void empty()
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Empty argument array.");

		CompareUtils.compare();
	}

	@Test
	public void nullL()
	{
		Assert.assertEquals(-1, CompareUtils.compare(null, ""));
	}

	@Test
	public void nullR()
	{
		Assert.assertEquals(1, CompareUtils.compare("", null));
	}

	@Test
	public void nullNull()
	{
		Assert.assertEquals(0, CompareUtils.compare(null, null));
	}

	@Test
	public void nullOther()
	{
		Assert.assertEquals(0, CompareUtils.compare("a", "a", null, null, "b", "b"));
		Assert.assertEquals(1, CompareUtils.compare("a", "a", "a", null, "b", "b"));
		Assert.assertEquals(-1, CompareUtils.compare("a", "a", null, "a", "b", "b"));
	}

	@Test
	public void mismatchedPairs()
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Mismatched argument pair.");

		CompareUtils.compare('a', 'b', 'c');
	}

	@Test
	public void basicString()
	{
		Assert.assertEquals("a".compareTo("b"), CompareUtils.compare("a", "b"));
	}

	@Test
	public void secondaryString()
	{
		Assert.assertEquals("b".compareTo("a"), CompareUtils.compare("a", "a", "b", "a"));
	}

	@Test
	public void tertiaryString()
	{
		Assert.assertEquals("a".compareTo("b"), CompareUtils.compare("b", "b", "a", "a", "a", "b"));
	}
}