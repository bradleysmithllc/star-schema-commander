package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;

public class FactDimensionKeyBuilderTest extends ProfileFactory
{
	@Test
	public void missingDimensionName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Dimension name required");
		createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create().newFactDimensionKey().create();
	}

	@Test
	public void missingDimensionKeyName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Key name required");
		createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").create();
	}

	@Test
	public void missingDimensionJoins() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Joins required");
		createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").keyName("k").create();
	}

	@Test
	public void invalidDimension() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Dimension [d_dd] not found");
		createProfile().defaultReportingArea().newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").keyName("k").join("f", "t").create();
	}

	@Test
	public void invalidDimensionKey() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Dimension Key [k] not found");

		ReportingArea reportingArea = createProfile().defaultReportingArea();
		reportingArea.newDimension().schema("d").name("dd").create();
		reportingArea.newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").keyName("k").join("f", "t").create();
	}

	@Test
	public void invalidDimensionKeyColumn() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Column [t] not found in dimension");

		ReportingArea reportingArea = createProfile().defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").create();

		reportingArea.newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").keyName("k").join("f", "t").create();
	}

	@Test
	public void invalidFactKeyColumn() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Column [f] not found in fact");

		ReportingArea reportingArea = createProfile().defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").create();

		reportingArea.newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").keyName("k").join("f", "col").create();
	}

	@Test
	public void incompleteKeySpec() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Incomplete column specification");

		ReportingArea reportingArea = createProfile().defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").column("col2").create();

		reportingArea.newFact().schema("dbo").name("n").create().newFactDimensionKey().dimension("d_dd").keyName("k").join("f", "col").create();
	}

	@Test
	public void logicalName() throws IOException
	{
		ReportingArea reportingArea = createProfile().defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").column("col2").create();

		Fact fact = reportingArea.newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("fcol").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("fcol2").jdbcType(Types.INTEGER).create();

		FactDimensionKey fk = fact.newFactDimensionKey().dimension("d_dd").keyName("k").join("fcol", "col").join("fcol2", "col2").create();

		Assert.assertEquals("d_dd", fk.name());
		Assert.assertEquals("k_d_dd", fk.physicalName());

		Assert.assertFalse(fk.rolePlaying());
	}

	@Test
	public void physicalName() throws IOException
	{
		ReportingArea reportingArea = createProfile().defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").column("col2").create();

		Fact fact = reportingArea.newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("fcol").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("fcol2").jdbcType(Types.INTEGER).create();

		FactDimensionKey fk = fact.newFactDimensionKey().dimension("d_dd").keyName("k").physicalName("y").join("fcol", "col").join("fcol2", "col2").create();

		Assert.assertEquals("d_dd", fk.name());
		Assert.assertEquals("y", fk.physicalName());
	}

	@Test
	public void rolePlaying() throws IOException
	{
		ReportingArea reportingArea = createProfile().defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").column("col2").create();

		Fact fact = reportingArea.newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("fcol").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("fcol2").jdbcType(Types.INTEGER).create();

		FactDimensionKey fk1 = fact.newFactDimensionKey().dimension("d_dd").keyName("k").roleName("Figgit").join("fcol", "col").join("fcol2", "col2").create();
		FactDimensionKey fk2 = fact.newFactDimensionKey().dimension("d_dd").keyName("k").join("fcol", "col").join("fcol2", "col2").create();

		Assert.assertEquals("d_dd_Figgit", fk1.name());
		Assert.assertEquals("d_dd", fk2.name());

		Assert.assertTrue(fk1.rolePlaying());
		Assert.assertTrue(fk2.rolePlaying());
	}

	@Test
	public void create() throws IOException
	{
		Profile profile = createProfile();
		ReportingArea reportingArea = profile.defaultReportingArea();
		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col").column("col2").create();

		Fact fact = reportingArea.newFact().schema("dbo").name("n").create();
		fact.newDatabaseColumn().name("fcol").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("fcol2").jdbcType(Types.INTEGER).create();

		fact.newFactDimensionKey().dimension("d_dd").keyName("k").join("fcol", "col").join("fcol2", "col2").create();

		Assert.assertNotNull(fact.keys().get("d_dd"));
		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNotNull(p2.defaultReportingArea().facts().get("dbo_n").keys().get("d_dd"));

		p2.defaultReportingArea().facts().get("dbo_n").keys().get("d_dd").remove();
		Assert.assertNotNull(p2.defaultReportingArea().facts().get("dbo_n").keys().get("d_dd"));
		p2.persist();
		Assert.assertNull(p2.defaultReportingArea().facts().get("dbo_n").keys().get("d_dd"));

		p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNull(p2.defaultReportingArea().facts().get("dbo_n").keys().get("d_dd"));
	}
}
