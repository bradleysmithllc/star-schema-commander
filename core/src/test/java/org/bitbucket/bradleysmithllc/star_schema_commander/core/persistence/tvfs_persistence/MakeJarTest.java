package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.tvfs_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.*;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.jar_persistence.JarPersistedProfile;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public class MakeJarTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private AtomicInteger dirCount = new AtomicInteger(0);
	private AtomicInteger fileCount = new AtomicInteger(0);

	@Test
	public void test() throws IOException
	{
		File jarFile = temporaryFolder.newFile("archive.zip");
		jarFile.delete();

		JarPersistedProfile jpp = new JarPersistedProfile(jarFile);

		final JsonNode rootNode = JsonLoader.fromResource("/org/bitbucket/bradleysmithllc/star_schema_commander/core/persistence/tvfs_persistence/MakeJarTest.json");

		jpp.write(new WriteVisitor()
		{
			@Override
			public void visitWriter(PersistedProfile profile, PersistedWritableDirectory root) throws IOException
			{
				writeChildNode(rootNode, root);
			}
		});

		int writeDirs = dirCount.get();
		int writeFiles = fileCount.get();

		dirCount.set(0);
		fileCount.set(0);

		System.out.println(jarFile.getAbsolutePath());
		System.out.println(jarFile.getAbsolutePath());

		// this will verify the file data
		jpp.read(new ReadVisitor()
		{
			@Override
			public void visitReader(PersistedProfile profile, PersistedReadableDirectory root) throws IOException
			{
				readChildNode(rootNode, root);
			}
		});

		int readDirs = dirCount.get();
		int readFiles = fileCount.get();

		Assert.assertEquals("fileCount", writeFiles, readFiles);
		Assert.assertEquals("dirCount", writeDirs, readDirs);
	}

	private void readChildNode(JsonNode rootNode, PersistedReadableDirectory root) throws IOException
	{
		dirCount.incrementAndGet();
		Iterator<String> it = rootNode.fieldNames();

		// foe each one, if it is an object, recurse into it
		while (it.hasNext())
		{
			String fieldName = it.next();
			JsonNode nde = rootNode.get(fieldName);

			if (nde.isObject())
			{
				readChildNode(nde, root.openReadableDirectory(fieldName));
			}
			else if (nde.isTextual())
			{
				fileCount.incrementAndGet();
				StringWriter str = new StringWriter();
				Reader reader = root.openReadableFile(fieldName).reader();

				try
				{
					IOUtils.copy(reader, str);
					Assert.assertEquals(nde.asText(), str.toString());
				}
				finally
				{
					reader.close();
				}
			}
			else
			{
				throw new IllegalStateException();
			}
		}
	}

	private void writeChildNode(JsonNode rootNode, PersistedWritableDirectory root) throws IOException
	{
		dirCount.incrementAndGet();
		Iterator<String> it = rootNode.fieldNames();

		// foe each one, if it is an object, recurse into it
		while (it.hasNext())
		{
			String fieldName = it.next();
			JsonNode nde = rootNode.get(fieldName);

			if (nde.isObject())
			{
				writeChildNode(nde, root.openWritableDirectory(fieldName));
			}
			else if (nde.isTextual())
			{
				fileCount.incrementAndGet();

				Writer writer = root.openWritableFile(fieldName).writer();

				try
				{
					writer.write(nde.asText());
				}
				finally
				{
					writer.close();
				}
			}
			else
			{
				throw new IllegalStateException();
			}
		}
	}
}
