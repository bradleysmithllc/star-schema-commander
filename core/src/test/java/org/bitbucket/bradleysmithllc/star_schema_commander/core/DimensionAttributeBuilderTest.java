package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;

public class DimensionAttributeBuilderTest extends ProfileFactory
{
	@Test
	public void missingColumn() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Column must be supplied");

		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();

		dim.newDimensionAttribute().create();
	}

	@Test
	public void createWithInheritedLogical() throws IOException
	{
		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();

		dim.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();

		DimensionAttribute dima = dim.newDimensionAttribute().column("n").create();
		Assert.assertEquals("n", dima.logicalName());
		Assert.assertEquals("[default].sch_nam.n", dima.qualifiedName());
	}

	@Test
	public void createWithInExplicitLogical() throws IOException
	{
		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();
		DatabaseColumn col = dim.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();

		DimensionAttribute dima = dim.newDimensionAttribute().column("n").logicalName("log").create();
		Assert.assertEquals("log", dima.logicalName());
		Assert.assertEquals("[default].sch_nam.log", dima.qualifiedName());
	}

	@Test
	public void duplicateAttribute() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Attribute already added [log]");

		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();
		DatabaseColumn col = dim.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();

		DimensionAttribute dima = dim.newDimensionAttribute().column("n").logicalName("log").create();
		dima = dim.newDimensionAttribute().column("n").logicalName("log").create();
	}

	@Test
	public void invalidColumn() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Column [n] not found");

		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();

		DimensionAttribute dima = dim.newDimensionAttribute().column("n").logicalName("log").create();
		dima = dim.newDimensionAttribute().column("n").logicalName("log").create();
	}

	@Test
	public void serialize() throws IOException
	{
		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();
		dim.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();

		dim.newDimensionAttribute().column("n").logicalName("log").create();

		p.persist();

		Profile p2 = new Profile(p.getProfileDirectory());
		dim = p2.defaultReportingArea().dimensions().get("sch_nam");
		Assert.assertNotNull(dim);

		Assert.assertNotNull(dim.attributes().get("log"));
	}

	@Test
	public void remove() throws IOException
	{
		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();
		dim.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();

		dim.newDimensionAttribute().column("n").logicalName("log").create();

		p.persist();

		Profile p2 = new Profile(p.getProfileDirectory());
		dim = p2.defaultReportingArea().dimensions().get("sch_nam");
		Assert.assertNotNull(dim);

		DimensionAttribute log = dim.attributes().get("log");
		Assert.assertNotNull(log);

		log.remove();
		Assert.assertNotNull(dim.attributes().get("log"));
		p2.persist();
		Assert.assertNull(dim.attributes().get("log"));

		Profile p3 = new Profile(p.getProfileDirectory());
		dim = p3.defaultReportingArea().dimensions().get("sch_nam");
		Assert.assertNotNull(dim);

		log = dim.attributes().get("log");
		Assert.assertNull(log);
	}

	@Test
	public void deletedColumn() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Column [n] not found");

		Profile p = createProfile();

		Dimension dim = p.defaultReportingArea().newDimension().schema("sch").name("nam").create();
		DatabaseColumn col = dim.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();

		dim.newDimensionAttribute().column("n").logicalName("log").create();

		col.remove();
		p.persist();

		Profile p2 = new Profile(p.getProfileDirectory());
		dim = p2.defaultReportingArea().dimensions().get("sch.nam");
		Assert.assertNotNull(dim);
		DimensionAttribute attr = dim.attributes().get("log");
		Assert.assertNull(attr);
	}
}
