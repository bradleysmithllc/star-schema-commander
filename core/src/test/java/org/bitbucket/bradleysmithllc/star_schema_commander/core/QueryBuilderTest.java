package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.QueryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;

public class QueryBuilderTest extends ProfileFactory
{
	private ReportingArea reportingArea;
	private Profile profile;

	@Test
	public void namedQueryDuplicateName() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Query name [name] has already been used.");

		Fact f = fact();
		Query query = f.newQuery().include(f.measures().get("col1")).create();

		profile.registerNamedQuery("name", query);
		profile.registerNamedQuery("name", query);
	}

	@Test
	public void namedQueryFactRemoved() throws IOException
	{
		Fact f = fact();
		Fact fnew = profile.defaultReportingArea().newFact().schema("s").name("n").create();
		fnew.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fnew.newFactMeasure().column("col1").create();

		Query query = fnew.newQuery().include(fnew.measures().get("col1")).create();

		profile.registerNamedQuery("name", query);
		profile.persist();

		// read back in.
		profile.invalidate();

		// remove fact
		profile.defaultReportingArea().facts().get("s_n").remove();
		profile.persist();

		// read back in.
		profile.invalidate();
	}

	@Test
	public void namedQueryRaRemoved() throws IOException
	{
		Fact f = fact();
		ReportingArea ranew = profile.defaultReportingArea().newReportingArea().name("new").create();
		Fact fnew = ranew.newFact().schema("s").name("n").create();
		fnew.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fnew.newFactMeasure().column("col1").create();

		Query query = fnew.newQuery().include(fnew.measures().get("col1")).create();

		profile.registerNamedQuery("name", query);
		profile.persist();

		// read back in.
		profile.invalidate();

		// remove reporting area
		profile.defaultReportingArea().reportingAreas().get("new").remove();

		// remove reporting area
		fnew.remove();
		profile.persist();

		// read back in.
		profile.invalidate();
	}

	@Test
	public void duplicateMeasure() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Measure [col1] has already been added");

		Fact f = fact();
		f.newQuery().include(f.measures().get("col1")).include(f.measures().get("col1"));
	}

	@Test
	public void duplicateMeasureAgg() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Measure [col1] has already been added");

		Fact f = fact();
		f.newQuery().include(f.measures().get("col1"), Query.aggregation.average_distinct).include(f.measures().get("col1"), Query.aggregation.average_distinct);
	}

	@Test
	public void nonduplicateMeasure() throws IOException
	{
		Fact f = fact();
		f.newQuery().include(f.measures().get("col1")).include(f.measures().get("col1"), Query.aggregation.average);
	}

	@Test
	public void duplicateAliasAttributeMeasure() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Alias [alias] was already used.");

		Fact f = fact();
		QueryBuilder qb = f.newQuery().join(f.keys().get("d_n"));
		qb.include(f.keys().get("d_n").dimensionKey().dimension().attributes().get("d"), Query.aggregation.none, "alias");
		qb.include(f.measures().get("col1"), Query.aggregation.none, "alias");
	}

	@Test
	public void duplicateAliasMeasure() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Alias [alias] was already used.");

		Fact f = fact();
		f.newQuery().include(f.measures().get("col1"), Query.aggregation.none, "alias").include(f.measures().get("col1"), Query.aggregation.average, "alias");
	}

	@Test
	public void wrongFactMeasure() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Measure [col1] does not belong to this fact");

		Fact f1 = fact();
		Fact f2 = profile.defaultReportingArea().newFact().name("n").schema("s").create();
		f2.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		f2.newFactMeasure().column("col1").create();

		f1.newQuery().include(f2.measures().get("col1"));
	}

	@Test
	public void duplicateKey() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Key [d_n] has already been added");

		Fact f = fact();
		f.newQuery().join(f.keys().get("d_n")).join(f.keys().get("d_n"));
	}

	@Test
	public void duplicateAliasAttribute() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Alias [alias] was already used.");

		Fact f = fact();
		QueryBuilder qb = f.newQuery().join(f.keys().get("d_n"));
		qb.include(f.keys().get("d_n").dimensionKey().dimension().attributes().get("d"), Query.aggregation.none, "alias");
		qb.include(f.keys().get("d_n").dimensionKey().dimension().attributes().get("d"), Query.aggregation.average, "alias");
	}

	@Test
	public void wrongFactKey() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Dimension key [d_n] does not belong to this fact");

		Fact f1 = fact();

		Fact f2 = profile.defaultReportingArea().newFact().name("n").schema("s").create();
		f2.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		f2.newFactDimensionKey().dimension("d_n").keyName("key").join("col1", "d").create();

		f1.newQuery().join(f2.keys().get("d_n"));
	}

	@Test
	public void limitLTZero() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Limit may not be less than or equal to 0");

		fact().newQuery().limit(-1);
	}

	@Test
	public void limitZero() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Limit may not be less than or equal to 0");

		fact().newQuery().limit(0);
	}

	@Test
	public void reallyNoMeasures() throws IOException
	{
		Fact fact = fact();
		FactDimensionKey factDimensionKey = fact.keys().get("d_n");
		Query q = fact.newQuery().limit(1000).reallyNoMeasures().join(factDimensionKey).include(factDimensionKey.dimensionKey().dimension().attributes().get("d")).create();

		String str = q.query();

		Assert.assertEquals("SELECT TOP 1000\n" +
			"\td_n.d AS d_n_d\n" +
			"FROM\n" +
			"\td.n AS FACT\n" +
			"JOIN\n" +
			"\td.n AS d_n\n" +
			"\t\tON\n" +
			"\t\t\tFACT.col1 = d_n.d\n", str);
	}

	@Before
	public void createP() throws IOException
	{
		profile = createProfile();
	}

	private Fact fact() throws IOException
	{
		reportingArea = profile.defaultReportingArea();

		Dimension d = reportingArea.newDimension().name("n").schema("d").create();
		d.newDatabaseColumn().name("d").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d2").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d3").jdbcType(Types.INTEGER).create();

		d.newDimensionAttribute().column("d").create();


		d.newDimensionKey().logicalName("key").column("d").create();
		d.newDimensionKey().logicalName("key2").column("d2").column("d3").create();

		Fact fact = reportingArea.newFact().name("n").schema("d").create();

		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();

		fact.newFactMeasure().column("col1").create();
		fact.newFactMeasure().column("col2").logicalName("Column 2").create();

		fact.newFactDimensionKey().dimension("d_n").keyName("key").join("col1", "d").create();
		fact.newFactDimensionKey().dimension("d_n").roleName("D 2").keyName("key2").join("col2", "d2").join("col3", "d3").create();

		return fact;
	}
}
