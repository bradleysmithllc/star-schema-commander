package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.Query;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.query.QueryBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Types;
import java.util.*;

@RunWith(Parameterized.class)
public class QueryTest extends ProfileFactory
{
	private final TestSpec testSpec;
	private ReportingArea reportingArea;
	private Profile profile;

	public QueryTest(TestSpec spec)
	{
		testSpec = spec;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		URL url = QueryTest.class.getResource("/query/TestSpecs.json");

		List<Object[]> specs = new ArrayList<Object[]>();

		JsonNode node = JsonLoader.fromURL(url);

		ArrayNode an = (ArrayNode) node.get("specs");

		Iterator<JsonNode> it = an.iterator();

		while (it.hasNext())
		{
			JsonNode specnode = it.next();

			String id = specnode.get("name").asText();

			TestSpec testSpec1 = new TestSpec();
			testSpec1.id = id;
			specs.add(new Object[]{testSpec1});

			JsonNode measarray = specnode.get("measures");

			Iterator<JsonNode> ait = measarray.iterator();

			while (ait.hasNext())
			{
				JsonNode omnode = ait.next();

				TestSpec.Name name = new TestSpec.Name();
				name.name = omnode.get("logicalName").asText();

				JsonNode aggregation = omnode.get("aggregation");
				if (aggregation != null)
				{
					name._aggregation = Query.aggregation.valueOf(aggregation.asText());
				}

				JsonNode alias = omnode.get("alias");
				if (alias != null)
				{
					name.alias = alias.asText();
				}

				testSpec1.measures.add(name);
			}

			JsonNode lnode = specnode.get("limit");
			if (lnode != null && lnode.getNodeType() == JsonNodeType.NUMBER)
			{
				testSpec1.limit = lnode.asInt();
			}

			JsonNode keysObj = specnode.get("keys");

			if (keysObj != null)
			{
				Iterator<String> knit = keysObj.fieldNames();

				while (knit.hasNext())
				{
					String name = knit.next();

					JsonNode keynode = keysObj.get(name);

					testSpec1.keys.put(name, new ArrayList<TestSpec.Name>());

					JsonNode ia = keynode.get("include-attributes");

					ait = ia.iterator();

					while (ait.hasNext())
					{
						JsonNode oanode = ait.next();

						TestSpec.Name sname = new TestSpec.Name();

						sname.name = oanode.get("logicalName").asText();

						JsonNode aggregation = oanode.get("aggregation");
						if (aggregation != null)
						{
							sname._aggregation = Query.aggregation.valueOf(aggregation.asText());
						}

						JsonNode alias = oanode.get("alias");
						if (alias != null)
						{
							sname.alias = alias.asText();
						}

						testSpec1.keys.get(name).add(sname);
					}
				}
			}
		}


		return specs;
	}

	@Test
	public void runDiffTests() throws Exception
	{
		if (testSpec.execute)
		{
			System.out.println(testSpec.id);
			Fact fact = fact();

			QueryBuilder nqb = fact.newQuery();

			if (testSpec.limit != -1)
			{
				nqb = nqb.limit(testSpec.limit);
			}

			for (TestSpec.Name meas : testSpec.measures)
			{
				nqb.include(fact.measures().get(meas.name), meas._aggregation, meas.alias);
			}

			for (Map.Entry<String, List<TestSpec.Name>> key : testSpec.keys.entrySet())
			{
				String fdkName = key.getKey();
				Map<String, FactDimensionKey> dimensionKeyMap = fact.keys();
				FactDimensionKey factDimensionKey = dimensionKeyMap.get(fdkName);

				Dimension dim = factDimensionKey.dimensionKey().dimension();

				nqb = nqb.join(factDimensionKey);

				for (TestSpec.Name attr : key.getValue())
				{
					nqb = nqb.include(dim.attributes().get(attr.name), attr._aggregation, attr.alias);
				}
			}

			Query query = nqb.create();

			String sql = query.query();

			// look up the assertion query and compare
			InputStream url = QueryTest.class.getResourceAsStream("/query/" + testSpec.id + ".sql");

			Assert.assertEquals(testSpec.id, IOUtils.toString(url), sql);

			// now persist and read back in to exercise the persistence layer
			profile.registerNamedQuery(testSpec.id, query);
			profile.persist();

			profile.invalidate();

			Assert.assertEquals(1, profile.namedQueries().size());
			Assert.assertEquals(query, profile.namedQueries().get(testSpec.id));
		}
	}

	private Fact fact() throws IOException
	{
		profile = createProfile();
		reportingArea = profile.defaultReportingArea();

		Dimension d = reportingArea.newDimension().name("n").schema("d").catalog("DB").logicalName("d_n").create();
		d.newDatabaseColumn().name("d").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d2").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d3").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d4").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d5").jdbcType(Types.INTEGER).create();
		d.newDatabaseColumn().name("d6").jdbcType(Types.INTEGER).create();

		d.newDimensionAttribute().column("d").create();
		d.newDimensionAttribute().column("d2").create();
		d.newDimensionAttribute().column("d3").create();
		d.newDimensionAttribute().column("d4").create();
		d.newDimensionAttribute().column("d5").create();
		d.newDimensionAttribute().column("d6").logicalName("a").create();

		d.newDimensionKey().logicalName("key").column("d").create();
		d.newDimensionKey().logicalName("key2").column("d2").column("d3").create();

		d = reportingArea.newDimension().name("n").schema("dr").catalog("DB").create();
		d.newDatabaseColumn().name("d").jdbcType(Types.INTEGER).create();
		d.newDimensionAttribute().column("d").create();
		d.newDimensionKey().logicalName("key").column("d").create();

		Fact fact = reportingArea.newFact().name("f").schema("d").create();

		fact.newDatabaseColumn().name("col1").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col4").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col5").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col6").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col7").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col8").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col9").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("col10").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("ddcol").jdbcType(Types.VARCHAR).create();
		fact.newDatabaseColumn().name("ddcol_2").jdbcType(Types.BIGINT).create();

		fact.newDegenerateDimension().column("ddcol").column("ddcol_2").logicalName("Degen1").create();

		fact.newFactMeasure().column("col1").create();
		fact.newFactMeasure().column("col2").logicalName("Column 2").create();
		fact.newFactMeasure().column("col3").create();
		fact.newFactMeasure().column("col4").create();
		fact.newFactMeasure().column("col5").create();
		fact.newFactMeasure().column("col6").create();
		fact.newFactMeasure().column("col7").create();
		fact.newFactMeasure().column("col8").create();
		fact.newFactMeasure().column("col9").create();
		fact.newFactMeasure().column("col10").create();

		fact.newFactDimensionKey().dimension("d_n").keyName("key").join("col1", "d").create();
		fact.newFactDimensionKey().dimension("d_n").roleName("D 2").keyName("key2").join("col2", "d2").join("col3", "d3").create();

		fact.newFactDimensionKey().dimension("DB_dr_n").roleName("ASA").keyName("key").join("col2", "d").create();

		return fact;
	}

	private static final class TestSpec
	{
		String id;
		boolean execute = true;
		int limit = -1;
		List<Name> measures = new ArrayList<Name>();
		Map<String, List<Name>> keys = new HashMap<String, List<Name>>();

		private static class Name
		{
			String name;
			Query.aggregation _aggregation = Query.aggregation.none;
			String alias;
		}
	}
}
