package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;
import java.util.Map;

public class FactBuilderTest extends ProfileFactory
{
	@Test
	public void logicalName() throws IOException
	{
		Profile profile = createProfile();
		Fact f = profile.defaultReportingArea().newFact().schema("s").name("phy").create();
		Assert.assertEquals("[default].s_phy", f.qualifiedName());
		Assert.assertEquals("s_phy", f.logicalName());
		Assert.assertEquals("phy", f.name());

		f = profile.defaultReportingArea().newFact().schema("s").name("phy2").logicalName("log").create();
		Assert.assertEquals("[default].log", f.qualifiedName());
		Assert.assertEquals("log", f.logicalName());
		Assert.assertEquals("phy2", f.name());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());

		Assert.assertEquals(2, p2.defaultReportingArea().facts().size());
		Assert.assertNotNull(p2.defaultReportingArea().facts().get("s_phy"));
		Assert.assertNotNull(p2.defaultReportingArea().facts().get("log"));

		Assert.assertEquals("s_phy", p2.defaultReportingArea().facts().get("s_phy").logicalName());
		Assert.assertEquals("log", p2.defaultReportingArea().facts().get("log").logicalName());
	}

	@Test
	public void catalog() throws IOException
	{
		Profile profile = createProfile();
		Fact f = profile.defaultReportingArea().newFact().schema("s").name("phy").catalog("cat").create();
		Assert.assertEquals("[default].cat_s_phy", f.qualifiedName());
		Assert.assertEquals("cat_s_phy", f.logicalName());
		Assert.assertEquals("phy", f.name());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());

		Assert.assertEquals(1, p2.defaultReportingArea().facts().size());
		Assert.assertNotNull(p2.defaultReportingArea().facts().get("cat_s_phy"));
		Assert.assertEquals("cat", p2.defaultReportingArea().facts().get("cat_s_phy").catalog());
	}

	@Test
	public void duplicateLogicalName() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Fact logical name [logName] already added");

		ReportingArea reportingArea = createProfile().defaultReportingArea();

		reportingArea.newFact().schema("s").name("name").logicalName("logName").create();
		reportingArea.newFact().schema("s2").name("name2").logicalName("logName").create();
	}

	@Test
	public void missingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Name is required");
		createProfile().defaultReportingArea().newFact().create();
	}

	@Test
	public void missingSchema() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Schema is required");
		createProfile().defaultReportingArea().newFact().name("name").create();
	}

	@Test
	public void keysByDimension() throws IOException
	{
		Profile profile = createProfile();
		ReportingArea reportingArea = profile.defaultReportingArea();

		// one fact
		Fact fact = reportingArea.newFact().name("name").schema("sch").create();

		Dimension dimension = reportingArea.newDimension().schema("d").name("dd").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("dd_k").column("col").column("col2").create();

		Dimension dimension2 = reportingArea.newDimension().schema("d").name("dd2").create();
		dimension2.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension2.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension2.newDimensionKey().logicalName("dd2_k").column("col").column("col2").create();

		// prepare fact columns
		fact.newDatabaseColumn().name("dd2col").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("dd2col2").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("ddcol").jdbcType(Types.INTEGER).create();
		fact.newDatabaseColumn().name("ddcol2").jdbcType(Types.INTEGER).create();

		// now create foreign keys
		// one for dd
		fact.newFactDimensionKey().dimension("d_dd").keyName("dd_k").join("ddcol", "col").join("ddcol2", "col2").create();

		// twp different keys for dd2
		fact.newFactDimensionKey().dimension("d_dd2").keyName("dd2_k").join("dd2col", "col").join("dd2col2", "col2").create();
		fact.newFactDimensionKey().dimension("d_dd2").keyName("dd2_k").roleName("dd2_k2").join("dd2col", "col").join("dd2col2", "col2").create();

		// now we can interrogate the fat for dimension keys
		Assert.assertEquals(3, fact.keys().size());


		Map<String, FactDimensionKey> factDimensionKeyMap = fact.keys(dimension);
		Assert.assertEquals(1, factDimensionKeyMap.size());

		Assert.assertNotNull(factDimensionKeyMap.get("d_dd"));

		Map<String, FactDimensionKey> factDimensionKeyMap1 = fact.keys(dimension2);

		Assert.assertEquals(2, factDimensionKeyMap1.size());
		Assert.assertNotNull(factDimensionKeyMap1.get("d_dd2"));
		Assert.assertNotNull(factDimensionKeyMap1.get("d_dd2_dd2_k2"));
	}

	@Test
	public void create() throws IOException
	{
		Profile profile = createProfile();
		profile.defaultReportingArea().newFact().name("name").schema("sch").create();
		profile.persist();

		Assert.assertNotNull(profile.defaultReportingArea().facts().get("sch_name"));

		Profile p2 = new Profile(profile.getProfileDirectory());
		Fact fact = p2.defaultReportingArea().facts().get("sch_name");
		Assert.assertNotNull(fact);
		fact.remove();
		p2.persist();

		Profile p3 = new Profile(profile.getProfileDirectory());
		fact = p3.defaultReportingArea().facts().get("sch_name");
		Assert.assertNull(fact);
	}
}
