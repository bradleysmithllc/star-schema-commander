package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.Types;
import java.util.List;

public class DimensionKeyBuilderTest extends ProfileFactory
{
	@Test
	public void missingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Logical name must be supplied");
		createProfile().defaultReportingArea().newDimension().name("na").schema("sch").create().newDimensionKey().create();
	}

	@Test
	public void missingColumns() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Columns must be supplied");
		createProfile().defaultReportingArea().newDimension().name("na").schema("sch").create().newDimensionKey().logicalName("n").create();
	}

	@Test
	public void invalidColumn() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Column [col] not present in dimension");
		createProfile().defaultReportingArea().newDimension().name("na").schema("sch").create().newDimensionKey().logicalName("n").column("col").create();
	}

	@Test
	public void duplicateKey() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Duplicate key [n]");
		Dimension dimension = createProfile().defaultReportingArea().newDimension().name("na").schema("sch").create();
		dimension.newDatabaseColumn().name("n").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("n").column("n").create();
		dimension.newDimensionKey().logicalName("n").column("n").create();
	}

	@Test
	public void physicalIsLogicalIfNotSupplied() throws IOException
	{
		Profile profile = createProfile();
		Dimension dimension = profile.defaultReportingArea().newDimension().name("na").schema("sch").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		DimensionKey dim = dimension.newDimensionKey().logicalName("k").column("col2").create();

		Assert.assertEquals(dim.logicalName(), dim.physicalName());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());
		dimension = p2.defaultReportingArea().dimensions().get("sch_na");
		Assert.assertNotNull(dimension);
		DimensionKey key = dimension.keys().get("k");
		Assert.assertNotNull(key);

		Assert.assertEquals(key.logicalName(), key.physicalName());
	}

	@Test
	public void physicalOverridesLogical() throws IOException
	{
		Profile profile = createProfile();
		Dimension dimension = profile.defaultReportingArea().newDimension().name("na").schema("sch").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		DimensionKey dim = dimension.newDimensionKey().logicalName("k").physicalName("y").column("col2").create();

		Assert.assertEquals("y", dim.physicalName());
		Assert.assertEquals("k", dim.logicalName());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());
		dimension = p2.defaultReportingArea().dimensions().get("sch_na");
		Assert.assertNotNull(dimension);
		DimensionKey key = dimension.keys().get("k");
		Assert.assertNotNull(key);

		Assert.assertEquals("y", dim.physicalName());
		Assert.assertEquals("k", dim.logicalName());
	}

	@Test
	public void create() throws IOException
	{
		Profile profile = createProfile();
		Dimension dimension = profile.defaultReportingArea().newDimension().name("na").schema("sch").create();
		dimension.newDatabaseColumn().name("col").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col2").jdbcType(Types.INTEGER).create();
		dimension.newDatabaseColumn().name("col3").jdbcType(Types.INTEGER).create();
		dimension.newDimensionKey().logicalName("k").column("col2").create();
		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());
		dimension = p2.defaultReportingArea().dimensions().get("sch_na");
		Assert.assertNotNull(dimension);
		DimensionKey key = dimension.keys().get("k");
		Assert.assertNotNull(key);
		List<DatabaseColumn> cols = key.columns();
		Assert.assertEquals(1, cols.size());
		Assert.assertEquals("col2", cols.get(0).name());
		key.remove();
		Assert.assertNotNull(dimension.keys().get("k"));
		p2.persist();
		Assert.assertNull(dimension.keys().get("k"));

		Profile p3 = new Profile(profile.getProfileDirectory());
		dimension = p3.defaultReportingArea().dimensions().get("sch_na");
		Assert.assertNotNull(dimension);
		key = dimension.keys().get("k");
		Assert.assertNull(key);
	}
}
