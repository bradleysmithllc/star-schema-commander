package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Map;

public class DimensionFromMetaDataTest extends ProfileFactory
{
	static final String[][] columns = {
		{
			"INTID",
			String.valueOf(Types.INTEGER)
		},
		{
			"BIGINTID",
			String.valueOf(Types.BIGINT)
		},
		{
			"SMALLINTID",
			String.valueOf(Types.SMALLINT)
		},
		{
			"CH",
			String.valueOf(Types.CHAR)
		},
		{
			"VCH",
			String.valueOf(Types.VARCHAR)
		},
		{
			"DT",
			String.valueOf(Types.TIMESTAMP)
		},
		{
			"NU",
			String.valueOf(Types.DECIMAL)
		}
		// << H2 quirk.  Numeric identifies as Decimal.
	};

	@Test
	public void logical() throws IOException, SQLException
	{
		Profile p = createProfile();
		DatabaseConnection conn = p.newH2Connection().name("h2").create();

		Statement st = conn.open().createStatement();
		st.execute("CREATE TABLE PUBLIC.DIM (INTID INT, BIGINTID BIGINT, SMALLINTID SMALLINT, ch CHAR(1), vch VARCHAR(10), DT DATETIME, NU NUMERIC(10,5));");

		Dimension dim = p.defaultReportingArea().loadDimensionFromMetaData(conn, null, "PUBLIC", "DIM", "LOGICAL");

		Assert.assertEquals("LOGICAL", dim.logicalName());
	}

	@Test
	public void physical() throws IOException, SQLException
	{
		Profile p = createProfile();
		DatabaseConnection conn = p.newH2Connection().name("h2").create();

		Statement st = conn.open().createStatement();
		st.execute("CREATE TABLE PUBLIC.DIM (INTID INT, BIGINTID BIGINT, SMALLINTID SMALLINT, ch CHAR(1), vch VARCHAR(10), DT DATETIME, NU NUMERIC(10,5));");

		Dimension dim = p.defaultReportingArea().loadDimensionFromMetaData(conn, "PUBLIC", "DIM");

		Assert.assertEquals("PUBLIC_DIM", dim.logicalName());
	}

	@Test
	public void typesAndColumns() throws IOException, SQLException
	{
		Profile p = createProfile();
		DatabaseConnection conn = p.newH2Connection().name("h2").create();

		Statement st = conn.open().createStatement();
		st.execute("CREATE TABLE PUBLIC.DIM (INTID INT, BIGINTID BIGINT, SMALLINTID SMALLINT, ch CHAR(1), vch VARCHAR(10), DT DATETIME, NU NUMERIC(10,5));");

		Dimension dim = p.defaultReportingArea().loadDimensionFromMetaData(conn, "PUBLIC", "DIM");

		for (String[] cols : columns)
		{
			DatabaseColumn id = dim.columns().get(cols[0]);
			Assert.assertNotNull(id.name(), id);
			Assert.assertEquals(id.name(), Integer.parseInt(cols[1]), id.jdbcType());
		}
	}

	@Test
	public void badTable() throws IOException, SQLException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Table [BILLYBAH.PUBLIC.DIM] is not usable.");

		Profile p = createProfile();
		DatabaseConnection conn = p.newH2Connection().name("h2").create();

		Dimension dim = p.defaultReportingArea().loadDimensionFromMetaData(conn, "BILLYBAH", "PUBLIC", "DIM");
	}

	@Test
	public void keys() throws IOException, SQLException
	{
		Profile p = createProfile();
		DatabaseConnection conn = p.newH2Connection().name("h2").create();

		Statement st = conn.open().createStatement();
		st.execute("" +
				"CREATE TABLE PUBLIC.DIM (" +
				"INTID INT, " +
				"BIGINTID BIGINT, " +
				"SMALLINTID SMALLINT, " +
				"ch CHAR(1), " +
				"vch VARCHAR(10), " +
				"DT DATETIME, " +
				"NU NUMERIC(10,5), " +
				"PRIMARY KEY(INTID)" +
				");" +
				"CREATE UNIQUE INDEX UNQ ON PUBLIC.DIM(BIGINTID);" +
				"CREATE UNIQUE INDEX UNQ2 ON PUBLIC.DIM(DT, NU, ch);"
		);

		Dimension dim = p.defaultReportingArea().loadDimensionFromMetaData(conn, "PUBLIC", "DIM");

		Map<String, DimensionKey> keys = dim.keys();
		Assert.assertEquals(3, keys.size());

		DimensionKey key = keys.get("UNQ");
		Assert.assertNotNull(key);
		Assert.assertEquals("BIGINTID", key.columns().get(0).name());

		key = keys.get("UNQ2");
		Assert.assertNotNull(key);
		Assert.assertEquals("DT", key.columns().get(0).name());
		Assert.assertEquals("NU", key.columns().get(1).name());
		Assert.assertEquals("CH", key.columns().get(2).name());
	}
}
