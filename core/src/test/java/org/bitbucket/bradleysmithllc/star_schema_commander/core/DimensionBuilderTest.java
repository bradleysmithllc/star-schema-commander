package org.bitbucket.bradleysmithllc.star_schema_commander.core;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class DimensionBuilderTest extends ProfileFactory
{
	@Test
	public void missingName() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Name must be specified");
		createProfile().defaultReportingArea().newDimension().create();
	}

	@Test
	public void logicalName() throws IOException
	{
		Profile profile = createProfile();
		Dimension dim = profile.defaultReportingArea().newDimension().schema("s").name("phy").create();
		Assert.assertEquals("[default].s_phy", dim.qualifiedName());
		Assert.assertEquals("s_phy", dim.logicalName());

		dim = profile.defaultReportingArea().newDimension().schema("s").name("phy2").logicalName("log").create();
		Assert.assertEquals("[default].log", dim.qualifiedName());
		Assert.assertEquals("log", dim.logicalName());
		Assert.assertEquals("phy2", dim.name());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());

		Assert.assertEquals(2, p2.defaultReportingArea().dimensions().size());
		Assert.assertNotNull(p2.defaultReportingArea().dimensions().get("s_phy"));
		Assert.assertNotNull(p2.defaultReportingArea().dimensions().get("log"));

		Assert.assertEquals("s_phy", p2.defaultReportingArea().dimensions().get("s_phy").logicalName());
		Assert.assertEquals("log", p2.defaultReportingArea().dimensions().get("log").logicalName());
	}

	@Test
	public void catalog() throws IOException
	{
		Profile profile = createProfile();
		Dimension dim = profile.defaultReportingArea().newDimension().schema("s").name("phy").catalog("cat").create();
		Assert.assertEquals("[default].cat_s_phy", dim.qualifiedName());
		Assert.assertEquals("cat_s_phy", dim.logicalName());

		dim = profile.defaultReportingArea().newDimension().schema("s").name("phy2").catalog("caty").logicalName("scat").create();
		Assert.assertEquals("[default].scat", dim.qualifiedName());
		Assert.assertEquals("scat", dim.logicalName());

		profile.persist();

		Profile p2 = new Profile(profile.getProfileDirectory());
		Assert.assertNotNull(p2.defaultReportingArea().dimensions().get("cat_s_phy"));
		Assert.assertEquals("cat", p2.defaultReportingArea().dimensions().get("cat_s_phy").catalog());

		Assert.assertNotNull(p2.defaultReportingArea().dimensions().get("scat"));
		Assert.assertEquals("caty", p2.defaultReportingArea().dimensions().get("scat").catalog());
	}

	@Test
	public void duplicateLogicalName() throws IOException
	{
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Duplicate dimension logical name [logName]");

		ReportingArea reportingArea = createProfile().defaultReportingArea();

		reportingArea.newDimension().schema("s").name("name").logicalName("logName").create();
		reportingArea.newDimension().schema("s2").name("name2").logicalName("logName").create();
	}

	@Test
	public void missingSchema() throws IOException
	{
		expectedException.expect(IllegalStateException.class);
		expectedException.expectMessage("Schema must be specified");
		createProfile().defaultReportingArea().newDimension().name("n").create();
	}

	@Test
	public void notDegenerate() throws IOException
	{
		Profile p = createProfile();
		Dimension dim = p.defaultReportingArea().newDimension().schema("s").name("n").create();

		Assert.assertFalse(dim.degenerate());

		p.persist();

		p = new Profile(p.getProfileDirectory());

		Assert.assertFalse(p.defaultReportingArea().dimensions().get("s_n").degenerate());
	}

	@Test
	public void degenerate() throws IOException
	{
		Profile p = createProfile();
		Dimension dim = p.defaultReportingArea().newDimension().schema("s").name("n").degenerate().create();

		Assert.assertTrue(dim.degenerate());

		p.persist();

		p = new Profile(p.getProfileDirectory());

		Assert.assertTrue(p.defaultReportingArea().dimensions().get("s_n").degenerate());
	}

	@Test
	public void create() throws IOException
	{
		Profile p = createProfile();
		p.defaultReportingArea().newDimension().schema("s").name("n").create();
		p.persist();

		// test
		p = new Profile(p.getProfileDirectory());
		Map<String, Dimension> ds = p.defaultReportingArea().dimensions();
		Assert.assertEquals(1, ds.size());
		Assert.assertEquals("n", ds.get("s_n").name());
		Assert.assertEquals("s", ds.get("s_n").schema());

		ds.get("s_n").remove();
		p.persist();

		p = new Profile(p.getProfileDirectory());
		ds = p.defaultReportingArea().dimensions();
		Assert.assertEquals(0, ds.size());
	}
}
