package org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.tvfs_persistence;

/*
 * #%L
 * core
 * %%
 * Copyright (C) 2010 - 2015 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.star_schema_commander.core.persistence.FileChangeWatcher;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class PathWatcherTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	private File pathToWatch;
	private FileChangeWatcher jarFileWatcher;

	@Before
	public void setUp() throws IOException
	{
		pathToWatch = temporaryFolder.newFile();
		jarFileWatcher = new FileChangeWatcher(pathToWatch);
	}

	@After
	public void stopNow()
	{
		if (jarFileWatcher.watching())
		{
			jarFileWatcher.stopWatching();
		}
	}

	@Test
	public void pathInitialized() throws IOException
	{
		FileUtils.forceDelete(pathToWatch);
		Assert.assertFalse(pathToWatch.exists());

		MyFileChangeWatcher watcher = new MyFileChangeWatcher();

		jarFileWatcher.watch(watcher);

		watcher.waitForState(istate.initialized);
	}

	@Test
	public void pathCreated() throws IOException
	{
		FileUtils.forceDelete(pathToWatch);
		Assert.assertFalse(pathToWatch.exists());

		MyFileChangeWatcher watcher = new MyFileChangeWatcher();

		jarFileWatcher.watch(watcher);

		FileUtils.touch(pathToWatch);

		watcher.waitForState(istate.created);
	}

	@Test
	public void pathDeleted() throws IOException
	{
		MyFileChangeWatcher watcher = new MyFileChangeWatcher();

		jarFileWatcher.watch(watcher);

		FileUtils.forceDelete(pathToWatch);
		Assert.assertFalse(pathToWatch.exists());

		watcher.waitForState(istate.deleted);
	}

	@Test
	public void pathModifiedSize() throws Exception
	{
		MyFileChangeWatcher watcher = new MyFileChangeWatcher();

		jarFileWatcher.watch(watcher);

		// we have to give the watcher a chance to record the file before changing it
		watcher.waitForState(istate.initialized);

		// we have to write something here because the second resolution makes the test unpredictable
		FileUtils.write(pathToWatch, "hi");

		watcher.waitForState(istate.modified);
	}

	@Test
	public void pathModifiedDate() throws Exception
	{
		MyFileChangeWatcher watcher = new MyFileChangeWatcher();

		FileUtils.write(pathToWatch, "hi");

		jarFileWatcher.watch(watcher);

		watcher.waitForState(istate.initialized);

		// we have to write something here because the second resolution makes the test unpredictable
		pathToWatch.setLastModified(System.currentTimeMillis() + 1000L);

		watcher.waitForState(istate.modified);
	}

	@Test
	public void testExistsThenDeletedThenExistsAgain() throws Exception
	{
		MyFileChangeWatcher watcher = new MyFileChangeWatcher();

		// start watcher and wait for the init message
		jarFileWatcher.watch(watcher);

		watcher.waitForState(istate.initialized);

		// delete the file and wait for the delete message
		pathToWatch.delete();
		watcher.waitForState(istate.deleted);

		// create the file again and get exactly ONE created message
		FileUtils.write(pathToWatch, "hi");
		watcher.waitForState(istate.created);

		Assert.assertEquals(1, watcher.invocationCount(istate.initialized));
		Assert.assertEquals(1, watcher.invocationCount(istate.deleted));
		Assert.assertEquals(1, watcher.invocationCount(istate.created));
		// unfortunately this requires that we just wait a little bit
		Thread.sleep(4000L);

		Assert.assertEquals(1, watcher.invocationCount(istate.created));
	}

	enum istate
	{
		deleted,
		initialized,
		modified,
		created
	}

	private static class MyFileChangeWatcher implements FileChangeWatcher.FileChangeListener
	{
		private final AtomicInteger state = new AtomicInteger(0);
		private final Semaphore triggered = new Semaphore(1000);

		private final AtomicInteger[] invocations;

		{
			triggered.drainPermits();

			invocations = new AtomicInteger[istate.values().length];

			for (int i = 0; i < istate.values().length; i++)
			{
				invocations[i] = new AtomicInteger(0);
			}
		}

		@Override
		public void pathInitialized(File path)
		{
			int ordinal = istate.initialized.ordinal();
			state.set(ordinal);
			invocations[ordinal].incrementAndGet();
			triggered.release();
		}

		@Override
		public void pathCreated(File path)
		{
			int ordinal = istate.created.ordinal();
			state.set(ordinal);
			invocations[ordinal].incrementAndGet();
			triggered.release();
		}

		@Override
		public void pathDeleted(File path)
		{
			int ordinal = istate.deleted.ordinal();
			state.set(ordinal);
			invocations[ordinal].incrementAndGet();
			triggered.release();
		}

		@Override
		public void pathModified(File path)
		{
			int ordinal = istate.modified.ordinal();
			state.set(ordinal);
			invocations[ordinal].incrementAndGet();
			triggered.release();
		}

		public void waitForState(istate st)
		{
			while (state.get() != st.ordinal())
			{
				Thread.yield();
			}
		}

		public int invocationCount(istate created)
		{
			return invocations[created.ordinal()].get();
		}
	}
}
